//  前端导出excel
/**
 * 使用方式
 * const downLoadModel = () => {
    const fileName = ''; // excel的名字
    const columnName = '手机号,姓名,积分'; // 列名 用,分割
    const columnValue = 'phone,userName,point'; // 列相对应的值
    const data = [
      {
        // 表格数据
        phone: '15023376400',
        userName: '张三',
        point: '积分',
      }
    ];
    createExcel({
      fileName,
      columnName,
      columnValue,
      data,
    });
  };
 */

import * as XLSX from 'xlsx';

const createExcel = (obj: any) => {
  const module = obj;
  const rootDoc = document.children[0];
  const root = document.createElement('div');
  root.setAttribute('data-id', 'export-s');
  root.style.display = 'none';
  rootDoc.appendChild(root);
  function sheet2blob(sheet: any, sheetName: string) {
    sheetName = sheetName || 'sheet1';
    const workbook = {
      SheetNames: [sheetName],
      Sheets: {},
    };
    workbook.Sheets[sheetName] = sheet; // 生成excel的配置项

    const wopts = {
      bookType: 'xlsx', // 要生成的文件类型
      bookSST: false, // 是否生成Shared String Table，官方解释是，如果开启生成速度会下降，但在低版本IOS设备上有更好的兼容性
      type: 'binary',
    };
    function s2ab(s: any) {
      const buf = new ArrayBuffer(s.length);
      const view = new Uint8Array(buf);
      // eslint-disable-next-line no-bitwise
      for (let i = 0; i !== s.length; i += 1) view[i] = s.charCodeAt(i) & 0xff;
      return buf;
    }
    const wbout = XLSX.write(workbook, wopts);
    const blob = new Blob([s2ab(wbout)], {
      type: 'application/octet-stream',
    }); // 字符串转ArrayBuffer

    return blob;
  }

  function openDownloadDialog(url: any, saveName: string) {
    if (typeof url === 'object' && url instanceof Blob) {
      url = URL.createObjectURL(url); // 创建blob地址
    }
    const aLink = document.createElement('a');
    aLink.href = url;
    aLink.download = saveName || ''; // HTML5新增的属性，指定保存文件名，可以不要后缀，注意，file:///模式下不会生效
    let event;
    if (window.MouseEvent) event = new MouseEvent('click');
    else {
      event = document.createEvent('MouseEvents');
      event.initMouseEvent(
        'click',
        true,
        false,
        window,
        0,
        0,
        0,
        0,
        0,
        false,
        false,
        false,
        false,
        0,
        null
      );
    }
    aLink.dispatchEvent(event);
    root.remove();
  }
  function exports() {
    const table1 = root.children[0];
    const newOptions = {
      raw: true, // 如果为真，每个单元格数据都将保存为原始字符串
    };
    const sheet = XLSX.utils.table_to_sheet(table1, newOptions); // 将一个table对象转换成一个sheet对象
    sheet['!cols'] = [];
    Object.keys(sheet).map((item) => {
      sheet['!cols'].push({
        wpx: 90,
      }); // 修改列宽
      return item;
    });
    openDownloadDialog(sheet2blob(sheet), `${module.fileName}.xlsx`);
  }

  function html() {
    const Name = module.columnName.split(','); // 转成数组
    const Value = module.columnValue.split(',');
    const table = document.createElement('table');
    table.setAttribute('data-id', 'table1-expot-on1');
    table.setAttribute('border', '1');
    const str = `<thead><tr id='table-thead-tr-ex'></tr></thead><tbody id='table-tbody-tr-ex'></tbody>`;
    table.innerHTML = str;
    const tbody = table.children[1];
    const thead = table.children[0].children[0];

    Name.forEach((item: any) => {
      const td = document.createElement('td');
      td.innerText = `${item}`;
      thead.appendChild(td);
    });
    module.data.forEach((m: any) => {
      const ctr = document.createElement('tr');
      Value.forEach((item: any) => {
        const td = document.createElement('td');
        td.innerText = `${m[item]}`;
        ctr.appendChild(td);
      });
      tbody.appendChild(ctr);
    });
    root.appendChild(table);
    exports();
  }
  html();
};

export default createExcel;
