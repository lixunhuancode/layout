import {
  successResponseWrap,
  mock,
  pagerResponseWrap,
} from '@/utils/setup-mock';

export const shopList = [
  {
    id: '64',
    name: '直营1号店',
    code: 'A041',
    type: '经销',
    address: '重庆市渝中区',
    mobile: '13111112222',
    principal: '张三',
  },
  {
    id: '65',
    name: '直营2号店',
    code: 'A042',
    type: '联营',
    address: '重庆市渝中区',
    mobile: '18811111111',
    principal: '李四',
  },
  {
    id: '66',
    name: '加盟1号店',
    code: 'A042',
    type: '联营',
    address: '重庆市渝中区',
    mobile: '18811111111',
    principal: '李四',
  },
  {
    id: '67',
    name: '加盟2号店',
    code: 'A042',
    type: '联营',
    address: '重庆市渝中区',
    mobile: '18811111111',
    principal: '李四',
  },
];

export const list = (req: any) => {
  return mock(pagerResponseWrap(req, shopList));
};

export const all = () => {
  return mock(successResponseWrap(shopList));
};

export default mock;
