export default [
  {
    path: 'management/list',
    name: 'stock.management.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '库存管理',
      requiresAuth: false,
    },
  },
  {
    path: 'management/detail/:id',
    name: 'stock.management.detail',
    component: () => import('./Detail/index.vue'),
    meta: {
      locale: '订单详情',
      hideInMenu: true,
    },
  },
];
