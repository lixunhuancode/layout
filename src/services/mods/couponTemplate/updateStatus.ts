/**
 * @desc 修改状态
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.UpdateCouponTemplateStatusDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.UpdateCouponTemplateStatusDTO };

export function request(
  data: defs.UpdateCouponTemplateStatusDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`admin/coupon/template/updateStatus`, {
    data,
    method: 'POST',
    ...options,
  });
}
