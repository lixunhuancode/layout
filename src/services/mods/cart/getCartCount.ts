/**
 * @desc 获取购物车数量
 */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = undefined;

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`front/cart/getCartCount`, {
    method: 'GET',
    ...options,
  });
}
