/**
 * @description 内容管理-会员权益-前台接口
 */
import * as detail from './detail';
import * as list from './list';

export { detail, list };
