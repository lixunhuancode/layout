import axios from 'axios';

export function list(data: any) {
  return axios.post('/api/purchase-settlement/list', data);
}

export default axios;
