export default [
  {
    path: 'payment',
    name: 'cashier.payment',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '缴款管理',
    },
  },
  {
    path: 'payment/detail',
    name: 'cashier.payment.detail',
    component: () => import('./details/index.vue'),
    meta: {
      locale: '缴款详情',
    },
  },
];
