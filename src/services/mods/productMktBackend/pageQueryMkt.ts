/**
   * @desc 分页查询营销分类列表
分页查询营销分类列表
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryProductMktDTO;

export const init = new defs.ProductMktVO();

export type ParamsAndBody = { data: defs.QueryProductMktDTO };

export function request(
  data: defs.QueryProductMktDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/productMkt/pageQueryMkt`, {
    data,
    method: 'POST',
    ...options,
  });
}
