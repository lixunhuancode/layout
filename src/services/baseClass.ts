export class AdminAsyncTask {
  /** 业务数据内容json串 */
  businessContent = '';

  /** createTime */
  createTime = '';

  /** 文件名称 */
  fileName = '';

  /** 处理后文件路径地址 */
  fileUrl = '';

  /** finishTime */
  finishTime = '';

  /** 业务请求header */
  header = '';

  /** 唯一id */
  id = undefined;

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 备注 */
  remark = '';

  /** 处理状态（0：待处理，1：处理中，2：已完成，3：失败） */
  status = undefined;

  /** 任务名称 */
  taskName = '';

  /** 任务类型 */
  taskType = '';

  /** 上传文件路径地址 */
  uploadFileUrl = '';

  /** 是否删除（1 ：正常 ，-1： 删除） */
  validity = undefined;
}

export class AdminSysLog {
  /** 管理员ID */
  adminId = undefined;

  /** 管理员 */
  adminName = '';

  /** 发生时间 */
  createTime = '';

  /** id */
  id = undefined;

  /** IP地址 */
  ip = '';

  /** 操作的功能名称 */
  operationName = '';

  /** 操作标签方便归类查询 */
  operationTag = '';

  /** 请求参数 */
  requestParams = '';

  /** 操作url */
  requestUrl = '';

  /** 操作结果 */
  result = '';
}

export class AdminSysLogQueryDTO {
  /** 用户名 */
  adminName = '';

  /** 操作时间 */
  createEndTime = '';

  /** 操作时间 */
  createStartTime = '';

  /** 页码(>=1) */
  pageIndex = undefined;

  /** pageSize */
  pageSize = undefined;
}

export class AdminUserDTO {
  /** 数据权限 */
  dataResources = [];

  /** 是否拥有全部数据权限 1是 0 否 */
  hasAllDataResource = undefined;

  /** 用户名id，修改时必须 */
  id = undefined;

  /** 姓名 */
  name = '';

  /** 密码 */
  password = '';

  /** 备注 */
  remark = '';

  /** 角色 */
  roleIds = [];

  /** 1启用 2禁用 */
  status = undefined;

  /** 管理员类型1超管 2普通管理员 */
  type = undefined;

  /** 用户名 */
  username = '';
}

export class AdminUserDataResourceDTO {
  /** 资源唯一标识 */
  resource = '';

  /** 资源名称 */
  resourceName = '';

  /** 资源类型：1、供应商 */
  resourceType = undefined;
}

export class AdminUserQueryDTO {
  /** 姓名 */
  name = '';

  /** 页码(>=1) */
  pageIndex = undefined;

  /** pageSize */
  pageSize = undefined;

  /** 更新时间 */
  updateEndTime = '';

  /** 更新人 */
  updateName = '';

  /** 更新时间 */
  updateStartTime = '';

  /** 用户名 */
  username = '';
}

export class AdminUserVO {
  /** 创建人 */
  createName = '';

  /** 添加时间 */
  createTime = '';

  /** 数据权限 */
  dataResources = [];

  /** 是否拥有全部数据权限 1是 0 否 */
  hasAllDataResource = undefined;

  /** id */
  id = undefined;

  /** 登录时间 */
  loginAt = '';

  /** 姓名 */
  name = '';

  /** 权限项不包含数据权限 */
  permissions = [];

  /** 备注 */
  remark = '';

  /** 角色 */
  roles = [];

  /** 1启用 2禁用 */
  status = undefined;

  /** 管理员类型1超管 2普通管理员 */
  type = undefined;

  /** 更新人 */
  updateName = '';

  /** 修改时间 */
  updateTime = '';

  /** 用户名 */
  username = '';
}

export class AftersaleApplyDTO {
  /** 售后类型(1:退货退款 2:仅退款) */
  aftersaleType = undefined;

  /** 退货原因类型(1:不喜欢/买错了 2:不合适(尺码型号不合适) 3:发错货(颜色/尺码/型号不对) 4:缺少件(缺少相关附件) 5:质量问题 6:其他) */
  applyReasonType = undefined;

  /** 订单商品行id */
  orderDetailIds = [];

  /** 订单id */
  orderId = undefined;

  /** 备注 */
  remark = '';

  /** 退回方式(1:快递退回 2:退回门店 3: 无需退回) */
  returnType = undefined;
}

export class AftersaleAuditDTO {
  /** 审核备注 */
  auditRemark = '';

  /** 审核结果(1:通过 2:驳回) */
  auditResult = undefined;

  /** 售后单id集合 */
  ids = [];
}

export class AftersaleDetailInfoVO {
  /** 售后数量 */
  aftersaleCount = undefined;

  /** 售后单号 */
  aftersaleNo = '';

  /** 售后收货地址 */
  aftersaleReceiveAddr = '';

  /** 售后收货人 */
  aftersaleReceiveName = '';

  /** 售后收货联系电话 */
  aftersaleReceivePhone = '';

  /** 售后状态码(20000：待审核 20040：已完成(审核通过) 20050：已关闭(审核驳回)) */
  aftersaleStatusCode = undefined;

  /** 售后状态名称 */
  aftersaleStatusName = '';

  /** 售后类型(1：退货退款 2：仅退款) */
  aftersaleType = undefined;

  /** 售后申请日期 */
  applyDate = '';

  /** 申请原因类型描述 */
  applyReasonDesc = '';

  /** 申请原因备注 */
  applyReasonRemark = '';

  /** 申请原因类型((1:不喜欢/买错了 2:不合适(尺码型号不合适) 3:发错货(颜色/尺码/型号不对) 4:缺少件(缺少相关附件) 5:质量问题 6:其他)) */
  applyReasonType = undefined;

  /** 售后申请时间 */
  applyTime = '';

  /** 售后单审核人id */
  auditId = undefined;

  /** 售后单审核人 */
  auditName = '';

  /** 售后单审核备注 */
  auditRemark = '';

  /** 售后单审核时间 */
  auditTime = '';

  /** 审核关闭原因 */
  closeReason = '';

  /** 审核关闭时间 */
  closeTime = '';

  /** 售后单创建人id */
  createdId = undefined;

  /** 售后单创建人 */
  createdName = '';

  /** 配送方式 */
  deliveryType = undefined;

  /** 运费 */
  fare = undefined;

  /** 主键id */
  id = undefined;

  /** 用户id */
  memberId = undefined;

  /** 用户名 */
  memberName = '';

  /** 用户手机号 */
  memberPhone = '';

  /** 操作时间 */
  operTime = '';

  /** 售后商品信息 */
  orderAftersaleDetailInfos = [];

  /** 订单id */
  orderId = undefined;

  /** 下单时间 */
  orderTime = '';

  /** 订单类型 */
  orderType = undefined;

  /** 订单号 */
  orgOrderNo = '';

  /** 应付金额 */
  payAmount = undefined;

  /** 应付积分 */
  payPoints = undefined;

  /** 支付方式 */
  payType = undefined;

  /** 收货/提货地址 */
  receiveAddr = '';

  /** 收/提货人 */
  receiveName = '';

  /** 收/提货人电话 */
  receivePhone = '';

  /** 收货时间 */
  receiveTime = '';

  /** 售后单退款状态(0:未发起 1:退款中 2:退款失败 3:退款成功 4:线下已退款) */
  refundStatus = undefined;

  /** 售后单退款时间 */
  refundTime = '';

  /** 售后备注 */
  remark = '';

  /** 退货金额 */
  returnAmount = undefined;

  /** 退货积分 */
  returnPoints = undefined;

  /** 退回方式(1：快递退回 2：送回门店 3：无需退回) */
  returnType = undefined;

  /** 供应商id */
  supplierId = undefined;

  /** 供应商名称 */
  supplierName = '';

  /** 供应商简称 */
  supplierShortName = '';
}

export class AftersaleListQueryDTO {
  /** 售后单号 */
  aftersaleNo = '';

  /** 售后状态(20000:待审核 20040：已完成(审核通过) 20050：已关闭(审核驳回)) */
  aftersaleStatusCodes = [];

  /** 售后类型(1:退货退款 2:仅退款) */
  aftersaleType = undefined;

  /** 售后原因类型 */
  applyReasonType = undefined;

  /** 申请结束时间 */
  applyTimeEnd = '';

  /** 申请开始时间 */
  applyTimeStart = '';

  /** 审核人 */
  auditName = '';

  /** 审核结束时间 */
  auditTimeEnd = '';

  /** 审核开始时间 */
  auditTimeStart = '';

  /** 发起人 */
  createdName = '';

  /** 用户姓名 */
  memberName = '';

  /** 手机号 */
  memberPhone = '';

  /** 订单号 */
  orgOrderNo = '';

  /** 当前页 */
  pageIndex = undefined;

  /** 每页大小 */
  pageSize = undefined;

  /** 供应商id列表 */
  supplierIds = [];
}

export class AsyncTaskDTO {
  /** 业务数据内容json串 */
  businessContent = '';

  /** 业务请求header */
  header = '';

  /** 任务类型("SUPPLIER_LIST_EXPORT:供应商列表导出","ORDER_LIST_EXPORT:订单列表导出","AFTERSALE_LIST_EXPORT:售后列表导出","SALEDETAIL_LIST_EXPORT:交易明细列表导出","SALESTATISTICS_LIST_EXPORT:交易统计列表导出") */
  taskType = 'SUPPLIER_LIST_EXPORT';

  /** 上传文件路径地址(导入任务使用) */
  uploadFileUrl = '';
}

export class AuditRechargeDTO {
  /** 审批备注 */
  auditRemark = '';

  /** 1通过,2驳回 */
  auditStatus = undefined;

  /** 批次号 */
  batchNo = undefined;
}

export class BaseBusinessTypeDTO {
  /** createTime */
  createTime = '';

  /** 主键ID */
  id = undefined;

  /** 业务类型名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 备注 */
  remark = '';

  /** 是否删除 -1:删除 1:正常 */
  validity = undefined;
}

export class BaseBusinessTypeVO {
  /** createTime */
  createTime = '';

  /** 主键ID */
  id = undefined;

  /** 业务类型名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 备注 */
  remark = '';

  /** 是否删除 -1:删除 1:正常 */
  validity = undefined;
}

export class BaseCarBrandDTO {
  /** createTime */
  createTime = '';

  /** 图标url */
  icon = '';

  /** 主键ID */
  id = undefined;

  /** 名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 备注 */
  remark = '';

  /** 是否删除 -1:删除 1:正常 */
  validity = undefined;
}

export class BaseCarBrandVO {
  /** 车型列表 */
  carTypeList = [];

  /** createTime */
  createTime = '';

  /** 图标url */
  icon = '';

  /** 主键ID */
  id = undefined;

  /** 名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 备注 */
  remark = '';

  /** 是否删除 -1:删除 1:正常 */
  validity = undefined;
}

export class BaseCarTypeDTO {
  /** 车型品牌ID */
  brandId = undefined;

  /** createTime */
  createTime = '';

  /** 主键ID */
  id = undefined;

  /** 名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 备注 */
  remark = '';

  /** 是否删除 -1:删除 1:正常 */
  validity = undefined;
}

export class BaseCarTypeObject {
  /** 车型品牌ID */
  brandId = undefined;

  /** createTime */
  createTime = '';

  /** 主键ID */
  id = undefined;

  /** 名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 备注 */
  remark = '';

  /** 是否删除 -1:删除 1:正常 */
  validity = undefined;
}

export class BaseCarTypeVO {
  /** 车型品牌ID */
  brandId = undefined;

  /** 车型品牌 */
  brandName = '';

  /** createTime */
  createTime = '';

  /** icon */
  icon = '';

  /** 主键ID */
  id = undefined;

  /** 名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 备注 */
  remark = '';

  /** 是否删除 -1:删除 1:正常 */
  validity = undefined;
}

export class BaseCardLabelDTO {
  /** createTime */
  createTime = '';

  /** 主键ID */
  id = undefined;

  /** 名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 备注 */
  remark = '';

  /** 是否删除 -1:删除 1:正常 */
  validity = false;
}

export class BaseCardLabelVO {
  /** createTime */
  createTime = '';

  /** 主键ID */
  id = undefined;

  /** 名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 备注 */
  remark = '';

  /** 是否删除 -1:删除 1:正常 */
  validity = false;
}

export class BaseDTO {
  /** 开始时间 */
  beginTime = '';

  /** 结束时间 */
  endTime = '';

  /** 排序方式 */
  orderBy = '';

  /** 页码 */
  pageIndex = undefined;

  /** 页面大小 */
  pageSize = undefined;

  /** 排序字段 */
  sortBy = '';
}

export class BaseMemberLabelDTO {
  /** createTime */
  createTime = '';

  /** 主键ID */
  id = undefined;

  /** 名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 备注 */
  remark = '';

  /** 是否删除 -1:删除 1:正常 */
  validity = undefined;
}

export class BaseMemberLabelVO {
  /** createTime */
  createTime = '';

  /** 主键ID */
  id = undefined;

  /** 名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 备注 */
  remark = '';

  /** 是否删除 -1:删除 1:正常 */
  validity = undefined;
}

export class BaseRechargeReasonDTO {
  /** code */
  code = '';

  /** createTime */
  createTime = '';

  /** 主键id */
  id = undefined;

  /** 名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';
}

export class BaseRechargeReasonVO {
  /** code */
  code = '';

  /** createTime */
  createTime = '';

  /** 主键id */
  id = undefined;

  /** 名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';
}

export class BaseRefundRequest {
  /** 订单号 */
  orderNo = '';

  /** 支付系统业务流水号 */
  payNo = '';

  /** 退款金额,不支持部分退款 */
  refundAmount = undefined;

  /** 退款订单号 */
  refundOrderNo = '';

  /** 退款积分 */
  refundPoints = undefined;

  /** 退款原因 */
  refundReason = '';
}

export class CarListVO {
  /** 购买时间 */
  buyTime = '';

  /** 车系 */
  carModel = undefined;

  /** 车系 */
  carModelName = '';

  /** 行驶证图片 */
  driveLicense = '';

  /** 行驶里程数 */
  driveMileage = '';

  /** 发动机号 */
  engineNo = '';

  /**  车架号 */
  frameNo = '';

  /** id */
  id = undefined;

  /** 所属用户 */
  owner = '';

  /** 用户手机号 */
  phone = '';

  /** 车牌 */
  plate = '';

  /** 车牌号-市 */
  plateCity = '';

  /** 车牌号 */
  plateNo = '';

  /** 车牌号-省 */
  plateProvince = '';

  /** 备注 */
  remark = '';

  /** 赠送原因 */
  sendReason = '';

  /** 用户id */
  userId = undefined;

  /** 用户姓名 */
  userName = '';
}

export class CartItemDTO {
  /** 商品数量 */
  count = undefined;

  /** SKU ID */
  skuId = '';

  /** 供应商id */
  supplierId = '';
}

export class CartItemProductDto {
  /** 数 */
  count = undefined;

  /** skuId */
  skuId = '';
}

export class CartItemVO {
  /** cartId */
  cartId = undefined;

  /** count */
  count = undefined;

  /** createTime */
  createTime = '';

  /** itemId */
  itemId = undefined;

  /** operTime */
  operTime = '';

  /** price */
  price = undefined;

  /** 规格名称 */
  propertyValueName = '';

  /** 上下架状态 0: 下架 1: 上架 2: 发布中(审核中) 3: 待上架 */
  putOnShelves = undefined;

  /** score */
  score = undefined;

  /** skuId */
  skuId = '';

  /** spuId */
  spuId = '';

  /** spu名称 */
  spuName = '';

  /** 商品spu图片地址(封面) */
  spuPictureUrl = '';

  /** stock */
  stock = undefined;

  /** supplierId */
  supplierId = '';

  /** 供应商名称 */
  supplierName = '';

  /** 商品行总价 */
  totalPrice = undefined;

  /** 商品行积分 */
  totalScore = undefined;

  /** 商品状态 1:正常 0:回收站 -1:删除 */
  validity = undefined;
}

export class CartPriceVO {
  /** 总价 */
  totalPrice = undefined;

  /** 积分 */
  totalScore = undefined;
}

export class CartVO {
  /** 购物车id */
  cartId = undefined;

  /** 购物车商品列表 */
  cartItemVOList = [];

  /** 总件数 */
  totalCount = undefined;

  /** 用户唯一ID */
  uniqueId = '';
}

export class CashierPayDto {
  /** 订单号 */
  orderNo = '';

  /** 支付密码 */
  password = '';

  /** payAmount */
  payAmount = undefined;

  /** payPoints */
  payPoints = undefined;

  /** 支付方式 */
  payWay = undefined;

  /** 收款码名称 */
  rcvQrCodeName = '';

  /** supplierId */
  supplierId = '';

  /** 交易类型 1APP支付 2用户扫二维码支付 3扫用户付款码支付 */
  tradeType = undefined;
}

export class CategoryDTO {
  /** 品类编码 */
  categoryCode = '';

  /** 品类名称 */
  categoryName = '';

  /** 上级品类编码 */
  parentCode = '';

  /** 品类备注 */
  remark = '';
}

export class CategoryVO {
  /** 品类编码 */
  categoryCode = '';

  /** 品类名称 */
  categoryName = '';

  /** 子品类 */
  child = [];

  /** 创建时间 */
  createTime = '';

  /** 是否已关联营销分类 1:是 0:否 */
  existMkt = undefined;

  /** 品类层级 */
  level = undefined;

  /** 操作时间 */
  operTime = '';

  /** 父级品类编码 */
  parentCode = '';

  /** 商品数量 */
  productCount = undefined;

  /** 备注 */
  remark = '';
}

export class ChangePasswordDTO {
  /** 用户id */
  id = undefined;

  /** 密码 */
  newPassword = '';

  /** 密码 */
  password = '';
}

export class ChangeStatusDTO {
  /** 主键ID */
  id = undefined;

  /** 状态 0:禁用 1:启用 */
  status = 'ENABLED';
}

export class CmsPageItem {
  /** 结束时间 */
  endTime = '';

  /** 模块配置内容标识 */
  itemCode = '';

  /** 模块配置内容数据 */
  itemData = '';

  /** 模块配置内容ID */
  itemId = undefined;

  /** 模块配置内容顺序 */
  itemSort = undefined;

  /** 模块标识 */
  moduleCode = '';

  /** 模块id */
  moduleId = undefined;

  /** 模块标签 商品product 图文image 导航nav 活动activity */
  moduleTag = '';

  /** 页面ID */
  pageId = undefined;

  /** 页面版本号 */
  pageVersionId = undefined;

  /** 开始时间 */
  startTime = '';
}

export class CountryAreaObject {
  /** code */
  code = '';

  /** depth */
  depth = undefined;

  /** id */
  id = undefined;

  /** initial */
  initial = '';

  /** latitude */
  latitude = '';

  /** longitude */
  longitude = '';

  /** name */
  name = '';

  /** parentId */
  parentId = undefined;

  /** parentName */
  parentName = '';

  /** rootId */
  rootId = undefined;

  /** zipcode */
  zipcode = '';
}

export class CouponObject {
  /** 失效日期 */
  activeEndTime = '';

  /** 消费时间 */
  consumeTime = '';

  /** 优惠券编码 */
  couponCode = undefined;

  /** customerId */
  customerId = undefined;

  /** 用户手姓名 */
  customerName = '';

  /** 用户手机号 */
  customerPhone = '';

  /** 使用说明 */
  description = '';

  /** id */
  id = undefined;

  /** 卡券名称 */
  name = '';

  /** 发放批次id */
  publishBatchId = undefined;

  /** 领取时间 */
  receiveTime = '';

  /** 优惠券状态AVAILABLE可用、CONSUMED已核销、EXPIRED已过期 DISCARD已废弃 */
  status = '';

  /** 供应商id */
  supplierId = undefined;

  /** 供应商名称 */
  supplierName = '';

  /** 券模板id */
  templateId = undefined;

  /** updateId */
  updateId = undefined;

  /** updateTime */
  updateTime = '';

  /** 核销码 */
  verifyCode = '';

  /** 核销人姓名 */
  verifyName = '';

  /** 核销备注 */
  verifyRemark = '';

  /** 核销方式 1.后端核销 2.小程序核销 */
  verifyType = undefined;

  /** 核销员账号 */
  verifyUsername = '';
}

export class CouponPublishBatchAuditDTO {
  /** 审核意见 */
  auditRemark = '';

  /** 0待审核1通过2驳回 */
  auditStatus = undefined;

  /** 发放批次id */
  id = undefined;
}

export class CouponPublishBatchCustomerDTO {
  /** 姓名 */
  name = '';

  /** 手机号 */
  phone = '';
}

export class CouponPublishBatchDTO {
  /** 卡券 */
  couponTemplates = [];

  /** 用户 */
  customers = [];

  /** 上传附件 */
  files = [];

  /** 批次id 修改时需要 */
  id = undefined;

  /** 备注 */
  remark = '';

  /** 标题 */
  title = '';
}

export class CouponPublishBatchFileDTO {
  /** 文件名 */
  name = '';

  /** 文件url */
  url = '';
}

export class CouponPublishBatchListVO {
  /** 审核人名字 */
  auditName = '';

  /** 0待审核1通过2驳回 */
  auditStatus = undefined;

  /** 审核时间 */
  auditTime = '';

  /** 创建人名字 */
  createName = '';

  /** 创建时间 */
  createTime = '';

  /** 人数 */
  headcount = undefined;

  /** id */
  id = undefined;

  /** 标题 */
  title = '';
}

export class CouponPublishBatchObject {
  /** 审核人id */
  auditId = undefined;

  /** 审核人名字 */
  auditName = '';

  /** 审核意见 */
  auditRemark = '';

  /** 0待审核1通过2驳回 */
  auditStatus = undefined;

  /** 审核时间 */
  auditTime = '';

  /** 卡券 */
  couponTemplates = [];

  /** 创建人id */
  createId = undefined;

  /** 创建人名字 */
  createName = '';

  /** 创建时间 */
  createTime = '';

  /** 用户 */
  customers = [];

  /** 上传附件 json数组 */
  files = [];

  /** 上传附件 json数组 */
  filesJson = '';

  /** 人数 */
  headcount = undefined;

  /** id */
  id = undefined;

  /** 发放优惠券数量 */
  publishQuantity = undefined;

  /** 0待发放 1发放成功 2发放失败 */
  publishStatus = undefined;

  /** 备注 */
  remark = '';

  /** 标题 */
  title = '';

  /** updateId */
  updateId = undefined;

  /** updateTime */
  updateTime = '';
}

export class CouponPublishBatchQueryDTO {
  /** 审核时间止 */
  auditEndTime = '';

  /** 审核时间起 */
  auditStartTime = '';

  /** 0待审核1通过2驳回 */
  auditStatus = undefined;

  /** 创建时间止 */
  createEndTime = '';

  /** 创建时间起 */
  createStartTime = '';

  /** id */
  id = undefined;

  /** 页码(>=1) */
  pageIndex = undefined;

  /** pageSize */
  pageSize = undefined;

  /** 标题 */
  title = '';
}

export class CouponPublishBatchTemplateDTO {
  /** 发放数量 */
  quantity = undefined;

  /** 券模板id */
  templateId = undefined;
}

export class CouponPublishBatchTemplateVO {
  /** 有效期类型1.领取后*个月内有效 2.领取后*天内有效 */
  activeDateType = undefined;

  /** 参见类型，例如当类型为1时存1表示1一个月内有效，类型为2时表示一天内有效 */
  activeDateValue = undefined;

  /** 卡券名称 */
  name = '';

  /** 发放数量 */
  quantity = undefined;

  /** 供应商id */
  supplierId = undefined;

  /** 供应商名称 */
  supplierName = '';

  /** 券模板id */
  templateId = undefined;
}

export class CouponQueryDTO {
  /** 消费时间止 */
  consumeEndTime = '';

  /** 消费时间起 */
  consumeStartTime = '';

  /** 优惠券编码 */
  couponCode = undefined;

  /** 用户手姓名 */
  customerName = '';

  /** 用户手机号 */
  customerPhone = '';

  /** 卡券名称 */
  name = '';

  /** 页码(>=1) */
  pageIndex = undefined;

  /** pageSize */
  pageSize = undefined;

  /** 发放时间止 */
  publishEndTime = '';

  /** 发放时间起 */
  publishStartTime = '';

  /** 优惠券状态AVAILABLE可用、CONSUMED已核销、EXPIRED已过期 DISCARD已废弃 */
  status = '';

  /** 供应商ids */
  supplierIds = [];

  /** 券模板id */
  templateId = undefined;

  /** 核销方式 1.后端核销 2.小程序核销 */
  verifyType = undefined;
}

export class CouponTemplateDTO {
  /** 有效期类型1.领取后*个月内有效 2.领取后*天内有效 */
  activeDateType = undefined;

  /** 参见类型，例如当类型为1时存1表示1一个月内有效，类型为2时表示一天内有效 */
  activeDateValue = undefined;

  /** 使用说明 */
  description = '';

  /** 券模板id 修改时需要 */
  id = undefined;

  /** 卡券名称 */
  name = '';

  /** 状态0启用 1禁用 */
  status = undefined;

  /** 供应商id */
  supplierId = undefined;

  /** 供应商名称 */
  supplierName = '';

  /** 优惠券标签id */
  tags = [];

  /** 卡券类型 1、兑换券 */
  type = undefined;
}

export class CouponTemplateQueryDTO {
  /** 创建时间止 */
  createEndTime = '';

  /** 创建时间起 */
  createStartTime = '';

  /** 卡券id */
  id = undefined;

  /** 卡券名称 */
  name = '';

  /** 页码(>=1) */
  pageIndex = undefined;

  /** pageSize */
  pageSize = undefined;

  /** 状态0启用 1禁用 */
  status = undefined;

  /** 供应商id */
  supplierId = undefined;

  /** 标签id */
  tagIds = [];

  /** 卡券类型 1、兑换券 */
  type = undefined;
}

export class CouponTemplateVO {
  /** 有效期类型1.领取后*个月内有效 2.领取后*天内有效 */
  activeDateType = undefined;

  /** 参见类型，例如当类型为1时存1表示1一个月内有效，类型为2时表示一天内有效 */
  activeDateValue = undefined;

  /** createId */
  createId = undefined;

  /** createName */
  createName = '';

  /** createTime */
  createTime = '';

  /** 使用说明 */
  description = '';

  /** 券模板id 修改时需要 */
  id = undefined;

  /** 卡券名称 */
  name = '';

  /** 下发数量 */
  publishQuantity = undefined;

  /** 状态0启用 1禁用 */
  status = undefined;

  /** 供应商id */
  supplierId = undefined;

  /** 供应商名称 */
  supplierName = '';

  /** 优惠券标签id */
  tags = [];

  /** 卡券类型 1、兑换券 */
  type = undefined;

  /** updateId */
  updateId = undefined;

  /** updateTime */
  updateTime = '';
}

export class CouponVerifyDTO {
  /** 优惠券编码 */
  couponCode = undefined;

  /** 核销码 */
  verifyCode = '';

  /** 核销备注 */
  verifyRemark = '';
}

export class CreateOrUpdateLabelDTO {
  /** 标签code */
  labelCode = '';

  /** 标签名称 */
  labelName = '';

  /** 图标地址 */
  labelPictureUrl = '';

  /** 备注 */
  remark = '';
}

export class CreateOrUpdateMktDTO {
  /** 分类编码列表 */
  categoryCodeList = [];

  /** 营销分类Id */
  id = undefined;

  /** 营销分类编码 */
  mktCode = '';

  /** 分类名称 */
  mktName = '';

  /** 图标地址 */
  mktPictureUrl = '';

  /** 排序 */
  mktSort = undefined;

  /** 父类编码 */
  parentMktCode = '';

  /** 父类名称 */
  parentMktName = '';

  /** 备注 */
  remark = '';
}

export class CreateOrUpdateProductDTO {
  /** 品牌id */
  brandId = undefined;

  /** 品类编码 */
  categoryCode = '';

  /** 配送方式 0:无需配送 1:快递 2:自提 3:快递或自提 */
  deliveryType = undefined;

  /** 商品描述 */
  description = '';

  /** 详细参数 */
  detailedParametersDTOList = [];

  /** 商品标签列表 */
  labelCodeList = [];

  /** 商品图片列表 */
  pictureUrlList = [];

  /** 商品sku信息 */
  productSkuDTOList = [];

  /** 商品备注 */
  remark = '';

  /** 服务保障列表 */
  serviceCodeList = [];

  /** spuId */
  spuId = '';

  /** spu名称 */
  spuName = '';

  /** 商品spu图片地址(封面) */
  spuPictureUrl = '';

  /** 商品视屏地址 */
  spuViewUrl = '';

  /** 供应商编码 */
  supplierId = '';

  /** 供应商名称 */
  supplierName = '';

  /** 供应商简称 */
  supplierShortName = '';
}

export class CreateQRCodeDTO {
  /** 参数 */
  params = undefined;

  /** 跳转地址 */
  path = '';

  /** 二维码的宽度，单位 px。最小 280px，最大 1280px(默认430) */
  width = undefined;
}

export class CreateRechargeBatchDTO {
  /** 附件（逗号分隔） */
  files = '';

  /** 充值账号及金额 */
  rechargeAccount = [];

  /** 充值描述 */
  rechargeReason = '';

  /** 备注 */
  remark = '';

  /** 标题 */
  title = '';

  /** 类型:1充值,2赠送 */
  type = undefined;
}

export class CreateWechatQRCodeDTO {
  /** 参数 */
  params = undefined;

  /** 小程序页面链接 */
  path = '';

  /** 二维码的宽度，单位 px。最小 280px，最大 1280px(默认430) */
  width = undefined;
}

export class DetailedParameterVO {
  /** 参数名称 */
  parametersName = '';

  /** 参数值 */
  parametersValue = '';
}

export class DetailedParametersDTO {
  /** 参数名称 */
  parametersName = '';

  /** 参数值 */
  parametersValue = '';
}

export class DiscardDTO {
  /** 优惠券编码 */
  couponCode = undefined;
}

export class File {
  /** absolute */
  absolute = false;

  /** absoluteFile */
  absoluteFile = {};

  /** absolutePath */
  absolutePath = '';

  /** canonicalFile */
  canonicalFile = {};

  /** canonicalPath */
  canonicalPath = '';

  /** directory */
  directory = false;

  /** executable */
  executable = false;

  /** file */
  file = false;

  /** freeSpace */
  freeSpace = undefined;

  /** hidden */
  hidden = false;

  /** lastModified */
  lastModified = undefined;

  /** name */
  name = '';

  /** parent */
  parent = '';

  /** parentFile */
  parentFile = {};

  /** path */
  path = '';

  /** readable */
  readable = false;

  /** totalSpace */
  totalSpace = undefined;

  /** usableSpace */
  usableSpace = undefined;

  /** writable */
  writable = false;
}

export class GetchemeDTO {
  /** 通过scheme码进入的小程序页面路径，必须是已经发布的小程序存在的页面，不可携带query。path为空时会跳转小程序主页。 */
  path = '';

  /** 通过scheme码进入小程序时的query，最大1024个字符，只支持数字，大小写英文以及部分特殊字符：!#$&'()*+,/:;=?@-._~ */
  query = '';
}

export class GoodsInfoDTO {
  /** 数量 */
  count = undefined;

  /** 商品skuid */
  skuId = '';
}

export class IdDTO {
  /** ID */
  id = undefined;
}

export class IdLongDTO {
  /** ID */
  id = undefined;
}

export class InputStream {}

export class LoginDTO {
  /** 密码 */
  password = '';

  /** 用户名 */
  username = '';
}

export class Map {}

export class MemberBenefitsBasicVO {
  /** 描述 */
  benefitDesc = '';

  /** 权益名称 */
  benefitName = '';

  /** 图标 */
  icon = '';

  /** ID */
  id = undefined;

  /** 等级编码 */
  levelCode = '';

  /** 排序 */
  sort = undefined;
}

export class MemberBenefitsDTO {
  /** 描述 */
  benefitDesc = '';

  /** 权益名称 */
  benefitName = '';

  /** 图标 */
  icon = '';

  /** ID */
  id = undefined;
}

export class MemberBenefitsVO {
  /** 描述 */
  benefitDesc = '';

  /** 权益名称 */
  benefitName = '';

  /** 创建时间 */
  createTime = '';

  /** 图标 */
  icon = '';

  /** ID */
  id = undefined;

  /** 等级编码 */
  levelCode = '';

  /** 操作人ID */
  operId = undefined;

  /** 操作人 */
  operName = '';

  /** 更新时间 */
  operTime = '';

  /** 排序 */
  sort = undefined;
}

export class MiniAppBindMobileDTO {
  /** encryptedData */
  encryptedData = '';

  /** iv */
  iv = '';

  /** openId */
  openId = '';

  /** 微信获取手机号code */
  phoneCode = '';
}

export class ModifyCarDTO {
  /** 购买时间 */
  buyTime = '';

  /** 车品牌 */
  carBrand = undefined;

  /** 车系 */
  carModel = undefined;

  /** 行驶证图片 */
  driveLicense = '';

  /** 行驶里程数 */
  driveMileage = '';

  /** 发动机号 */
  engineNo = '';

  /** 车架号 */
  frameNo = '';

  /** id,更新传 */
  id = undefined;

  /** 车牌号-市 */
  plateCity = '';

  /** 车牌号 */
  plateNo = '';

  /** 车牌号-省 */
  plateProvince = '';

  /** 备注 */
  remark = '';

  /** 赠送币 */
  sendPoint = undefined;

  /** 赠送原因 */
  sendReason = '';

  /** 车辆所属用户id */
  userId = undefined;
}

export class ModifyRechargeBatchDTO {
  /** 附件（逗号分隔） */
  files = '';

  /** id */
  id = undefined;

  /** 充值账号及金额 */
  rechargeAccount = [];

  /** 充值描述 */
  rechargeReason = '';

  /** 备注 */
  remark = '';

  /** 标题 */
  title = '';

  /** 类型:1充值,2赠送 */
  type = undefined;
}

export class ModifyUserDTO {
  /** 生日 */
  birthday = '';

  /** 车辆信息 */
  carInfo = [];

  /** id,更新传 */
  id = undefined;

  /**  身份证号 */
  identNo = '';

  /** 会员等级:general普通会员，senior高级会员 */
  memberLevel = '';

  /** 手机号 */
  phone = '';

  /** 备注 */
  remark = '';

  /** 性别:F男，M女 */
  sex = '';

  /** 状态,1启用，2禁用 */
  status = undefined;

  /** 标签(逗号分隔) */
  tags = '';

  /** 真实姓名 */
  userName = '';
}

export class ModifyUserPhoneDTO {
  /** 新手机号 */
  newPhone = '';

  /** 短信验证码 */
  smsCode = '';

  /** 用户id */
  userId = undefined;
}

export class MyCouponListVO {
  /** 失效日期 */
  activeEndTime = '';

  /** 使用说明 */
  description = '';

  /** 卡券名称 */
  name = '';

  /** 发放批次id */
  publishBatchId = undefined;

  /** 数量 */
  quantity = undefined;

  /** 券模板id */
  templateId = undefined;

  /** 优惠券状态AVAILABLE可用、CONSUMED已核销、INVALID已失效（已过期+废弃） */
  viewStatus = '';
}

export class MyCouponQueryDTO {
  /** 页码(>=1) */
  pageIndex = undefined;

  /** pageSize */
  pageSize = undefined;

  /** 优惠券状态AVAILABLE可用、CONSUMED已核销、INVALID已失效（已过期+废弃） */
  viewStatus = '';
}

export class OrderAftersaleDetailInfoVO {
  /** 订单商品购买数量 */
  count = undefined;

  /** 订单商品行优惠金额 */
  discountAmount = undefined;

  /** 订单商品一级品类id */
  firstCategoryId = '';

  /** 售后商品详情id */
  id = undefined;

  /** 订单商品主图图片地址 */
  mainPictureUrl = '';

  /** 订单商品详情id */
  orderDetailId = undefined;

  /** 订单商品id */
  productId = '';

  /** 订单商品名称 */
  productName = '';

  /** 退货商品金额 */
  returnAmount = undefined;

  /** 退货商品数量 */
  returnCount = undefined;

  /** 退货商品积分 */
  returnPoints = undefined;

  /** 订单商品行金额 */
  saleAmount = undefined;

  /** 订单商品行积分单价 */
  salePoint = undefined;

  /** 订单商品行积分总额 */
  salePoints = undefined;

  /** 订单商品销售单价 */
  salePrice = undefined;

  /** 订单商品skuid */
  skuId = '';

  /** 订单商品sku名称 */
  skuName = '';

  /** 订单商品行uuid */
  uuid = '';
}

export class OrderCancelDTO {
  /** 取消原因 */
  cancelReason = '';

  /** 订单id */
  id = undefined;
}

export class OrderCreateDTO {
  /** 渠道来源 */
  channelSource = '';

  /** 收货/提货地址市编码 */
  cityCode = '';

  /** 收货/提货地址区/县编码 */
  countyCode = '';

  /** 配送类型 0：无需配送 1：快递 2：自提 */
  deliveryType = undefined;

  /** 是否购物车下单 0：否(直接购买) 1：是 */
  isCart = undefined;

  /** 下单商品列表 */
  products = [];

  /** 收货/提货地址省编码 */
  provinceCode = '';

  /** 收货/自提地址 */
  receiveAddr = '';

  /** 收货/提货人 */
  receiveName = '';

  /** 收货/提货联系电话 */
  receivePhone = '';

  /** 备注 */
  remark = '';

  /** 收货/提货地址街道编码(四级地址编码) */
  streetCode = '';

  /** 供应商id */
  supplierId = undefined;
}

export class OrderDetailDTO {
  /** sku购买数量 */
  count = undefined;

  /** sku id */
  skuId = '';

  /** sku名称 */
  skuName = '';

  /** 商品id */
  spuId = '';

  /** 商品名称 */
  spuName = '';
}

export class OrderDetailInfoVO {
  /** 订单后台状态名称 */
  backendStatusName = '';

  /** 取消原因 */
  cancelReason = '';

  /** 取消退款状态 */
  cancelRefundStatus = undefined;

  /** 取消时间 */
  cancelTime = '';

  /** 下单终端 */
  channelSource = '';

  /** 发货时间 */
  deliveryTime = '';

  /** 配送方式(0:无需配送 1:快递 2:自提) */
  deliveryType = undefined;

  /** 优惠总金额=商品优惠总金额+运费优惠总金额 */
  discountAmount = undefined;

  /** 运费 */
  fare = undefined;

  /** 运费优惠总金额 */
  fareDiscountAmount = undefined;

  /** 订单前台状态名称 */
  frontStatusName = '';

  /** 主键id */
  id = undefined;

  /** 是否已评价(0:未评价 1:已评价) */
  isComment = undefined;

  /** 收货/提货地址纬度 */
  latitude = '';

  /** 收货/提货地址经度 */
  longitude = '';

  /** 用户id */
  memberId = undefined;

  /** 用户名 */
  memberName = '';

  /** 用户手机号 */
  memberPhone = '';

  /** 订单总计金额=订单商品总计金额+运费 */
  orderAmount = undefined;

  /** 下单日期 */
  orderDate = '';

  /** 订单确认状态(0:) */
  orderStatus = undefined;

  /** 下单时间 */
  orderTime = '';

  /** 订单类型(1:商城订单 2:收款码订单) */
  orderType = undefined;

  /** 订单号 */
  orgOrderNo = '';

  /** 第三方订单号 */
  orgThirdNo = '';

  /** 订单商品原价总计金额 */
  originalAmount = undefined;

  /** 父订单号 */
  pOrgOrderNo = '';

  /** 订单应付金额=订单总计金额-优惠总金额 */
  payAmount = undefined;

  /** 订单应付积分 */
  payPoints = undefined;

  /** 支付时间 */
  payTime = '';

  /** 支付方式(1:积分 2:微信) */
  payType = undefined;

  /** 商品优惠总金额 */
  productDiscountAmount = undefined;

  /** 订单商品信息 */
  products = [];

  /** 收/提货地址 */
  receiveAddr = '';

  /** 收/提货地址编码 */
  receiveAddrCode = '';

  /** 收货截止时间 */
  receiveEndTime = '';

  /** 收/提货人 */
  receiveName = '';

  /** 收/提货人电话 */
  receivePhone = '';

  /** 收货时间 */
  receiveTime = '';

  /** 备注 */
  remark = '';

  /** 订单商品总计金额 */
  saleAmount = undefined;

  /** 订单状态码 */
  statusCode = undefined;

  /** 供应商id */
  supplierId = undefined;

  /** 供应商名称 */
  supplierName = '';

  /** 供应商名称简称 */
  supplierShortName = '';

  /** 支付超时时间(秒) */
  termOfValidity = undefined;
}

export class OrderDetailVO {
  /** 售后状态(0:未售后 1:售后中 2:售后完成) */
  aftersaleStatus = undefined;

  /** 品牌id */
  brandId = '';

  /** 品牌名称 */
  brandName = '';

  /** 品类id */
  categoryId = '';

  /** 品类名称 */
  categoryName = '';

  /** 购买数量 */
  count = undefined;

  /** 商品行优惠券明细 */
  couponCodeDetail = '';

  /** 商品行优惠金额 */
  discountAmount = undefined;

  /** 商品行优惠明细 */
  discountAmountDetail = '';

  /** 一级品类id */
  firstCategoryId = '';

  /** 一级品类名称 */
  firstCategoryName = '';

  /** 订单详情id */
  id = undefined;

  /** 是否已评价 */
  isComment = undefined;

  /** 商品主图 */
  mainPictureUrl = '';

  /** 订单id */
  orderId = undefined;

  /** 商品行原始总金额=商品原始单价*购买数量 */
  originalAmount = undefined;

  /** 商品原始单价 */
  originalPrice = undefined;

  /** 商品id */
  productId = '';

  /** 名称 */
  productName = '';

  /** 已售后数量 */
  returnCount = undefined;

  /** 商品行销售总金额=商品销售单价*购买数量 */
  saleAmount = undefined;

  /** 单商品积分 */
  salePoint = undefined;

  /** 商品行积分=单商品积分*购买数量 */
  salePoints = undefined;

  /** 商品销售单价 */
  salePrice = undefined;

  /** skuid */
  skuId = '';

  /** sku名称 */
  skuName = '';

  /** uuid */
  uuid = '';

  /** 核销渠道(1:小程序核销 2:后台核销) */
  verifyChannel = undefined;

  /** 核销码 */
  verifyCode = '';

  /** 核销人id */
  verifyId = undefined;

  /** 核销人 */
  verifyName = '';

  /** 核销备注 */
  verifyRemark = undefined;

  /** 核销状态(0:未核销 1:已核销 2:已取消 3:无需核销) */
  verifyStatus = undefined;

  /** 核销时间 */
  verifyTime = '';

  /** 核销类型(1:核销码核销 2:强制核销) */
  verifyType = undefined;
}

export class OrderInfoDTO {
  /** 订单号 */
  orderNo = '';

  /** 商品行 */
  productInfos = [];

  /** 供应商id */
  supplierId = '';
}

export class OrderItem {
  /** asc */
  asc = false;

  /** column */
  column = '';
}

export class OrderListQueryDTO {
  /** 配送方式 */
  deliveryType = undefined;

  /** 用户id */
  memberId = undefined;

  /** 用户名 */
  memberName = '';

  /** 手机号 */
  memberPhone = '';

  /** 是否需要屏蔽敏感信息 */
  needShieldSensitiveInfo = false;

  /** 订单结束时间 */
  orderTimeEnd = '';

  /** 订单开始时间 */
  orderTimeStart = '';

  /** 订单类型 */
  orderType = undefined;

  /** 订单号 */
  orgOrderNo = '';

  /** 订单号列表 */
  orgOrderNos = [];

  /** 当前页 */
  pageIndex = undefined;

  /** 每页数据大小 */
  pageSize = undefined;

  /** 订单状态 */
  statusCodes = [];

  /** 供应商id列表 */
  supplierIds = [];
}

export class OrderStatusCountVO {
  /** 已完成订单数 */
  completeCount = undefined;

  /** 待支付订单数 */
  waitPayCount = undefined;

  /** 待自提订单数 */
  waitSelfDeliveryCount = undefined;
}

export class OrderTraceVO {
  /** 订单后台状态名称 */
  backendStatusName = '';

  /** 订单前台状态名称 */
  frontStatusName = '';

  /** 主键id */
  id = undefined;

  /** 操作时间 */
  operTime = '';

  /** 操作人id */
  operatorId = undefined;

  /** 操作人姓名 */
  operatorName = '';

  /** 操作人电话 */
  operatorPhone = '';

  /** 操作人类型（1：前台用户 2：后台用户 3：系统） */
  operatorType = undefined;

  /** 订单id */
  orderId = undefined;

  /** 操作描述 */
  orderTraceLog = '';

  /** 订单状态码 */
  statusCode = undefined;
}

export class Page {
  /** countId */
  countId = '';

  /** current */
  current = undefined;

  /** hitCount */
  hitCount = false;

  /** maxLimit */
  maxLimit = undefined;

  /** optimizeCountSql */
  optimizeCountSql = false;

  /** orders */
  orders = [];

  /** pages */
  pages = undefined;

  /** records */
  records = [];

  /** searchCount */
  searchCount = false;

  /** size */
  size = undefined;

  /** total */
  total = undefined;
}

export class PageContentDTO {
  /** 页面背景色 */
  backgroundColor = '';

  /** 页面背景图片 */
  backgroundImage = '';

  /** 页面封面图片 */
  cover = '';

  /** 页面模块数据 */
  items = [];

  /** 页面ID */
  pageId = undefined;

  /** 页面名称 */
  title = '';

  /** 页面顶部图标 */
  topIcon = '';
}

export class PageDTO {
  /** 背景色 */
  backgroundColor = '';

  /** 背景图片 */
  backgroundImage = '';

  /** 页面封面图片 */
  cover = '';

  /** ID */
  id = undefined;

  /** 描述 */
  pageDesc = '';

  /** 标题 */
  title = '';

  /** 页面顶部图标 */
  topIcon = '';
}

export class PageIdDTO {
  /** ID */
  id = undefined;
}

export class PageItemBasicVO {
  /** 结束时间 */
  endTime = '';

  /** 模块配置内容标识 */
  itemCode = '';

  /** 模块配置内容数据 */
  itemData = '';

  /** 模块配置内容ID */
  itemId = undefined;

  /** 模块标识 */
  moduleCode = '';

  /** 模块标签 商品product 图文image 导航nav 活动activity */
  moduleTag = '';

  /** 开始时间 */
  startTime = '';
}

export class PageItemDTO {
  /** 结束时间 */
  endTime = '';

  /** 模块内容标识 */
  itemCode = '';

  /** 模块内容数据 */
  itemData = '';

  /** 模块内容ID */
  itemId = undefined;

  /** 模块内容顺序 */
  itemSort = undefined;

  /** 模块标识 */
  moduleCode = '';

  /** 模块ID */
  moduleId = undefined;

  /** 模块标签 商品product 图文image 导航nav 活动activity */
  moduleTag = '';

  /** 开始时间 */
  startTime = '';
}

export class PageRecommendBasicVO {
  /** 图标 */
  icon = '';

  /** ID */
  id = undefined;

  /** 位置编码 */
  locationCode = '';

  /** 路径 */
  path = '';

  /** 排序 */
  sort = undefined;

  /** 启用状态 1启用 0禁用 */
  status = undefined;

  /** 入口名称 */
  title = '';
}

export class PageRecommendDTO {
  /** 图标 */
  icon = '';

  /** ID */
  id = undefined;

  /** 位置编码 */
  locationCode = '';

  /** 路径 */
  path = '';

  /** 排序 */
  sort = undefined;

  /** 启用状态 1启用 0禁用 */
  status = undefined;

  /** 名称 */
  title = '';
}

export class PageRecommendVO {
  /** 创建时间 */
  createTime = '';

  /** 图标 */
  icon = '';

  /** ID */
  id = undefined;

  /** 位置编码 */
  locationCode = '';

  /** 操作人ID */
  operId = undefined;

  /** 操作人 */
  operName = '';

  /** 更新时间 */
  operTime = '';

  /** 路径 */
  path = '';

  /** 排序 */
  sort = undefined;

  /** 启用状态 1启用 0禁用 */
  status = undefined;

  /** 入口名称 */
  title = '';
}

export class PageTextDTO {
  /** 内容 */
  content = '';

  /** ID */
  id = undefined;

  /** 链接 */
  linkUrl = '';

  /** 页面类型 1富文本 2小程序 */
  pageType = undefined;

  /** 短链接 */
  shortUrl = '';

  /** 标题 */
  title = '';
}

export class PageTextVO {
  /** 小程序appId */
  appId = '';

  /** 自定义内容 */
  content = '';

  /** 创建时间 */
  createTime = '';

  /** 页面地址 */
  hrefUrl = '';

  /** 内容 */
  html = '';

  /** ID */
  id = undefined;

  /** 是否删除 0否,1是 */
  isDel = undefined;

  /** 链接 */
  linkUrl = '';

  /** 操作人ID */
  operId = undefined;

  /** 操作人 */
  operName = '';

  /** 更新时间 */
  operTime = '';

  /** 页面编码 price价格说明 score如何获取积分 privac隐私说明 */
  pageCode = '';

  /** 页面类型 1富文本页 2小程序 */
  pageType = undefined;

  /** 模板内容 */
  resource = '';

  /** 短链接 */
  shortUrl = '';

  /** 页面模板标识 home首页 topic专题 service服务 */
  templateCode = '';

  /** 页面模板ID */
  templateId = undefined;

  /** 页面模板类型 page配置页面 text富文本页 */
  templateType = '';

  /** 标题 */
  title = '';
}

export class PageVO {
  /** 审核状态 1审核通过 0审核不通过 */
  auditStatus = undefined;

  /** 背景色 */
  backgroundColor = '';

  /** 背景图片 */
  backgroundImage = '';

  /** 页面封面图片 */
  cover = '';

  /** 创建时间 */
  createTime = '';

  /** ID */
  id = undefined;

  /** 是否删除 0否,1是 */
  isDel = undefined;

  /** 发布上线时间 */
  onlineTime = '';

  /** 操作人ID */
  operId = undefined;

  /** 操作人 */
  operName = '';

  /** 更新时间 */
  operTime = '';

  /** 描述 */
  pageDesc = '';

  /** 页面版本 */
  pageVersionId = undefined;

  /** 页面状态： 1待提交审核  2待审核 3审核通过 4审核不通过 */
  status = undefined;

  /** 页面模板标识 home首页 topic专题 service服务 */
  templateCode = '';

  /** 页面模板ID */
  templateId = undefined;

  /** 页面模板类型 page配置页面 text富文本页 */
  templateType = '';

  /** 标题 */
  title = '';

  /** 页面顶部图标 */
  topIcon = '';
}

export class PageWithItemsBasicVO {
  /** 背景色 */
  backgroundColor = '';

  /** 背景图片 */
  backgroundImage = '';

  /** 页面封面图片 */
  cover = '';

  /** ID */
  id = undefined;

  /** pageItems */
  pageItems = [];

  /** 页面版本 */
  pageVersionId = undefined;

  /** 页面模板标识 home首页 topic专题 service服务 */
  templateCode = '';

  /** 页面模板类型 page配置页面 text富文本页 */
  templateType = '';

  /** 标题 */
  title = '';

  /** 页面顶部图标 */
  topIcon = '';
}

export class PageWithItemsVO {
  /** 审核状态 1审核通过 0审核不通过 */
  auditStatus = undefined;

  /** 背景色 */
  backgroundColor = '';

  /** 背景图片 */
  backgroundImage = '';

  /** 页面封面图片 */
  cover = '';

  /** 创建时间 */
  createTime = '';

  /** ID */
  id = undefined;

  /** 是否删除 0否,1是 */
  isDel = undefined;

  /** 发布上线时间 */
  onlineTime = '';

  /** 操作人ID */
  operId = undefined;

  /** 操作人 */
  operName = '';

  /** 更新时间 */
  operTime = '';

  /** 描述 */
  pageDesc = '';

  /** pageItems */
  pageItems = [];

  /** 页面版本 */
  pageVersionId = undefined;

  /** 页面状态： 1待提交审核  2待审核 3审核通过 4审核不通过 */
  status = undefined;

  /** 页面模板标识 home首页 topic专题 service服务 */
  templateCode = '';

  /** 页面模板ID */
  templateId = undefined;

  /** 页面模板类型 page配置页面 text富文本页 */
  templateType = '';

  /** 标题 */
  title = '';

  /** 页面顶部图标 */
  topIcon = '';
}

export class Pagination {
  /** 总页数 */
  pageCount = undefined;

  /** 当前页 */
  pageIndex = undefined;

  /** 每页记录数 */
  pageSize = undefined;

  /** 总记录数 */
  recordCount = undefined;

  /** 当前页的集合 */
  result = [];
}

export class PaymentCodeDTOObject {
  /** 收款地点 */
  address = '';

  /** 收款码CODE */
  code = '';

  /** 创建时间 */
  createTime = '';

  /** 主键 */
  id = undefined;

  /** 收款码名称 */
  name = '';

  /** 操作人id */
  operId = undefined;

  /** 操作人名称 */
  operName = '';

  /** 操作时间 */
  operTime = '';

  /** 备注 */
  remark = '';

  /** 收款码图片地址 */
  resUrl = '';

  /** 状态 0:禁用 1:启用 */
  status = undefined;

  /** 供应商ID */
  supplierId = undefined;

  /** 是否删除 -1:删除 1:正常 */
  validity = undefined;
}

export class PaymentCodeVOObject {
  /** 收款地点 */
  address = '';

  /** 收款码CODE */
  code = '';

  /** 创建时间 */
  createTime = '';

  /** 主键 */
  id = undefined;

  /** 收款码名称 */
  name = '';

  /** 操作人id */
  operId = undefined;

  /** 操作人名称 */
  operName = '';

  /** 操作时间 */
  operTime = '';

  /** 备注 */
  remark = '';

  /** 收款码图片地址 */
  resUrl = '';

  /** 状态 0:禁用 1:启用 */
  status = undefined;

  /** 编码 */
  supplierCode = '';

  /** 供应商ID */
  supplierId = undefined;

  /** 供应商名称 */
  supplierName = '';

  /** 供应商简称 */
  supplierShortName = '';

  /** 是否删除 -1:删除 1:正常 */
  validity = undefined;
}

export class PaymentVO {
  /** 余额 */
  balance = undefined;

  /** 支付方式描述 */
  description = '';

  /** 不可用原因 */
  disableReason = '';

  /** 图标 */
  icon = '';

  /** 支付方式名称 */
  name = '';

  /** 支付方式 1积分 */
  payWay = undefined;

  /** 是否置灰标识 */
  showFlag = false;
}

export class Permission {
  /** api的http请求方法 */
  apiMethod = '';

  /** 后端接口api */
  apiPath = '';

  /** children */
  children = [];

  /** 创建时间 */
  createTime = '';

  /** 0正常1删除 */
  deleted = undefined;

  /** 描述 */
  description = '';

  /** id */
  id = undefined;

  /** 权限项标识 */
  key = '';

  /** 权限名 */
  name = '';

  /** 父级ID */
  pid = undefined;

  /** 排序 */
  sort = undefined;

  /** 1菜单 2按钮 */
  type = undefined;
}

export class PointNoticeVO {
  /** 当前积点 */
  currentPoint = undefined;

  /** 增加积点 */
  point = undefined;
}

export class PointRecordListDTO {
  /** 页码 */
  pageIndex = undefined;

  /** 每页显示条数 */
  pageSize = undefined;

  /** 交易类型:1收入,2支出,不传全部 */
  tradeType = undefined;
}

export class PointTrandVO {
  /** 失败信息 */
  msg = '';

  /** 剩余积点 */
  point = undefined;

  /** 状态：true成功，false失败 */
  status = false;

  /** 交易流水 */
  transNo = '';
}

export class PointTransInfoDTO {
  /** 售后单号 */
  aftersaleNo = '';

  /** 订单号 */
  orderNo = '';

  /** 交易积点 */
  point = undefined;

  /** 备注 */
  remark = '';

  /** 交易方 */
  tradeCompany = '';

  /** 交易类型:1赠送,2充值,3商城消费,4消费售后 */
  tradeType = 'SEND';

  /** 交易流水 */
  transNo = '';

  /** 用户标识 */
  userId = undefined;
}

export class ProductApprovalDTO {
  /** 驳回原因 */
  refuseReason = '';

  /** 商品id列表 */
  spuIdList = [];

  /** 审核状态  0: 待审核 1: 驳回 2: 通过 */
  status = undefined;
}

export class ProductBrandDTO {
  /** 品牌英文名称 */
  brandEnglishName = '';

  /** 品牌名称 */
  brandName = '';

  /** 品牌ID */
  id = undefined;

  /** 备注 */
  remark = '';
}

export class ProductBrandVO {
  /** 品牌英文名称 */
  brandEnglishName = '';

  /** 品牌名称 */
  brandName = '';

  /** 创建时间 */
  createTime = '';

  /** 品牌ID */
  id = undefined;

  /** 更新时间 */
  operTime = '';

  /** 备注 */
  remark = '';
}

export class ProductDetailsVO {
  /** 品牌英文名 */
  brandEnglishName = '';

  /** 品牌id */
  brandId = undefined;

  /** 品牌名称 */
  brandName = '';

  /** 品类编码 */
  categoryCode = '';

  /** 品类全称 */
  categoryFullName = '';

  /** 品类名称 */
  categoryName = '';

  /** 配送方式 0:无需配送 1:快递 2:自提 3:快递或自提 */
  deliveryType = undefined;

  /** 商品描述 */
  description = '';

  /** 详细参数 */
  detailedParameterVOList = [];

  /** 商品标签编码列表 */
  labelCodeList = [];

  /** 上架时间 */
  onShelvesBeginTime = '';

  /** 下架时间 */
  onShelvesEndTime = '';

  /** 商品图片列表 */
  pictureUrlList = [];

  /** 商品标签列表 */
  productLabelVOList = [];

  /** 规格组列表 */
  productPropertyVOList = [];

  /** sku列表 */
  productSkuVOList = [];

  /** 上下架状态 0: 下架 1: 上架 2: 发布中 3: 待上架 */
  putOnShelves = undefined;

  /** 商品备注 */
  remark = '';

  /** 服务保障编码列表 */
  serviceCodeList = [];

  /** spuId */
  spuId = '';

  /** spu名称 */
  spuName = '';

  /** 商品spu图片地址(封面) */
  spuPictureUrl = '';

  /** 商品视屏地址 */
  spuViewUrl = '';

  /** 供应商编码 */
  supplierId = '';

  /** 供应商名称 */
  supplierName = '';

  /** 供应商简称 */
  supplierShortName = '';

  /** 商品总库存 */
  totalStock = undefined;
}

export class ProductLabelVO {
  /** 标签code */
  labelCode = '';

  /** 标签名称 */
  labelName = '';

  /** 图标地址 */
  labelPictureUrl = '';

  /** 状态 0:禁用 1:启用 */
  labelStatus = undefined;

  /** 更新时间 */
  operTime = '';

  /** 备注 */
  remark = '';
}

export class ProductLogVO {
  /** 创建时间(操作时间) */
  createTime = '';

  /** 事件描述 */
  eventDesc = '';

  /** 操作来源 1:商品管理 2:商品回收站 3:审核管理 4:库存管理 */
  eventSrc = undefined;

  /** 事件类型 */
  eventType = undefined;

  /** 操作人名称 */
  operName = '';

  /** 上下架状态 0: 下架 1: 上架 2: 发布中(审核中) 3: 待上架 */
  putOnShelves = undefined;

  /** spuId */
  spuId = '';
}

export class ProductMktVO {
  /** 品类列表 */
  categoryVOList = [];

  /** 主键ID */
  id = undefined;

  /** 营销分类编码 */
  mktCode = '';

  /** 分类名称 */
  mktName = '';

  /** 图标地址 */
  mktPictureUrl = '';

  /** 排序 */
  mktSort = undefined;

  /** 状态 0:禁用 1:启用 */
  mktStatus = undefined;

  /** 父类编码 */
  parentMktCode = '';

  /** 父类名称 */
  parentMktName = '';

  /** 备注 */
  remark = '';
}

export class ProductPropertyDTO {
  /** 规格组ID */
  id = undefined;

  /** 规格组名称 */
  name = '';

  /** 规格组备注 */
  remark = '';

  /** 规格值列表 */
  values = [];
}

export class ProductPropertyVO {
  /** 创建时间 */
  createTime = '';

  /** 规格组ID */
  id = undefined;

  /** 规格组KEY */
  key = '';

  /** 规格组名称 */
  name = '';

  /** 更新时间 */
  operTime = '';

  /** 规格组备注 */
  remark = '';

  /** 规格值集合 */
  values = [];
}

export class ProductPropertyValueDTO {
  /** 规格值ID */
  id = undefined;

  /** 规格值 */
  value = '';
}

export class ProductPropertyValueVO {
  /** 规格值ID */
  id = undefined;

  /** 规格值KEY */
  key = '';

  /** 规格组ID */
  propertyId = undefined;

  /** 规格值 */
  value = '';
}

export class ProductSalesVO {
  /** 品牌id */
  brandId = undefined;

  /** 品牌名称 */
  brandName = '';

  /** 品类编码 */
  categoryCode = '';

  /** 品类名称 */
  categoryName = '';

  /** 交易完成时间 */
  finishTime = '';

  /** 销售数量 */
  salesNumber = undefined;

  /** 交易价格 */
  salesPrice = undefined;

  /** 交易积分 */
  salesScore = undefined;

  /** 销售类型 1:交易 2:售后 */
  salesType = undefined;

  /** skuId */
  skuId = '';

  /** spuId */
  spuId = '';

  /** spu名称 */
  spuName = '';

  /** 供应商编码 */
  supplierId = '';

  /** 供应商名称 */
  supplierName = '';

  /** 交易号(交易-订单号 售后-售后单号) */
  tradeNo = '';
}

export class ProductSkuDTO {
  /** 最小购买数量 */
  minPurchaseQty = undefined;

  /** 吊牌价 */
  msrp = undefined;

  /** 价格策略 1:积分 2:售价 3: 积分+售价 */
  priceStrategy = undefined;

  /** 规格信息Map<规格组key,规格值key> */
  propertyValueMap = undefined;

  /** 规格名称 */
  propertyValueName = '';

  /** 售价 */
  salePrice = undefined;

  /** 积分 */
  score = undefined;

  /** skuId */
  skuId = '';

  /** 可售库存 */
  stock = undefined;
}

export class ProductSkuVO {
  /** 最小购买数量 */
  minPurchaseQty = undefined;

  /** 吊牌价 */
  msrp = undefined;

  /** 价格策略 1:积分 2:售价 3: 积分+售价 */
  priceStrategy = undefined;

  /** 规格信息Map<规格组key,规格值key> */
  propertyValueMap = '';

  /** 规格名称 */
  propertyValueName = '';

  /** 售价 */
  salePrice = undefined;

  /** 积分 */
  score = undefined;

  /** skuId */
  skuId = '';

  /** sku销量 */
  skuSales = undefined;

  /** 顺序(从1开始,  顺序为1的是默认sku) */
  skuSort = undefined;

  /** spuId */
  spuId = '';

  /** 可售库存 */
  stock = undefined;
}

export class ProductWithApprovalVO {
  /** 申请人姓名 */
  applicantName = '';

  /** 申请时间 */
  applicantTime = '';

  /** 审核人姓名 */
  approvalName = '';

  /** 审核时间 */
  approvalTime = '';

  /** 品牌id */
  brandId = undefined;

  /** 品牌名称 */
  brandName = '';

  /** 品类编码 */
  categoryCode = '';

  /** 品类全名 */
  categoryFullName = '';

  /** 品类名称 */
  categoryName = '';

  /** 一级品类编码 */
  firstCategoryCode = '';

  /** 一级品类名称 */
  firstCategoryName = '';

  /** 最小购买数量 */
  minPurchaseQty = undefined;

  /** 吊牌价 */
  msrp = undefined;

  /** 新增库存值 */
  newStock = undefined;

  /** 上架时间 */
  onShelvesBeginTime = '';

  /** 下架时间 */
  onShelvesEndTime = '';

  /** 操作时间 */
  operTime = '';

  /** 价格策略 1:积分 2:售价 3: 积分+售价 */
  priceStrategy = undefined;

  /** 标签列表 */
  productLabelVOList = [];

  /** 规格信息Map<规格组key,规格值key> */
  propertyValueMap = '';

  /** 规格名称 */
  propertyValueName = '';

  /** 上下架状态 0: 下架 1: 上架 2: 发布中(审核中) 3: 待上架 */
  putOnShelves = undefined;

  /** 驳回原因 */
  refuseReason = '';

  /** 售价 */
  salePrice = undefined;

  /** 积分 */
  score = undefined;

  /** skuId */
  skuId = '';

  /** sku销量 */
  skuSales = undefined;

  /** 顺序(从1开始,  顺序为1的是默认sku) */
  skuSort = undefined;

  /** spuId */
  spuId = '';

  /** spu名称 */
  spuName = '';

  /** 商品spu图片地址(封面) */
  spuPictureUrl = '';

  /** 审核状态 0:待审核 1:驳回 2:通过 */
  status = undefined;

  /** 可售库存 */
  stock = undefined;

  /** 供应商编码 */
  supplierId = '';

  /** 供应商名称 */
  supplierName = '';

  /** 供应商简称 */
  supplierShortName = '';

  /** 商品状态 1:正常 0:回收站 -1:删除 */
  validity = undefined;
}

export class ProductWithSkuVO {
  /** 品牌id */
  brandId = undefined;

  /** 品牌名称 */
  brandName = '';

  /** 品类编码 */
  categoryCode = '';

  /** 品类全名 */
  categoryFullName = '';

  /** 品类名称 */
  categoryName = '';

  /** 一级品类编码 */
  firstCategoryCode = '';

  /** 一级品类名称 */
  firstCategoryName = '';

  /** 最小购买数量 */
  minPurchaseQty = undefined;

  /** 吊牌价 */
  msrp = undefined;

  /** 新增库存值 */
  newStock = undefined;

  /** 上架时间 */
  onShelvesBeginTime = '';

  /** 下架时间 */
  onShelvesEndTime = '';

  /** 操作时间 */
  operTime = '';

  /** 价格策略 1:积分 2:售价 3: 积分+售价 */
  priceStrategy = undefined;

  /** 标签列表 */
  productLabelVOList = [];

  /** 规格信息Map<规格组key,规格值key> */
  propertyValueMap = '';

  /** 规格名称 */
  propertyValueName = '';

  /** 上下架状态 0: 下架 1: 上架 2: 发布中(审核中) 3: 待上架 */
  putOnShelves = undefined;

  /** 售价 */
  salePrice = undefined;

  /** 积分 */
  score = undefined;

  /** skuId */
  skuId = '';

  /** sku销量 */
  skuSales = undefined;

  /** 顺序(从1开始,  顺序为1的是默认sku) */
  skuSort = undefined;

  /** spuId */
  spuId = '';

  /** spu名称 */
  spuName = '';

  /** 商品spu图片地址(封面) */
  spuPictureUrl = '';

  /** 可售库存 */
  stock = undefined;

  /** 供应商编码 */
  supplierId = '';

  /** 供应商名称 */
  supplierName = '';

  /** 供应商简称 */
  supplierShortName = '';

  /** 商品状态 1:正常 0:回收站 -1:删除 */
  validity = undefined;
}

export class PublicDto {
  /** 页码 */
  pageIndex = undefined;

  /** 每页显示条数 */
  pageSize = undefined;
}

export class QueryBatchManageStockDTO {
  /** 开始时间 */
  beginTime = '';

  /** 缓存Id */
  cacheId = '';

  /** 结束时间 */
  endTime = '';

  /** 页码 */
  pageIndex = undefined;

  /** 页面大小 */
  pageSize = undefined;
}

export class QueryBrandDTO {
  /** 开始时间 */
  beginTime = '';

  /** 品牌英文名称 */
  brandEnglishName = '';

  /** 品牌名称 */
  brandName = '';

  /** 结束时间 */
  endTime = '';

  /** 品牌ID */
  id = undefined;

  /** 页码 */
  pageIndex = undefined;

  /** 页面大小 */
  pageSize = undefined;
}

export class QueryCarListDTO {
  /** 购买时间结束 */
  buyEndTime = '';

  /** 购买时间开始 */
  buyStartTime = '';

  /** 车系 */
  carModels = [];

  /** 车架号 */
  frameNo = '';

  /** id */
  id = undefined;

  /** 页码 */
  pageIndex = undefined;

  /** 每页显示条数 */
  pageSize = undefined;

  /** 用户手机号 */
  phone = '';

  /** 车牌号-市 */
  plateCity = '';

  /** 车牌号 */
  plateNo = '';

  /** 车牌号-省 */
  plateProvince = '';

  /** 状态 */
  status = undefined;
}

export class QueryCarTypeDTO {
  /** 开始时间 */
  beginTime = '';

  /** 车型品牌ID */
  brandId = undefined;

  /** 结束时间 */
  endTime = '';

  /** 名称 */
  name = '';

  /** 排序方式 */
  orderBy = '';

  /** 页码 */
  pageIndex = undefined;

  /** 页面大小 */
  pageSize = undefined;

  /** 排序字段 */
  sortBy = '';
}

export class QueryCategoryDTO {
  /** 开始时间 */
  beginTime = '';

  /** 品类编码 */
  categoryCode = '';

  /** 品类名称 */
  categoryName = '';

  /** 结束时间 */
  endTime = '';

  /** 页码 */
  pageIndex = undefined;

  /** 页面大小 */
  pageSize = undefined;
}

export class QueryDictDTO {
  /** 开始时间 */
  beginTime = '';

  /** 编码 */
  code = '';

  /** 结束时间 */
  endTime = '';

  /** 名称 */
  name = '';

  /** 排序方式 */
  orderBy = '';

  /** 页码 */
  pageIndex = undefined;

  /** 页面大小 */
  pageSize = undefined;

  /** 排序字段 */
  sortBy = '';
}

export class QueryMemberBenefitsDTO {
  /** 权益名称 */
  benefitName = '';

  /** ID */
  id = undefined;

  /** 操作人 */
  operName = '';

  /** 更新时间结束 */
  operTimeEnd = '';

  /** 更新时间开始 */
  operTimeStart = '';
}

export class QueryPageDTO {
  /** ID */
  id = undefined;

  /** 操作人 */
  operName = '';

  /** 更新时间结束 */
  operTimeEnd = '';

  /** 更新时间开始 */
  operTimeStart = '';

  /** 页码 */
  pageIndex = undefined;

  /** 页面大小 */
  pageSize = undefined;

  /** startRow */
  startRow = undefined;

  /** 标题 */
  title = '';
}

export class QueryPageRecommendDTO {
  /** ID */
  id = undefined;

  /** 位置编码 个人中心常用功能：myCommon */
  locationCode = '';

  /** 操作人 */
  operName = '';

  /** 更新时间结束 */
  operTimeEnd = '';

  /** 更新时间开始 */
  operTimeStart = '';

  /** 启用状态 1启用 0禁用 */
  status = undefined;

  /** 入口名称 */
  title = '';
}

export class QueryPageRecommendFrontDTO {
  /** 位置编码 个人中心常用功能：myCommon */
  locationCode = '';
}

export class QueryPageTextDTO {
  /** ID */
  id = undefined;

  /** ID或标题 */
  keyword = '';

  /** 操作人 */
  operName = '';

  /** 更新时间结束 */
  operTimeEnd = '';

  /** 更新时间开始 */
  operTimeStart = '';

  /** 页面编码 price价格说明 score如何获取积分 privac隐私说明 */
  pageCode = '';

  /** 页码 */
  pageIndex = undefined;

  /** 页面大小 */
  pageSize = undefined;

  /** 页面类型 */
  pageType = undefined;

  /** startRow */
  startRow = undefined;

  /** 标题 */
  title = '';
}

export class QueryPaymentCodeDTO {
  /** 开始时间 */
  beginTime = '';

  /** 结束时间 */
  endTime = '';

  /** 操作时间结束 */
  operEndTime = '';

  /** 更新人名称 */
  operName = '';

  /** 操作时间开始 */
  operStartTime = '';

  /** 排序方式 */
  orderBy = '';

  /** 页码 */
  pageIndex = undefined;

  /** 页面大小 */
  pageSize = undefined;

  /** 排序字段 */
  sortBy = '';

  /** 状态 0:禁用 1:启用 */
  status = undefined;

  /** 供应商IDs */
  supplierIds = [];

  /** 查询信息，模糊匹配，名称/编码 */
  supplierInfo = '';

  /** 是否删除 -1:删除 1:正常 */
  validity = undefined;
}

export class QueryPointRecordDTO {
  /** 结束时间 */
  end = '';

  /** 单号 */
  orderNo = '';

  /** 页码 */
  pageIndex = undefined;

  /** 每页显示条数 */
  pageSize = undefined;

  /** 手机号 */
  phone = '';

  /** 积点来源 1线上,2线下 */
  sourceType = undefined;

  /** 开始时间 */
  start = '';

  /** 交易类型:1充值,2赠送,3商城消费,4消费售后 */
  tradeType = undefined;

  /** 交易类型:1充值,2赠送,3商城消费,4消费售后 */
  tradeTypes = [];

  /** 用户id或姓名 */
  userName = '';
}

export class QueryProductDTO {
  /** 申请人名称 */
  applicantName = '';

  /** 申请时间(始) */
  applicantTimeBegin = '';

  /** 申请时间(止) */
  applicantTimeEnd = '';

  /** 审核人名称 */
  approvalName = '';

  /** 审核状态列表 0:待审核 1:驳回 2:通过 */
  approvalStatusList = [];

  /** 审核时间(始) */
  approvalTimeBegin = '';

  /** 审核时间(止) */
  approvalTimeEnd = '';

  /** 开始时间 */
  beginTime = '';

  /** 品牌Id列表 */
  brandIdList = [];

  /** 品类列表 */
  categoryCodeList = [];

  /** 结束时间 */
  endTime = '';

  /** 商品标签列表 */
  labelCodeList = [];

  /** 营销分类列表 */
  mktCodeList = [];

  /** 下架时间(始) */
  offShelvesTimeBegin = '';

  /** 下架时间(止) */
  offShelvesTimeEnd = '';

  /** 上架时间(始) */
  onShelvesTimeBegin = '';

  /** 上架时间(止) */
  onShelvesTimeEnd = '';

  /** 更新时间(始) */
  operTimeBegin = '';

  /** 更新时间(止) */
  operTimeEnd = '';

  /** 页码 */
  pageIndex = undefined;

  /** 页面大小 */
  pageSize = undefined;

  /** 商品查询 名称/编号(spuId)/skuId */
  productQuery = '';

  /** 上下架状态 0:下架 1:上架 2:发布中 3:待上架 */
  putOnShelvesList = [];

  /** 积分区间(大) */
  scoreBig = undefined;

  /** 积分区间(小) */
  scoreSmall = undefined;

  /** sku列表 */
  skuIdList = [];

  /** spu列表 */
  spuIdList = [];

  /** 库存区间(大) */
  stockBig = undefined;

  /** 出入库记录操作人名称 */
  stockRecordsOperName = '';

  /** 出入库记录订单号 */
  stockRecordsOrderNo = '';

  /** 出入库记录开始时间 */
  stockRecordsTimeBegin = '';

  /** 出入库记录结束时间 */
  stockRecordsTimeEnd = '';

  /** 库存区间(小) */
  stockSmall = undefined;

  /** 库存预警标志( 默认查所有列表 1:无货预警 2:低于安全库存预警) */
  stockWarnFlag = undefined;

  /** 供应商列表 */
  supplierIdList = [];

  /** 使用类型列表(1.商品核销 2.手动出库 3.售后入库 4.手动入库) */
  useTypeList = [];
}

export class QueryProductLabelDTO {
  /** 开始时间 */
  beginTime = '';

  /** 结束时间 */
  endTime = '';

  /** 标签code */
  labelCode = '';

  /** 标签名称 */
  labelName = '';

  /** 状态 0:禁用 1:启用 */
  labelStatus = undefined;

  /** 页码 */
  pageIndex = undefined;

  /** 页面大小 */
  pageSize = undefined;
}

export class QueryProductLogDTO {
  /** 开始时间 */
  beginTime = '';

  /** 结束时间 */
  endTime = '';

  /** 页码 */
  pageIndex = undefined;

  /** 页面大小 */
  pageSize = undefined;

  /** 商品查询 名称/编号(spuId) */
  productQuery = '';

  /** 上下架状态 0:下架 1:上架 2:发布中 3:待上架 */
  putOnShelvesList = [];

  /** 供应商列表 */
  supplierIdList = [];
}

export class QueryProductMktDTO {
  /** 开始时间 */
  beginTime = '';

  /** 结束时间 */
  endTime = '';

  /** 营销分类编码 */
  mktCode = '';

  /** 分类名称 */
  mktName = '';

  /** 状态 0:禁用 1:启用 */
  mktStatus = undefined;

  /** 页码 */
  pageIndex = undefined;

  /** 页面大小 */
  pageSize = undefined;

  /** 查询营销分类(名称或编码) */
  queryMkt = '';
}

export class QueryProductSalesDTO {
  /** 开始时间 */
  beginTime = '';

  /** 品牌Id列表 */
  brandIdList = [];

  /** 品类列表 */
  categoryCodeList = [];

  /** 结束时间 */
  endTime = '';

  /** 页码 */
  pageIndex = undefined;

  /** 页面大小 */
  pageSize = undefined;

  /** 商品查询 名称/编号(spuId)/skuId */
  productQuery = '';

  /** skuId列表 */
  skuIdList = [];

  /** spuId列表 */
  spuIdList = [];

  /** 供应商列表 */
  supplierIdList = [];

  /** 交易号(交易-订单号 售后-售后单号) */
  tradeNo = '';
}

export class QueryPropertyDTO {
  /** 开始时间 */
  beginTime = '';

  /** 结束时间 */
  endTime = '';

  /** 规格组ID */
  id = undefined;

  /** 规格组名称 */
  name = '';

  /** 页码 */
  pageIndex = undefined;

  /** 页面大小 */
  pageSize = undefined;
}

export class QuerySearchKeywordDTO {
  /** ID */
  id = undefined;

  /** 关键词 */
  keyword = '';

  /** 操作人 */
  operName = '';

  /** 更新时间结束 */
  operTimeEnd = '';

  /** 更新时间开始 */
  operTimeStart = '';

  /** 启用状态 1启用 0禁用 */
  status = undefined;
}

export class QueryServiceAssuranceDTO {
  /** 操作人 */
  operName = '';

  /** 更新时间结束 */
  operTimeEnd = '';

  /** 更新时间开始 */
  operTimeStart = '';

  /** 页码 */
  pageIndex = undefined;

  /** 页面大小 */
  pageSize = undefined;

  /** 权益名称 */
  serviceCode = '';

  /** startRow */
  startRow = undefined;

  /** 供应商ID */
  supperId = undefined;
}

export class QueryStockDTO {
  /** skuId */
  skuId = '';

  /** 供应商id */
  supplierId = '';
}

export class QuerySupplierDTO {
  /** 开始时间 */
  beginTime = '';

  /** 业务类型 */
  businessType = [];

  /** 创建时间结束 */
  createEndTime = '';

  /** 创建时间开始 */
  createStartTime = '';

  /** 结束时间 */
  endTime = '';

  /** 主键IDs */
  ids = [];

  /** 排序方式 */
  orderBy = '';

  /** 页码 */
  pageIndex = undefined;

  /** 页面大小 */
  pageSize = undefined;

  /** 是否启用收款码（1:启用，0:不启用） */
  paymentCodeEnable = undefined;

  /** 结算方式 1:经销，2:联营，3扣点 */
  settlementMode = undefined;

  /** 排序字段 */
  sortBy = '';

  /** 状态 0:禁用 1:启用 */
  status = undefined;

  /** 查询信息，模糊匹配，名称/编码 */
  supplierInfo = '';

  /** 是否删除 -1:删除 1:正常 */
  validity = undefined;
}

export class QueryUserListDTO {
  /** 注册结束时间 */
  endTime = '';

  /** 会员等级:1普通会员，2高级会员 */
  memberLevel = '';

  /** 页码 */
  pageIndex = undefined;

  /** 每页显示条数 */
  pageSize = undefined;

  /** 手机号 */
  phone = '';

  /** 注册开始时间 */
  startTime = '';

  /** 状态,1启用，2禁用 */
  status = undefined;

  /** 标签 */
  tags = '';

  /** 用户id */
  userId = undefined;

  /** 姓名 */
  userName = '';
}

export class QueryUserPointRecordDTO {
  /** 结束时间 */
  end = '';

  /** 页码 */
  pageIndex = undefined;

  /** 每页显示条数 */
  pageSize = undefined;

  /** 开始时间 */
  start = '';

  /** 交易类型:1赠送,2充值,3商城消费,4消费售后 */
  tradeType = undefined;

  /** 用户id */
  userId = undefined;
}

export class RechargeAccount {
  /** 用户手机号 */
  phone = '';

  /** 充值金额 */
  point = undefined;

  /** 用户id */
  userId = undefined;

  /** 用户姓名 */
  userName = '';
}

export class RechargeDetailVO {
  /** 审批备注 */
  auditRemark = '';

  /** 审批状态:0未审批，1已审批,2驳回 */
  auditStatus = undefined;

  /** 审批时间 */
  auditTime = '';

  /** 审批人 */
  auditUser = '';

  /** batchNo */
  batchNo = undefined;

  /** 创建时间 */
  createTime = '';

  /** 创建人 */
  createUser = '';

  /** 附件（逗号分隔） */
  files = '';

  /** id */
  id = undefined;

  /** 充值账号及金额 */
  rechargeAccount = [];

  /** 充值描述 */
  rechargeReason = '';

  /** 发放状态：0未审核未发放,1审核通过未发放,2发放中,3发放完成 */
  sendStatus = undefined;

  /** 发放成功人数 */
  successMember = undefined;

  /** 标题 */
  title = '';

  /** 批次发放人数 */
  totalMember = undefined;

  /** 批次总发放积分 */
  totalPoint = undefined;

  /** 类型:1充值,2赠送 */
  type = undefined;
}

export class RechargePageDTO {
  /** 申请结束时间 */
  applyEndTime = '';

  /** 申请开始时间 */
  applyStartTime = '';

  /** 审核结束时间 */
  auditEndTime = '';

  /** 审核开始时间 */
  auditStartTime = '';

  /** 审批状态:0未审批，1已审批,2驳回 */
  auditStatus = undefined;

  /** 批次号 */
  batchNo = undefined;

  /** id */
  id = undefined;

  /** 页码 */
  pageIndex = undefined;

  /** 每页显示条数 */
  pageSize = undefined;

  /** 标题 */
  title = '';

  /** 1充值2赠送 */
  type = undefined;
}

export class ResetPasswordDTO {
  /** 用户名id */
  id = undefined;

  /** 用户名id */
  password = '';
}

export class Resource {
  /** description */
  description = '';

  /** file */
  file = new File();

  /** filename */
  filename = '';

  /** inputStream */
  inputStream = new InputStream();

  /** open */
  open = false;

  /** readable */
  readable = false;

  /** uri */
  uri = new URI();

  /** url */
  url = new URL();
}

export class ResponseModel {
  /** code */
  code = '';

  /** data */
  data = new AdminSysLog();

  /** message */
  message = '';

  /** msg */
  msg = '';
}

export class RoleDTO {
  /** 描述 */
  description = '';

  /** 角色id 修改时需要 */
  id = undefined;

  /** 角色名 */
  name = '';

  /** 权限 */
  permissions = [];
}

export class RoleListVO {
  /** 创建人 */
  createName = '';

  /** 添加时间 */
  createTime = '';

  /** 描述 */
  description = '';

  /** 角色id 修改时需要 */
  id = undefined;

  /** 角色名 */
  name = '';

  /** 更新人 */
  updateName = '';

  /** 修改时间 */
  updateTime = '';
}

export class RoleQueryDTO {
  /** 名称 */
  name = '';

  /** 页码(>=1) */
  pageIndex = undefined;

  /** pageSize */
  pageSize = undefined;

  /** 更新时间 */
  updateEndTime = '';

  /** 更新人 */
  updateName = '';

  /** 更新时间 */
  updateStartTime = '';
}

export class RoleVO {
  /** 角色id */
  id = undefined;

  /** 角色名 */
  name = '';
}

export class SaleDetailListQueryDTO {
  /** 当前页 */
  pageIndex = undefined;

  /** 每页大小 */
  pageSize = undefined;

  /** 交易单号 */
  saleNo = '';

  /** 交易结束时间 */
  saleTimeEnd = '';

  /** 交易开始时间 */
  saleTimeStart = '';

  /** 交易类型(1:交易 2:售后 3:交易冲正) */
  saleType = undefined;

  /** 供应商id列表 */
  supplierIds = [];
}

export class SalesCountVO {
  /** 销售总量 */
  salesNumberTotal = undefined;

  /** 销售总价格 */
  salesPriceTotal = undefined;

  /** 销售总积分 */
  salesScoreTotal = undefined;
}

export class SearchKeywordBasicVO {
  /** ID */
  id = undefined;

  /** 关键词 */
  keyword = '';

  /** 排序 */
  sort = undefined;

  /** 启用状态 */
  status = undefined;
}

export class SearchKeywordDTO {
  /** ID */
  id = undefined;

  /** 关键词 */
  keyword = '';

  /** 排序 */
  sort = undefined;

  /** 启用状态 */
  status = undefined;
}

export class SearchKeywordVO {
  /** 创建时间 */
  createTime = '';

  /** ID */
  id = undefined;

  /** 关键词 */
  keyword = '';

  /** 操作人ID */
  operId = undefined;

  /** 操作人 */
  operName = '';

  /** 更新时间 */
  operTime = '';

  /** 排序 */
  sort = undefined;

  /** 启用状态 */
  status = undefined;
}

export class SearchProductDTO {
  /** 开始时间 */
  beginTime = '';

  /** 结束时间 */
  endTime = '';

  /** 商品关键词 */
  keyWord = '';

  /** 价格筛选 最大价格 */
  maxSalePrice = undefined;

  /** 积分筛选 最大积分 */
  maxScore = undefined;

  /** 价格筛选 最小价格 */
  minSalePrice = undefined;

  /** 积分筛选 最小积分 */
  minScore = undefined;

  /** 营销分类列表 */
  mktCodeList = [];

  /** 页码 */
  pageIndex = undefined;

  /** 页面大小 */
  pageSize = undefined;

  /** 排序字段【积分:score 价格:salePrice 销量:productSales】 */
  sortField = '';

  /** 排序方式 升序:ASC 降序:DESC 默认升序(ASC) */
  sortType = '';

  /** 供应商列表 */
  supplierIdList = [];
}

export class SearchProductVO {
  /** 品牌英文名 */
  brandEnglishName = '';

  /** 品牌id */
  brandId = '';

  /** 品牌名称 */
  brandName = '';

  /** 品类编码 */
  categoryCode = '';

  /** 品类名称 */
  categoryName = '';

  /** 吊牌价 */
  msrp = undefined;

  /** 商品标签信息 */
  productLabelVOList = [];

  /** 销量 */
  productSales = undefined;

  /** 售价 */
  salePrice = undefined;

  /** 积分 */
  score = undefined;

  /** skuId */
  skuId = '';

  /** skuId */
  spuId = '';

  /** spu名称 */
  spuName = '';

  /** 商品spu图片地址(封面) */
  spuPictureUrl = '';

  /** 供应商编码 */
  supplierId = '';

  /** 供应商名称 */
  supplierName = '';

  /** 商品总库存 */
  totalStock = undefined;
}

export class ServiceAssuranceBasicVO {
  /** ID */
  id = undefined;

  /** 服务保障编码 */
  serviceCode = '';

  /** 描述 */
  serviceDesc = '';

  /** 名称 */
  serviceName = '';

  /** 供应商编码 */
  supperId = undefined;

  /** 供应商名称 */
  supperName = '';
}

export class ServiceAssuranceDTO {
  /** ID */
  id = undefined;

  /** 描述 */
  serviceDesc = '';

  /** 名称 */
  serviceName = '';

  /** 供应商ID */
  supperId = undefined;

  /** 供应商名称 */
  supperName = '';
}

export class ServiceAssuranceVO {
  /** 创建时间 */
  createTime = '';

  /** ID */
  id = undefined;

  /** 操作人ID */
  operId = undefined;

  /** 操作人 */
  operName = '';

  /** 更新时间 */
  operTime = '';

  /** 服务保障编码 */
  serviceCode = '';

  /** 描述 */
  serviceDesc = '';

  /** 名称 */
  serviceName = '';

  /** 供应商编码 */
  supperId = undefined;

  /** 供应商名称 */
  supperName = '';
}

export class SettingDictDTO {
  /** 编码 */
  code = '';

  /** createTime */
  createTime = '';

  /** 字典值列表 */
  dictDetailDTOS = [];

  /** 主键ID */
  id = undefined;

  /** 名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 备注 */
  remark = '';
}

export class SettingDictDetailDTO {
  /** createTime */
  createTime = '';

  /** 字典ID */
  dictId = undefined;

  /** 扩展字段 */
  extend = '';

  /** 主键ID */
  id = undefined;

  /** 名称 */
  label = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 备注 */
  remark = '';

  /** 排序 */
  sort = '';

  /** 值 */
  value = '';
}

export class SettingDictDetailObject {
  /** createTime */
  createTime = '';

  /** dictId */
  dictId = undefined;

  /** 扩展字段 */
  extend = '';

  /** 主键ID */
  id = undefined;

  /** label */
  label = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 备注 */
  remark = '';

  /** sort */
  sort = '';

  /** value */
  value = '';
}

export class SettingDictDetailVO {
  /** createTime */
  createTime = '';

  /** dictId */
  dictId = undefined;

  /** 扩展字段 */
  extend = '';

  /** 主键ID */
  id = undefined;

  /** label */
  label = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 备注 */
  remark = '';

  /** sort */
  sort = '';

  /** value */
  value = '';
}

export class SettingDictVO {
  /** 编码 */
  code = '';

  /** 字段数 */
  count = undefined;

  /** createTime */
  createTime = '';

  /** 字典值列表 */
  dictDetailVOS = [];

  /** 主键ID */
  id = undefined;

  /** 名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 备注 */
  remark = '';
}

export class SettingMallDTO {
  /** createTime */
  createTime = '';

  /** 首页描述 */
  homeDesc = '';

  /** 主键ID */
  id = undefined;

  /** 图片 */
  imgUrl = '';

  /** logo图片 */
  logo = '';

  /** 小程序描述 */
  miniAppDesc = '';

  /** 商场名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 积分名称 */
  scoreName = '';

  /** 开放登录 0:禁用 1:启用 */
  validity = undefined;
}

export class SettingMallVO {
  /** createTime */
  createTime = '';

  /** 首页描述 */
  homeDesc = '';

  /** 主键ID */
  id = undefined;

  /** 图片 */
  imgUrl = '';

  /** logo图片 */
  logo = '';

  /** 小程序描述 */
  miniAppDesc = '';

  /** 商场名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 积分名称 */
  scoreName = '';

  /** 开放登录 0:禁用 1:启用 */
  validity = undefined;
}

export class SettingScoreDTO {
  /** createTime */
  createTime = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 积分名称 */
  scoreName = '';

  /** 积分规则列表 */
  scores = [];
}

export class SettingScoreObject {
  /** 会员等级code */
  code = '';

  /** createTime */
  createTime = '';

  /** 会员等级名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 积分 */
  score = undefined;
}

export class SettingScoreVO {
  /** 商城信息 */
  mall = new SettingMallVO();

  /** 积分规则列表 */
  scores = [];
}

export class SettingVersionDTO {
  /** createTime */
  createTime = '';

  /** 主键ID */
  id = undefined;

  /** 名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 描述 */
  remark = '';
}

export class SettingVersionObject {
  /** createTime */
  createTime = '';

  /** 主键ID */
  id = undefined;

  /** 名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 描述 */
  remark = '';
}

export class ShortLinkVO {
  /** 长链接 */
  longLink = '';

  /** 短链接 */
  shortLink = '';
}

export class SkuStockVO {
  /** 库存数量 */
  count = undefined;

  /** skuId */
  skuId = '';

  /** 供应商id */
  supplierId = '';
}

export class SmsSendDTO {
  /** 手机号 */
  mobile = '';

  /** 短信类型:2:重新绑定会员卡发送短信 */
  smsType = undefined;
}

export class SmsVerifyDTO {
  /** 手机号 */
  mobile = '';

  /** 验证码类型！ */
  smsType = undefined;

  /** 验证码,验证时必传 */
  verificationCode = '';
}

export class StatusChangeDTO {
  /** ID */
  id = undefined;

  /** 启用状态 1启用 0禁用 */
  status = undefined;
}

export class StockItemDTO {
  /** count */
  count = undefined;

  /** skuId */
  skuId = '';
}

export class StockListVO {
  /** propertyValueName */
  propertyValueName = '';

  /** 上下架状态 0: 下架 1: 上架 2: 发布中 3: 待上架 */
  putOnShelves = undefined;

  /** putOnShelvesName */
  putOnShelvesName = '';

  /** score */
  score = undefined;

  /** skuId */
  skuId = '';

  /** spuId */
  spuId = '';

  /** spuName */
  spuName = '';

  /** stock */
  stock = undefined;

  /** supplierName */
  supplierName = '';
}

export class StockRecordsWithProductVO {
  /** 品牌英文名 */
  brandEnglishName = '';

  /** 品牌id */
  brandId = undefined;

  /** 品牌名称 */
  brandName = '';

  /** 品类编码 */
  categoryCode = '';

  /** 品类全称 */
  categoryFullName = '';

  /** 品类名称 */
  categoryName = '';

  /** 创建时间 */
  createTime = '';

  /** 配送方式 0:无需配送 1:快递 2:自提 3:快递或自提 */
  deliveryType = undefined;

  /** 商品描述 */
  description = '';

  /** 商品标签编码列表 */
  labelCodeList = [];

  /** 剩余库存 */
  leftStock = undefined;

  /** 上架时间 */
  onShelvesBeginTime = '';

  /** 下架时间 */
  onShelvesEndTime = '';

  /** 操作人姓名 */
  operName = '';

  /** 订单号 */
  orderNo = '';

  /** 规格名称 */
  propertyValueName = '';

  /** 上下架状态 0: 下架 1: 上架 2: 发布中 3: 待上架 */
  putOnShelves = undefined;

  /** 商品备注 */
  remark = '';

  /** 服务保障编码列表 */
  serviceCodeList = [];

  /** skuId */
  skuId = '';

  /** spuId */
  spuId = '';

  /** spu名称 */
  spuName = '';

  /** 商品spu图片地址(封面) */
  spuPictureUrl = '';

  /** 商品视屏地址 */
  spuViewUrl = '';

  /** 库存值(可为负) */
  stock = undefined;

  /** 供应商编码 */
  supplierId = '';

  /** 供应商名称 */
  supplierName = '';

  /** 供应商简称 */
  supplierShortName = '';

  /** 使用类型(1.商品核销 2.手动出库 3.售后入库 4.手动入库) */
  useType = undefined;
}

export class StockWarnWithProductVO {
  /** 品牌英文名 */
  brandEnglishName = '';

  /** 品牌id */
  brandId = undefined;

  /** 品牌名称 */
  brandName = '';

  /** 品类编码 */
  categoryCode = '';

  /** 品类全称 */
  categoryFullName = '';

  /** 品类名称 */
  categoryName = '';

  /** 配送方式 0:无需配送 1:快递 2:自提 3:快递或自提 */
  deliveryType = undefined;

  /** 商品描述 */
  description = '';

  /** 商品标签编码列表 */
  labelCodeList = [];

  /** 锁定库存 */
  lockStock = undefined;

  /** 吊牌价 */
  msrp = undefined;

  /** 上架时间 */
  onShelvesBeginTime = '';

  /** 下架时间 */
  onShelvesEndTime = '';

  /** 规格名称 */
  propertyValueName = '';

  /** 上下架状态 0: 下架 1: 上架 2: 发布中 3: 待上架 */
  putOnShelves = undefined;

  /** 商品备注 */
  remark = '';

  /** 安全库存 */
  safeStock = undefined;

  /** 售价 */
  salePrice = undefined;

  /** 积分 */
  score = undefined;

  /** 服务保障编码列表 */
  serviceCodeList = [];

  /** skuId */
  skuId = '';

  /** spuId */
  spuId = '';

  /** spu名称 */
  spuName = '';

  /** 商品spu图片地址(封面) */
  spuPictureUrl = '';

  /** 商品视屏地址 */
  spuViewUrl = '';

  /** 可售库存 */
  stock = undefined;

  /** 库存预警 0:关闭 1:开启 */
  stockWarn = undefined;

  /** 供应商编码 */
  supplierId = '';

  /** 供应商名称 */
  supplierName = '';

  /** 供应商简称 */
  supplierShortName = '';
}

export class SupplierBusinessTypeObject {
  /** 业务类型ID */
  businessTypeId = undefined;

  /** 主键ID */
  id = undefined;
}

export class SupplierDTO {
  /** 所属行业IDS */
  businessTypes = [];

  /** 编码 */
  code = '';

  /** 联系人地址 */
  contactAddress = '';

  /** 联系人 */
  contactName = '';

  /** 联系人电话 */
  contactPhone = '';

  /** 客服电话 */
  customerPhone = '';

  /** 自提点列表 */
  deliveryAddresses = [];

  /** 主键ID */
  id = undefined;

  /** 介绍 */
  intro = '';

  /** 供应商名称 */
  name = '';

  /** 是否启用收款码（1:启用，0:不启用） */
  paymentCodeEnable = undefined;

  /** 备注 */
  remark = '';

  /** 结算方式 1:经销，2:联营，3扣点 */
  settlementMode = undefined;

  /** 供应商简称 */
  shortName = '';

  /** 状态 0:禁用 1:启用 */
  status = undefined;

  /** 核销账号列表 */
  verifyAccounts = [];

  /** 预警手机号 */
  warningMobile = '';
}

export class SupplierDeliveryAddressDTO {
  /** 详细地址 */
  address = '';

  /** 区编码 */
  area = '';

  /** 区编码name */
  areaName = '';

  /** 市编码 */
  city = '';

  /** 市编码name */
  cityName = '';

  /** 国家编码 */
  country = '';

  /** 国家编码name */
  countryName = '';

  /** createTime */
  createTime = '';

  /** 主键ID */
  id = undefined;

  /** 纬度 */
  latitude = '';

  /** 经度 */
  longitude = '';

  /** 自提点名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 省编码 */
  province = '';

  /** 省编码name */
  provinceName = '';

  /** 状态 0:禁用 1:启用 */
  status = undefined;

  /** 供应商ID */
  supplierId = undefined;

  /** 服务电话 */
  tel = '';

  /** 是否删除 -1:删除 1:正常 */
  validity = undefined;
}

export class SupplierDeliveryAddressObject {
  /** 详细地址 */
  address = '';

  /** 区编码 */
  area = '';

  /** 市编码 */
  city = '';

  /** 国家编码 */
  country = '';

  /** createTime */
  createTime = '';

  /** 主键ID */
  id = undefined;

  /** 纬度 */
  latitude = '';

  /** 经度 */
  longitude = '';

  /** 自提点名称 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 省编码 */
  province = '';

  /** 状态 0:禁用 1:启用 */
  status = undefined;

  /** 供应商ID */
  supplierId = undefined;

  /** 服务电话 */
  tel = '';

  /** 是否删除 -1:删除 1:正常 */
  validity = undefined;
}

export class SupplierDetailVO {
  /** 所属行业IDS */
  businessTypes = [];

  /** 编码 */
  code = '';

  /** 联系人地址 */
  contactAddress = '';

  /** 联系人 */
  contactName = '';

  /** 联系人电话 */
  contactPhone = '';

  /** 创建时间 */
  createTime = '';

  /** 客服电话 */
  customerPhone = '';

  /** 自提点列表 */
  deliveryAddresses = [];

  /** 主键ID */
  id = undefined;

  /** 介绍 */
  intro = '';

  /** 供应商名称 */
  name = '';

  /** 操作人id */
  operId = undefined;

  /** 操作人名称 */
  operName = '';

  /** 操作时间 */
  operTime = '';

  /** 是否启用收款码（1:启用，0:不启用） */
  paymentCodeEnable = undefined;

  /** 备注 */
  remark = '';

  /** 结算方式 1:经销，2:联营，3扣点 */
  settlementMode = undefined;

  /** 供应商简称 */
  shortName = '';

  /** 状态 0:禁用 1:启用 */
  status = undefined;

  /** 是否删除 -1:删除 1:正常 */
  validity = undefined;

  /** 核销账号列表 */
  verifyAccounts = [];

  /** 预警手机号 */
  warningMobile = '';
}

export class SupplierVO {
  /** 编码 */
  code = '';

  /** 联系人地址 */
  contactAddress = '';

  /** 联系人 */
  contactName = '';

  /** 联系人电话 */
  contactPhone = '';

  /** 创建时间 */
  createTime = '';

  /** 客服电话 */
  customerPhone = '';

  /** 自提点（启用） */
  deliveryAddresses = new SupplierDeliveryAddressObject();

  /** 主键ID */
  id = undefined;

  /** 介绍 */
  intro = '';

  /** 供应商名称 */
  name = '';

  /** 操作人id */
  operId = undefined;

  /** 操作人名称 */
  operName = '';

  /** 操作时间 */
  operTime = '';

  /** 是否启用收款码（1:启用，0:不启用） */
  paymentCodeEnable = undefined;

  /** 备注 */
  remark = '';

  /** 结算方式 1:经销，2:联营，3扣点 */
  settlementMode = undefined;

  /** 供应商简称 */
  shortName = '';

  /** 状态 0:禁用 1:启用 */
  status = undefined;

  /** 是否删除 -1:删除 1:正常 */
  validity = undefined;

  /** 预警手机号 */
  warningMobile = '';
}

export class SupplierVerifyAccountObject {
  /** createTime */
  createTime = '';

  /** 主键ID */
  id = undefined;

  /** 核销账号（手机号） */
  mobile = '';

  /** 姓名 */
  name = '';

  /** operId */
  operId = undefined;

  /** operName */
  operName = '';

  /** operTime */
  operTime = '';

  /** 供应商ID */
  supplierId = undefined;

  /** 是否删除 -1:删除 1:正常 */
  validity = undefined;
}

export class TokenVO {
  /** 过期时间秒 */
  expireIn = undefined;

  /** 刷新token */
  refreshToken = '';

  /** token */
  token = '';
}

export class TreeNode {
  /** children */
  children = [];

  /** node */
  node = new ProductMktVO();
}

export class URI {
  /** absolute */
  absolute = false;

  /** authority */
  authority = '';

  /** fragment */
  fragment = '';

  /** host */
  host = '';

  /** opaque */
  opaque = false;

  /** path */
  path = '';

  /** port */
  port = undefined;

  /** query */
  query = '';

  /** rawAuthority */
  rawAuthority = '';

  /** rawFragment */
  rawFragment = '';

  /** rawPath */
  rawPath = '';

  /** rawQuery */
  rawQuery = '';

  /** rawSchemeSpecificPart */
  rawSchemeSpecificPart = '';

  /** rawUserInfo */
  rawUserInfo = '';

  /** scheme */
  scheme = '';

  /** schemeSpecificPart */
  schemeSpecificPart = '';

  /** userInfo */
  userInfo = '';
}

export class URL {
  /** authority */
  authority = '';

  /** content */
  content = undefined;

  /** defaultPort */
  defaultPort = undefined;

  /** deserializedFields */
  deserializedFields = new URLStreamHandler();

  /** file */
  file = '';

  /** host */
  host = '';

  /** path */
  path = '';

  /** port */
  port = undefined;

  /** protocol */
  protocol = '';

  /** query */
  query = '';

  /** ref */
  ref = '';

  /** serializedHashCode */
  serializedHashCode = undefined;

  /** userInfo */
  userInfo = '';
}

export class URLStreamHandler {}

export class UpdateCartItemVO {
  /** 商品数量 */
  count = undefined;

  /** 购物车商品项唯一id */
  itemId = undefined;

  /** skuId */
  skuId = '';
}

export class UpdateCouponTemplateStatusDTO {
  /** 券模板id 修改时需要 */
  id = undefined;

  /** 状态0启用 1禁用 */
  status = undefined;
}

export class UserCar {
  /** buyTime */
  buyTime = '';

  /** carBrand */
  carBrand = undefined;

  /** carBrandName */
  carBrandName = '';

  /** carModel */
  carModel = undefined;

  /** carModelName */
  carModelName = '';

  /** createTime */
  createTime = '';

  /** driveLicense */
  driveLicense = '';

  /** driveMileage */
  driveMileage = '';

  /** engineNo */
  engineNo = '';

  /** frameNo */
  frameNo = '';

  /** icon */
  icon = '';

  /** id */
  id = undefined;

  /** plateCity */
  plateCity = '';

  /** plateNo */
  plateNo = '';

  /** plateProvince */
  plateProvince = '';

  /** remark */
  remark = '';

  /** sendPoint */
  sendPoint = undefined;

  /** sendReason */
  sendReason = '';

  /** updateTime */
  updateTime = '';

  /** userId */
  userId = undefined;
}

export class UserCarListVO {
  /** 购买时间 */
  buyTime = '';

  /** 车系 */
  carModel = '';

  /** 车系名称 */
  carModelName = '';

  /** id */
  id = undefined;

  /** 车牌号-市 */
  plateCity = '';

  /** 车牌号 */
  plateNo = '';

  /** 车牌号-省 */
  plateProvince = '';
}

export class UserInfo {
  /** birthday */
  birthday = '';

  /** carNum */
  carNum = undefined;

  /** createTime */
  createTime = '';

  /** 是否是员工,1是，0否 */
  employer = undefined;

  /** forzePoint */
  forzePoint = undefined;

  /** id */
  id = undefined;

  /** identNo */
  identNo = '';

  /** memberLevel */
  memberLevel = '';

  /** memberLevelBenefit */
  memberLevelBenefit = '';

  /** nickname */
  nickname = '';

  /** openid */
  openid = '';

  /** phone */
  phone = '';

  /** point */
  point = undefined;

  /** regChannel */
  regChannel = undefined;

  /** remark */
  remark = '';

  /** sex */
  sex = '';

  /** status */
  status = undefined;

  /** statusName */
  statusName = '';

  /** tags */
  tags = '';

  /** tagsName */
  tagsName = [];

  /** tagsNames */
  tagsNames = '';

  /** unionid */
  unionid = '';

  /** uniqueId */
  uniqueId = undefined;

  /** userName */
  userName = '';

  /** userType */
  userType = undefined;
}

export class UserInfoVO {
  /** 车辆数量 */
  carNum = undefined;

  /** 是否是员工,1是，0否 */
  employer = undefined;

  /** 会员等级:1普通会员，2高级会员 */
  memberLevel = '';

  /** 昵称 */
  nickname = '';

  /** 用户电话 */
  phone = '';

  /** 账户余额 */
  point = undefined;

  /** 用户车辆信息 */
  userCarList = [];

  /** 用户名 */
  userName = '';
}

export class UserLoginVO {
  /** 是否是新用户 */
  isNew = false;

  /** true:需要绑定手机号 */
  needBind = false;

  /** 提示信息 */
  notice = '';

  /** openid */
  openid = '';

  /** 用户登录成功后返回ticket */
  ticket = '';
}

export class UserPointRecordListVO {
  /** 交易后积点 */
  afterPoint = undefined;

  /** batchNo */
  batchNo = undefined;

  /** createTime */
  createTime = '';

  /** 交易前积点 */
  currentPoint = undefined;

  /** 交易积分(有正负) */
  flowPoint = undefined;

  /** orderNo */
  orderNo = '';

  /** phone */
  phone = '';

  /** remark */
  remark = '';

  /** 交易方 */
  tradeCompany = '';

  /** tradeType */
  tradeType = undefined;

  /** userId */
  userId = undefined;

  /** userName */
  userName = '';
}

export class UserPointVO {
  /** list */
  list = new Page();

  /** point */
  point = undefined;
}

export class UserRechargeBatch {
  /** auditRemark */
  auditRemark = '';

  /** auditStatus */
  auditStatus = undefined;

  /** auditStatusName */
  auditStatusName = '';

  /** auditTime */
  auditTime = '';

  /** auditUser */
  auditUser = '';

  /** batchNo */
  batchNo = undefined;

  /** createTime */
  createTime = '';

  /** createUser */
  createUser = '';

  /** files */
  files = '';

  /** id */
  id = undefined;

  /** 充值描述 */
  rechargeReason = '';

  /** 备注 */
  remark = '';

  /** sendStatus */
  sendStatus = undefined;

  /** successMember */
  successMember = undefined;

  /** title */
  title = '';

  /** totalMember */
  totalMember = undefined;

  /** totalPoint */
  totalPoint = undefined;

  /** type */
  type = undefined;

  /** typeName */
  typeName = '';
}

export class UserSmsLoginDTO {
  /** 手机号 */
  mobile = '';

  /** 短信验证码 */
  veriftyCode = '';
}

export class VerifiedCouponListVO {
  /** 消费时间 */
  consumeTime = '';

  /** 卡券名称 */
  name = '';

  /** 卡券类型 1、兑换券 */
  type = undefined;

  /** 核销码 */
  verifyCode = '';

  /** 核销人姓名 */
  verifyName = '';
}

export class VerifyDTO {
  /** 订单详情id */
  orderDetailId = undefined;

  /** 强制核销说明(强制核销必传) */
  remark = '';

  /** 核销码(核销码核销时必传) */
  verifyCode = '';

  /** 核销类型(1:核销码核销 2:强制核销) */
  verifyType = undefined;
}

export class WechatGenerateUrlDTO {
  /** 云开发静态网站自定义 H5 配置参数，可配置中转的云开发 H5 页面。不填默认用官方 H5 页面 */
  cloud_base = '';

  /** 到期失效的URL Link的失效间隔天数。生成的到期失效URL Link在该间隔时间到达前有效。最长间隔天数为365天。expire_type 为 1 必填 */
  expire_interval = undefined;

  /** 到期失效的 URL Link 的失效时间，为 Unix 时间戳。生成的到期失效 URL Link 在该时间前有效。最长有效期为1年。expire_type 为 0 必填 */
  expire_time = undefined;

  /** 失效类型，失效时间：0，失效间隔天数：1 */
  expire_type = undefined;

  /** 生成的 URL Link 类型，到期失效：true，永久有效：false */
  is_expire = false;

  /** URL Link 进入的小程序页面路径，必须是已经发布的小程序存在的页面，不可携带 query 。path 为空时会跳转小程序主页 */
  path = '';

  /** 通过 URL Link 进入小程序时的query，最大1024个字符，只支持数字，大小写英文以及部分特殊字符：!#$&'()*+,/:;=?@-._~ */
  query = '';
}
