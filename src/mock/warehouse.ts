import {
  successResponseWrap,
  mock,
  pagerResponseWrap,
} from '@/utils/setup-mock';

export const data = [
  {
    id: '1',
    name: '一号仓库',
    shortName: 'supplier',
    code: 'A041',
    businessPattern: 1,
    clearingForm: 1,
    status: 1,
    createDate: '2021-12-12 12:00',
  },
  {
    id: '2',
    name: '二号仓库',
    shortName: 'supplier',
    code: 'A042',
    businessPattern: 2,
    clearingForm: 2,
    status: 1,
    createDate: '2021-12-12 12:00',
  },
  {
    id: '3',
    name: '三号仓库',
    shortName: 'supplier',
    code: 'A042',
    businessPattern: 3,
    clearingForm: 1,
    status: 2,
    createDate: '2021-12-12 12:00',
  },
];

export const list = (req: any) => {
  return mock(pagerResponseWrap(req, data));
};

export const all = () => {
  return mock(successResponseWrap(data));
};

export const detail = (id: any) => {
  return mock(successResponseWrap(data.find((item) => item.id === id)));
};

export default mock;
