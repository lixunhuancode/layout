export default [
  {
    path: 'recharge/list',
    name: 'member.integral.recharge.list',
    component: () => import('./list/index.vue'),
    meta: {
      sort: 1,
      locale: '积分充值',
    },
  },
  {
    path: 'recharge/detail/:id',
    name: 'member.integral.recharge.detail',
    component: () => import('./detail/index.vue'),
    meta: {
      sort: 2,
      locale: '充值详情',
      hideInMenu: true,
    },
  },
  {
    path: 'recharge/batch',
    name: 'member.integral.recharge.batch',
    component: () => import('./batch/index.vue'),
    meta: {
      sort: 3,
      locale: '批量充值积分',
      hideInMenu: true,
    },
  },
];
