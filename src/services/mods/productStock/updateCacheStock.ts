/**
   * @desc 批量修改Sku缓存中的库存(单个修改传一个Map键值对)
批量修改Sku缓存中的库存(单个修改传一个Map键值对)
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** 缓存Id */
  cacheId?: string;
}

export type RequestBody = ObjectMap<any, number>;

export const init = false;

export type ParamsAndBody = { params: Params; data: ObjectMap<any, number> };

export function request(
  { params, data }: { params: Params; data: ObjectMap<any, number> },
  options: AxiosRequestConfig,
) {
  return axiosHandle(
    `backend/productStock/updateCacheStock/${params?.cacheId}`,
    {
      params,
      data,
      method: 'POST',
      ...options,
    },
  );
}
