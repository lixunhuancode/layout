/**
   * @desc 分页查询销售报表
分页查询销售报表
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryProductSalesDTO;

export const init = new defs.ProductSalesVO();

export type ParamsAndBody = { data: defs.QueryProductSalesDTO };

export function request(
  data: defs.QueryProductSalesDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/productSales/pageQuerySales`, {
    data,
    method: 'POST',
    ...options,
  });
}
