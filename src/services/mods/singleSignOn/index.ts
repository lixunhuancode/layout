/**
 * @description 用户注册登录相关接口
 */
import * as logout from './logout';
import * as miniBindMobile from './miniBindMobile';
import * as miniLogin from './miniLogin';
import * as smsLogin from './smsLogin';
import * as unlockout from './unlockout';

export { logout, miniBindMobile, miniLogin, smsLogin, unlockout };
