/**
 * @desc 获取车型列表通过品牌ID
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** brandId */
  brandId: number;
}

export const init = new defs.ResponseModel();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`backend/carType/getListByBrandId`, {
    params,
    method: 'POST',
    ...options,
  });
}
