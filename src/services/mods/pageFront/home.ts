/**
 * @desc 查询页面配置内容
 */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = new defs.ResponseModel();

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`front/cms/page/home`, {
    method: 'GET',
    ...options,
  });
}
