/**
 * @desc 获取供应商列表（分页）
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QuerySupplierDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.QuerySupplierDTO };

export function request(
  data: defs.QuerySupplierDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/supplier/list`, {
    data,
    method: 'POST',
    ...options,
  });
}
