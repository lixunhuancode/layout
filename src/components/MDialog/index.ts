import { App, createVNode, render } from 'vue';
import DialogComponent from './DialogWrapper.vue';
import DialogFooter from './DialogFooter.vue';
import type { ConfirmData } from './Confirm/types';
import type { PreviewHtmlDialogOptions } from '../PreviewHtmlDialog/types';

import store from './store';

export const useDialog = () => store;

export const dialogInstall = function install(app: App) {
  const vm = createVNode(DialogComponent);
  const container = document.createElement('div');
  container.setAttribute('class', `dialog-container`);
  // eslint-disable-next-line
  vm.appContext = app._context;
  render(vm, container);
  document.body.appendChild(container);
  // render(DialogComponent, app._context);

  app.component('DialogFooter', DialogFooter);

  const { globalProperties } = app.config;
  globalProperties.$useDialog = useDialog;

  globalProperties.$confirm = async (options: ConfirmData | string) => {
    if (typeof options === 'string') {
      options = { content: options };
    }
    return store.open({
      class: 'confirm-dialog-com',
      title: options.title,
      component: () => import('./Confirm/index.vue'),
      data() {
        return options;
      },
    });
  };

  globalProperties.$openPreviewHtmlDialog = async (
    options: PreviewHtmlDialogOptions
  ) => {
    if (typeof options === 'string') {
      options = { content: options };
    }

    return store.open({
      title: options.title,
      component: () => import('../PreviewHtmlDialog/index.vue'),
      data() {
        return options.content;
      },
    });
  };
};

export default dialogInstall;
