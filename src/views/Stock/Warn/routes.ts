export default [
  {
    path: 'warn/list',
    name: 'stock.warn.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '库存预警',
      requiresAuth: false,
    },
  },
  {
    path: 'warn/batchadd',
    name: 'stock.warn.batch.add',
    component: () => import('@/views/Stock/Batch/index.vue'),
    meta: {
      locale: '批量补充库存',
      requiresAuth: false,
      hideInMenu: true,
    },
  },
  {
    path: 'warn/add',
    name: 'stock.warn.add',
    component: () => import('@/views/Stock/Batch/index.vue'),
    meta: {
      locale: '补充库存',
      requiresAuth: false,
      hideInMenu: true,
    },
  },
];
