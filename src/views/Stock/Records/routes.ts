export default [
  {
    path: 'records/list',
    name: 'stock.records.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '出入库记录',
      requiresAuth: false,
    },
  },
];
