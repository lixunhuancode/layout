import { pagerResponseWrap, mock } from '@/utils/setup-mock';

export const list = (req: any) => {
  console.log({ req });
  const data = [
    {
      name: '测试商品1',
      className: '裤子',
      a1: 'D164860713892039',
      a2: '金科一店',
      a3: '张三',
      a4: '185****5555',
      a5: '快递发货',
      a6: '微信商城',
      a7: 1,
      a8: 4,
      a9: '¥100.00',
      a10: '¥80.00',
    },
    {
      name: '测试商品2',
      className: '衣服',
      a1: 'D164860713892039',
      a2: '金科一店',
      a3: '张三',
      a4: '185****5555',
      a5: '快递发货',
      a6: '微信商城',
      a7: 3,
      a8: 5,
      a9: '¥100.00',
      a10: '¥80.00',
    },
  ];
  return mock(pagerResponseWrap(req, data));
};

export default mock;
