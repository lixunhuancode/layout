/**
   * @desc 获取订单操作日志
获取订单操作日志
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** id */
  id: number;
}

export const init = new defs.OrderTraceVO();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`backend/order/getOrderOperateLog/${params.id}`, {
    params,
    method: 'GET',
    ...options,
  });
}
