/**
   * @desc 更新品牌状态
更新品牌状态
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** 品牌ID */
  brandId: number;
  /** 状态 0:禁用 1:启用 */
  status: number;
}

export const init = false;

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(
    `backend/brand/operateBrandStatus/${params.brandId}/${params.status}`,
    {
      params,
      method: 'PUT',
      ...options,
    },
  );
}
