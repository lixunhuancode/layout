/**
 * @description 后端积分设置管理
 */
import * as getDetail from './getDetail';
import * as update from './update';

export { getDetail, update };
