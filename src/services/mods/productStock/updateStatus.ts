/**
   * @desc 修改库存预警状态
修改库存预警状态
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** 状态:0:关闭 1:开启 */
  stockWarn?: number;
}

export type RequestBody = Array<string>;

export const init = false;

export type ParamsAndBody = { params: Params; data: Array<string> };

export function request(
  { params, data }: { params: Params; data: Array<string> },
  options: AxiosRequestConfig,
) {
  return axiosHandle(
    `backend/productStock/updateStockWarn/${params?.stockWarn}`,
    {
      params,
      data,
      method: 'POST',
      ...options,
    },
  );
}
