/**
 * @desc 通过id获取详细信息
 */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = new defs.ResponseModel();

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`backend/payment-code/getNewPaymentCode`, {
    method: 'GET',
    ...options,
  });
}
