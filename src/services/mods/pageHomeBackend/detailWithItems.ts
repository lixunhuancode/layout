/**
 * @desc 查询页面配置内容
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** id */
  id: number;
}

export const init = new defs.ResponseModel();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`backend/cms/page/home/detailWithItems/${params.id}`, {
    params,
    method: 'GET',
    ...options,
  });
}
