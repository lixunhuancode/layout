/**
 * @desc 根据服务保障编号查询服务保障详情
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** code */
  code: string;
}

export const init = new defs.ResponseModel();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(
    `backend/cms/serviceAssurance/detailByCode/${params.code}`,
    {
      params,
      method: 'GET',
      ...options,
    },
  );
}
