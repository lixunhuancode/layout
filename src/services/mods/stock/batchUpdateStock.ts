/**
   * @desc 批量修改库存
批量修改库存
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = Array<defs.StockItemDTO>;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: Array<defs.StockItemDTO> };

export function request(
  data: Array<defs.StockItemDTO>,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/stock/batchUpdateStock`, {
    data,
    method: 'POST',
    ...options,
  });
}
