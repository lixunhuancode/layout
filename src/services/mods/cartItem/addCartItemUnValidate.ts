/**
   * @desc 添加商品到购物车（购物车内用，独立增加数量，不做校验）
添加商品到购物车
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.CartItemDTO;

export const init = new defs.CartItemProductDto();

export type ParamsAndBody = { data: defs.CartItemDTO };

export function request(data: defs.CartItemDTO, options: AxiosRequestConfig) {
  return axiosHandle(`front/cartItem/addCartItemUnValidate`, {
    data,
    method: 'POST',
    ...options,
  });
}
