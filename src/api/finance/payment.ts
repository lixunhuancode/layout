import axios from 'axios';

export function list(data: any) {
  return axios.post('/api/payment/list', data);
}

export default axios;
