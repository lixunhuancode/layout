/**
   * @desc 批量下架
批量下架
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = Array<string>;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: Array<string> };

export function request(data: Array<string>, options: AxiosRequestConfig) {
  return axiosHandle(`backend/spu/offShelf`, {
    data,
    method: 'POST',
    ...options,
  });
}
