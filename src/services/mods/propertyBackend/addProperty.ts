/**
   * @desc 新增规格组
新增规格组
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.ProductPropertyDTO;

export const init = false;

export type ParamsAndBody = { data: defs.ProductPropertyDTO };

export function request(
  data: defs.ProductPropertyDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/property/add`, {
    data,
    method: 'POST',
    ...options,
  });
}
