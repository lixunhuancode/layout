/**
 * @desc 列表
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.CouponTemplateQueryDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.CouponTemplateQueryDTO };

export function request(
  data: defs.CouponTemplateQueryDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`admin/coupon/template/list`, {
    data,
    method: 'POST',
    ...options,
  });
}
