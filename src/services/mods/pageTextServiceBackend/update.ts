/**
 * @desc 编辑服务内容
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.PageTextDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.PageTextDTO };

export function request(data: defs.PageTextDTO, options: AxiosRequestConfig) {
  return axiosHandle(`backend/cms/pageText/service/update`, {
    data,
    method: 'POST',
    ...options,
  });
}
