import { DEFAULT_LAYOUT } from '@/router/routes/base';
import activityProductSearch from './activityProductSearch/routes';
import marketingSite from './marketingSite/routes';
import storeCoupon from './storeCoupon/routes';
import coupon from './coupon/routes';
import timePrice from './timePrice/routes';
import promotion from './promotion/routes';

export default {
  path: '/marketing',
  name: 'marketing',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '营销',
    icon: 'icon-c-marketing',
    sort: 5,
  },
  children: [
    marketingSite,
    activityProductSearch,
    {
      path: 'activities',
      name: 'marketing.activities',
      component: () => import('@/components/routeView.vue'),
      meta: {
        locale: '营销活动',
        icon: 'icon-relation',
        sort: 3,
      },
      children: [...coupon, storeCoupon, ...timePrice, ...promotion],
    },
  ],
};
