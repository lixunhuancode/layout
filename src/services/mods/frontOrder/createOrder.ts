/**
   * @desc 创建订单
创建订单
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.OrderCreateDTO;

export const init = '';

export type ParamsAndBody = { data: defs.OrderCreateDTO };

export function request(
  data: defs.OrderCreateDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`front/order/createOrder`, {
    data,
    method: 'POST',
    ...options,
  });
}
