export default [
  {
    path: 'home/list',
    name: 'cms.home.list',
    component: () => import('@/views/CMS/Home/List/index.vue'),
    meta: {
      locale: '首页管理',
      requiresAuth: false,
    },
  },
  {
    path: 'home/form',
    name: 'cms.home.add',
    component: () => import('./Form/index.vue'),
    meta: {
      hideInMenu: true,
      locale: '新增首页',
    },
  },
  {
    path: 'home/form/:id',
    name: 'cms.home.edit',
    component: () => import('./Form/index.vue'),
    meta: {
      hideInMenu: true,
      locale: '编辑首页',
    },
  },
];
