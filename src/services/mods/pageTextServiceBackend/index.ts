/**
 * @description 内容管理-服务内容-后台接口
 */
import * as detail from './detail';
import * as list from './list';
import * as update from './update';

export { detail, list, update };
