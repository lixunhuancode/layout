/**
 * @desc 获取首页列表
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryPageDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.QueryPageDTO };

export function request(data: defs.QueryPageDTO, options: AxiosRequestConfig) {
  return axiosHandle(`backend/cms/page/home/list`, {
    data,
    method: 'POST',
    ...options,
  });
}
