import { DEFAULT_LAYOUT } from '@/router/routes/base';
import MerchanDiseSales from './MerchandiseSales/routes';
import BuyPoints from './BuyPoints/routes';
import Transaction from './Transaction/routes';

export default {
  path: '/statistics',
  name: 'statistics',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '统计',
    requiresAuth: false,
    icon: 'icon-c-statistics',
    order: 8,
  },
  children: [...MerchanDiseSales, ...BuyPoints, ...Transaction],
};
