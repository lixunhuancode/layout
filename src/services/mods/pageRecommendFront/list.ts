/**
 * @desc 获取页面功能推荐(个人中心)推荐
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryPageRecommendFrontDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.QueryPageRecommendFrontDTO };

export function request(
  data: defs.QueryPageRecommendFrontDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`front/cms/pageRecommend/list`, {
    data,
    method: 'POST',
    ...options,
  });
}
