import { pagerResponseWrap, mock } from '@/utils/setup-mock';

export const list = (req: any) => {
  console.log({ req });
  const data = [
    {
      storeName: '金科一店',
      pay: '1',
      num: '22',
      a3: '333',
    },
    {
      storeName: '金科一店',
      pay: '1',
      num: '22',
      a3: '333',
    },
    {
      storeName: '金科一店',
      pay: '1',
      num: '22',
      a3: '333',
    },
    {
      storeName: '金科一店',
      pay: '1',
      num: '22',
      a3: '333',
    },
    {
      storeName: '金科一店',
      pay: '1',
      num: '22',
      a3: '333',
    },
    {
      storeName: '金科二店',
      pay: '1',
      num: '22',
      a3: '333',
    },
    {
      storeName: '金科二店',
      pay: '1',
      num: '22',
      a3: '333',
    },
    {
      storeName: '金科二店',
      pay: '1',
      num: '22',
      a3: '333',
    },
    {
      storeName: '金科二店',
      pay: '1',
      num: '22',
      a3: '333',
    },
    {
      storeName: '金科二店',
      pay: '1',
      num: '22',
      a3: '333',
    },
    {
      storeName: '金科二店',
      pay: '1',
      num: '22',
      a3: '333',
    },
    {
      storeName: '金科二店',
      pay: '1',
      num: '22',
      a3: '333',
    },
  ];
  return mock(pagerResponseWrap(req, data));
};

export default mock;
