import {
  successResponseWrap,
  mock,
  pagerResponseWrap,
} from '@/utils/setup-mock';

export const data = [
  {
    id: '1',
    storeName: '总部',
    type: '直营店',
    org: '直营店',
    s1: 100,
    s2: -100,
    s3: 100,
    s4: 100,
    s5: -100,
    s6: -100,
    s7: 100,
  },
  {
    id: '2',
    storeName: '金科一店',
    type: '直营店',
    org: '直营店',
    s1: 100,
    s2: -100,
    s3: 100,
    s4: 100,
    s5: -100,
    s6: -100,
    s7: 100,
  },
  {
    id: '3',
    storeName: '金科二店',
    type: '加盟店',
    org: 'xx加盟店',
    s1: 100,
    s2: -100,
    s3: 100,
    s4: 100,
    s5: -100,
    s6: -100,
    s7: 100,
  },
];

export const list = (req: any) => {
  return mock(pagerResponseWrap(req, data));
};

export const detail = (id: any) => {
  return mock(successResponseWrap(data.find((item) => item.id === id)));
};

export default mock;
