/**
   * @desc 更新品牌
更新品牌
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.ProductBrandDTO;

export const init = false;

export type ParamsAndBody = { data: defs.ProductBrandDTO };

export function request(
  data: defs.ProductBrandDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/brand/updateBrand`, {
    data,
    method: 'POST',
    ...options,
  });
}
