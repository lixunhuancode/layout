/**
 * @description 二维码相关相关接口
 */
import * as createQRCode from './createQRCode';
import * as createWechatQRCode from './createWechatQRCode';
import * as generateUrl from './generateUrl';
import * as generatescheme from './generatescheme';
import * as checkPoints from './checkPoints';

export {
  createQRCode,
  createWechatQRCode,
  generateUrl,
  generatescheme,
  checkPoints,
};
