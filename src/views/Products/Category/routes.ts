export default [
  {
    path: 'category/list',
    name: 'products.category.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '分类管理',
      requiresAuth: false,
    },
  },
];
