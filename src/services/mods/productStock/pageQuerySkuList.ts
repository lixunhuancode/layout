/**
   * @desc 分页查询商品Sku列表
分页查询商品Sku列表
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryProductDTO;

export const init = new defs.ProductWithSkuVO();

export type ParamsAndBody = { data: defs.QueryProductDTO };

export function request(
  data: defs.QueryProductDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/productStock/pageQuerySkuList`, {
    data,
    method: 'POST',
    ...options,
  });
}
