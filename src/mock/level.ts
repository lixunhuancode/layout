/*
 * @Author: shanshan.hu
 * @FilePath: \liansuobao-admin-html\src\mock\member.ts
 */
import {
  mock,
  pagerResponseWrap,
  successResponseWrap,
} from '@/utils/setup-mock';
import mockjs, { Random } from 'mockjs';

const levelData = mockjs.mock({
  'mock|4': [
    {
      'a1|+1': ['VIP1', 'VIP2', 'VIP3', 'VIP4'],
      'a2|+1': ['#6ea1c5', '#cf9d8c', '#d2c2f5', '#ffce42'],
      'a3|+1': ['普通会员', '白银会员', '黄金会员', '钻石会员'],
      'a4|+1': [0, 1000, 2000, 3000],
      'a5|+1': [0, '是', '是', '是'],
      'a6|+1': [0, 0, '9.6', '9.0'],
      'a7|+1': [0, 0, 0, '1.5'],
      'a8|+1': [0, 10, 50, 100],
      'a9|+1': [0, 0, 1, 2],
      'a10|+1': [0, 0, 1, 2],
      'a11|1': true,
    },
  ],
});

export const getLevelList = (req: any) => {
  return mock(pagerResponseWrap(req, levelData.mock));
};

export const all = () => {
  return mock(successResponseWrap(levelData.mock));
};

export const preferentialCradData = (req: any) => {
  const data = mockjs.mock({
    'mock|5': [
      {
        'a1|1-1000': 20,
        'a2': '会员优惠券',
        'a3|1': ['满减券', '增值券', '兑换券'],
        'a4|1-30': 20,
        'a5|1-200': 20,
        'a6|1-900': 20,
      },
    ],
  });
  return mock(pagerResponseWrap(req, data.mock));
};

export const cardList = mockjs.mock({
  'mock|5': [
    {
      'a1|1-1000': 20,
      'a2': '会员优惠券',
      'a3|1': ['满减券', '增值券', '兑换券'],
      'a4|1-30': 20,
      'a5|1-200': 20,
      'a6|1-900': 20,
    },
  ],
});

export const preferentialCradDataEdit = (req: any) => {
  const data = mockjs.mock({
    'mock|5': [
      {
        'a1|1-1000': 20,
        'a2|1': [
          mockjs.mock('@color()'),
          mockjs.mock('@color()'),
          mockjs.mock('@color()'),
          mockjs.mock('@color()'),
          mockjs.mock('@color()'),
          mockjs.mock('@color()'),
        ],
        'a3|1': ['满减券', '增值券', '兑换券'],
        'a4|1-30': 20,
        'a5|1-200': 20,
        'a6|1-900': 20,
        'a7|1-900': 20,
        'a8|1-900': 20,
        'a9|1-900': 20,
        'a10|1-900': 20,
        'a11|1-900': 20,
      },
    ],
  });
  return mock(pagerResponseWrap(req, data.mock));
};

export const addCardRequest = (req: any) => {
  const data = mockjs.mock({
    'mock|5': [
      {
        'a1|1-1000': 20,
        'a2|1': [
          mockjs.mock('@color()'),
          mockjs.mock('@color()'),
          mockjs.mock('@color()'),
          mockjs.mock('@color()'),
          mockjs.mock('@color()'),
          mockjs.mock('@color()'),
        ],
        'a3|1': ['满减券', '增值券', '兑换券'],
        'a4|1-30': 20,
        'a5|1-200': 20,
        'a6|1-900': 20,
      },
    ],
  });
  return mock(pagerResponseWrap(req, data.mock));
};

export const addColsRequest = (req: any) => {
  const data = mockjs.mock({
    'mock|5': [
      {
        'id|+1': 1,
        'name|+1': ['权益名称1', '权益名称2', '权益名称3', '权益名称4'],
        'remark|+1': ['test1', 'test2', 'test3', 'test4'],
      },
    ],
  });
  return mock(pagerResponseWrap(req, data.mock));
};

export const getGrowUpDetailData = (req: any) => {
  const data = [
    {
      id: 1,
      a1: '2023-05-01 10:30:00',
      a2: '138****1234',
      a3: '小明',
      a4: '消费',
      a5: 50,
      a6: 200,
      a7: '白银会员',
      a8: '加盟1号店',
      a9: '20230501001',
    },
    {
      id: 2,
      a1: '2023-05-02 14:20:00',
      a2: '139*5678',
      a3: '小红',
      a4: '完善资料',
      a5: 10,
      a6: 110,
      a7: '普通会员',
      a8: '直营1号店',
      a9: '20230502001',
    },
    {
      id: 3,
      a1: '2023-05-03 09:45:00',
      a2: '136*9876',
      a3: '小李',
      a4: '消费冲正',
      a5: -80,
      a6: 280,
      a7: '黄金会员',
      a8: '直营2号店',
      a9: '20230503001',
    },
    {
      id: 4,
      a1: '2023-05-04 16:10:00',
      a2: '137*2345',
      a3: '小张',
      a4: '完善资料',
      a5: 10,
      a6: 110,
      a7: '普通会员',
      a8: '直营2号店',
      a9: '20230504001',
    },
    {
      id: 5,
      a1: '2023-05-05 11:20:00',
      a2: '138*5678',
      a3: '小王',
      a4: '消费',
      a5: 100,
      a6: 300,
      a7: '黄金会员',
      a8: '加盟1号店',
      a9: '20230505001',
    },
    {
      id: 6,
      a1: '2023-05-06 13:30:00',
      a2: '139*1234',
      a3: '小刘',
      a4: '完善资料',
      a5: 10,
      a6: 110,
      a7: '普通会员',
      a8: '加盟2号店',
      a9: '20230506001',
    },
    {
      id: 7,
      a1: '2023-05-07 15:40:00',
      a2: '136*5678',
      a3: '小陈',
      a4: '消费',
      a5: 70,
      a6: 270,
      a7: '白银会员',
      a8: '加盟2号店',
      a9: '20230507001',
    },
    {
      id: 8,
      a1: '2023-05-08 12:50:00',
      a2: '137*9876',
      a3: '小赵',
      a4: '完善资料',
      a5: 10,
      a6: 110,
      a7: '普通会员',
      a8: '加盟1号店',
      a9: '20230508001',
    },
    {
      id: 9,
      a1: '2023-05-09 11:00:00',
      a2: '138*2345',
      a3: '小孙',
      a4: '消费',
      a5: 90,
      a6: 290,
      a7: '黄金会员',
      a8: '直营1号店',
      a9: '20230509001',
    },
    {
      id: 10,
      a1: '2023-05-10 14:15:00',
      a2: '139*5678',
      a3: '小周',
      a4: '过期',
      a5: -100,
      a6: 110,
      a7: '普通会员',
      a8: '加盟1号店',
      a9: '20230510001',
    },
  ];
  return mock(pagerResponseWrap(req, data));
};

export default mock;
