/**
   * @desc 小程序注册
小程序注册
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** code */
  code: string;
}

export const init = new defs.UserLoginVO();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`sso/miniLogin/${params.code}`, {
    params,
    method: 'GET',
    ...options,
  });
}
