/**
 * @description 内容管理-首页管理-后台接口
 */
import * as create from './create';
import * as remove from './remove';
import * as detail from './detail';
import * as detailWithItems from './detailWithItems';
import * as list from './list';
import * as saveContent from './saveContent';
import * as setOnline from './setOnline';
import * as update from './update';
import * as usePage from './usePage';

export {
  create,
  remove,
  detail,
  detailWithItems,
  list,
  saveContent,
  setOnline,
  update,
  usePage,
};
