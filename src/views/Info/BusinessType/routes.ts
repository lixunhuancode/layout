export default [
  {
    path: 'business-type/list',
    name: 'info.business-type.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '业务类型',
      requiresAuth: false,
    },
  },
];
