export default [
  {
    path: 'topic/list',
    name: 'cms.topic.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '专题页装修',
      sort: 2,
    },
  },
  {
    path: 'topic/form',
    name: 'cms.topic.add',
    component: () => import('./Form/index.vue'),
    meta: {
      hideInMenu: true,
      locale: '新增专题',
    },
  },
  {
    path: 'topic/form/:id',
    name: 'cms.topic.edit',
    component: () => import('./Form/index.vue'),
    meta: {
      hideInMenu: true,
      locale: '编辑专题',
    },
  },
];
