export default [
  {
    path: 'property/list',
    name: 'products.property.list',
    component: () => import('@/views/Products/Property/List/index.vue'),
    meta: {
      locale: '规格管理',
      requiresAuth: false,
    },
  },
];
