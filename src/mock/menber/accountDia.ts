import {
  successResponseWrap,
  mock,
  pagerResponseWrap,
} from '@/utils/setup-mock';

export const data = [
  {
    a1: '2023-02-14 12:00:00',
    a2: '消费',
    a3: '出账',
    a4: '-1000',
    a5: '10000',
    a6: '订单号：100000',
  },
  {
    a1: '2023-02-14 12:00:00',
    a2: '储值',
    a3: '入账',
    a4: '1000',
    a5: '10000',
    a6: '储值单号：100000',
  },
  {
    a1: '2023-02-14 12:00:00',
    a2: '售后',
    a3: '入账',
    a4: '100',
    a5: '10000',
    a6: '订单号：100000',
  },
  {
    a1: '2023-02-14 12:00:00',
    a2: '消费',
    a3: '出账',
    a4: '-1000',
    a5: '10000',
    a6: '订单号：100000',
  },
  {
    a1: '2023-02-14 12:00:00',
    a2: '消费',
    a3: '出账',
    a4: '-1000',
    a5: '10000',
    a6: '订单号：100000',
  },
  {
    a1: '2023-02-14 12:00:00',
    a2: '消费',
    a3: '出账',
    a4: '-1000',
    a5: '10000',
    a6: '订单号：100000',
  },
  {
    a1: '2023-02-14 12:00:00',
    a2: '消费',
    a3: '出账',
    a4: '-1000',
    a5: '10000',
    a6: '订单号：100000',
  },
  {
    a1: '2023-02-14 12:00:00',
    a2: '消费',
    a3: '出账',
    a4: '-1000',
    a5: '10000',
    a6: '订单号：100000',
  },
  {
    a1: '2023-02-14 12:00:00',
    a2: '消费',
    a3: '出账',
    a4: '-1000',
    a5: '10000',
    a6: '订单号：100000',
  },
  {
    a1: '2023-02-14 12:00:00',
    a2: '消费',
    a3: '出账',
    a4: '-1000',
    a5: '10000',
    a6: '订单号：100000',
  },
  {
    a1: '2023-02-14 12:00:00',
    a2: '消费',
    a3: '出账',
    a4: '-1000',
    a5: '10000',
    a6: '订单号：100000',
  },
  {
    a1: '2023-02-14 12:00:00',
    a2: '消费',
    a3: '出账',
    a4: '-1000',
    a5: '10000',
    a6: '订单号：100000',
  },
];
export const list = (req: any) => {
  return mock(pagerResponseWrap(req, data));
};

const scoreData = [
  {
    a1: '2023-02-14 12:00:00',
    a2: '中秋活动赠送',
    a3: '出账',
    a4: '-1000',
    a5: '10000',
    a6: '后台 admin',
  },
  {
    a1: '2023-02-14 12:00:00',
    a2: '多发扣除冲正',
    a3: '入账',
    a4: '1000',
    a5: '10000',
    a6: '后台 admin',
  },
  {
    a1: '2023-02-14 12:00:00',
    a2: '中秋活动赠送',
    a3: '出账',
    a4: '-1000',
    a5: '10000',
    a6: '后台 admin',
  },
  {
    a1: '2023-02-14 12:00:00',
    a2: '中秋活动赠送',
    a3: '出账',
    a4: '-1000',
    a5: '10000',
    a6: '后台 admin',
  },
];
export const ScoreList = (req: any) => {
  return mock(pagerResponseWrap(req, scoreData));
};

const storeData = [
  {
    a1: '2023-02-14 12:00:00',
    a2: '手动变更',
    a3: '无归属',
    a4: '金科二店',
    a5: '后台 admin',
  },
  {
    a1: '2023-02-14 12:00:00',
    a2: '手动变更',
    a3: '金科二店',
    a4: '金科一店',
    a5: '后台 admin',
  },
  {
    a1: '2023-02-14 12:00:00',
    a2: '用户变更',
    a3: '金科一店',
    a4: '金科二店',
    a5: '前端用户',
  },
];
export const StoreList = (req: any) => {
  return mock(pagerResponseWrap(req, storeData));
};
const growUpData = [
  {
    a1: '2023-02-14 12:00:00',
    a2: '售后',
    a3: '-1000',
    a4: '1000',
    a5: '注册会员',
  },
  {
    a1: '2023-02-14 12:00:00',
    a2: '消费',
    a3: '+1000',
    a4: '1000',
    a5: '注册会员',
  },
  {
    a1: '2023-02-14 12:00:00',
    a2: '完善资料',
    a3: '+1000',
    a4: '1000',
    a5: '注册会员',
  },
  {
    a1: '2023-02-14 12:00:00',
    a2: '销售冲正',
    a3: '-100',
    a4: '1000',
    a5: '注册会员',
  },
];
export const growUpList = (req: any) => {
  return mock(pagerResponseWrap(req, growUpData));
};

export default mock;
