/**
 * @description 后端收款码管理
 */
import * as add from './add';
import * as postChangeStatus from './postChangeStatus';
import * as remove from './remove';
import * as getDetailById from './getDetailById';
import * as getNewPaymentCode from './getNewPaymentCode';
import * as getPageInfo from './getPageInfo';
import * as update from './update';

export {
  add,
  postChangeStatus,
  remove,
  getDetailById,
  getNewPaymentCode,
  getPageInfo,
  update,
};
