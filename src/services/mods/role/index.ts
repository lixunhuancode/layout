/**
 * @description 后端角色管理
 */
import * as add from './add';
import * as remove from './remove';
import * as detail from './detail';
import * as list from './list';
import * as update from './update';

export { add, remove, detail, list, update };
