export default [
  {
    path: 'guide',
    name: 'inventory.guide',
    component: () => import('./index.vue'),
    meta: {
      sort: -1,
      locale: '库存向导',
    },
  },
];
