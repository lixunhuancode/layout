export default [
  {
    path: 'query',
    name: 'inventory.query',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '库存查询',
      sort: 2,
    },
  },
];
