export default [
  {
    path: 'sales-report/statistical',
    name: 'bi.sales-report.statistical',
    component: () => import('./statistical/index.vue'),
    meta: {
      locale: '销售统计',
    },
  },
];
