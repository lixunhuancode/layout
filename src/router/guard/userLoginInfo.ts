import { Router, LocationQueryRaw } from 'vue-router';
import NProgress from 'nprogress'; // progress bar
import { useUserStore } from '@/store';

export default function setupUserLoginInfoGuard(router: Router) {
  // 默认的静态路由
  const staticRouterName = [
    'dashboard',
    'dashboard.welcome',
    'login',
    'redirectWrapper',
    'notFound',
  ];
  router.beforeEach(async (to, from, next) => {
    NProgress.start();

    if (to.path === '/') {
      next({ path: '/dashboard/welcome' });
      return;
    }

    if (to.name === 'login') {
      // 清空路由
      router.getRoutes().forEach((route) => {
        router.removeRoute(
          (!staticRouterName.includes(route.name as string)
            ? route.name
            : null) as string
        );
      });
      next();
      return;
    }

    const userStore = useUserStore();

    if (userStore.isLogin) {
      // console.log({ to });
      // router.push({ name: 'notFound' });
      next();
      return;
    }
    try {
      await userStore.info();
      next();
    } catch (error) {
      await userStore.logout();
      next({
        name: 'login',
        query: {
          redirect: to.fullPath,
          ...to.query,
        } as LocationQueryRaw,
      });
    }
  });
}
