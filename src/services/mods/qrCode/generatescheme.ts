/**
 * @desc 获取短信跳转scheme码
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.GetchemeDTO;

export const init = '';

export type ParamsAndBody = { data: defs.GetchemeDTO };

export function request(data: defs.GetchemeDTO, options: AxiosRequestConfig) {
  return axiosHandle(`qrCode/generatescheme`, {
    data,
    method: 'POST',
    ...options,
  });
}
