/**
 * @desc 添加
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.BaseRechargeReasonDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.BaseRechargeReasonDTO };

export function request(
  data: defs.BaseRechargeReasonDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/rechargeReason/add`, {
    data,
    method: 'POST',
    ...options,
  });
}
