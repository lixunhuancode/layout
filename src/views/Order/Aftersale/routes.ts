export default [
  {
    path: 'aftersale/list',
    name: 'order.aftersale.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '售后管理',
      requiresAuth: false,
    },
  },
  {
    path: 'aftersale/detail/:id',
    name: 'order.aftersale.detail',
    component: () => import('./Detail/index.vue'),
    meta: {
      locale: '售后详情',
      hideInMenu: true,
    },
  },
];
