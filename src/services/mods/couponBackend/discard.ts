/**
 * @desc 废弃
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.DiscardDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.DiscardDTO };

export function request(data: defs.DiscardDTO, options: AxiosRequestConfig) {
  return axiosHandle(`admin/coupon/discard`, {
    data,
    method: 'POST',
    ...options,
  });
}
