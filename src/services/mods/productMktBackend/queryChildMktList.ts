/**
   * @desc 查询下级营销分类列表
查询下级营销分类列表
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** parentMktCode */
  parentMktCode: string;
  /** 状态 1-启用 0-禁用 默认查启用 */
  status?: number;
}

export const init = new defs.ProductMktVO();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(
    `backend/productMkt/queryChildMktList/${params.parentMktCode}`,
    {
      params,
      method: 'GET',
      ...options,
    },
  );
}
