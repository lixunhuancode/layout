export interface DictOption {
  label: string;
  value: number | string;
  [key: string]: any;
}
export interface Dict {
  code: string;
  name: string;
  data: DictOption[];
  valMap: {
    [key: string]: DictOption;
  };
  loading?: boolean;
}

export interface DictMap {
  [key: string]: Dict;
}

export interface DictState {
  dict: {
    [key: string]: Dict;
  };
  codes: string[];
}
