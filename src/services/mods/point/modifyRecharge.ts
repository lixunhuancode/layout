/**
 * @desc 积点充值修改
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.ModifyRechargeBatchDTO;

export const init = false;

export type ParamsAndBody = { data: defs.ModifyRechargeBatchDTO };

export function request(
  data: defs.ModifyRechargeBatchDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`points/modifyRecharge`, {
    data,
    method: 'POST',
    ...options,
  });
}
