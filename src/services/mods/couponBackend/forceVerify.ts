/**
 * @desc 强制核销优惠券
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.CouponVerifyDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.CouponVerifyDTO };

export function request(
  data: defs.CouponVerifyDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`admin/coupon/forceVerify`, {
    data,
    method: 'POST',
    ...options,
  });
}
