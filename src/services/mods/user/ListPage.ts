/**
 * @desc 积分变动记录
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.ModifyUserPhoneDTO;

export const init = false;

export type ParamsAndBody = { data: defs.ModifyUserPhoneDTO };

export function request(
  data: defs.ModifyUserPhoneDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`webapi/user/changePhone`, {
    data,
    method: 'POST',
    ...options,
  });
}
