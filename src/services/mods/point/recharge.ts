/**
 * @desc 积点充值
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.CreateRechargeBatchDTO;

export const init = false;

export type ParamsAndBody = { data: defs.CreateRechargeBatchDTO };

export function request(
  data: defs.CreateRechargeBatchDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`points/recharge`, {
    data,
    method: 'POST',
    ...options,
  });
}
