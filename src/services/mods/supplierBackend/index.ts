/**
 * @description 后端供应商管理
 */
import * as add from './add';
import * as queryAllList from './queryAllList';
import * as queryAuthList from './queryAuthList';
import * as postChangeStatus from './postChangeStatus';
import * as remove from './remove';
import * as getDetailById from './getDetailById';
import * as getEditDetailById from './getEditDetailById';
import * as getPageInfo from './getPageInfo';
import * as getListByIds from './getListByIds';
import * as update from './update';

export {
  add,
  queryAllList,
  queryAuthList,
  postChangeStatus,
  remove,
  getDetailById,
  getEditDetailById,
  getPageInfo,
  getListByIds,
  update,
};
