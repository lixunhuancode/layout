export const WHITE_LIST = [
  { name: 'notFound', children: [] },
  { name: 'login', children: [] },
];

export const NOT_FOUND = {
  name: 'notFound',
};

export const REDIRECT_ROUTE_NAME = 'Redirect';

export const DEFAULT_ROUTE_NAME = 'dashboard.welcome';

export const DEFAULT_ROUTE = {
  title: '欢迎',
  name: DEFAULT_ROUTE_NAME,
  fullPath: '/dashboard/welcome',
};
