/**
 * @desc 获取关键词列表
 */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = new defs.ResponseModel();

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`front/cms/searchKeyword/list`, {
    method: 'GET',
    ...options,
  });
}
