/**
 * @desc 获取车型列表（分页）
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryCarTypeDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.QueryCarTypeDTO };

export function request(
  data: defs.QueryCarTypeDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/carType/list`, {
    data,
    method: 'POST',
    ...options,
  });
}
