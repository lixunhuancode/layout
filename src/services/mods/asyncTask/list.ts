/**
 * @desc 任务查询
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** taskType, SHIPMENT_TASK:发货任务导出 */
  taskType?:
    | 'SUPPLIER_LIST_EXPORT'
    | 'SUPPLIER_LIST_IMPORT'
    | 'SUPPLIER_PAYMENT_CODE_EXPORT'
    | 'COUPON_LIST_EXPORT'
    | 'COUPON_PUBLISH_LIST_EXPORT'
    | 'PRODUCT_LIST_EXPORT'
    | 'PRODUCT_RECYCLE_LIST_EXPORT'
    | 'PRODUCT_SALES_LIST_EXPORT'
    | 'PRODUCT_SALES_DETAILS_LIST_EXPORT'
    | 'STOCK_RECORDS_LIST_EXPORT'
    | 'STOCK_WARN_LIST_EXPORT'
    | 'ORDER_LIST_EXPORT'
    | 'AFTERSALE_LIST_EXPORT'
    | 'SALEDETAIL_LIST_EXPORT'
    | 'SALESTATISTICS_LIST_EXPORT'
    | 'CUSTOMER_LIST_EXPORT'
    | 'CUSTOMER_IMPORT'
    | 'CUSTOMER_RECHARGE_LIST_EXPORT'
    | 'CUSTOMER_RECHARGE_REPORT_EXPORT'
    | 'CUSTOMER_CAR_LIST_EXPORT'
    | 'CUSTOMER_CAR_IMPORT';
}

export const init = new defs.ResponseModel();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`admin/async-task/list`, {
    params,
    method: 'GET',
    ...options,
  });
}
