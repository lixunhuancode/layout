/**
   * @desc 分页查询规格组列表
分页查询规格组列表
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryPropertyDTO;

export const init = new defs.ProductPropertyVO();

export type ParamsAndBody = { data: defs.QueryPropertyDTO };

export function request(
  data: defs.QueryPropertyDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/property/query/page`, {
    data,
    method: 'POST',
    ...options,
  });
}
