/**
   * @desc 销售数量统计
销售数量统计
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryProductSalesDTO;

export const init = new defs.SalesCountVO();

export type ParamsAndBody = { data: defs.QueryProductSalesDTO };

export function request(
  data: defs.QueryProductSalesDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/productSales/salesCount`, {
    data,
    method: 'POST',
    ...options,
  });
}
