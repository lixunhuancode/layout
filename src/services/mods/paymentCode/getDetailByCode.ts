/**
 * @desc 通过code获取详细信息
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** code */
  code: string;
}

export const init = new defs.ResponseModel();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`front/payment-code/detailByCode/${params.code}`, {
    params,
    method: 'GET',
    ...options,
  });
}
