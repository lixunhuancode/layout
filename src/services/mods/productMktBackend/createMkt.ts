/**
   * @desc 创建营销分类
创建营销分类
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.CreateOrUpdateMktDTO;

export const init = '';

export type ParamsAndBody = { data: defs.CreateOrUpdateMktDTO };

export function request(
  data: defs.CreateOrUpdateMktDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/productMkt/createMkt`, {
    data,
    method: 'POST',
    ...options,
  });
}
