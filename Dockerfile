#阶段1 - 打包
FROM 172.30.37.192/public/node:16.14.0-alpine
ADD lst-demo-admin-html.tar.gz /
RUN mkdir -p /node/work/build

#自定义命令 下载打包命令
# RUN  npm install -g cnpm --registry=https://registry.npm.taobao.org \
#    && cnpm install \
#    && npm run build:test \
#    && mv dist /node/work/build/
#RUN echo -e "https://mirror.tuna.tsinghua.edu.cn/alpine/v3.4/main\n\ https://mirror.tuna.tsinghua.edu.cn/alpine/v3.4/community" > /etc/apk/repositories \
#    && apk update \
#    && apk add git python make gcc g++

#pnmp 实现示例
#RUN if ! type pnpm ; then npm install pnpm -g ; fi
#RUN pnpm install \
#   && pnpm run build:test \
#   && mv dist /node/work/build/ \
#   && pnpm run build \
#   && mv dist /node/work/build/online
#node版本正常打包示例
#本地源 如果有问题可以将下面这行注释取消 下下一行增加注释
#RUN npm install \
# RUN npm config set registry http://172.30.37.60:8081/repository/npm-public/  && npm install \
#    && npm run build \
#    && mv dist /node/work/build/ \
#    && npm run build:prod \
#    && mv dist /node/work/build/online

RUN yarn install

RUN npm run build \
   && mv dist /node/work/build/ \
   && npm run build:prod \
   && mv dist /node/work/build/online
#---分割线---
RUN cp -rp /nginx /node/work/build
WORKDIR /node
RUN tar zcf lst-demo-admin-html.tar.gz /node/work


# 阶段2 - 运行时 test
# Dockerfile 基于阶段 1 的应用程序包生成镜像
# 该镜像将在正式环境中运行
# 基础镜像
FROM base/openresty:1.17.8.1
LABEL maintainer="mingzi.xing <mingzi.xing@sjgo365.com>"
# 新建目录
RUN mkdir -p /home/finance/App/foobar.cbestcd.lo/
WORKDIR /home/finance/App/foobar.cbestcd.lo/
# 导入打包好的文件
COPY --from=0 /node/lst-demo-admin-html.tar.gz /home/finance/App/foobar.cbestcd.lo/
# 使 nginx 日志输出在标准输出
RUN tar zxvf lst-demo-admin-html.tar.gz \
    && echo "=================" \
    && mv node/work/* ./ \
    && ls -a /home/finance/App/foobar.cbestcd.lo/ \
    && ln -snf /dev/stdout /home/finance/App/foobar.cbestcd.lo/build/nginx/logs/access.log \
    && ln -snf /dev/stderr /home/finance/App/foobar.cbestcd.lo/build/nginx/logs/error.log
# 启动命令
CMD ["/opt/openresty/nginx/sbin/nginx", "-p", "build/nginx", "-g", "daemon off;"]


