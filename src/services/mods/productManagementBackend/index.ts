/**
 * @description 商品管理后台接口【商品管理】
 */
import * as createProduct from './createProduct';
import * as deleteProduct from './deleteProduct';
import * as pageQueryProduct from './pageQueryProduct';
import * as pageQueryProductLog from './pageQueryProductLog';
import * as pageQuerySkuList from './pageQuerySkuList';
import * as queryProductBySkuIdList from './queryProductBySkuIdList';
import * as queryProductDetails from './queryProductDetails';
import * as queryProductWithDefSkuBySpuId from './queryProductWithDefSkuBySpuId';
import * as syncOnShelvesProduct from './syncOnShelvesProduct';
import * as updateProduct from './updateProduct';

export {
  createProduct,
  deleteProduct,
  pageQueryProduct,
  pageQueryProductLog,
  pageQuerySkuList,
  queryProductBySkuIdList,
  queryProductDetails,
  queryProductWithDefSkuBySpuId,
  syncOnShelvesProduct,
  updateProduct,
};
