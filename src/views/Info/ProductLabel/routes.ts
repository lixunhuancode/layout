export default [
  {
    path: 'product-label/list',
    name: 'info.product-label.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '商品标签',
      requiresAuth: false,
    },
  },
];
