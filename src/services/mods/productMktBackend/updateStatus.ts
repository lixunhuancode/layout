/**
   * @desc 根据Id修改营销分类状态
根据Id修改营销分类状态
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** id */
  id: number;
}

export const init = false;

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`backend/productMkt/updateStatus/${params.id}`, {
    params,
    method: 'POST',
    ...options,
  });
}
