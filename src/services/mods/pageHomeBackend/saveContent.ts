/**
 * @desc 装修页面-首页
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.PageContentDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.PageContentDTO };

export function request(
  data: defs.PageContentDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/cms/page/home/saveContent`, {
    data,
    method: 'POST',
    ...options,
  });
}
