/**
 * @description 后端权限管理
 */
import * as add from './add';
import * as remove from './remove';
import * as list from './list';
import * as tree from './tree';
import * as update from './update';

export { add, remove, list, tree, update };
