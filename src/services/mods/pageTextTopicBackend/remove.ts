/**
 * @desc 删除专题
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.IdLongDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.IdLongDTO };

export function request(data: defs.IdLongDTO, options: AxiosRequestConfig) {
  return axiosHandle(`backend/cms/pageText/topic/delete`, {
    data,
    method: 'POST',
    ...options,
  });
}
