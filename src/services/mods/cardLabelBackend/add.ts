/**
 * @desc 添加
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.BaseCardLabelDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.BaseCardLabelDTO };

export function request(
  data: defs.BaseCardLabelDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/cardLabel/add`, {
    data,
    method: 'POST',
    ...options,
  });
}
