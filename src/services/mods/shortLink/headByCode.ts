/**
 * @desc 跳转到原始地址
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** code */
  code: string;
}

export const init = '';

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`link/${params.code}`, {
    params,
    method: 'HEAD',
    ...options,
  });
}
