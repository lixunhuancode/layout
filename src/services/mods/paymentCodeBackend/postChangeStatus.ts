/**
 * @desc 改变状态
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.ChangeStatusDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.ChangeStatusDTO };

export function request(
  data: defs.ChangeStatusDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/payment-code/changeStatus`, {
    data,
    method: 'POST',
    ...options,
  });
}
