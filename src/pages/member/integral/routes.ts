import recharge from './recharge/routes';
import approval from './approval/routes';
import setting from './setting/routes';
import details from './details/routes';

export default {
  path: 'integral',
  name: 'integral',
  component: () => import('@/components/routeView.vue'),
  meta: {
    locale: '会员积分',
    icon: 'icon-c-info',
    sort: 3,
  },
  children: [...recharge, ...approval, ...setting, ...details],
};
