/**
 * @desc 积点充值详情查看
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** id */
  id: number;
}

export const init = new defs.RechargeDetailVO();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`points/rechargeDetail/${params.id}`, {
    params,
    method: 'POST',
    ...options,
  });
}
