/**
   * @desc 修改该缓存下的全部Sku库存
修改该缓存下的全部Sku库存
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** 缓存Id */
  cacheId?: string;
  /** 库存值 */
  stock?: number;
}

export const init = false;

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(
    `backend/productStock/updateAllSkuCacheStock/${params?.cacheId}/${params?.stock}`,
    {
      params,
      method: 'POST',
      ...options,
    },
  );
}
