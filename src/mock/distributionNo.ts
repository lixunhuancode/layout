import {
  successResponseWrap,
  mock,
  pagerResponseWrap,
} from '@/utils/setup-mock';

export const data = [
  {
    id: 1,
    unitId: 10001,
    unitLabel: '件',
    mainPictureUrl:
      'https://img-mall.sjgo365.com/cbest-mall-center/images/20230403/DtV4OaiPMPkE5bswcpgLVM3x0j2ddsyUGpAjgEdY.png',
    productId: '1651975730308763648',
    productName: '全棉时代纯棉净颜洗脸巾20cm×20cm（3袋装）',
    productCode: '60062773',
    salePrice: 666.66,
    price: 999.99,
    no: 'YH100100101',
    clearingForm: '先货后款',
    shopName: '金科二店',
    shopId: 2,
    shippingTypes: '门店自提',
    marketingChannel2: 10,
    productType: '实物商品',
    sapBrandName: '美的',
    categoryName: '挂机',
    brandName: '美的',
    status: 3,
    operTime: '2023-05-04 16:08:40',
    boxSapCodeColl: null,
    shipperType: '美的',
    stock: 121,
    unitDescription: '台',
    deliveryTypeName: '门店自提',
    createTime: '2023-05-09 12:35:14',
    productResource: '自建',
    type: 1,
  },
  {
    id: 2,
    type: 1,
    unitId: 10002,
    unitLabel: '箱',
    mainPictureUrl:
      'https://img-mall.sjgo365.com/cbest-mall-center/images/20230410/DC6YWyE0RNtbFTs32js2AkZWZdenIX1cfyQgjYyb.png',
    productId: '1651975730308763648',
    productName: 'Mouncamp双人椅MPQ2018（64*37/74*109cm）',
    productCode: '60062773',
    salePrice: 666.66,
    price: 999.99,
    no: 'YH100100102',
    clearingForm: '先货后款',
    shopName: '金科一店',
    shopId: 1,
    shippingTypes: '快递发货',
    marketingChannel2: 10,
    productType: '实物商品',
    sapBrandName: '美的',
    categoryName: '挂机',
    brandName: 'Mouncamp',
    status: 2,
    operTime: '2023-05-04 16:08:40',
    boxSapCodeColl: null,
    shipperType: '美的',
    stock: 121,
    unitDescription: '台',
    deliveryTypeName: '快递发货',
    createTime: '2023-05-09 12:35:14',
    productResource: '自建',
  },
];

export const list = (req: any) => {
  return mock(pagerResponseWrap(req, data));
};

export const all = () => {
  return mock(successResponseWrap(data));
};

export const detail = (id: any) => {
  return mock(successResponseWrap(data.find((item) => item.id === Number(id))));
};

export default mock;
