/**
   * @desc 申请售后
申请售后
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.AftersaleApplyDTO;

export const init = '';

export type ParamsAndBody = { data: defs.AftersaleApplyDTO };

export function request(
  data: defs.AftersaleApplyDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/aftersale/applyAftersale`, {
    data,
    method: 'POST',
    ...options,
  });
}
