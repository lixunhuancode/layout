/**
   * @desc 根据用户id或手机号查询用户信息
根据用户id或手机号查询用户信息
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** userId */
  userId?: number;
  /** phone */
  phone?: string;
}

export const init = new defs.UserInfo();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`webapi/user/getUser`, {
    params,
    method: 'GET',
    ...options,
  });
}
