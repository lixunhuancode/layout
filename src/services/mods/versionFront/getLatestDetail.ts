/**
 * @desc 获取最新版本详细信息
 */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = new defs.ResponseModel();

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`front/version/latest/detail`, {
    method: 'GET',
    ...options,
  });
}
