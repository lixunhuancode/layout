export default [
  {
    path: 'timePrice/list',
    name: 'marketing.timePrice.list',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '限时降价',
    },
  },
  {
    path: 'timePrice/add',
    name: 'marketing.timePrice.add',
    component: () => import('./form/index.vue'),
    meta: {
      locale: '新建活动',
      hideInMenu: true,
    },
  },
  {
    path: 'timePrice/edit',
    name: 'marketing.timePrice.edit',
    component: () => import('./form/index.vue'),
    meta: {
      locale: '编辑活动',
      hideInMenu: true,
    },
  },
  {
    path: 'timePrice/detail',
    name: 'marketing.timePrice.detail',
    component: () => import('./details/index.vue'),
    meta: {
      locale: '活动详情',
      hideInMenu: true,
    },
  },
  {
    path: 'timePrice/statement',
    name: 'marketing.timePrice.statement',
    component: () => import('./statement/index.vue'),
    meta: {
      locale: '统计数据',
      hideInMenu: true,
    },
  },
];
