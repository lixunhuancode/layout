/**
 * @desc 编辑
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.BaseBusinessTypeDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.BaseBusinessTypeDTO };

export function request(
  data: defs.BaseBusinessTypeDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/businessType/update`, {
    data,
    method: 'POST',
    ...options,
  });
}
