export type PreviewHtmlDialogOptions = {
  content: string;
  title?: string;
};
