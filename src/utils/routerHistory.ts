import { ref } from 'vue';
import { uniqBy } from 'lodash';
import { RouteLocationNormalized, RouteRecordName } from 'vue-router';

export type RouteType = {
  icon: string;
  locale: string;
  name: RouteRecordName;
};
const ROUTE_LIST_KEY = 'route.list.key';
const localRouteList = localStorage.getItem(ROUTE_LIST_KEY);
const routeListVal = localRouteList ? JSON.parse(localRouteList) : [];
export const routeList = ref<RouteType[]>(routeListVal);
// 记录最新访问的路由历史
export default function routeHistory(to: RouteLocationNormalized) {
  if (to.path === '/' || !(to.name as string)?.includes('.')) return;
  const { icon, locale } = to.meta;
  if (icon && locale && to.name) {
    const obj: RouteType = {
      icon,
      locale,
      name: to.name,
    };
    routeList.value = uniqBy([obj, ...routeList.value], 'name').slice(0, 6);
    localStorage.setItem(ROUTE_LIST_KEY, JSON.stringify(routeList.value));
  }
}
