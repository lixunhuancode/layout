export default {
  path: 'staffAccount',
  name: 'setting.staffAccount',
  component: () => import('@/components/routeView.vue'),
  meta: {
    locale: '员工账号',
    icon: 'icon-c-info',
    sort: 2,
  },
  children: [
    {
      path: 'account',
      name: 'setting.staffAccount.account',
      component: () => import('./account/list/index.vue'),
      meta: {
        sort: 1,
        locale: '账号管理',
      },
    },
    {
      path: 'account/form',
      name: 'setting.staffAccount.account.form',
      component: () => import('./account/form/index.vue'),
      meta: {
        sort: 1,
        locale: '创建账号',
        hideInMenu: true,
      },
    },
    {
      path: 'account/update/:id',
      name: 'setting.staffAccount.account.update',
      component: () => import('./account/form/index.vue'),
      meta: {
        sort: 1,
        locale: '修改账号',
        hideInMenu: true,
      },
    },
    {
      path: 'role',
      name: 'setting.staffAccount.role',
      component: () => import('./role/list/index.vue'),
      meta: {
        sort: 1,
        locale: '角色管理',
      },
    },
    {
      path: 'role/form',
      name: 'setting.staffAccount.role.form',
      component: () => import('./role/form/index.vue'),
      meta: {
        sort: 1,
        locale: '创建角色',
        hideInMenu: true,
      },
    },
    {
      path: 'role/update/:id',
      name: 'setting.staffAccount.role.update',
      component: () => import('./role/form/index.vue'),
      meta: {
        sort: 1,
        locale: '修改角色',
        hideInMenu: true,
      },
    },
  ],
};
