/**
   * @desc 获取核销记录
获取核销记录
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.PublicDto;

export const init = new defs.OrderDetailInfoVO();

export type ParamsAndBody = { data: defs.PublicDto };

export function request(data: defs.PublicDto, options: AxiosRequestConfig) {
  return axiosHandle(`front/order/getVerifyRecord`, {
    data,
    method: 'POST',
    ...options,
  });
}
