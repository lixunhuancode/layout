import {
  successResponseWrap,
  mock,
  pagerResponseWrap,
} from '@/utils/setup-mock';

export const data = [
  {
    id: 1,
    a1: '2023051401',
    a2: '母亲节活动',
    a3: '/product/index?productId=2023051401',
    a4: '2023-05-14',
    a5: '直营1号店',
    a6: '小明',
    a7: '启用',
  },
  {
    id: 2,
    a1: '2023051302',
    a2: '端午节活动',
    a3: '/product/index?productId=2023051302',
    a4: '2023-05-13',
    a5: '直营2号店',
    a6: '小红',
    a7: '启用',
  },
  {
    id: 3,
    a1: '2023051203',
    a2: '儿童节活动',
    a3: '/product/index?productId=2023051203',
    a4: '2023-05-12',
    a5: '加盟1号店',
    a6: '小刚',
    a7: '启用',
  },
  {
    id: 4,
    a1: '2023051104',
    a2: '父亲节活动',
    a3: '/product/index?productId=2023051104',
    a4: '2023-05-11',
    a5: '加盟2号店',
    a6: '小李',
    a7: '启用',
  },
  {
    id: 5,
    a1: '2023051005',
    a2: '520情人节活动',
    a3: '/product/index?productId=2023051005',
    a4: '2023-05-10',
    a5: '直营1号店',
    a6: '小张',
    a7: '启用',
  },
  {
    id: 6,
    a1: '2023050906',
    a2: '购物节活动',
    a3: '/product/index?productId=2023050906',
    a4: '2023-05-09',
    a5: '直营2号店',
    a6: '小王',
    a7: '启用',
  },
  {
    id: 7,
    a1: '2023050807',
    a2: '51劳动节活动',
    a3: '/product/index?productId=2023050807',
    a4: '2023-04-24',
    a5: '加盟1号店',
    a6: '小刘',
    a7: '启用',
  },
  {
    id: 8,
    a1: '2023050708',
    a2: '清明节活动',
    a3: '/product/index?productId=2023050708',
    a4: '2023-03-17',
    a5: '加盟2号店',
    a6: '小陈',
    a7: '启用',
  },
  {
    id: 9,
    a1: '2023050609',
    a2: '植树节活动',
    a3: '/product/index?productId=2023050609',
    a4: '2023-02-23',
    a5: '直营1号店',
    a6: '小周',
    a7: '禁用',
  },
  {
    id: 10,
    a1: '2023050501',
    a2: '元旦节活动',
    a3: '/product/index?productId=2023050501',
    a4: '2022-12-22',
    a5: '直营2号店',
    a6: '小赵',
    a7: '禁用',
  },
];

export const list = (req: any) => {
  return mock(pagerResponseWrap(req, data));
};

export const all = () => {
  return mock(successResponseWrap(data));
};

export const detail = (id: any) => {
  return mock(successResponseWrap(data.find((item) => item.id === id)));
};

export default mock;
