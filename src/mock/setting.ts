import { pagerResponseWrap, mock } from '@/utils/setup-mock';

export const list = (req: any) => {
  console.log({ req });
  const data = [
    {
      id: 1,
      a1: 'https://img-mall.sjgo365.com/cbest-mall-center/images/20230403/DtV4OaiPMPkE5bswcpgLVM3x0j2ddsyUGpAjgEdY.png',
      a2: '积分倍率',
      a3: '赠送多倍积分',
      a4: '系统判断',
      a5: 1,
    },
    {
      id: 2,
      a1: 'https://img-mall.sjgo365.com/cbest-mall-center/images/20230403/DtV4OaiPMPkE5bswcpgLVM3x0j2ddsyUGpAjgEdY.png',
      a2: '送优惠券',
      a3: '赠送多倍积分',
      a4: '系统判断',
      a5: 0,
    },
  ];
  return mock(pagerResponseWrap(req, data));
};

export default mock;
