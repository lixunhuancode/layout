export default [
  {
    path: 'coupon/list',
    name: 'marketing.coupon.list',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '优惠券',
      sort: 1,
    },
  },
  {
    path: 'coupon/add',
    name: 'marketing.coupon.add',
    component: () => import('./form/index.vue'),
    meta: {
      locale: '新建优惠券',
      hideInMenu: true,
    },
  },
  {
    path: 'coupon/edit',
    name: 'marketing.coupon.edit',
    component: () => import('./form/index.vue'),
    meta: {
      locale: '编辑优惠券',
      hideInMenu: true,
    },
  },
  {
    path: 'coupon/detail',
    name: 'marketing.coupon.detail',
    component: () => import('./details/index.vue'),
    meta: {
      locale: '优惠券详情',
      hideInMenu: true,
    },
  },
  {
    path: 'coupon/statement',
    name: 'marketing.coupon.statement',
    component: () => import('./statement/index.vue'),
    meta: {
      locale: '领券与核销统计',
      hideInMenu: true,
    },
  },
];
