import {
  successResponseWrap,
  mock,
  pagerResponseWrap,
} from '@/utils/setup-mock';

export const data = [
  {
    id: '1',
    name: '张三',
    des: '运营部 / 财务部',
    phone: '18555555555',
    role: '总部-管理员  /  门店-运营',
    status: 1,
  },
  {
    id: '2',
    name: '张三',
    des: '运营部 / 财务部',
    phone: '18555555555',
    role: '总部-管理员  /  门店-运营',
    status: 2,
  },
  {
    id: '3',
    name: '张三',
    des: '运营部 / 财务部',
    phone: '18555555555',
    role: '总部-管理员  /  门店-运营',
    status: 1,
  },
];

export const list = (req: any) => {
  return mock(pagerResponseWrap(req, data));
};

export const all = () => {
  return mock(successResponseWrap(data));
};

export default mock;
