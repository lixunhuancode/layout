export default {
  path: 'organizationManage',
  name: 'setting.organizationManage',
  component: () => import('@/components/routeView.vue'),
  meta: {
    locale: '组织管理',
    icon: 'icon-c-info',
    sort: 2,
  },
  children: [
    {
      path: 'organization',
      name: 'setting.organizationManage.organization',
      component: () => import('./organization/list/index.vue'),
      meta: {
        sort: 1,
        locale: '组织机构',
      },
    },
    {
      path: 'organization/store/add',
      name: 'setting.organizationManage.organization.store.add',
      component: () => import('./organization/form/index.vue'),
      meta: {
        hideInMenu: true,
        locale: '创建线下门店',
      },
    },
    {
      path: 'organization/warehouse/add',
      name: 'setting.organizationManage.organization.warehouse.add',
      component: () => import('./organization/form/warehouseForm.vue'),
      meta: {
        hideInMenu: true,
        locale: '创建仓库',
      },
    },
    {
      path: 'organization/store/detail',
      name: 'setting.organizationManage.organization.store.detail',
      component: () => import('./organization/detail/index.vue'),
      meta: {
        hideInMenu: true,
        locale: '查看线下门店',
      },
    },
    {
      path: 'organization/warehouse/detail',
      name: 'setting.organizationManage.organization.warehouse.detail',
      component: () => import('./organization/detail/warehouseDetail.vue'),
      meta: {
        hideInMenu: true,
        locale: '查看仓库',
      },
    },
    {
      path: 'department',
      name: 'setting.organizationManage.department',
      component: () => import('./department/list/index.vue'),
      meta: {
        sort: 1,
        locale: '部门管理',
      },
    },
    {
      path: 'department/add',
      name: 'setting.organizationManage.department.add',
      component: () => import('./department/form/index.vue'),
      meta: {
        hideInMenu: true,
        locale: '创建部门',
      },
    },
    {
      path: 'department/edit',
      name: 'setting.organizationManage.department.edit',
      component: () => import('./department/form/index.vue'),
      meta: {
        hideInMenu: true,
        locale: '编辑部门',
      },
    },
  ],
};
