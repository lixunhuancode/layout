export default [
  {
    path: 'account/list',
    name: 'setting.account.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '账号管理',
      requiresAuth: false,
    },
  },
];
