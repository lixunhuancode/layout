/**
 * @desc 获取所有有效状态车型品牌
 */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = new defs.ResponseModel();

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`backend/carBrand/all`, {
    method: 'POST',
    ...options,
  });
}
