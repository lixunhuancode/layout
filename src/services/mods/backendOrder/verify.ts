/**
   * @desc 核销订单
核销订单
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.VerifyDTO;

export const init = undefined;

export type ParamsAndBody = { data: defs.VerifyDTO };

export function request(data: defs.VerifyDTO, options: AxiosRequestConfig) {
  return axiosHandle(`backend/order/verify`, {
    data,
    method: 'POST',
    ...options,
  });
}
