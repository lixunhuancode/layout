/**
   * @desc 分页获取售后单列表
分页获取售后单列表
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.AftersaleListQueryDTO;

export const init = new defs.AftersaleDetailInfoVO();

export type ParamsAndBody = { data: defs.AftersaleListQueryDTO };

export function request(
  data: defs.AftersaleListQueryDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/aftersale/getAftersalePagedInfo`, {
    data,
    method: 'POST',
    ...options,
  });
}
