/**
 * @desc 查询权限范围内的所有启用状态供应商列表
 */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = new defs.ResponseModel();

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`backend/supplier/auth/list`, {
    method: 'GET',
    ...options,
  });
}
