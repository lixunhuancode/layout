export default {
  path: 'tagManage',
  name: 'member.tagManage',
  component: () => import('@/components/routeView.vue'),
  meta: {
    locale: '标签管理',
  },
  children: [
    {
      path: 'list',
      name: 'member.tagManage.list',
      component: () => import('./list.vue'),
      meta: {
        locale: '标签管理',
      },
    },
    {
      path: 'form/:id',
      name: 'member.tagManage.form',
      component: () => import('./form.vue'),
      meta: {
        locale: '新增标签',
        hideInMenu: true,
      },
    },
  ],
};
