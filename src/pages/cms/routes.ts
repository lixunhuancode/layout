import { DEFAULT_LAYOUT } from '@/router/routes/base';
import home from './home/routes';
import topic from './topic/routes';
import link from './link/routes';

export default {
  path: '/cms',
  name: 'cms',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '网店',
    requiresAuth: false,
    icon: 'icon-c-shop',
    sort: 6,
  },
  children: [...home, ...topic, ...link],
};
