/**
   * @desc 选择商品设置库存(最大限制1000)
选择商品设置库存(最大限制1000)
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = ObjectMap<any, number>;

export const init = '';

export type ParamsAndBody = { data: ObjectMap<any, number> };

export function request(
  data: ObjectMap<any, number>,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/productStock/chooseSkuSetStock`, {
    data,
    method: 'POST',
    ...options,
  });
}
