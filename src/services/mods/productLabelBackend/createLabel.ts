/**
   * @desc 创建标签
创建标签
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.CreateOrUpdateLabelDTO;

export const init = '';

export type ParamsAndBody = { data: defs.CreateOrUpdateLabelDTO };

export function request(
  data: defs.CreateOrUpdateLabelDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/productLabel/createLabel`, {
    data,
    method: 'POST',
    ...options,
  });
}
