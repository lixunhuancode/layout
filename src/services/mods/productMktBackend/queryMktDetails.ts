/**
   * @desc 根据Id查询营销分类详情
根据Id查询营销分类详情
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** id */
  id: number;
}

export const init = new defs.ProductMktVO();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`backend/productMkt/queryMktDetails/${params.id}`, {
    params,
    method: 'GET',
    ...options,
  });
}
