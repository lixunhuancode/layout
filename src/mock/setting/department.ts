/*
 * @Author: shanshan.hu
 * @FilePath: \liansuobao-admin-html\src\mock\member.ts
 */
import {
  mock,
  pagerResponseWrap,
  successResponseWrap,
} from '@/utils/setup-mock';
import mockjs, { Random } from 'mockjs';

const departmentData = mockjs.mock({
  'mock|22': [
    {
      'storeCode|+1': 'Z0006',
      'name|+1': [
        '23年五一节限时降价活动1',
        '23年五一节限时降价活动2',
        '23年五一节限时降价活动3',
        '23年五一节限时降价活动4',
      ],
      'a2|+1': ['经销', '---'],
      'a3|+1': ['直营', '加盟'],
      'storeName|+1': ['金科一店', '金科二店'],
      'a4|1': ['Y10005'],
      'a5|1': [
        '2023-05-06 22:00:00',
        '2023-05-03 22:00:00',
        '2023-05-04 22:00:00',
      ],
      'storeUser|1': ['zhangsan', 'lisi', 'wangmazi'],
      'phone|1': ['13521629423', '13527482540', '18979012768'],
      'a6|100-1000': 100,
      'a7|+1': [
        '重庆市 重庆市 大渡口区 晋愉盛世融城',
        '重庆市 重庆市 大渡口区 xxxxxxxxxx城',
      ],
      'createUserID|1-10': 1,
      'status|0-1': 0,
    },
  ],
});

export const getActivityList = (req: any) => {
  return mock(pagerResponseWrap(req, departmentData.mock));
};

export const getAllList = () => {
  return mock(successResponseWrap(departmentData.mock));
};

export default mock;
