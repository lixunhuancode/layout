import { pagerResponseWrap, mock } from '@/utils/setup-mock';

export const list = (req: any) => {
  console.log(req);
  let result: any = [];
  const data1 = [
    {
      id: 1,
      a1: '001',
      a2: 1,
      a3: 1,
      a4: 1000,
      a5: '生日礼物',
      a6: '张三',
      a7: '2023-04-05',
    },
    {
      id: 2,
      a1: '002',
      a2: 2,
      a3: 5,
      a4: 5000,
      a5: '集团客户奖励',
      a6: '销售2组',
      a7: '2023-04-10',
    },
    {
      id: 3,
      a1: '003',
      a2: 1,
      a3: 1,
      a4: 2000,
      a5: '客户投诉补偿',
      a6: '客服部门',
      a7: '2023-04-15',
    },
    {
      id: 4,
      a1: '004',
      a2: 1,
      a3: 1,
      a4: 500,
      a5: '促销活动奖励',
      a6: '营销部门',
      a7: '2023-04-20',
    },
    {
      id: 5,
      a1: '005',
      a2: 2,
      a3: 10,
      a4: 10000,
      a5: '新产品推广',
      a6: '研发部门',
      a7: '2023-04-25',
    },
    {
      id: 6,
      a1: '006',
      a2: 1,
      a3: 1,
      a4: 3000,
      a5: '业绩奖励',
      a6: '销售1组',
      a7: '2023-05-01',
    },
    {
      id: 7,
      a1: '007',
      a2: 2,
      a3: 3,
      a4: 3000,
      a5: '5.1活动赠送',
      a6: '运营部门',
      a7: '2023-05-05',
    },
    {
      id: 8,
      a1: '008',
      a2: 1,
      a3: 1,
      a4: 800,
      a5: '客户满意度调查奖励',
      a6: '客服部门',
      a7: '2023-05-10',
    },
    {
      id: 9,
      a1: '009',
      a2: 1,
      a3: 1,
      a4: 1500,
      a5: '产品质量问题补偿',
      a6: '质量部门',
      a7: '2023-05-12',
    },
    {
      id: 10,
      a1: '010',
      a2: 2,
      a3: 8,
      a4: 8000,
      a5: '业务拓展奖励',
      a6: 'BD部门',
      a7: '2023-05-14',
    },
  ];
  const data2 = [
    {
      id: 1,
      a1: '001',
      a2: 1,
      a3: 1,
      a4: 1000,
      a5: '生日礼物',
      a6: '张三',
      a7: '2023-04-05 10:30:00',
      a8: '李四',
      a9: '2023-04-06 14:20:00',
    },
    {
      id: 2,
      a1: '002',
      a2: 2,
      a3: 5,
      a4: 5000,
      a5: '团队奖励',
      a6: '王五',
      a7: '2023-04-10 15:45:00',
      a8: '赵六',
      a9: '2023-04-11 09:10:00',
    },
    {
      id: 3,
      a1: '003',
      a2: 1,
      a3: 1,
      a4: 2000,
      a5: '活动奖励',
      a6: '李四',
      a7: '2023-04-15 11:20:00',
      a8: '张三',
      a9: '2023-04-16 13:50:00',
    },
    {
      id: 4,
      a1: '004',
      a2: 1,
      a3: 1,
      a4: 500,
      a5: '补偿积分',
      a6: '赵六',
      a7: '2023-04-20 16:30:00',
      a8: '王五',
      a9: '2023-04-21 10:15:00',
    },
    {
      id: 5,
      a1: '005',
      a2: 2,
      a3: 3,
      a4: 3000,
      a5: '促销活动',
      a6: '张三',
      a7: '2023-04-25 14:50:00',
      a8: '李四',
      a9: '2023-04-26 11:40:00',
    },
    {
      id: 6,
      a1: '006',
      a2: 1,
      a3: 1,
      a4: 1000,
      a5: '生日礼物',
      a6: '王五',
      a7: '2023-05-01 09:00:00',
      a8: '赵六',
      a9: '2023-05-02 12:30:00',
    },
    {
      id: 7,
      a1: '007',
      a2: 2,
      a3: 4,
      a4: 4000,
      a5: '团队奖励',
      a6: '李四',
      a7: '2023-05-05 13:15:00',
      a8: '张三',
      a9: '2023-05-06 15:20:00',
    },
    {
      id: 8,
      a1: '008',
      a2: 1,
      a3: 1,
      a4: 800,
      a5: '活动奖励',
      a6: '赵六',
      a7: '2023-05-10 10:45:00',
      a8: '王五',
      a9: '2023-05-11 14:10:00',
    },
    {
      id: 9,
      a1: '009',
      a2: 1,
      a3: 1,
      a4: 200,
      a5: '补偿积分',
      a6: '张三',
      a7: '2023-05-12 16:20:00',
      a8: '李四',
      a9: '2023-05-13 09:50:00',
    },
    {
      id: 10,
      a1: '010',
      a2: 2,
      a3: 2,
      a4: 2000,
      a5: '促销活动',
      a6: '王五',
      a7: '2023-05-14 11:30:00',
      a8: '赵六',
      a9: '2023-05-14 15:40:00',
    },
  ];
  const data3 = [
    {
      id: 1,
      a1: '001',
      a2: 1,
      a3: 1,
      a4: 100,
      a5: '生日礼物',
      a6: '张三',
      a7: '2023-04-02',
      a8: '李四',
      a9: '2023-04-03',
    },
    {
      id: 2,
      a1: '002',
      a2: 2,
      a3: 5,
      a4: 500,
      a5: '团队奖励',
      a6: '王五',
      a7: '2023-04-05',
      a8: '赵六',
      a9: '2023-04-07',
    },
    {
      id: 3,
      a1: '003',
      a2: 1,
      a3: 1,
      a4: 50,
      a5: '客户投诉',
      a6: '李四',
      a7: '2023-04-08',
      a8: '张三',
      a9: '2023-04-09',
    },
    {
      id: 4,
      a1: '004',
      a2: 1,
      a3: 1,
      a4: 200,
      a5: '产品推广',
      a6: '赵六',
      a7: '2023-04-10',
      a8: '王五',
      a9: '2023-04-11',
    },
    {
      id: 5,
      a1: '005',
      a2: 2,
      a3: 3,
      a4: 300,
      a5: '团队奖励',
      a6: '张三',
      a7: '2023-04-15',
      a8: '李四',
      a9: '2023-04-16',
    },
    {
      id: 6,
      a1: '006',
      a2: 1,
      a3: 1,
      a4: 100,
      a5: '生日礼物',
      a6: '王五',
      a7: '2023-04-20',
      a8: '赵六',
      a9: '2023-04-21',
    },
    {
      id: 7,
      a1: '007',
      a2: 1,
      a3: 1,
      a4: 50,
      a5: '客户投诉',
      a6: '李四',
      a7: '2023-04-25',
      a8: '张三',
      a9: '2023-04-26',
    },
    {
      id: 8,
      a1: '008',
      a2: 2,
      a3: 2,
      a4: 200,
      a5: '团队奖励',
      a6: '赵六',
      a7: '2023-05-01',
      a8: '王五',
      a9: '2023-05-02',
    },
    {
      id: 9,
      a1: '009',
      a2: 1,
      a3: 1,
      a4: 100,
      a5: '产品推广',
      a6: '张三',
      a7: '2023-05-05',
      a8: '李四',
      a9: '2023-05-06',
    },
    {
      id: 10,
      a1: '010',
      a2: 1,
      a3: 1,
      a4: 50,
      a5: '客户投诉',
      a6: '王五',
      a7: '2023-05-10',
      a8: '赵六',
      a9: '2023-05-11',
    },
  ];
  if (req.a11 === 1) {
    result = data1;
  }
  if (req.a11 === 2) {
    result = data2;
  }
  if (req.a11 === 3) {
    result = data3;
  }
  return mock(pagerResponseWrap(req, result));
};

export default mock;
