/**
   * @desc 修改商品
修改商品
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.CreateOrUpdateProductDTO;

export const init = false;

export type ParamsAndBody = { data: defs.CreateOrUpdateProductDTO };

export function request(
  data: defs.CreateOrUpdateProductDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/productManagement/updateProduct`, {
    data,
    method: 'POST',
    ...options,
  });
}
