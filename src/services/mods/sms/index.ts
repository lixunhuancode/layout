/**
 * @description 短信相关接口
 */
import * as verifySmsCode from './verifySmsCode';
import * as sendMsg from './sendMsg';
import * as postVerifyVerifySmsCode from './postVerifyVerifySmsCode';

export { verifySmsCode, sendMsg, postVerifyVerifySmsCode };
