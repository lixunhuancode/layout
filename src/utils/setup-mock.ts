import debug from './env';

export default ({ mock, setup }: { mock?: boolean; setup: () => void }) => {
  if (mock !== false && debug) setup();
};

export const successResponseWrap = (data: unknown) => {
  return {
    data,
    status: 'ok',
    msg: '请求成功',
    code: 'SYS.SUCCESS',
  };
};

export const failResponseWrap = (data: unknown, msg: string, code = 50000) => {
  return {
    data,
    status: 'fail',
    msg,
    code,
  };
};

export const parseBody: any = (req: any) => {
  try {
    return JSON.parse(req.body);
  } catch (e) {
    return {};
  }
};

export const pagerResponseWrap = (req: any, list: any[]) => {
  console.log('list', list);
  const body = { ...req };
  console.log(body);
  console.log(
    list.slice(
      (body.pageIndex - 1) * body.pageSize,
      body.pageIndex * body.pageSize
    )
  );
  const res = {
    pageIndex: body.pageIndex,
    pageSize: body.pageSize,
    recordCount: list.length,
    pageCount: Math.ceil(list.length / body.pageSize),
    result:
      list.slice(
        (body.pageIndex - 1) * body.pageSize,
        body.pageIndex * body.pageSize
      ) || [],
  };

  return successResponseWrap(res);
};

export const mock = (data: any) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(data);
    }, 1000 * Math.random());
  });
};
