/**
 * @desc 添加
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.BaseMemberLabelDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.BaseMemberLabelDTO };

export function request(
  data: defs.BaseMemberLabelDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/memberLabel/add`, {
    data,
    method: 'POST',
    ...options,
  });
}
