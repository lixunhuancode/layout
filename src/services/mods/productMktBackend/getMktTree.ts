/**
   * @desc 获取营销分类树
获取营销分类树
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** status */
  status: number;
}

export const init = new defs.ProductMktVO();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`backend/productMkt/getMktTree/${params.status}`, {
    params,
    method: 'GET',
    ...options,
  });
}
