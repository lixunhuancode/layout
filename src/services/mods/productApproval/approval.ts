/**
   * @desc 批量审核
批量审核
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.ProductApprovalDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.ProductApprovalDTO };

export function request(
  data: defs.ProductApprovalDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/spu/approval`, {
    data,
    method: 'POST',
    ...options,
  });
}
