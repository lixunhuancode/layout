/**
 * @description 微信二维码
 */
import * as createQRCode from './createQRCode';
import * as checkPoints from './checkPoints';

export { createQRCode, checkPoints };
