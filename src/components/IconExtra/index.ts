import { App } from 'vue';
import createSvgComponent from './createSvg';

const modules = import.meta.glob('@/components/IconExtra/icons/icon*.ts', {
  eager: true,
});

function formatModules(_modules: any, result: any) {
  Object.keys(_modules).forEach((key) => {
    const defaultModule = _modules[key].default;
    if (!defaultModule) {
      return;
    }
    const name = key.replace(/^\/.+\/(.+).ts$/, '$1');
    result.push({ name, child: defaultModule });
  });
  return result;
}
export default {
  install(Vue: App) {
    const moduleList = formatModules(modules, []);
    moduleList.forEach((item: any) => {
      const com = createSvgComponent(item.name, item.child);
      Vue.component(item.name, com);
    });
  },
};
