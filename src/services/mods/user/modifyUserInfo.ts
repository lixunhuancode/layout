/**
   * @desc 更新/新增用户信息
更新/新增用户信息
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.ModifyUserDTO;

export const init = false;

export type ParamsAndBody = { data: defs.ModifyUserDTO };

export function request(data: defs.ModifyUserDTO, options: AxiosRequestConfig) {
  return axiosHandle(`webapi/user/modifyUserInfo`, {
    data,
    method: 'POST',
    ...options,
  });
}
