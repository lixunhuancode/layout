/**
 * @desc 获取购物车信息(硬刷，会拿回商品实时信息)
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = Array<string>;

export const init = new defs.CartVO();

export type ParamsAndBody = { data: Array<string> };

export function request(data: Array<string>, options: AxiosRequestConfig) {
  return axiosHandle(`front/cart/getCartByList`, {
    data,
    method: 'POST',
    ...options,
  });
}
