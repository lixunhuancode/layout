/**
   * @desc 根据订单号查询订单详细信息
根据订单号查询订单详细信息
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** orgOrderNo */
  orgOrderNo: string;
}

export const init = new defs.OrderDetailInfoVO();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`front/order/getOrderByOrgOrderNo/${params.orgOrderNo}`, {
    params,
    method: 'GET',
    ...options,
  });
}
