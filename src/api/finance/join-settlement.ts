import axios from 'axios';

export function list(data: any) {
  return axios.post('/api/join-settlement/list', data);
}

export function getOrderList(data: any) {
  return axios.post('/api/join-settlement/getOrderList', data);
}

export default axios;
