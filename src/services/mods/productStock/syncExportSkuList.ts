/**
   * @desc 同步导出商品Sku列表(最多1000条)
同步导出商品Sku列表(最多1000条)
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryProductDTO;

export const init = new defs.StockListVO();

export type ParamsAndBody = { data: defs.QueryProductDTO };

export function request(
  data: defs.QueryProductDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/productStock/syncExportSkuList`, {
    data,
    method: 'POST',
    ...options,
  });
}
