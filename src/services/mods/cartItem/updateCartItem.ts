/**
 * @desc 更新购物车商品sku
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.UpdateCartItemVO;

export const init = undefined;

export type ParamsAndBody = { data: defs.UpdateCartItemVO };

export function request(
  data: defs.UpdateCartItemVO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`front/cartItem/updateCartItem`, {
    data,
    method: 'POST',
    ...options,
  });
}
