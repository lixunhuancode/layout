/**
   * @desc 删除品类
删除品类
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** 品类编码 */
  categoryCode: string;
}

export const init = false;

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`backend/category/deleteCategory/${params.categoryCode}`, {
    params,
    method: 'DELETE',
    ...options,
  });
}
