/**
   * @desc 查询商品详情信息
查询商品详情信息
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** spuId */
  spuId: string;
}

export const init = new defs.ProductDetailsVO();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(
    `backend/productManagement/queryProductDetails/${params.spuId}`,
    {
      params,
      method: 'GET',
      ...options,
    },
  );
}
