export default [
  {
    path: 'details',
    name: 'member.integral.details',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '积分流水',
      sort: 99,
    },
  },
];
