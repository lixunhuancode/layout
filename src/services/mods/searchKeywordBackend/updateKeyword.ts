/**
 * @desc 编辑搜索关键词
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.SearchKeywordDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.SearchKeywordDTO };

export function request(
  data: defs.SearchKeywordDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/cms/searchKeyword/update`, {
    data,
    method: 'POST',
    ...options,
  });
}
