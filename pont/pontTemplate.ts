import { Interface, BaseClass, Property, CodeGenerator } from 'pont-engine';

interface IParameters {
  path: Property[];
  body: Property[];
  query: Property[];
}

export default class MyGenerator extends CodeGenerator {
  // eslint-disable-next-line class-methods-use-this
  getInterfaceContentInDeclaration(inter: Interface) {
    const paramsCode = inter.getParamsCode('Params');
    const bodyParamsCode = inter.getBodyParamsCode();
    const parameters: IParameters = {
      path: [],
      body: [],
      query: [],
    };

    inter.parameters.forEach((item) => {
      if (item.in === 'path') {
        parameters.path.push(item);
      }
      if (item.in === 'body') {
        parameters.body.push(item);
      }
      if (item.in === 'query') {
        parameters.query.push(item);
      }
    });

    const hasParams = parameters.path.length || parameters.query.length;

    if (!hasParams && !parameters.body.length) {
      return `
      export type Response = ${inter.responseType}

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
      `;
    }

    const exportParamsCode = hasParams ? `export ${paramsCode}` : '';

    const paramsAndBody: string[] = [];

    if (hasParams) {
      paramsAndBody.push(`params: Params;`);
    }

    let RequestBody = '';
    if (bodyParamsCode) {
      RequestBody = `export type RequestBody = ${bodyParamsCode};`;
      paramsAndBody.push(`data: ${bodyParamsCode};`);
    }

    let inputParams = '';

    if (hasParams && bodyParamsCode) {
      inputParams = `paramsAndBody:{params: Params; data: ${bodyParamsCode};}`;
    } else if (bodyParamsCode) {
      inputParams = `data:${bodyParamsCode}`;
    } else if (parameters.path.length || parameters.query.length) {
      inputParams = `params:Params`;
    }

    return `
      ${exportParamsCode}

      ${RequestBody}

      export type Response = ${inter.responseType}

      export const init: Response;

      export function request(${inputParams}, options?:AxiosRequestConfig): Promise<Response>;
    `;
  }

  getBaseClassInDeclaration(base: BaseClass) {
    const originProps = base.properties;

    base.properties = base.properties.map((prop) => {
      return new Property({
        ...prop,
        required: false,
      });
    });

    const result = super.getBaseClassInDeclaration(base);
    base.properties = originProps;

    return result;
  }

  getInterfaceContent(inter: Interface) {
    const paramsCode = inter.getParamsCode('Params', this.surrounding);
    const parameters: IParameters = {
      path: [],
      body: [],
      query: [],
    };

    inter.parameters.forEach((item) => {
      if (item.in === 'path') {
        parameters.path.push(item);
      }
      if (item.in === 'body') {
        parameters.body.push(item);
      }
      if (item.in === 'query') {
        parameters.query.push(item);
      }
    });

    // eslint-disable-next-line no-use-before-define
    return createInterfaceContentt(inter, parameters, paramsCode);
  }
}

const createInterfaceContentt = (
  inter: Interface,
  parameters: IParameters,
  paramsCode: string
) => {
  const method = inter.method.toUpperCase();
  const bodyParamsCode = inter.getBodyParamsCode();
  const paramsAndBody: string[] = [];
  const requestOptions: string[] = [];
  let apiPath = inter.path.replace(/^\//, '');

  if (parameters.path.length) {
    parameters.path.forEach((item) => {
      apiPath = apiPath.replace(
        `{${item.name}`,
        `$\{params${item.required ? '' : '?'}.${item.name}`
      );
    });
  }

  let paramsCodeExport = '';
  if (parameters.path.length || parameters.query.length) {
    paramsAndBody.push(`params: Params`);
    requestOptions.push('params');
    paramsCodeExport = `export ${paramsCode}`;
  }

  if (parameters.body.length) {
    paramsAndBody.push(`data: ${bodyParamsCode};`);
    requestOptions.push('data');
  }

  if (!requestOptions.length) {
    return ` /**
    * @desc ${inter.description}
    */

   import { AxiosRequestConfig } from 'axios';
   import axiosHandle from '@/api/request';
   import * as defs from '../../baseClass';

   export const init = ${inter.response.getInitialValue()};

   export function request(options:AxiosRequestConfig) {
      return axiosHandle(\`${apiPath}\`, {
        method: '${method}',
        ...options,
      });
    }`;
  }

  let requestBody = '';
  if (bodyParamsCode) {
    requestBody = `export type RequestBody = ${bodyParamsCode};`;
  }

  let inputParams = '';

  if (requestOptions.length === 2) {
    inputParams = `{params, data}:{params: Params; data: ${bodyParamsCode};}`;
  } else if (bodyParamsCode) {
    inputParams = `data:${bodyParamsCode}`;
  } else if (parameters.path.length || parameters.query.length) {
    inputParams = `params:Params`;
  }

  return `
  /**
   * @desc ${inter.description}
   */
  import { AxiosRequestConfig } from 'axios';
  import axiosHandle from '@/api/request';
  import * as defs from '../../baseClass';

  ${paramsCodeExport}

  ${requestBody}

  export const init = ${inter.response.getInitialValue()};

  export type ParamsAndBody = {${paramsAndBody.join('; ')}};

  export function request(${inputParams}, options:AxiosRequestConfig) {
    return axiosHandle(\`${apiPath}\`, {
      ${requestOptions.join(',')},
      method: '${method}',
      ...options,
    });
  }
 `;
};
