/**
   * @desc 查询库存
查询库存
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = Array<defs.QueryStockDTO>;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: Array<defs.QueryStockDTO> };

export function request(
  data: Array<defs.QueryStockDTO>,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/stock/queryStockList`, {
    data,
    method: 'POST',
    ...options,
  });
}
