export default [
  {
    path: 'search/list',
    name: 'member.stored.search.list',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '储值查询',
    },
  },
  {
    path: 'search/detail/:id',
    name: 'member.stored.search.detail',
    component: () => import('./detail/index.vue'),
    meta: {
      hideInMenu: true,
      locale: '储值详情',
    },
  },
];
