/**
 * @desc 获取会员权益列表
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryMemberBenefitsDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.QueryMemberBenefitsDTO };

export function request(
  data: defs.QueryMemberBenefitsDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/cms/memberBenefits/list`, {
    data,
    method: 'POST',
    ...options,
  });
}
