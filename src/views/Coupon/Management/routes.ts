export default [
  {
    path: 'management/list',
    name: 'coupon.management.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '卡券管理',
      requiresAuth: false,
    },
  },
  {
    path: 'management/detail/:id',
    name: 'coupon.management.detail',
    component: () => import('./Detail/index.vue'),
    meta: {
      locale: '卡券详情',
      requiresAuth: false,
      hideInMenu: true,
    },
  },
];
