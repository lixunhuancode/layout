/**
 * @description 后端字典管理
 */
import * as add from './add';
import * as remove from './remove';
import * as getDetailById from './getDetailById';
import * as getDetailByCodeLabel from './getDetailByCodeLabel';
import * as getImportTemplate from './getImportTemplate';
import * as getPageInfo from './getPageInfo';
import * as getListByCodes from './getListByCodes';
import * as update from './update';

export {
  add,
  remove,
  getDetailById,
  getDetailByCodeLabel,
  getImportTemplate,
  getPageInfo,
  getListByCodes,
  update,
};
