import { defineStore } from 'pinia';
import { setToken, clearToken, getToken } from '@/utils/auth';
import { removeRouteListener } from '@/utils/route-listener';
import * as shop from '@/mock/shop';
import roles, { rolesMap } from '@/mock/role/index';

import useDictionary from '../dictionary/index';
import useAppStore from '../app';
import useTabbarStore from '../tab-bar';

interface UserState {
  userinfo: defs.AdminUserVO;
  role: string;
  supplierList: any[];
  supplier: any;
  isCheckLogin: boolean;
  isLogin: boolean;
  permissions?: (string | undefined)[];
  permissionsKeyMap: { [key: string]: string };
  routeLoad?: boolean;
  roles: any;
  rolesMap: any;
  menus: any;
  routerRefreshKey: any;
}

const useUserStore = defineStore('user', {
  state: (): UserState => ({
    userinfo: {},
    roles,
    role: 'master',
    supplierList: [],
    supplier: {},
    isCheckLogin: false,
    isLogin: false,
    permissions: [...roles[0].resource],
    routeLoad: false,
    permissionsKeyMap: {},
    rolesMap,
    menus: [],
    routerRefreshKey: Math.random(),
  }),

  getters: {
    userInfo(state: UserState): UserState {
      return { ...state };
    },
    supplierId(state: UserState) {
      return state.supplier.id;
    },
  },

  actions: {
    // Set user's information
    setInfo(partial: Partial<defs.AdminUserVO>) {
      this.$patch((state) => {
        if (partial.permissions) {
          const { permissions } = partial;
          const permissionsKeyMap = {};
          //
          state.permissions = (permissions || []).map(
            (item: defs.Permission) => {
              permissionsKeyMap[item.key as string] = item.name;
              return item.key;
            }
          );
          state.permissionsKeyMap = permissionsKeyMap;
          // 创建路由
        }
        state.userinfo = partial;
      });
    },

    // Reset user's information
    resetInfo() {
      this.$patch((state) => {
        state.routeLoad = false;
        this.$reset();
      });
    },

    // 初始化业务系统基础数据
    async info() {
      const dictionary = useDictionary();

      const res = {
        id: '1',
        username: 'admin',
        name: '李小明',
        type: 1,
        remark: 'admin',
        createTime: '2023-05-09 11:49:15',
        updateTime: '2023-05-09 11:49:15',
        createName: '',
        updateName: 'admin',
        dataResources: [],
        status: 1,
        loginAt: '2023-05-09 11:49:16',
        hasAllDataResource: 1,
      };
      this.setInfo(res || {});

      this.$patch((state) => {
        state.supplierList = shop.shopList;
        state.isLogin = true;
      });

      this.$patch((state) => {
        state.supplier = { ...shop.shopList[0] };
      });
    },
    // Login
    async login(loginForm: API.auth.login.RequestBody) {
      try {
        // const res = await API.auth.login.request(loginForm);
        setToken('122222');
      } catch (err) {
        clearToken();
        throw err;
      }
    },
    logoutCallBack() {
      const appStore = useAppStore();
      const tabbarStore = useTabbarStore();
      this.resetInfo();
      clearToken();
      removeRouteListener();
      tabbarStore.resetTabList();
      appStore.clearServerMenu();
    },
    // Logout
    async logout() {
      this.logoutCallBack();
      // try {
      //   if (getToken()) {
      //     await API.auth.logout.request();
      //   }
      // } finally {
      //   this.logoutCallBack();
      // }
    },
  },
});

export default useUserStore;
