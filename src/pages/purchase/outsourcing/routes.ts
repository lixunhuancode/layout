export default {
  path: 'outsourcing',
  name: 'purchase.outsourcing',
  component: () => import('@/components/routeView.vue'),
  meta: {
    locale: '外采管理',
    icon: 'icon-c-info',
    sort: 4,
  },
  children: [
    {
      path: 'receipt/list',
      name: 'purchase.receipt.list',
      component: () => import('./receipt/list/index.vue'),
      meta: {
        sort: 1,
        locale: '采购进货',
      },
    },
    {
      path: 'receipt/form',
      name: 'purchase.receipt.form',
      component: () => import('./receipt/form/index.vue'),
      meta: {
        locale: '新建采购进货单',
        hideInMenu: true,
      },
    },
    {
      path: 'receipt/form/:id',
      name: 'purchase.receipt.update',
      component: () => import('./receipt/form/index.vue'),
      meta: {
        locale: '编辑采购进货单',
        hideInMenu: true,
      },
    },
    {
      path: 'receipt/detail/:id',
      name: 'purchase.receipt.detail',
      component: () => import('./receipt/form/index.vue'),
      meta: {
        locale: '采购进货单详情',
        hideInMenu: true,
      },
    },
    {
      path: 'return/list',
      name: 'purchase.return.list',
      component: () => import('./return/list/index.vue'),
      meta: {
        locale: '采购退货',
        sort: 2,
      },
    },
    {
      path: 'return/form',
      name: 'purchase.return.form',
      component: () => import('./return/form/index.vue'),
      meta: {
        locale: '新建采购退货单',
        hideInMenu: true,
      },
    },
    {
      path: 'return/form/:id',
      name: 'purchase.return.update',
      component: () => import('./return/form/index.vue'),
      meta: {
        locale: '编辑采购退货单',
        hideInMenu: true,
      },
    },
    {
      path: 'return/detail/:id',
      name: 'purchase.return.detail',
      component: () => import('./return/form/index.vue'),
      meta: {
        locale: '采购退货单详情',
        hideInMenu: true,
      },
    },
    {
      path: 'supplier/list',
      name: 'purchase.supplier.list',
      component: () => import('./supplier/list/index.vue'),
      meta: {
        sort: 7,
        locale: '供应商管理',
      },
    },
    {
      path: 'supplier/form',
      name: 'purchase.supplier.form',
      component: () => import('./supplier/form/index.vue'),
      meta: {
        locale: '添加供应商管理',
        hideInMenu: true,
      },
    },
    {
      path: 'supplier/form/:id',
      name: 'purchase.supplier.update',
      component: () => import('./supplier/form/index.vue'),
      meta: {
        locale: '编辑供应商管理',
        hideInMenu: true,
      },
    },
  ],
};
