/**
   * @desc 搜索商品
搜索商品
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.SearchProductDTO;

export const init = new defs.SearchProductVO();

export type ParamsAndBody = { data: defs.SearchProductDTO };

export function request(
  data: defs.SearchProductDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`front/product/searchProduct`, {
    data,
    method: 'POST',
    ...options,
  });
}
