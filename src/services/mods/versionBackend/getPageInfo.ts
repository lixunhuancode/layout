/**
 * @desc 获取版本列表（分页）
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.BaseDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.BaseDTO };

export function request(data: defs.BaseDTO, options: AxiosRequestConfig) {
  return axiosHandle(`backend/version/list`, {
    data,
    method: 'POST',
    ...options,
  });
}
