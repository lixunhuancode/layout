/**
 * @desc 通过code,label获取字典信息
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** code */
  code: string;
  /** label */
  label?: string;
}

export const init = new defs.ResponseModel();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`backend/dict/detailByCodeLabel`, {
    params,
    method: 'GET',
    ...options,
  });
}
