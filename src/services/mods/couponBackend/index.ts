/**
 * @description 优惠券管理
 */
import * as discard from './discard';
import * as forceVerify from './forceVerify';
import * as getDetailByVerifyCode from './getDetailByVerifyCode';
import * as list from './list';
import * as verify from './verify';

export { discard, forceVerify, getDetailByVerifyCode, list, verify };
