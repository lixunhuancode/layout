import { DEFAULT_LAYOUT } from '@/router/routes/base';

export default [
  {
    path: '/dashboard',
    name: 'dashboard',
    component: DEFAULT_LAYOUT,
    meta: {
      icon: 'icon-c-welcome',
      locale: '概览',
      requiresAuth: false,
      sort: -1,
    },
    children: [
      {
        path: 'welcome',
        name: 'dashboard.welcome',
        component: () => import('./index.vue'),
        meta: {
          locale: '仪表盘',
          requiresAuth: false,
        },
      },
    ],
  },
];
