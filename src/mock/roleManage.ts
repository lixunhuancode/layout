import {
  successResponseWrap,
  mock,
  pagerResponseWrap,
} from '@/utils/setup-mock';

export const data = [
  {
    id: '1',
    name: '高级管理员',
    des: '具备XX的管理权限',
    role: '商品【一级菜单】、商品库【二级菜单】',
    num: '5',
    status: 1,
    type: true,
  },
  {
    id: '2',
    name: '客服',
    des: '具备XX的管理权限',
    role: '商品【一级菜单】、商品库【二级菜单】',
    num: '4',
    status: 1,
    type: true,
  },
  {
    id: '3',
    name: '财务',
    des: '具备XX的管理权限',
    role: '商品【一级菜单】、商品库【二级菜单】',
    num: '2',
    status: 2,
    type: true,
  },
  {
    id: '4',
    name: '运营',
    des: '具备XX的管理权限',
    role: '商品【一级菜单】、商品库【二级菜单】',
    num: '2',
    status: 2,
    type: false,
  },
  {
    id: '5',
    name: '普通员工',
    des: '具备XX的管理权限',
    role: '商品【一级菜单】、商品库【二级菜单】',
    num: '15',
    status: 2,
    type: false,
  },
  {
    id: '5',
    name: '收银员',
    des: '具备XX的管理权限',
    role: '商品【一级菜单】、商品库【二级菜单】',
    num: '6',
    status: 2,
    type: true,
  },
];

export const list = (req: any) => {
  return mock(pagerResponseWrap(req, data));
};

export const all = () => {
  return mock(successResponseWrap(data));
};

export default mock;
