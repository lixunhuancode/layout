/**
 * @desc 获取供应商收款码列表（分页）
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryPaymentCodeDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.QueryPaymentCodeDTO };

export function request(
  data: defs.QueryPaymentCodeDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/payment-code/list`, {
    data,
    method: 'POST',
    ...options,
  });
}
