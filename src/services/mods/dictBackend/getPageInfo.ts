/**
 * @desc 获取字典列表（分页）
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryDictDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.QueryDictDTO };

export function request(data: defs.QueryDictDTO, options: AxiosRequestConfig) {
  return axiosHandle(`backend/dict/list`, {
    data,
    method: 'POST',
    ...options,
  });
}
