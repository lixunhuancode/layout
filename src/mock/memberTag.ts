import {
  successResponseWrap,
  mock,
  pagerResponseWrap,
} from '@/utils/setup-mock';

export const data = [
  {
    id: 1,
    a1: '活跃会员',
    a2: 1,
    a3: '282',
    a4: '-',
  },
  {
    id: 2,
    a1: '高价值会员',
    a2: 1,
    a3: '102',
    a4: '-',
  },
  {
    id: 3,
    a1: '低价值会员',
    a2: 1,
    a3: '5',
    a4: '-',
  },
  {
    id: 4,
    a1: '沉默会员',
    a2: 1,
    a3: '36',
    a4: '-',
  },
  {
    id: 5,
    a1: '分享达人',
    a2: 1,
    a3: '22',
    a4: '-',
  },
  {
    id: 6,
    a1: '评论达人',
    a2: 1,
    a3: '14',
    a4: '-',
  },
  {
    id: 7,
    a1: '品牌忠实粉丝',
    a2: 1,
    a3: '5',
    a4: '-',
  },
  {
    id: 8,
    a1: '羊毛党',
    a2: 1,
    a3: '34',
    a4: '-',
  },
  {
    id: 9,
    a1: '精英会员',
    a2: 1,
    a3: '124',
    a4: '-',
  },
  {
    id: 10,
    a1: '优惠敏感会员',
    a2: 1,
    a3: '34',
    a4: '-',
  },
];

export const list = (req: any) => {
  return mock(pagerResponseWrap(req, data));
};

export const all = () => {
  return mock(successResponseWrap(data));
};

export const detail = (id: any) => {
  return mock(successResponseWrap(data.find((item) => item.id === Number(id))));
};

export default mock;
