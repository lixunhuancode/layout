export default [
  {
    path: 'batch',
    name: 'stock.batch',
    component: () => import('./index.vue'),
    meta: {
      locale: '批量管理',
      requiresAuth: false,
      hideInMenu: true,
    },
  },
];
