import {
  successResponseWrap,
  mock,
  pagerResponseWrap,
} from '@/utils/setup-mock';

export const data = [
  {
    id: 'TJ100100101',
    a1: 'TJ100100101',
    a2: 1,
    a3: '商品库商品',
    a4: '建议零售价',
    a5: '金科一店',
    a6: '网店渠道',
    a7: '2023/02/11 12:00',
    a8: '张三',
    a9: '2023/02/11 12:00',
  },
  {
    id: 'TJ100100102',
    a1: 'TJ100100102',
    a2: 2,
    a3: '商品库商品',
    a4: '建议零售价',
    a5: '金科一店',
    a6: '网店渠道',
    a7: '2023/02/11 12:00',
    a8: '张三',
    a9: '2023/02/11 12:00',
  },
  {
    id: 'TJ100100103',
    a1: 'TJ100100103',
    a2: 3,
    a3: '商品库商品',
    a4: '建议零售价',
    a5: '金科一店',
    a6: '网店渠道',
    a7: '2023/02/11 12:00',
    a8: '张三',
    a9: '2023/02/11 12:00',
  },
  {
    id: 'TJ100100104',
    a1: 'TJ100100104',
    a2: 4,
    a3: '商品库商品',
    a4: '建议零售价',
    a5: '金科一店',
    a6: '网店渠道',
    a7: '2023/02/11 12:00',
    a8: '张三',
    a9: '2023/02/11 12:00',
  },
];

export const list = (req: any) => {
  return mock(pagerResponseWrap(req, data));
};

export const all = () => {
  return mock(successResponseWrap(data));
};

export default mock;
