/**
 * @desc 列表
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.CouponPublishBatchQueryDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.CouponPublishBatchQueryDTO };

export function request(
  data: defs.CouponPublishBatchQueryDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`admin/coupon/publishBatch/list`, {
    data,
    method: 'POST',
    ...options,
  });
}
