export default {
  path: 'after-sale',
  name: 'order.after-sale',
  component: () => import('@/components/routeView.vue'),
  meta: {
    locale: '订单售后',
  },
  children: [
    {
      path: 'list',
      name: 'order.after-sale.list',
      component: () => import('./list/index.vue'),
      meta: {
        locale: '售后列表',
      },
    },
    {
      path: 'detail/:id',
      name: 'order.after-sale.detail',
      component: () => import('./detail/index.vue'),
      meta: {
        locale: '售后详情',
        hideInMenu: true,
      },
    },
  ],
};
