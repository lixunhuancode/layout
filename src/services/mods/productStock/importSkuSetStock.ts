/**
    * @desc 导入商品设置库存(最大限制1000)
选择商品设置库存(最大限制1000)
    */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = '';

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`backend/productStock/importSkuSetStock`, {
    method: 'POST',
    ...options,
  });
}
