import { DEFAULT_LAYOUT } from '@/router/routes/base';
import Management from './Management/routes';
import Aftersale from './Aftersale/routes';

export default {
  path: '/order',
  name: 'order',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '订单',
    requiresAuth: false,
    icon: 'icon-c-order',
    order: 6,
  },
  children: [...Management, ...Aftersale],
};
