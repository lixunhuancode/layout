/**
   * @desc 查询品牌信息
查询品牌信息
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** 品牌ID */
  brandId: number;
}

export const init = new defs.ProductBrandVO();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`backend/brand/getBrandById/${params.brandId}`, {
    params,
    method: 'GET',
    ...options,
  });
}
