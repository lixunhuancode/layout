export default [
  {
    path: 'collect/list',
    name: 'integralSettlement.collect.list',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '积分汇总报表',
      requiresAuth: false,
      hideInMenu: true,
    },
  },
];
