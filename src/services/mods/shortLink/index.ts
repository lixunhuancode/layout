/**
 * @description 内容管理-短链接
 */
import * as createShortLink from './createShortLink';
import * as getResult from './getResult';
import * as headResult from './headResult';
import * as postResult from './postResult';
import * as putResult from './putResult';
import * as deleteResult from './deleteResult';
import * as optionsResult from './optionsResult';
import * as patchResult from './patchResult';
import * as redirect from './redirect';
import * as headByCode from './headByCode';
import * as postByCode from './postByCode';
import * as putByCode from './putByCode';
import * as deleteByCode from './deleteByCode';
import * as optionsByCode from './optionsByCode';
import * as patchByCode from './patchByCode';

export {
  createShortLink,
  getResult,
  headResult,
  postResult,
  putResult,
  deleteResult,
  optionsResult,
  patchResult,
  redirect,
  headByCode,
  postByCode,
  putByCode,
  deleteByCode,
  optionsByCode,
  patchByCode,
};
