export default [
  {
    path: 'service-content/list',
    name: 'cms.service-content.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '服务内容',
    },
  },
  {
    path: 'service-content/form',
    name: 'cms.service-content.add',
    component: () => import('./Form/index.vue'),
    meta: {
      hideInMenu: true,
      locale: '新增服务内容',
    },
  },
  {
    path: 'service-content/form/:id',
    name: 'cms.service-content.edit',
    component: () => import('./Form/index.vue'),
    meta: {
      hideInMenu: true,
      locale: '编辑服务内容',
    },
  },
];
