import axios from 'axios';

export function list(data: any) {
  return axios.post('/api/order/list', data);
}

export default axios;
