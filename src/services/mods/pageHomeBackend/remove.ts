/**
 * @desc 删除首页
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.PageIdDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.PageIdDTO };

export function request(data: defs.PageIdDTO, options: AxiosRequestConfig) {
  return axiosHandle(`backend/cms/page/home/delete`, {
    data,
    method: 'POST',
    ...options,
  });
}
