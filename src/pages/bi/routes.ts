import { DEFAULT_LAYOUT } from '@/router/routes/base';
import salesReport from './sales-report/routes';
import salesAnalysis from './sales-analysis/routes';

export default {
  path: '/bi',
  name: 'bi',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '数据',
    icon: 'icon-c-bi',
    sort: 9,
  },
  children: [...salesReport, ...salesAnalysis],
};

// sales-report
