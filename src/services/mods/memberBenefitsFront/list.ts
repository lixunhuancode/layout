/**
 * @desc 获取会员权益
 */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = new defs.ResponseModel();

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`front/cms/memberBenefits/list`, {
    method: 'GET',
    ...options,
  });
}
