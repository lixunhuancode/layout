/**
 * @desc 删除购物车中的商品
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = Array<string>;

export const init = undefined;

export type ParamsAndBody = { data: Array<string> };

export function request(data: Array<string>, options: AxiosRequestConfig) {
  return axiosHandle(`front/cartItem/deleteCartItems`, {
    data,
    method: 'POST',
    ...options,
  });
}
