/**
   * @desc 编辑标签
编辑标签
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.CreateOrUpdateLabelDTO;

export const init = false;

export type ParamsAndBody = { data: defs.CreateOrUpdateLabelDTO };

export function request(
  data: defs.CreateOrUpdateLabelDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/productLabel/updateLabel`, {
    data,
    method: 'POST',
    ...options,
  });
}
