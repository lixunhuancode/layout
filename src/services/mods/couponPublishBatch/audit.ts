/**
 * @desc 审核
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.CouponPublishBatchAuditDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.CouponPublishBatchAuditDTO };

export function request(
  data: defs.CouponPublishBatchAuditDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`admin/coupon/publishBatch/audit`, {
    data,
    method: 'POST',
    ...options,
  });
}
