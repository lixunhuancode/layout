import {
  successResponseWrap,
  mock,
  pagerResponseWrap,
} from '@/utils/setup-mock';

export const data = [
  {
    id: 1,
    a1: '瓶',
    a2: 10000,
    a3: '启用',
  },
  {
    id: 2,
    a1: '件',
    a2: 10001,
    a3: '启用',
  },
  {
    id: 3,
    a1: '箱',
    a2: 10002,
    a3: '启用',
  },
  {
    id: 4,
    a1: '盒',
    a2: 10003,
    a3: '启用',
  },
  {
    id: 5,
    a1: '条',
    a2: 10004,
    a3: '启用',
  },
  {
    id: 6,
    a1: '双',
    a2: 10005,
    a3: '启用',
  },
  {
    id: 7,
    a1: '支',
    a2: 10006,
    a3: '禁用',
  },
  {
    id: 8,
    a1: '斤',
    a2: 10007,
    a3: '启用',
  },
  {
    id: 9,
    a1: '对',
    a2: 10008,
    a3: '启用',
  },
  {
    id: 10,
    a1: '包',
    a2: 10009,
    a3: '禁用',
  },
];

export const list = (req: any) => {
  return mock(pagerResponseWrap(req, data));
};

export const all = () => {
  return mock(successResponseWrap(data));
};

export default mock;
