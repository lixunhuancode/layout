/**
    * @desc 查询可用的标签列表
查询可用的标签列表
    */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = new defs.ProductLabelVO();

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`backend/productLabel/labelList`, {
    method: 'POST',
    ...options,
  });
}
