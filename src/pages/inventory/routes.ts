import { DEFAULT_LAYOUT } from '@/router/routes/base';
import guide from './guide/routes';
import query from './query/routes';
import storeQuery from './storeQuery/routes';
import warning from './warning/routes';
import warehousing from './warehousing/routes';
import check from './check/routes';

export default {
  path: '/inventory',
  name: 'inventory',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '库存',
    icon: 'icon-c-inventory',
    sort: 8,
  },
  children: [...guide, ...query, ...storeQuery, ...warning, warehousing, check],
};
