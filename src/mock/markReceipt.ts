import {
  successResponseWrap,
  mock,
  pagerResponseWrap,
} from '@/utils/setup-mock';

export const data = [
  {
    id: 'BJ0004',
    name: '第三方微信收款',
  },
  {
    id: 'BJ0003',
    name: '第三方支付宝收款',
  },
  {
    id: 'BJ0002',
    name: '银联POS',
  },
  {
    id: 'BJ0001',
    name: '记账',
  },
];

export const list = (req: any) => {
  return mock(pagerResponseWrap(req, data));
};

export const all = () => {
  return mock(successResponseWrap(data));
};

export default mock;
