/**
 * @description 前端-字典管理
 */
import * as getDetailById from './getDetailById';
import * as getDetailByCodeLabel from './getDetailByCodeLabel';
import * as getListByCodes from './getListByCodes';

export { getDetailById, getDetailByCodeLabel, getListByCodes };
