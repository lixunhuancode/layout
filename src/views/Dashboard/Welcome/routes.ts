export default [
  {
    path: 'welcome',
    name: 'dashboard.welcome',
    component: () => import('./index.vue'),
    meta: {
      locale: '欢迎',
      requiresAuth: false,
      hideInMenu: true,
    },
  },
];
