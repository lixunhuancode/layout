export default [
  {
    path: 'brand/list',
    name: 'info.brand.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '品牌管理',
      requiresAuth: false,
    },
  },
];
