import {
  successResponseWrap,
  mock,
  pagerResponseWrap,
} from '@/utils/setup-mock';

export const data = [
  {
    id: '1',
    name: '会员标签1',
    label: '会员标签1',
    value: 1,
  },
  {
    id: '2',
    name: '会员标签2',
    label: '会员标签2',
    value: 2,
  },
  {
    id: '3',
    name: '会员标签3',
    label: '会员标签3',
    value: 3,
  },
];

export const list = (req: any) => {
  return mock(pagerResponseWrap(req, data));
};

export const all = () => {
  return mock(successResponseWrap(data));
};

export default mock;
