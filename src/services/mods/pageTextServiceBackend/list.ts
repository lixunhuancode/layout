/**
 * @desc 获取服务内容列表
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryPageTextDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.QueryPageTextDTO };

export function request(
  data: defs.QueryPageTextDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/cms/pageText/service/list`, {
    data,
    method: 'POST',
    ...options,
  });
}
