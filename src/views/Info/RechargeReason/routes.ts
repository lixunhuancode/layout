export default [
  {
    path: 'recharge-reason/list',
    name: 'info.recharge-reason.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '充值原因',
      requiresAuth: false,
    },
  },
];
