export default [
  {
    path: 'sales-analysis',
    name: 'bi.sales-analysis',
    component: () => import('./index.vue'),
    meta: {
      locale: '销售分析',
    },
  },
];
