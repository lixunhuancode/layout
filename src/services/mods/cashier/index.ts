/**
 * @description 收银台接口
 */
import * as listPayWay from './listPayWay';
import * as pay from './pay';

export { listPayWay, pay };
