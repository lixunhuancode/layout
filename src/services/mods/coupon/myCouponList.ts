/**
 * @desc 列表
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.MyCouponQueryDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.MyCouponQueryDTO };

export function request(
  data: defs.MyCouponQueryDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`coupon/myCouponList`, {
    data,
    method: 'POST',
    ...options,
  });
}
