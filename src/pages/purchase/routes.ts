import { DEFAULT_LAYOUT } from '@/router/routes/base';
import guide from './guide/routes';
import demand from './demand/routes';
import distribution from './distribution/routes';
import outsourcing from './outsourcing/routes';

export default {
  path: '/purchase',
  name: 'purchase',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '采购',
    icon: 'icon-c-purchase',
    sort: 7,
  },
  children: [...guide, demand, distribution, outsourcing],
};
