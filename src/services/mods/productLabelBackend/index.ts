/**
 * @description 商品管理后台接口【商品标签】
 */
import * as createLabel from './createLabel';
import * as deleteLabel from './deleteLabel';
import * as labelList from './labelList';
import * as pageQueryLabel from './pageQueryLabel';
import * as queryLabelDetails from './queryLabelDetails';
import * as updateLabel from './updateLabel';
import * as updateStatus from './updateStatus';

export {
  createLabel,
  deleteLabel,
  labelList,
  pageQueryLabel,
  queryLabelDetails,
  updateLabel,
  updateStatus,
};
