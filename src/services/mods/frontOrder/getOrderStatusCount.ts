/**
    * @desc 获取用户不同状态订单数量
获取用户不同状态订单数量
    */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = new defs.OrderStatusCountVO();

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`front/order/getOrderStatusCount`, {
    method: 'GET',
    ...options,
  });
}
