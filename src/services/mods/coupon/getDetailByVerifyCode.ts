/**
 * @desc 通过核销码获取优惠券详情
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** verifyCode */
  verifyCode: string;
}

export const init = new defs.ResponseModel();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`coupon/getDetailByVerifyCode`, {
    params,
    method: 'POST',
    ...options,
  });
}
