/**
 * @desc 设为当前页面-首页
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.IdLongDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.IdLongDTO };

export function request(data: defs.IdLongDTO, options: AxiosRequestConfig) {
  return axiosHandle(`backend/cms/page/home/usePage`, {
    data,
    method: 'POST',
    ...options,
  });
}
