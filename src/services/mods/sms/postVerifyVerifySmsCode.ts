/**
   * @desc 验证验证码(通用)
1:短信验证码登录,2:绑定会员卡号,3:绑定手机号
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.SmsVerifyDTO;

export const init = false;

export type ParamsAndBody = { data: defs.SmsVerifyDTO };

export function request(data: defs.SmsVerifyDTO, options: AxiosRequestConfig) {
  return axiosHandle(`sms/verify/verifySmsCode`, {
    data,
    method: 'POST',
    ...options,
  });
}
