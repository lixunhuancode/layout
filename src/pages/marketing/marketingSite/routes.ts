export default {
  path: 'marketingSite',
  name: 'marketing.marketingSite',
  component: () => import('./list/index.vue'),
  meta: {
    locale: '营销现场',
    sort: 1,
  },
};
