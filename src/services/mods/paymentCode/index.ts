/**
 * @description 前端-收款码相关
 */
import * as getDetailById from './getDetailById';
import * as getDetailByCode from './getDetailByCode';

export { getDetailById, getDetailByCode };
