/**
   * @desc 通过KEY批量查询规格组列表
通过KEY批量查询规格组列表
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = Array<string>;

export const init = new defs.ProductPropertyVO();

export type ParamsAndBody = { data: Array<string> };

export function request(data: Array<string>, options: AxiosRequestConfig) {
  return axiosHandle(`backend/property/getByKeyBatch`, {
    data,
    method: 'POST',
    ...options,
  });
}
