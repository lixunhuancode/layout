export default [
  {
    path: 'guide',
    name: 'purchase.guide',
    component: () => import('./index.vue'),
    meta: {
      sort: 1,
      locale: '采购向导',
    },
  },
];
