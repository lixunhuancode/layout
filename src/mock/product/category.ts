import {
  successResponseWrap,
  mock,
  pagerResponseWrap,
} from '@/utils/setup-mock';

export const data = [
  {
    id: 10000,
    name: '服饰',
    status: 1,
    children: [
      {
        id: 100001,
        pid: 10000,
        name: '男装',
        status: 1,
        children: [
          { id: 1000011, pid: 100001, name: '上衣', status: 1 },
          { id: 1000012, pid: 100001, name: '裤子', status: 2 },
        ],
      },
      {
        id: 100002,
        pid: 10000,
        name: '女装',
        status: 1,
        children: [
          { id: 1000021, pid: 100002, name: '上衣', status: 1 },
          { id: 1000022, pid: 100002, name: '裤子', status: 1 },
          { id: 1000023, pid: 100002, name: '裙子', status: 1 },
        ],
      },
    ],
  },
  {
    id: 10001,
    name: '日用',
    status: 1,
  },
  {
    id: 10002,
    name: '金银珠宝',
    status: 1,
    children: [
      {
        id: 100021,
        pid: 10002,
        name: '黄金',
        status: 1,
        children: [
          { id: 1000211, pid: 100021, name: '足金首饰', status: 1 },
          { id: 1000212, pid: 100021, name: 'K金首饰', status: 2 },
        ],
      },
      {
        id: 100022,
        pid: 10002,
        name: '铂金',
        status: 1,
        children: [
          { id: 1000221, pid: 100022, name: 'PT950铂金首饰', status: 1 },
          { id: 1000222, pid: 100022, name: 'PT990铂金首饰', status: 1 },
          { id: 1000223, pid: 100022, name: '镶嵌铂金首饰', status: 1 },
        ],
      },
    ],
  },
  {
    id: 10003,
    name: '运动休闲',
    status: 1,
    children: [
      {
        id: 100031,
        pid: 10003,
        name: '运动休闲装',
        status: 1,
      },
      {
        id: 100032,
        pid: 10003,
        name: '户外装',
        status: 1,
        children: [
          { id: 1000321, pid: 100032, name: '上装', status: 1 },
          { id: 1000322, pid: 100032, name: '下装', status: 1 },
          { id: 1000323, pid: 100032, name: '套装', status: 1 },
        ],
      },
    ],
  },
  {
    id: 10004,
    name: '男装',
    status: 1,
    children: [
      {
        id: 100041,
        pid: 10004,
        name: '上装',
        status: 1,
      },
      {
        id: 100042,
        pid: 10004,
        name: '外套',
        status: 1,
      },
      {
        id: 100043,
        pid: 10043,
        name: '下装',
        status: 1,
      },
      {
        id: 100044,
        pid: 10004,
        name: '配饰',
        status: 1,
      },
    ],
  },
  {
    id: 10005,
    name: '童装',
    status: 1,
    children: [
      {
        id: 100051,
        pid: 10005,
        name: '婴幼儿装',
        status: 1,
        children: [
          { id: 1000511, pid: 100051, name: '衬衫', status: 1 },
          { id: 1000512, pid: 100051, name: 'T恤', status: 2 },
        ],
      },
      {
        id: 100052,
        name: '儿童装',
        status: 1,
        children: [
          { id: 1000521, pid: 100052, name: '夹克', status: 1 },
          { id: 1000522, pid: 100052, name: '大衣', status: 1 },
          { id: 1000523, pid: 100052, name: '羽绒服', status: 1 },
        ],
      },
    ],
  },
  {
    id: 10006,
    name: '鞋包皮件',
    status: 1,
    children: [
      {
        id: 100061,
        pid: 10006,
        name: '女鞋',
        status: 1,
        children: [
          { id: 1000611, pid: 100061, name: '靴子', status: 1 },
          { id: 1000612, pid: 100061, name: '单鞋', status: 2 },
        ],
      },
    ],
  },
  {
    id: 10007,
    name: '饮料',
    status: 1,
    children: [
      {
        id: 100071,
        pid: 10007,
        name: '碳酸饮料',
        status: 1,
        children: [
          { id: 1000711, pid: 100071, name: '可乐', status: 1 },
          { id: 1000712, pid: 100071, name: '柠檬', status: 2 },
        ],
      },
    ],
  },
  {
    id: 10008,
    name: '糖果',
    status: 1,
    children: [
      {
        id: 100081,
        pid: 10008,
        name: '硬糖',
        status: 1,
        children: [
          { id: 1000811, pid: 100081, name: '水果硬糖', status: 1 },
          { id: 1000812, pid: 100081, name: '夹心硬糖', status: 2 },
        ],
      },
    ],
  },
  {
    id: 10009,
    name: '食用油',
    status: 1,
    children: [
      {
        id: 100091,
        pid: 10009,
        name: '桶装油',
        status: 1,
        children: [
          { id: 1000911, pid: 100091, name: '菜籽油', status: 1 },
          { id: 1000912, pid: 100091, name: '调和油', status: 2 },
        ],
      },
    ],
  },
];

export const list = (req: any) => {
  return mock(pagerResponseWrap(req, data));
};

export const all = () => {
  return mock(successResponseWrap(data));
};

export default mock;
