/**
 * @desc 获取所有有效状态卡券标签
 */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = new defs.ResponseModel();

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`backend/cardLabel/all`, {
    method: 'POST',
    ...options,
  });
}
