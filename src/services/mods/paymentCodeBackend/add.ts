/**
 * @desc 添加
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.PaymentCodeDTOObject;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.PaymentCodeDTOObject };

export function request(
  data: defs.PaymentCodeDTOObject,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/payment-code/add`, {
    data,
    method: 'POST',
    ...options,
  });
}
