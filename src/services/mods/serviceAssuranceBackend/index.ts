/**
 * @description 内容管理-服务保障-后台接口
 */
import * as create from './create';
import * as remove from './remove';
import * as detail from './detail';
import * as detailByCode from './detailByCode';
import * as list from './list';
import * as queryByCodes from './queryByCodes';
import * as update from './update';

export { create, remove, detail, detailByCode, list, queryByCodes, update };
