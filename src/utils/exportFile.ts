export const exportFileByUrl = (url: string, filename?: string) => {
  const link = document.createElement('a');
  link.style.display = 'none';
  link.href = url;
  if (filename) {
    link.setAttribute('download', filename);
  }
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
};

export default async (api: Promise<any>) => {
  const response = await api;
  const { headers, data } = response;
  const dispositions = headers['content-disposition'].split(';');
  let filename = '';
  dispositions.forEach((item: string) => {
    const str = item.replace(/\s+/g, '');

    if (/^filename=/.test(str)) {
      filename = str.replace(/^filename=/, '');
    }
  });

  if (!filename) {
    throw new Error('导出失败，缺少filename');
  }

  const url = window.URL.createObjectURL(new Blob([data]));
  exportFileByUrl(url, filename);
};
