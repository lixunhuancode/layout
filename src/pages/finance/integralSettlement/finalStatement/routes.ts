export default [
  {
    path: 'final/list',
    name: 'integralSettlement.final.list',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '积分结算',
    },
  },
  {
    path: 'final/form',
    name: 'integralSettlement.final.form',
    component: () => import('./form/index.vue'),
    meta: {
      locale: '创建积分结算单',
      hideInMenu: true,
    },
  },
  {
    path: 'final/detail/:id',
    name: 'integralSettlement.final.detail',
    component: () => import('./detail/index.vue'),
    meta: {
      locale: '积分结算单详情',
      hideInMenu: true,
    },
  },
];
