/**
 * @desc 会员充值列表
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.RechargePageDTO;

export const init = new defs.UserRechargeBatch();

export type ParamsAndBody = { data: defs.RechargePageDTO };

export function request(
  data: defs.RechargePageDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`points/rechargePage`, {
    data,
    method: 'POST',
    ...options,
  });
}
