export default [
  {
    path: 'guide',
    name: 'product.guide',
    component: () => import('./index.vue'),
    meta: {
      locale: '商品引导',
    },
  },
];
