import { DEFAULT_LAYOUT } from '@/router/routes/base';
import payment from './payment/routes';
import markReceipt from './markReceipt/routes';
import pos from './pos/routes';

export default {
  path: '/cashier',
  name: 'cashier',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '收银',
    icon: 'icon-c-order',
    sort: 11,
  },
  children: [...payment, ...markReceipt, ...pos],
};
