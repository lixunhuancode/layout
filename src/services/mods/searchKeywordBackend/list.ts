/**
 * @desc 获取关键词列表
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QuerySearchKeywordDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.QuerySearchKeywordDTO };

export function request(
  data: defs.QuerySearchKeywordDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/cms/searchKeyword/list`, {
    data,
    method: 'POST',
    ...options,
  });
}
