export default [
  {
    path: 'dict/list',
    name: 'setting.dict.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '字典管理',
      requiresAuth: false,
    },
  },
];
