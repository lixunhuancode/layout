/**
 * @description 商品管理后台接口【品牌管理接口】
 */
import * as createBrand from './createBrand';
import * as deleteBrand from './deleteBrand';
import * as getBrandById from './getBrandById';
import * as getBrandList from './getBrandList';
import * as getPageList from './getPageList';
import * as operateBrandStatus from './operateBrandStatus';
import * as updateBrand from './updateBrand';

export {
  createBrand,
  deleteBrand,
  getBrandById,
  getBrandList,
  getPageList,
  operateBrandStatus,
  updateBrand,
};
