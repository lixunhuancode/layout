/**
   * @desc 分页查询批量管理库存列表
分页查询批量管理库存列表
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryBatchManageStockDTO;

export const init = new defs.ProductWithSkuVO();

export type ParamsAndBody = { data: defs.QueryBatchManageStockDTO };

export function request(
  data: defs.QueryBatchManageStockDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/productStock/pageQueryBatchManageStock`, {
    data,
    method: 'POST',
    ...options,
  });
}
