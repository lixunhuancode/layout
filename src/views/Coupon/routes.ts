import { DEFAULT_LAYOUT } from '@/router/routes/base';
import management from './Management/routes';
import grant from './Grant/routes';
import detail from './Detail/routes';

export default {
  path: '/coupon',
  name: 'coupon',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '卡券',
    requiresAuth: false,
    icon: 'icon-c-coupon',
    order: 3,
  },
  children: [...management, ...grant, ...detail],
};
