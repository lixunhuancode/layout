/**
 * @desc 获取服务保障列表
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryServiceAssuranceDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.QueryServiceAssuranceDTO };

export function request(
  data: defs.QueryServiceAssuranceDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/cms/serviceAssurance/list`, {
    data,
    method: 'POST',
    ...options,
  });
}
