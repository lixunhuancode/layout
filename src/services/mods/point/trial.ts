/**
 * @desc 积点交易
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.PointTransInfoDTO;

export const init = new defs.PointTrandVO();

export type ParamsAndBody = { data: defs.PointTransInfoDTO };

export function request(
  data: defs.PointTransInfoDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`points/trial`, {
    data,
    method: 'POST',
    ...options,
  });
}
