/**
 * @desc 添加页面入口推荐(个人中心)
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.PageRecommendDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.PageRecommendDTO };

export function request(
  data: defs.PageRecommendDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/cms/pageRecommend/create`, {
    data,
    method: 'POST',
    ...options,
  });
}
