/**
   * @desc 获取车辆信息
获取车辆信息
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** userId */
  userId: number;
}

export const init = new defs.UserCar();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`webapi/user/carListByUser/${params.userId}`, {
    params,
    method: 'GET',
    ...options,
  });
}
