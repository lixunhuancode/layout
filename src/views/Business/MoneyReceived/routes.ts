export default [
  {
    path: 'money-received/list',
    name: 'business.money-received.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '收款码管理',
      requiresAuth: false,
    },
  },
];
