/**
   * @desc 确认库存
确认库存
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.OrderInfoDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.OrderInfoDTO };

export function request(data: defs.OrderInfoDTO, options: AxiosRequestConfig) {
  return axiosHandle(`backend/stock/confirmStock`, {
    data,
    method: 'POST',
    ...options,
  });
}
