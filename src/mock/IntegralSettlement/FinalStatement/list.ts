import {
  successResponseWrap,
  mock,
  pagerResponseWrap,
} from '@/utils/setup-mock';

export const data = [
  {
    id: '100001101',
    storeName: '总部',
    type: '直营店',
    org: '直营店',
    s1: '2023-02-14-2023-03-14',
    s2: -100,
    s3: 100,
    s4: '已结算',
    s5: '2023-02-14 14:00:00',
    s6: 'admin',
  },
  {
    id: '100001102',
    storeName: '金科一店',
    type: '直营店',
    org: '直营店',
    s1: '2023-02-14-2023-03-14',
    s2: -100,
    s3: 100,
    s4: '已作废',
    s5: '2023-02-14 14:00:00',
    s6: 'admin',
  },
  {
    id: '100001103',
    storeName: '金科二店',
    type: '加盟店',
    org: 'xx加盟店',
    s1: '2023-02-14-2023-03-14',
    s2: -100,
    s3: 100,
    s4: '待结算',
    s5: '2023-02-14 14:00:00',
    s6: 'admin',
  },
];

export const list = (req: any) => {
  return mock(pagerResponseWrap(req, data));
};

export const detail = (id: any) => {
  return mock(successResponseWrap(data.find((item) => item.id === id)));
};

export default mock;
