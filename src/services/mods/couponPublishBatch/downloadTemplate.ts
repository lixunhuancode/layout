/**
 * @desc 模板下载
 */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = undefined;

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`admin/coupon/publishBatch/downloadTemplate`, {
    method: 'GET',
    ...options,
  });
}
