export default [
  {
    path: 'member-label/list',
    name: 'info.member-label.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '会员标签',
      requiresAuth: false,
    },
  },
];
