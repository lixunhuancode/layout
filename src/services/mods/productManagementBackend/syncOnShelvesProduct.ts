/**
   * @desc 手动同步上架商品到ES
手动同步上架商品到ES
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = Array<string>;

export const init = false;

export type ParamsAndBody = { data: Array<string> };

export function request(data: Array<string>, options: AxiosRequestConfig) {
  return axiosHandle(`backend/productManagement/syncOnShelvesProduct`, {
    data,
    method: 'POST',
    ...options,
  });
}
