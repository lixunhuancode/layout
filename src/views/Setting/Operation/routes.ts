export default [
  {
    path: 'operation/list',
    name: 'setting.operation.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '操作日志',
      requiresAuth: false,
    },
  },
];
