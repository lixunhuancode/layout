/**
 * @desc 创建短域名
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = String;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: String };

export function request(data: String, options: AxiosRequestConfig) {
  return axiosHandle(`link/create`, {
    data,
    method: 'GET',
    ...options,
  });
}
