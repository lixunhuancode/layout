import axios from 'axios';
import type { AxiosRequestConfig, AxiosResponse } from 'axios';
import { Message } from '@arco-design/web-vue';
import { getToken } from '@/utils/auth';

export interface HttpResponse<T = unknown> {
  status: number;
  msg: string;
  message: string;
  code: string | number;
  data: T;
}

if (import.meta.env.VITE_API_CONTEXT_PATH) {
  axios.defaults.baseURL = import.meta.env.VITE_API_CONTEXT_PATH;
}

axios.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    // let each request carry token
    // this example using the JWT token
    // Authorization is a custom headers key
    // please modify it according to the actual situation
    const token = getToken();
    if (token) {
      if (!config.headers) {
        config.headers = {};
      }
      config.headers.authorization = `${token}`;
    }
    return config;
  },
  (error) => {
    // do something
    return Promise.reject(error);
  }
);
// add response interceptors
axios.interceptors.response.use(
  (response: AxiosResponse<HttpResponse>) => {
    const res = response.data;
    if (res instanceof Blob) {
      return response;
    }

    console.log({ res });
    if (res.code !== 'SYS.SUCCESS') {
      Message.error({
        content: res.message || res.msg || 'Error',
        duration: 5 * 1000,
      });
      return Promise.reject(new Error(res.message || 'Error'));
    }
    return res;
  },
  (error) => {
    const message =
      error?.response?.data?.message ||
      error.message ||
      error.msg ||
      'Request Error';
    Message.error({
      content: message,
      duration: 5 * 1000,
    });
    return Promise.reject(error);
  }
);
