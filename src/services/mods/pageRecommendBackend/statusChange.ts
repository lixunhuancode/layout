/**
 * @desc 启用禁用页面入口推荐(个人中心)
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.StatusChangeDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.StatusChangeDTO };

export function request(
  data: defs.StatusChangeDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/cms/pageRecommend/statusChange`, {
    data,
    method: 'POST',
    ...options,
  });
}
