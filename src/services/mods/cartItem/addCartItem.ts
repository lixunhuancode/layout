/**
   * @desc 添加商品到购物车（商品详情页用，实时校验库存等信息）
添加商品到购物车
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.CartItemDTO;

export const init = undefined;

export type ParamsAndBody = { data: defs.CartItemDTO };

export function request(data: defs.CartItemDTO, options: AxiosRequestConfig) {
  return axiosHandle(`front/cartItem/addCartItem`, {
    data,
    method: 'POST',
    ...options,
  });
}
