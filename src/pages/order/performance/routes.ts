export default {
  path: 'performance',
  name: 'order.performance',
  component: () => import('@/components/routeView.vue'),
  meta: {
    locale: '订单履约',
  },
  children: [
    {
      path: 'deliver-goods/list',
      name: 'order.deliver-goods.list',
      component: () => import('./deliver-goods/list/index.vue'),
      meta: {
        sort: 1,
        locale: '快递发货',
      },
    },
    {
      path: 'deliver-goods/detail/:id',
      name: 'order.deliver-goods.detail',
      component: () => import('./deliver-goods/detail/index.vue'),
      meta: {
        locale: '订单详情',
        hideInMenu: true,
      },
    },
    {
      path: 'intra-city-service/list',
      name: 'order.intra-city-service.list',
      component: () => import('./intra-city-service/list/index.vue'),
      meta: {
        sort: 2,
        locale: '同城配送',
      },
    },
    {
      path: 'intra-city-service/detail/:id',
      name: 'order.intra-city-service.detail',
      component: () => import('./intra-city-service/detail/index.vue'),
      meta: {
        hideInMenu: true,
        locale: '订单详情',
      },
    },
    {
      path: 'pick-order-store/list',
      name: 'order.pick-order-store.list',
      component: () => import('./pick-order-store/list/index.vue'),
      meta: {
        sort: 3,
        locale: '到店自提',
      },
    },
    {
      path: 'pick-order-store/detail/:id',
      name: 'order.pick-order-store.detail',
      component: () => import('./pick-order-store/detail/index.vue'),
      meta: {
        locale: '订单详情',
        hideInMenu: true,
      },
    },
  ],
};
