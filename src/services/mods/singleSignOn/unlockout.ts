/**
    * @desc 解锁openid并退出登录
解锁openid并退出登录
    */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = false;

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`sso/unlockout`, {
    method: 'GET',
    ...options,
  });
}
