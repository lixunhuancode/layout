import { DEFAULT_LAYOUT } from '@/router/routes/base';
import organizationManage from './organizationManage/routes';
import staffAccount from './staffAccount/routes';

export default {
  path: '/setting',
  name: 'setting',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '设置',
    icon: 'icon-c-purchase',
    sort: 99,
  },
  children: [organizationManage, staffAccount],
};
