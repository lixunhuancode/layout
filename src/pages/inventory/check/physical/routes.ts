export default [
  {
    path: 'physical/list',
    name: 'inventory.physical.list',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '实物盘点',
    },
  },
  {
    path: 'physical/form',
    name: 'inventory.physical.add',
    component: () => import('./form/index.vue'),
    meta: {
      locale: '新建盘点单',
      hideInMenu: true,
    },
  },
  {
    path: 'physical/form/:id',
    name: 'inventory.physical.edit',
    component: () => import('./form/index.vue'),
    meta: {
      locale: '编辑盘点单',
      hideInMenu: true,
    },
  },
  {
    path: 'physical/input',
    name: 'inventory.physical.input',
    component: () => import('./input/index.vue'),
    meta: {
      locale: '实盘录入',
      hideInMenu: true,
    },
  },
  {
    path: 'physical/detail/:id',
    name: 'inventory.physical.detail',
    component: () => import('./detail/index.vue'),
    meta: {
      locale: '查看',
      hideInMenu: true,
    },
  },
];
