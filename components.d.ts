// generated by unplugin-vue-components
// We suggest you to commit this file into source control
// Read more: https://github.com/vuejs/core/pull/3399
import '@vue/runtime-core'

export {}

declare module '@vue/runtime-core' {
  export interface GlobalComponents {
    AAffix: typeof import('@arco-design/web-vue')['Affix']
    AAlert: typeof import('@arco-design/web-vue')['Alert']
    AAvatar: typeof import('@arco-design/web-vue')['Avatar']
    ABreadcrumb: typeof import('@arco-design/web-vue')['Breadcrumb']
    ABreadcrumbItem: typeof import('@arco-design/web-vue')['BreadcrumbItem']
    AButton: typeof import('@arco-design/web-vue')['Button']
    ACard: typeof import('@arco-design/web-vue')['Card']
    ACardMeta: typeof import('@arco-design/web-vue')['CardMeta']
    ACarousel: typeof import('@arco-design/web-vue')['Carousel']
    ACarouselItem: typeof import('@arco-design/web-vue')['CarouselItem']
    ACascader: typeof import('@arco-design/web-vue')['Cascader']
    ACheckbox: typeof import('@arco-design/web-vue')['Checkbox']
    ACheckboxGroup: typeof import('@arco-design/web-vue')['CheckboxGroup']
    ACol: typeof import('@arco-design/web-vue')['Col']
    AConfigProvider: typeof import('@arco-design/web-vue')['ConfigProvider']
    ADatePicker: typeof import('@arco-design/web-vue')['DatePicker']
    ADescriptions: typeof import('@arco-design/web-vue')['Descriptions']
    ADescriptionsItem: typeof import('@arco-design/web-vue')['DescriptionsItem']
    ADivider: typeof import('@arco-design/web-vue')['Divider']
    ADoption: typeof import('@arco-design/web-vue')['Doption']
    ADrawer: typeof import('@arco-design/web-vue')['Drawer']
    ADropdown: typeof import('@arco-design/web-vue')['Dropdown']
    ADropdownButton: typeof import('@arco-design/web-vue')['DropdownButton']
    AEmpty: typeof import('@arco-design/web-vue')['Empty']
    AForm: typeof import('@arco-design/web-vue')['Form']
    AFormItem: typeof import('@arco-design/web-vue')['FormItem']
    AGrid: typeof import('@arco-design/web-vue')['Grid']
    AGridItem: typeof import('@arco-design/web-vue')['GridItem']
    AImage: typeof import('@arco-design/web-vue')['Image']
    AInput: typeof import('@arco-design/web-vue')['Input']
    AInputGroup: typeof import('@arco-design/web-vue')['InputGroup']
    AInputNumber: typeof import('@arco-design/web-vue')['InputNumber']
    AInputPassword: typeof import('@arco-design/web-vue')['InputPassword']
    AInputSearch: typeof import('@arco-design/web-vue')['InputSearch']
    ALayout: typeof import('@arco-design/web-vue')['Layout']
    ALayoutContent: typeof import('@arco-design/web-vue')['LayoutContent']
    ALayoutSider: typeof import('@arco-design/web-vue')['LayoutSider']
    ALink: typeof import('@arco-design/web-vue')['Link']
    AModal: typeof import('@arco-design/web-vue')['Modal']
    AOption: typeof import('@arco-design/web-vue')['Option']
    APagination: typeof import('@arco-design/web-vue')['Pagination']
    APopconfirm: typeof import('@arco-design/web-vue')['Popconfirm']
    APopover: typeof import('@arco-design/web-vue')['Popover']
    ARadio: typeof import('@arco-design/web-vue')['Radio']
    ARadioGroup: typeof import('@arco-design/web-vue')['RadioGroup']
    ARangePicker: typeof import('@arco-design/web-vue')['RangePicker']
    AResult: typeof import('@arco-design/web-vue')['Result']
    ARow: typeof import('@arco-design/web-vue')['Row']
    AScrollbar: typeof import('@arco-design/web-vue')['Scrollbar']
    ASelect: typeof import('@arco-design/web-vue')['Select']
    ASpace: typeof import('@arco-design/web-vue')['Space']
    ASpin: typeof import('@arco-design/web-vue')['Spin']
    AStatistic: typeof import('@arco-design/web-vue')['Statistic']
    AStep: typeof import('@arco-design/web-vue')['Step']
    ASteps: typeof import('@arco-design/web-vue')['Steps']
    ASwitch: typeof import('@arco-design/web-vue')['Switch']
    ATable: typeof import('@arco-design/web-vue')['Table']
    ATableColumn: typeof import('@arco-design/web-vue')['TableColumn']
    ATabPane: typeof import('@arco-design/web-vue')['TabPane']
    ATabs: typeof import('@arco-design/web-vue')['Tabs']
    ATag: typeof import('@arco-design/web-vue')['Tag']
    ATextarea: typeof import('@arco-design/web-vue')['Textarea']
    ATimeline: typeof import('@arco-design/web-vue')['Timeline']
    ATimelineItem: typeof import('@arco-design/web-vue')['TimelineItem']
    ATooltip: typeof import('@arco-design/web-vue')['Tooltip']
    ATree: typeof import('@arco-design/web-vue')['Tree']
    ATypographyParagraph: typeof import('@arco-design/web-vue')['TypographyParagraph']
    ATypographyText: typeof import('@arco-design/web-vue')['TypographyText']
    ATypographyTitle: typeof import('@arco-design/web-vue')['TypographyTitle']
    AUpload: typeof import('@arco-design/web-vue')['Upload']
    RouterLink: typeof import('vue-router')['RouterLink']
    RouterView: typeof import('vue-router')['RouterView']
  }
}
