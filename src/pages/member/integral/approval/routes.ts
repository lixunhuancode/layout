export default [
  {
    path: 'approval/list',
    name: 'member.integral.approval.list',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '积分审批',
    },
  },
  {
    path: 'approval/detail/:id',
    name: 'member.integral.approval.detail',
    component: () => import('./detail/index.vue'),
    meta: {
      locale: '审批详情',
      hideInMenu: true,
    },
  },
];
