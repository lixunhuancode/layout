export default {
  path: 'demand',
  name: 'purchase.demand',
  component: () => import('@/components/routeView.vue'),
  meta: {
    locale: '要货管理',
    icon: 'icon-c-info',
    sort: 2,
  },
  children: [
    {
      path: 'audit',
      name: 'purchase.apply.audit',
      component: () => import('./apply/audit/index.vue'),
      meta: {
        sort: 1,
        locale: '要货审核',
      },
    },
    {
      path: 'apply/list',
      name: 'purchase.apply.list',
      component: () => import('./apply/list/index.vue'),
      meta: {
        sort: 1,
        locale: '要货申请',
      },
    },
    {
      path: 'apply/form',
      name: 'purchase.apply.form',
      component: () => import('./apply/form/index.vue'),
      meta: {
        locale: '新建要货申请单',
        hideInMenu: true,
      },
    },
    {
      path: 'apply/form/:id',
      name: 'purchase.apply.update',
      component: () => import('./apply/form/index.vue'),
      meta: {
        locale: '编辑要货申请单',
        hideInMenu: true,
      },
    },
    {
      path: 'apply/detail/:id',
      name: 'purchase.apply.detail',
      component: () => import('./apply/form/index.vue'),
      meta: {
        locale: '要货申请单详情',
        hideInMenu: true,
      },
    },
    {
      path: 'distribution/list',
      name: 'purchase.distribution.list',
      component: () => import('./distribution/list/index.vue'),
      meta: {
        sort: 2,
        locale: '要货配货',
      },
    },
    {
      path: 'distribution/form',
      name: 'purchase.distribution.form',
      component: () => import('./distribution/form/index.vue'),
      meta: {
        locale: '新建配货管理单',
        hideInMenu: true,
      },
    },
    {
      path: 'distribution/form/:id',
      name: 'purchase.distribution.update',
      component: () => import('./distribution/form/index.vue'),
      meta: {
        locale: '编辑配货管理单',
        hideInMenu: true,
      },
    },
    {
      path: 'distribution/detail/:id',
      name: 'purchase.distribution.detail',
      component: () => import('./distribution/form/index.vue'),
      meta: {
        locale: '配货管理单详情',
        hideInMenu: true,
      },
    },
  ],
};
