import type { ComponentInternalInstance, ComponentPublicInstance } from 'vue';
import { getCurrentInstance } from 'vue';

export default () => {
  const { proxy } = getCurrentInstance() as ComponentInternalInstance;
  return proxy as ComponentPublicInstance;
};
