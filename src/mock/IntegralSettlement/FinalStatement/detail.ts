import {
  successResponseWrap,
  mock,
  pagerResponseWrap,
} from '@/utils/setup-mock';

export const data = [
  {
    id: '100023',
    s1: '用户91020',
    s2: '185****8250',
    s3: '注册会员',
    s4: '后台添加',
    s5: '10',
    s6: '金科一店',
    s7: '2023-02-14 12:00:00',
    s8: '100000',
    s9: '未结算',
    s10: '100',
  },
  {
    id: '100023',
    s1: '用户91020',
    s2: '185****8250',
    s3: '铜卡会员',
    s4: '积分消费',
    s5: '-10',
    s6: '金科一店',
    s7: '2023-02-14 12:00:00',
    s8: '100000',
    s9: '已结算',
    s10: '100',
  },
  {
    id: '100023',
    s1: '用户91020',
    s2: '185****8250',
    s3: '金卡会员',
    s4: '消费获得',
    s5: '10',
    s6: '金科一店',
    s7: '2023-02-14 12:00:00',
    s8: '100000',
    s9: '待结算',
    s10: '100',
  },
];

export const list = (req: any) => {
  return mock(pagerResponseWrap(req, data));
};

export const create = (req: any) => {
  return mock(successResponseWrap(req));
};

export default mock;
