/**
 * @desc 添加
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.AdminUserDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.AdminUserDTO };

export function request(data: defs.AdminUserDTO, options: AxiosRequestConfig) {
  return axiosHandle(`admin/user/add`, {
    data,
    method: 'POST',
    ...options,
  });
}
