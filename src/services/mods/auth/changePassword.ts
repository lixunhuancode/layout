/**
 * @desc 修改密码
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.ChangePasswordDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.ChangePasswordDTO };

export function request(
  data: defs.ChangePasswordDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`admin/auth/changePassword`, {
    data,
    method: 'POST',
    ...options,
  });
}
