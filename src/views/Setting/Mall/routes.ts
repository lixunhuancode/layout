export default [
  {
    path: 'mall/list',
    name: 'setting.mall.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '商城设置',
      requiresAuth: false,
    },
  },
];
