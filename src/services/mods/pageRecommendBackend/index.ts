/**
 * @description 内容管理-个人中心-后台接口
 */
import * as create from './create';
import * as remove from './remove';
import * as list from './list';
import * as statusChange from './statusChange';
import * as update from './update';

export { create, remove, list, statusChange, update };
