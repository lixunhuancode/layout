/**
    * @desc 测试库存预警短信
测试库存预警短信
    */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = false;

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`backend/productStock/test/sendMsg`, {
    method: 'POST',
    ...options,
  });
}
