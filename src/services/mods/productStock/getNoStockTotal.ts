/**
   * @desc 查询无货预警数量
查询无货预警数量
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryProductDTO;

export const init = undefined;

export type ParamsAndBody = { data: defs.QueryProductDTO };

export function request(
  data: defs.QueryProductDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/productStock/getNoStockTotal`, {
    data,
    method: 'POST',
    ...options,
  });
}
