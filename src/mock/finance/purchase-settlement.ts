import { pagerResponseWrap, mock } from '@/utils/setup-mock';

export const list = (req: any) => {
  console.log({ req });
  const data = [
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
  ];
  return mock(pagerResponseWrap(req, data));
};

export default mock;
