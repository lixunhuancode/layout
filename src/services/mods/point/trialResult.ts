/**
 * @desc 积点交易结果查询
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** transNo */
  transNo: string;
}

export const init = new defs.PointTrandVO();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`points/trialResult/${params.transNo}`, {
    params,
    method: 'GET',
    ...options,
  });
}
