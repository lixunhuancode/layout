/**
 * @desc 查询服务保障详情
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** id */
  id: number;
}

export const init = new defs.ResponseModel();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`backend/cms/serviceAssurance/detail/${params.id}`, {
    params,
    method: 'GET',
    ...options,
  });
}
