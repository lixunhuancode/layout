import physical from './physical/routes';
import task from './task/routes';

export default {
  path: 'check',
  name: 'inventory.check',
  component: () => import('@/components/routeView.vue'),
  meta: {
    locale: '库存盘点',
    icon: 'icon-relation',
    sort: 10,
  },
  children: [...physical, ...task],
};
