import { ref } from 'vue';

const MENU_LOCAL_KEY = 'operation.shortcut';
const MENU_LOCAL_KEY_ROUTER = 'operation.shortcut.router';

export type Tree = {
  iconName: string | undefined;
  title: string;
  key: string;
  path: string;
  children: Tree[] | undefined;
};

const localMenuRoute = window.localStorage.getItem(MENU_LOCAL_KEY_ROUTER);
const menuDataRoute = [
  {
    iconName: 'icon-c-q1',
    title: '会员列表',
    key: 'member.management.list',
  },
  {
    iconName: 'icon-c-q6',
    title: '营销现场',
    key: 'marketing.marketingSite',
  },
  {
    iconName: 'icon-c-q3',
    title: '要货配货',
    key: 'purchase.distribution.list',
  },
  {
    iconName: 'icon-c-q4',
    title: '订单列表',
    key: 'order.management.list',
  },
  {
    iconName: 'icon-c-q5',
    title: '销售分析',
    key: 'bi.sales-analysis',
  },
  {
    iconName: 'icon-c-q2',
    title: '进销存报表',
    key: 'finance.statement',
  },
];
const menuRouteList = ref(menuDataRoute as unknown as Tree[]);

export default function useStorage() {
  // 获取快捷菜单数据
  const localMenu = window.localStorage.getItem(MENU_LOCAL_KEY);
  const menuData = localMenu ? JSON.parse(localMenu) : [];
  const menuList = ref(menuData as unknown as string[]);

  const saveMenuList = () => {
    window.localStorage.setItem(MENU_LOCAL_KEY, JSON.stringify(menuList.value));
  };

  const saveRouteMenu = (menuRoute: Tree[]) => {
    const data = menuRoute.filter((item) => item.key.includes('.'));
    window.localStorage.setItem(MENU_LOCAL_KEY_ROUTER, JSON.stringify(data));
    menuRouteList.value = data;
  };

  return {
    menuList,
    menuRouteList,
    saveMenuList,
    saveRouteMenu,
  };
}
