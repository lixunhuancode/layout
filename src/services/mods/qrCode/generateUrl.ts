/**
 * @desc 获取短信跳转url
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.WechatGenerateUrlDTO;

export const init = '';

export type ParamsAndBody = { data: defs.WechatGenerateUrlDTO };

export function request(
  data: defs.WechatGenerateUrlDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`qrCode/generateUrl`, {
    data,
    method: 'POST',
    ...options,
  });
}
