import { DEFAULT_LAYOUT } from '@/router/routes/base';
import equityCard from './setting/routes';

export default {
  path: '/equityCard',
  name: 'equityCard',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '权益',
    icon: 'icon-c-info',
    sort: 11,
    hideInMenu: true,
  },
  children: [equityCard],
};
