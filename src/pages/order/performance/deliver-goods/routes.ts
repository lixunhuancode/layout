export default [
  {
    path: 'deliver-goods/list',
    name: 'order.deliver-goods.list',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '快递发货',
    },
  },
  {
    path: 'deliver-goods/detail/:id',
    name: 'order.deliver-goods.detail',
    component: () => import('./detail/index.vue'),
    meta: {
      locale: '订单详情',
      hideInMenu: true,
    },
  },
];
