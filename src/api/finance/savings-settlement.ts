import axios from 'axios';

export function list(data: any) {
  return axios.post('/api/savings-settlement/list', data);
}

export default axios;
