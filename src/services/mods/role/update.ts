/**
 * @desc 修改
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.RoleDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.RoleDTO };

export function request(data: defs.RoleDTO, options: AxiosRequestConfig) {
  return axiosHandle(`admin/role/update`, {
    data,
    method: 'POST',
    ...options,
  });
}
