import { DEFAULT_LAYOUT } from '@/router/routes/base';
import joinSettlement from './join-settlement/routes';
import savingsSettlement from './savings-settlement/routes';
import purchaseSettlement from './purchase-settlement/routes';
import payment from './payment/routes';
import statement from './statement/routes';
import integralSettlement from './integralSettlement/routes';

export default {
  path: '/finance',
  name: 'finance',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '财务',
    icon: 'icon-c-finance',
    sort: 10,
  },
  children: [
    {
      path: 'settlement',
      name: 'finance.settlement',
      component: () => import('@/components/routeView.vue'),
      meta: {
        locale: '结算管理',
        order: 1,
      },
      children: [
        ...joinSettlement,
        ...savingsSettlement,
        ...purchaseSettlement,
        ...integralSettlement,
      ],
    },
    {
      path: 'report',
      name: 'finance.report',
      component: () => import('@/components/routeView.vue'),
      meta: {
        locale: '财务报表',
        icon: 'icon-book',
        order: 2,
      },
      children: [statement, payment],
    },
  ],
};
