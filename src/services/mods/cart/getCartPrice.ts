/**
 * @desc 获取购物车价格
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = Array<string>;

export const init = new defs.CartPriceVO();

export type ParamsAndBody = { data: Array<string> };

export function request(data: Array<string>, options: AxiosRequestConfig) {
  return axiosHandle(`front/cart/getCartPrice`, {
    data,
    method: 'POST',
    ...options,
  });
}
