export default [
  {
    path: 'warning',
    name: 'inventory.warning',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '库存预警',
      sort: 4,
    },
  },
];
