/**
 * @description 优惠券发放
 */
import * as add from './add';
import * as audit from './audit';
import * as remove from './remove';
import * as detail from './detail';
import * as downloadTemplate from './downloadTemplate';
import * as list from './list';
import * as parseTemplate from './parseTemplate';
import * as update from './update';

export {
  add,
  audit,
  remove,
  detail,
  downloadTemplate,
  list,
  parseTemplate,
  update,
};
