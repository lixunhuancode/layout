import { App } from 'vue';
import { use } from 'echarts/core';
import { CanvasRenderer } from 'echarts/renderers';
import {
  BarChart,
  LineChart,
  PieChart,
  RadarChart,
  FunnelChart,
} from 'echarts/charts';
import {
  GridComponent,
  TooltipComponent,
  LegendComponent,
  DataZoomComponent,
  GraphicComponent,
} from 'echarts/components';
import Chart from './chart/index.vue';
import Breadcrumb from './breadcrumb/index.vue';
import ListLayout from './ListLayout/index.vue';
import ListLayoutSearch from './ListLayout/components/Search/index.vue';
import ListLayoutList from './ListLayout/components/List/index.vue';
import CSelect from './CSelect/index.vue';
import DateRange from './DateRange/index.vue';
import Uploader from './Uploader/index.vue';
import PageWrapper from './PageWrapper/index.vue';
import CEditor from './CEditor/index.vue';
import IconExtra from './IconExtra/index';
import DownloadTemplate from './DownloadTemplate/index.vue';
import BatchImport from './BatchImport/index.vue';
import BatchExport from './BatchExport/index.vue';
import CImage from './CImage/index.vue';
import SelectShop from './SelectShop/index.vue';
import SelectDepartment from './SelectDepartment/index.vue';
import SelectOrganization from './SelectOrganization/index.vue';
import SelectShopOrWarehouse from './SelectShopOrWarehouse/index.vue';
import SelectSupplier from './SelectSupplier/index.vue';
import SelectCategory from './SelectCategory/index.vue';
import SelectBrand from './SelectBrand/index.vue';
import SelectProductLabel from './SelectProductLabel/index.vue';
import SelectMarketingCategory from './SelectMarketingCategory/index.vue';
import TabView from './TabView/index.vue';
import FormLayout from './FormLayout/index.vue';
import FormGroup from './FormLayout/FormGroup/index.vue';
import SelectUnit from './SelectUnit/index.vue';
import TableForm from './TableForm/index.vue';

// Manually introduce ECharts modules to reduce packing size

use([
  CanvasRenderer,
  BarChart,
  LineChart,
  PieChart,
  RadarChart,
  FunnelChart,
  GridComponent,
  TooltipComponent,
  LegendComponent,
  DataZoomComponent,
  GraphicComponent,
]);

export default {
  install(Vue: App) {
    Vue.component('Chart', Chart);
    Vue.component('Breadcrumb', Breadcrumb);
    Vue.component('ListLayout', ListLayout);
    Vue.component('ListLayoutSearch', ListLayoutSearch);
    Vue.component('ListLayoutContent', ListLayoutList);
    Vue.component('CSelect', CSelect);
    Vue.component('DateRange', DateRange);
    Vue.component('Uploader', Uploader);
    Vue.component('PageWrapper', PageWrapper);
    Vue.component('CEditor', CEditor);
    Vue.component('DownloadTemplate', DownloadTemplate);
    Vue.component('BatchImport', BatchImport);
    Vue.component('BatchExport', BatchExport);
    Vue.component('CImage', CImage);
    Vue.component('SelectShop', SelectShop);
    Vue.component('SelectDepartment', SelectDepartment);
    Vue.component('SelectOrganization', SelectOrganization);
    Vue.component('SelectShopOrWarehouse', SelectShopOrWarehouse);
    Vue.component('SelectSupplier', SelectSupplier);
    Vue.component('SelectCategory', SelectCategory);
    Vue.component('SelectBrand', SelectBrand);
    Vue.component('SelectProductLabel', SelectProductLabel);
    Vue.component('SelectMarketingCategory', SelectMarketingCategory);
    Vue.component('SelectUnit', SelectUnit);
    Vue.component('TabView', TabView);
    Vue.component('FormLayout', FormLayout);
    Vue.component('FormGroup', FormGroup);
    Vue.component('TableForm', TableForm);

    Vue.use(IconExtra);
  },
};
