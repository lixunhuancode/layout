export default [
  {
    path: 'marketing-category/list',
    name: 'products.marketing.category.list',
    component: () =>
      import('@/views/Products/MarketingCategory/List/index.vue'),
    meta: {
      locale: '营销分类',
      requiresAuth: false,
    },
  },
];
