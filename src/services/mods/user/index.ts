/**
 * @description 用户后端接口
 */
import * as carInfo from './carInfo';
import * as carList from './carList';
import * as ListPage from './ListPage';
import * as getUser from './getUser';
import * as infoById from './infoById';
import * as modifyUserInfo from './modifyUserInfo';
import * as postPointListPage from './postPointListPage';
import * as removeCar from './removeCar';
import * as updateCarInfo from './updateCarInfo';
import * as userCarList from './userCarList';
import * as userList from './userList';

export {
  carInfo,
  carList,
  ListPage,
  getUser,
  infoById,
  modifyUserInfo,
  postPointListPage,
  removeCar,
  updateCarInfo,
  userCarList,
  userList,
};
