/**
   * @desc 分页查询商品操作日志
分页查询商品操作日志
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryProductLogDTO;

export const init = new defs.ProductLogVO();

export type ParamsAndBody = { data: defs.QueryProductLogDTO };

export function request(
  data: defs.QueryProductLogDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/productManagement/pageQueryProductLog`, {
    data,
    method: 'POST',
    ...options,
  });
}
