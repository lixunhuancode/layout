/**
   * @desc 创建商品
创建商品
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.CreateOrUpdateProductDTO;

export const init = '';

export type ParamsAndBody = { data: defs.CreateOrUpdateProductDTO };

export function request(
  data: defs.CreateOrUpdateProductDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/productManagement/createProduct`, {
    data,
    method: 'POST',
    ...options,
  });
}
