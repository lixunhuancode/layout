/**
   * @desc 审核列表
审核列表
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryProductDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.QueryProductDTO };

export function request(
  data: defs.QueryProductDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/spu/shelfList`, {
    data,
    method: 'POST',
    ...options,
  });
}
