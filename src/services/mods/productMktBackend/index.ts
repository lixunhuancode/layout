/**
 * @description 商品管理后台接口【营销分类】
 */
import * as createMkt from './createMkt';
import * as deleteMkt from './deleteMkt';
import * as getMktTree from './getMktTree';
import * as pageQueryMkt from './pageQueryMkt';
import * as queryChildMktList from './queryChildMktList';
import * as queryMktDetails from './queryMktDetails';
import * as updateMkt from './updateMkt';
import * as updateStatus from './updateStatus';

export {
  createMkt,
  deleteMkt,
  getMktTree,
  pageQueryMkt,
  queryChildMktList,
  queryMktDetails,
  updateMkt,
  updateStatus,
};
