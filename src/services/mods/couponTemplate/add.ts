/**
 * @desc 添加
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.CouponTemplateDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.CouponTemplateDTO };

export function request(
  data: defs.CouponTemplateDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`admin/coupon/template/add`, {
    data,
    method: 'POST',
    ...options,
  });
}
