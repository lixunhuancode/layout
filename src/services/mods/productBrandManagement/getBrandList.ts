/**
    * @desc 查询品牌列表
查询品牌列表
    */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = [];

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`backend/brand/getBrandList`, {
    method: 'GET',
    ...options,
  });
}
