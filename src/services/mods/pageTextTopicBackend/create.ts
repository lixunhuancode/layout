/**
 * @desc 添加专题
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.PageTextDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.PageTextDTO };

export function request(data: defs.PageTextDTO, options: AxiosRequestConfig) {
  return axiosHandle(`backend/cms/pageText/topic/create`, {
    data,
    method: 'POST',
    ...options,
  });
}
