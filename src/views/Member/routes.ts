import { DEFAULT_LAYOUT } from '@/router/routes/base';
import User from './User/routes';
import CarManage from './CarManage/routes';
import Recharge from './Recharge/routes';

export default {
  path: '/member',
  name: 'member',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '会员',
    requiresAuth: false,
    icon: 'icon-c-member',
    order: 2,
  },
  children: [...User, ...CarManage, ...Recharge],
};
