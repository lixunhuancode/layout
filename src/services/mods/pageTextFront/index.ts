/**
 * @description 内容管理-专题/服务内容管理-前台页面
 */
import * as detailByPageCode from './detailByPageCode';
import * as detailById from './detailById';

export { detailByPageCode, detailById };
