/**
   * @desc 用户列表
用户列表
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryUserListDTO;

export const init = new defs.UserInfo();

export type ParamsAndBody = { data: defs.QueryUserListDTO };

export function request(
  data: defs.QueryUserListDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`webapi/user/userList`, {
    data,
    method: 'POST',
    ...options,
  });
}
