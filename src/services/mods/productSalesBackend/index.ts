/**
 * @description 商品管理后台接口【商品销售报表】
 */
import * as pageQuerySales from './pageQuerySales';
import * as pageQuerySalesDetails from './pageQuerySalesDetails';
import * as salesCount from './salesCount';

export { pageQuerySales, pageQuerySalesDetails, salesCount };
