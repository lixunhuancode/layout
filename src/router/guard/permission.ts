import type { Router } from 'vue-router';
import NProgress from 'nprogress'; // progress bar
import { useUserStore } from '@/store';

import { appRoutes } from '../routes';

export default function setupPermissionGuard(router: Router) {
  router.beforeEach(async (to, from, next) => {
    const userStore = useUserStore();
    if (userStore.isLogin && !userStore.routeLoad) {
      userStore.routeLoad = true;
      const { permissions, permissionsKeyMap } = useUserStore();
      // 递归匹配有权限的路由
      const filterTreeArray = (tree: any, bList: any) => {
        return tree
          .filter((item: any) => {
            // return bList.indexOf(item.name) > -1 && !item.meta.isHidden;
            return true;
          })
          .map((item: any) => {
            item = { ...item };
            item.meta.locale = permissionsKeyMap[item.name] || item.meta.locale;
            if (item.children) {
              item.children = filterTreeArray(item.children, bList);
            }
            return item;
          });
      };

      console.log({ appRoutes, permissions });
      const routerList = filterTreeArray(appRoutes, permissions);
      routerList.forEach((item: any) => {
        router.addRoute(item);
      });
      // 添加完动态路由之后，需要在进行一次主动跳转
      next({
        path: to.fullPath,
        replace: true,
      });
    } else {
      next();
    }

    NProgress.done();
  });
}
