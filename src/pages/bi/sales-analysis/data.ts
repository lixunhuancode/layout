export const overviewData = [
  {
    title: '交易额（元）',
    today: 42.12,
    month: 172.1,
    on1: '10%',
    over1: '11%',
    on2: '9%',
    over2: '4%',
  },
  {
    title: '每单平均金额（元）',
    today: 142.12,
    month: 122.1,
    on1: '10%',
    over1: '11%',
    on2: '9%',
    over2: '4%',
  },
  {
    title: '订单数（元）',
    today: 102.12,
    month: 2772.1,
    on1: '9.43%',
    over1: '1.12%',
    on2: '9%',
    over2: '4%',
  },
];

export default {};
