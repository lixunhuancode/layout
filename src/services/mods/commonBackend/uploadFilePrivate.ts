/**
 * @desc 上传文件到私有桶
 */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = new defs.ResponseModel();

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`backend/cms/common/uploadFilePrivate`, {
    method: 'POST',
    ...options,
  });
}
