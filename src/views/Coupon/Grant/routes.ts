export default [
  {
    path: 'grant/list',
    name: 'coupon.grant.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '卡券发放',
      requiresAuth: false,
    },
  },
  {
    path: 'grant/list/form',
    name: 'coupon.grant.form',
    component: () => import('./Form/index.vue'),
    meta: {
      locale: '发券',
      requiresAuth: false,
      hideInMenu: true,
    },
  },
  {
    path: 'grant/list/form/:id',
    name: 'coupon.grant.edit',
    component: () => import('./Form/index.vue'),
    meta: {
      locale: '编辑',
      requiresAuth: false,
      hideInMenu: true,
    },
  },
  {
    path: 'grant/list/form/:id',
    name: 'coupon.grant.detail',
    component: () => import('./Form/index.vue'),
    meta: {
      locale: '详情',
      requiresAuth: false,
      hideInMenu: true,
    },
  },
];
