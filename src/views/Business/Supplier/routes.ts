export default [
  {
    path: 'supplier/list',
    name: 'business.supplier.list',
    component: () => import('@/views/Business/Supplier/List/index.vue'),
    meta: {
      locale: '供应商管理',
      requiresAuth: false,
    },
  },
  {
    path: 'supplier/form',
    name: 'business.supplier.form',
    component: () => import('@/views/Business/Supplier/Form/index.vue'),
    meta: {
      locale: '新增供应商',
      requiresAuth: false,
      hideInMenu: true,
    },
  },
  {
    path: 'supplier/form/:id',
    name: 'business.supplier.edit',
    component: () => import('@/views/Business/Supplier/Form/index.vue'),
    meta: {
      locale: '修改供应商',
      requiresAuth: false,
      hideInMenu: true,
    },
  },
  {
    path: 'supplier/form/:id',
    name: 'business.supplier.detail',
    component: () => import('@/views/Business/Supplier/Form/index.vue'),
    meta: {
      locale: '供应商详情',
      requiresAuth: false,
      hideInMenu: true,
    },
  },
];
