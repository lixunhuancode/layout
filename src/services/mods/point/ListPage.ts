/**
 * @desc 积点变动记录
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryPointRecordDTO;

export const init = new defs.Page();

export type ParamsAndBody = { data: defs.QueryPointRecordDTO };

export function request(
  data: defs.QueryPointRecordDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`points/pointListPage`, {
    data,
    method: 'POST',
    ...options,
  });
}
