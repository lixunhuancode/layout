/**
 * @desc 获取所有有效状态版本
 */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = new defs.ResponseModel();

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`backend/version/all`, {
    method: 'POST',
    ...options,
  });
}
