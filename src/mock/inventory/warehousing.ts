import { pagerResponseWrap, mock } from '@/utils/setup-mock';

export const inTable1List = (req: any) => {
  const data = [
    {
      id: 1,
      a1: 'CG20230505484901',
      a2: '供应商一',
      a3: '直营1号店',
      a4: '待入库',
      a5: '1500',
      a6: '-',
      a7: '张三',
      a8: '2023-05-05 14:35:53',
    },
    {
      id: 2,
      a1: 'CG20230506014509',
      a2: '供应商三',
      a3: '加盟1号店',
      a4: '待入库',
      a5: '890',
      a6: '-',
      a7: '李四',
      a8: '2023-05-05 17:34:11',
    },
    {
      id: 3,
      a1: 'CG20230507005621',
      a2: '供应商一',
      a3: '3号仓库',
      a4: '待入库',
      a5: '9975',
      a6: '-',
      a7: '张三',
      a8: '2023-05-07 10:23:41',
    },
    {
      id: 4,
      a1: 'CG20230507484901',
      a2: '供应商二',
      a3: '3号仓库',
      a4: '待入库',
      a5: '5900',
      a6: '-',
      a7: '李四',
      a8: '2023-05-09 11:42:09',
    },
    {
      id: 5,
      a1: 'CG20230501252599',
      a2: '供应商三',
      a3: '加盟1号店',
      a4: '待入库',
      a5: '52100',
      a6: '-',
      a7: '张三',
      a8: '2023-05-09 09:59:02',
    },
    {
      id: 6,
      a1: 'CG20230511484971',
      a2: '供应商二',
      a3: '直营1号店',
      a4: '待入库',
      a5: '1900',
      a6: '-',
      a7: '张三',
      a8: '2023-05-11 19:25:45',
    },
    {
      id: 7,
      a1: 'CG20230525424910',
      a2: '供应商一',
      a3: '3号仓库',
      a4: '待入库',
      a5: '5980',
      a6: '-',
      a7: '张三',
      a8: '2023-05-25 14:35:07',
    },
    {
      id: 8,
      a1: 'CG20230511477201',
      a2: '供应商一',
      a3: '直营1号店',
      a4: '待入库',
      a5: '240',
      a6: '-',
      a7: '张三',
      a8: '2023-05-11 10:15:42',
    },
    {
      id: 9,
      a1: 'CG20230513552552',
      a2: '供应商二',
      a3: '加盟2号店',
      a4: '待入库',
      a5: '190',
      a6: '-',
      a7: '李四',
      a8: '2023-05-13 08:55:33',
    },
    {
      id: 10,
      a1: 'CG20230513852680',
      a2: '供应商一',
      a3: '2号仓库',
      a4: '待入库',
      a5: '100',
      a6: '-',
      a7: '李四',
      a8: '2023-05-13 18:25:32',
    },
    {
      id: 11,
      a1: 'CG20230514109211',
      a2: '供应商二',
      a3: '1号仓库',
      a4: '已入库',
      a5: '1450',
      a6: '1450',
      a7: '李四',
      a8: '2023-05-13 15:22:01',
    },
    {
      id: 12,
      a1: 'CG20230515500677',
      a2: '供应商三',
      a3: '1号仓库',
      a4: '已入库',
      a5: '2200',
      a6: '2200',
      a7: '李四',
      a8: '2023-05-13 15:22:01',
    },
  ];
  return mock(pagerResponseWrap(req, data));
};

export const inTable2List = (req: any) => {
  const data = [
    {
      id: 1,
      a1: 'DB20230505484901',
      a2: '直营1号店',
      a3: '直营2号店',
      a4: '待入库',
      a5: '100',
      a6: '-',
      a7: '张三',
      a8: '2023-05-05 14:35:53',
      a9: '2023-05-07',
    },
    {
      id: 2,
      a1: 'DB20230506014509',
      a2: '供应商三',
      a3: '加盟1号店',
      a4: '待入库',
      a5: '400',
      a6: '-',
      a7: '李四',
      a8: '2023-05-05 17:34:11',
      a9: '2023-05-07',
    },
    {
      id: 3,
      a1: 'DB20230507005621',
      a2: '供应商一',
      a3: '加盟2号店',
      a4: '待入库',
      a5: '1350',
      a6: '-',
      a7: '张三',
      a8: '2023-05-07 10:23:41',
      a9: '2023-05-07',
    },
    {
      id: 4,
      a1: 'DB20230507484901',
      a2: '供应商二',
      a3: '3号仓库',
      a4: '待入库',
      a5: '600',
      a6: '-',
      a7: '李四',
      a8: '2023-05-09 11:42:09',
      a9: '2023-05-07',
    },
    {
      id: 5,
      a1: 'DB20230501252599',
      a2: '供应商三',
      a3: '加盟1号店',
      a4: '待入库',
      a5: '52100',
      a6: '-',
      a7: '张三',
      a8: '2023-05-09 09:59:02',
      a9: '2023-05-07',
    },
    {
      id: 6,
      a1: 'DB20230511484971',
      a2: '供应商二',
      a3: '直营1号店',
      a4: '待入库',
      a5: '890',
      a6: '-',
      a7: '张三',
      a8: '2023-05-11 19:25:45',
      a9: '2023-05-12',
    },
    {
      id: 7,
      a1: 'DB20230525424910',
      a2: '供应商一',
      a3: '直营1号店',
      a4: '待入库',
      a5: '400',
      a6: '-',
      a7: '张三',
      a8: '2023-05-25 14:35:07',
      a9: '2023-05-27',
    },
    {
      id: 8,
      a1: 'DB20230511477201',
      a2: '供应商一',
      a3: '2号仓库',
      a4: '待入库',
      a5: '240',
      a6: '-',
      a7: '张三',
      a8: '2023-05-11 10:15:42',
      a9: '2023-05-15',
    },
    {
      id: 9,
      a1: 'DB20230513552552',
      a2: '供应商二',
      a3: '直营2号店',
      a4: '待入库',
      a5: '900',
      a6: '-',
      a7: '李四',
      a8: '2023-05-13 08:55:33',
      a9: '2023-05-15',
    },
    {
      id: 10,
      a1: 'DB20230513852680',
      a2: '供应商一',
      a3: '2号仓库',
      a4: '待入库',
      a5: '100',
      a6: '-',
      a7: '李四',
      a8: '2023-05-13 18:25:32',
      a9: '2023-05-14',
    },
    {
      id: 11,
      a1: 'DB20230514109211',
      a2: '2号仓库',
      a3: '加盟1号店',
      a4: '已入库',
      a5: '1700',
      a6: '1550',
      a7: '李四',
      a8: '2023-05-13 15:22:01',
      a9: '2023-05-15',
    },
    {
      id: 12,
      a1: 'DB20230515500677',
      a2: '1号仓库',
      a3: '直营1号店',
      a4: '已入库',
      a5: '2200',
      a6: '2200',
      a7: '李四',
      a8: '2023-05-13 15:22:01',
      a9: '2023-05-20',
    },
  ];
  return mock(pagerResponseWrap(req, data));
};

export const inTable3List = (req: any) => {
  const data = [
    {
      id: 1,
      a1: 'SH20230505957284',
      a2: '直营1号店',
      a3: '待入库',
      a4: '2',
      a5: '-',
      a6: '张三',
      a7: '2023-05-05 14:35:53',
    },
    {
      id: 2,
      a1: 'SH20230505592047',
      a2: '直营1号店',
      a3: '待入库',
      a4: '1',
      a5: '-',
      a6: '张三',
      a7: '2023-05-05 17:34:11',
    },
    {
      id: 3,
      a1: 'SH20230505484901',
      a2: '加盟2号店',
      a3: '待入库',
      a4: '5',
      a5: '-',
      a6: '张三',
      a7: '2023-05-07 10:23:41',
    },
    {
      id: 4,
      a1: 'SH20230509089996',
      a2: '加盟2号店',
      a3: '待入库',
      a4: '106',
      a5: '-',
      a6: '李四',
      a7: '2023-05-09 11:42:09',
    },
    {
      id: 5,
      a1: 'SH20230507426528',
      a2: '加盟2号店',
      a3: '待入库',
      a4: '9',
      a5: '-',
      a6: '张三',
      a7: '2023-05-07 21:10:55',
    },
    {
      id: 6,
      a1: 'SH20230509002521',
      a2: '加盟2号店',
      a3: '待入库',
      a4: '30',
      a5: '-',
      a6: '张三',
      a7: '2023-05-09 09:18:05',
    },
    {
      id: 7,
      a1: 'SH20230509002507',
      a2: '直营2号店',
      a3: '待入库',
      a4: '54',
      a5: '-',
      a6: '李四',
      a7: '2023-05-08 09:28:57',
    },
    {
      id: 8,
      a1: 'SH20230509421087',
      a2: '加盟2号店',
      a3: '待入库',
      a4: '100',
      a5: '-',
      a6: '张三',
      a7: '2023-05-09 18:18:40',
    },
    {
      id: 9,
      a1: 'SH20230509002521',
      a2: '直营1号店',
      a3: '待入库',
      a4: '12',
      a5: '-',
      a6: '张三',
      a7: '2023-05-09 09:51:31',
    },
    {
      id: 10,
      a1: 'SH20230510723560',
      a2: '直营1号店',
      a3: '待入库',
      a4: '12',
      a5: '-',
      a6: '李四',
      a7: '2023-05-19 16:40:51',
    },
    {
      id: 11,
      a1: 'SH20230520100397',
      a2: '直营1号店',
      a3: '已入库',
      a4: '100',
      a5: '100',
      a6: '李四',
      a7: '2023-05-20 19:20:31',
    },
  ];
  return mock(pagerResponseWrap(req, data));
};

export const inTable4List = (req: any) => {
  const data = [
    {
      id: 1,
      a1: 'PT20230505484901',
      a2: '供应商一',
      a3: '直营1号店',
      a4: '待入库',
      a5: '1500',
      a6: '-',
      a7: '张三',
      a8: '2023-05-05 14:35:53',
    },
    {
      id: 2,
      a1: 'PT20230506014509',
      a2: '供应商三',
      a3: '加盟1号店',
      a4: '待入库',
      a5: '890',
      a6: '-',
      a7: '李四',
      a8: '2023-05-05 17:34:11',
    },
    {
      id: 3,
      a1: 'PT20230507005621',
      a2: '供应商一',
      a3: '3号仓库',
      a4: '待入库',
      a5: '9975',
      a6: '-',
      a7: '张三',
      a8: '2023-05-07 10:23:41',
    },
    {
      id: 4,
      a1: 'PT20230507484901',
      a2: '供应商二',
      a3: '3号仓库',
      a4: '待入库',
      a5: '5900',
      a6: '-',
      a7: '李四',
      a8: '2023-05-09 11:42:09',
    },
    {
      id: 5,
      a1: 'PT20230501252599',
      a2: '供应商三',
      a3: '加盟1号店',
      a4: '待入库',
      a5: '52100',
      a6: '-',
      a7: '张三',
      a8: '2023-05-09 09:59:02',
    },
    {
      id: 6,
      a1: 'PT20230511484971',
      a2: '供应商二',
      a3: '直营1号店',
      a4: '待入库',
      a5: '1900',
      a6: '-',
      a7: '张三',
      a8: '2023-05-11 19:25:45',
    },
    {
      id: 7,
      a1: 'PT20230525434910',
      a2: '供应商一',
      a3: '3号仓库',
      a4: '待入库',
      a5: '5980',
      a6: '-',
      a7: '张三',
      a8: '2023-05-25 14:35:07',
    },
    {
      id: 8,
      a1: 'PT20230511477201',
      a2: '供应商一',
      a3: '直营1号店',
      a4: '待入库',
      a5: '240',
      a6: '-',
      a7: '张三',
      a8: '2023-05-11 10:15:42',
    },
    {
      id: 9,
      a1: 'PT20230513552552',
      a2: '供应商二',
      a3: '加盟2号店',
      a4: '待入库',
      a5: '190',
      a6: '-',
      a7: '李四',
      a8: '2023-05-13 08:55:33',
    },
    {
      id: 10,
      a1: 'PT20230513852680',
      a2: '供应商一',
      a3: '2号仓库',
      a4: '待入库',
      a5: '100',
      a6: '-',
      a7: '李四',
      a8: '2023-05-13 18:25:32',
    },
    {
      id: 11,
      a1: 'PT20230514109211',
      a2: '供应商二',
      a3: '1号仓库',
      a4: '已入库',
      a5: '1450',
      a6: '1450',
      a7: '李四',
      a8: '2023-05-13 15:22:01',
    },
    {
      id: 12,
      a1: 'PT20230515500677',
      a2: '供应商三',
      a3: '1号仓库',
      a4: '已入库',
      a5: '2200',
      a6: '2200',
      a7: '李四',
      a8: '2023-05-13 15:22:01',
    },
  ];
  return mock(pagerResponseWrap(req, data));
};

export const inDetailList = (req: any) => {
  const data = [
    {
      id: 1,
      a1: 'RK20230505484901',
      a2: '供应商一',
      a3: '采购入库',
      a4: '100',
      a5: '¥3490.31',
      a6: '¥5905.68',
      a7: '2023-05-05 14:35:53',
      a8: '张三',
    },
    {
      id: 2,
      a1: 'RK20230510672990',
      a2: '加盟1号店',
      a3: '配销入库',
      a4: '350',
      a5: '¥25100.00',
      a6: '¥31850.00',
      a7: '2023-05-05 13:40:12',
      a8: '李四',
    },
    {
      id: 3,
      a1: 'RK20230505488510',
      a2: '加盟1号店',
      a3: '配销入库',
      a4: '350',
      a5: '¥25100.00',
      a6: '¥31850.00',
      a7: '2023-05-05 13:40:12',
      a8: '李四',
    },
    {
      id: 4,
      a1: 'RK20230507123542',
      a2: '1号仓库',
      a3: '采购入库',
      a4: '4000',
      a5: '¥825100.00',
      a6: '¥988860.00',
      a7: '2023-05-07 17:23:00',
      a8: '张三',
    },
    {
      id: 5,
      a1: 'RK20230509488510',
      a2: '2号仓库',
      a3: '采购入库',
      a4: '500',
      a5: '¥95100.00',
      a6: '¥178860.00',
      a7: '2023-05-09 07:33:20',
      a8: '张三',
    },
    {
      id: 6,
      a1: 'RK20230509578861',
      a2: '加盟1号店',
      a3: '调拨入库',
      a4: '300',
      a5: '¥5900.00',
      a6: '¥8790.00',
      a7: '2023-05-10 17:59:30',
      a8: '李四',
    },
    {
      id: 7,
      a1: 'RK20230505488588',
      a2: '加盟1号店',
      a3: '配销入库',
      a4: '320',
      a5: '¥22100.80',
      a6: '¥36850.00',
      a7: '2023-05-06 11:12:12',
      a8: '张三',
    },
    {
      id: 8,
      a1: 'RK20230519875904',
      a2: '直营1号店',
      a3: '盘盈入库',
      a4: '20',
      a5: '¥120.00',
      a6: '¥260.00',
      a7: '2023-05-19 17:33:20',
      a8: '李四',
    },
    {
      id: 9,
      a1: 'RK20230411920086',
      a2: '2号仓库',
      a3: '采购入库',
      a4: '9010',
      a5: '¥254800.00',
      a6: '¥472790.00',
      a7: '2023-04-11 12:42:31',
      a8: '张三',
    },
    {
      id: 10,
      a1: 'RK20230518864591',
      a2: '加盟2号店',
      a3: '调拨入库',
      a4: '90',
      a5: '¥51800.00',
      a6: '¥69000.00',
      a7: '2023-05-18 17:02:10',
      a8: '李四',
    },
  ];
  return mock(pagerResponseWrap(req, data));
};

export const outTable1List = (req: any) => {
  const data = [
    {
      id: 1,
      a1: 'DD20230505484901',
      a2: '1号仓库',
      a3: '直营1号店',
      a4: '待出库',
      a5: '快递',
      a6: '7',
      a7: '-',
      a8: '张三19988001234',
      a9: '2023-05-05 14:35:53',
    },
    {
      id: 2,
      a1: 'DD20230506014509',
      a2: '2号仓库',
      a3: '加盟2号店',
      a4: '待出库',
      a5: '同城配送',
      a6: '3',
      a7: '-',
      a8: '李四19988001234',
      a9: '2023-05-05 14:35:53',
    },
    {
      id: 3,
      a1: 'DD20230508005621',
      a2: '1号仓库',
      a3: '直营1号店',
      a4: '待出库',
      a5: '同城配送',
      a6: '1',
      a7: '-',
      a8: '张三19988001234',
      a9: '2023-05-11 10:15:42',
    },
    {
      id: 4,
      a1: 'DD20230508981122',
      a2: '2号仓库',
      a3: '加盟1号店',
      a4: '待出库',
      a5: '同城配送',
      a6: '2',
      a7: '-',
      a8: '李四19988001234',
      a9: '2023-05-15 12:44:30',
    },
    {
      id: 5,
      a1: 'DD20230508988578',
      a2: '1号仓库',
      a3: '加盟1号店',
      a4: '待出库',
      a5: '快递',
      a6: '7',
      a7: '-',
      a8: '张三19988001234',
      a9: '2023-05-13 08:55:33',
    },
    {
      id: 6,
      a1: 'DD20230508911297',
      a2: '1号仓库',
      a3: '加盟1号店',
      a4: '待出库',
      a5: '快递',
      a6: '2',
      a7: '-',
      a8: '李四19988001234',
      a9: '2023-05-13 15:22:01',
    },
    {
      id: 7,
      a1: 'DD20230508984678',
      a2: '1号仓库',
      a3: '加盟1号店',
      a4: '待出库',
      a5: '快递',
      a6: '1',
      a7: '-',
      a8: '李四19988001234',
      a9: '2023-05-13 08:41:55',
    },
    {
      id: 8,
      a1: 'DD20230508974681',
      a2: '1号仓库',
      a3: '加盟1号店',
      a4: '待出库',
      a5: '快递',
      a6: '1',
      a7: '-',
      a8: '张三19988001234',
      a9: '2023-05-27 12:13:50',
    },
    {
      id: 9,
      a1: 'DD20230508860012',
      a2: '1号仓库',
      a3: '加盟1号店',
      a4: '待出库',
      a5: '同城配送',
      a6: '4',
      a7: '-',
      a8: '张三19988001234',
      a9: '2023-05-27 12:13:50',
    },
    {
      id: 10,
      a1: 'DD20230508911297',
      a2: '1号仓库',
      a3: '加盟1号店',
      a4: '待出库',
      a5: '快递',
      a6: '2',
      a7: '-',
      a8: '李四19988001234',
      a9: '2023-05-13 15:22:01',
    },
  ];
  return mock(pagerResponseWrap(req, data));
};

export const outTable2List = (req: any) => {
  const data = [
    {
      id: 1,
      a1: 'DB20230505484901',
      a2: '直营1号店',
      a3: '直营2号店',
      a4: '待出库',
      a5: '100',
      a6: '-',
      a7: '张三',
      a8: '2023-05-05 14:35:53',
      a9: '2023-05-07',
    },
    {
      id: 2,
      a1: 'DB20230506014509',
      a2: '供应商三',
      a3: '加盟1号店',
      a4: '待出库',
      a5: '400',
      a6: '-',
      a7: '李四',
      a8: '2023-05-05 17:34:11',
      a9: '2023-05-07',
    },
    {
      id: 3,
      a1: 'DB20230507005621',
      a2: '供应商一',
      a3: '加盟2号店',
      a4: '待出库',
      a5: '1350',
      a6: '-',
      a7: '张三',
      a8: '2023-05-07 10:23:41',
      a9: '2023-05-07',
    },
    {
      id: 4,
      a1: 'DB20230507484901',
      a2: '供应商二',
      a3: '3号仓库',
      a4: '待出库',
      a5: '600',
      a6: '-',
      a7: '李四',
      a8: '2023-05-09 11:42:09',
      a9: '2023-05-07',
    },
    {
      id: 5,
      a1: 'DB20230501252599',
      a2: '供应商三',
      a3: '加盟1号店',
      a4: '待出库',
      a5: '52100',
      a6: '-',
      a7: '张三',
      a8: '2023-05-09 09:59:02',
      a9: '2023-05-07',
    },
    {
      id: 6,
      a1: 'DB20230511484971',
      a2: '供应商二',
      a3: '直营1号店',
      a4: '待出库',
      a5: '890',
      a6: '-',
      a7: '张三',
      a8: '2023-05-11 19:25:45',
      a9: '2023-05-12',
    },
    {
      id: 7,
      a1: 'DB20230525424910',
      a2: '供应商一',
      a3: '直营1号店',
      a4: '待出库',
      a5: '400',
      a6: '-',
      a7: '张三',
      a8: '2023-05-25 14:35:07',
      a9: '2023-05-27',
    },
    {
      id: 8,
      a1: 'DB20230511477201',
      a2: '供应商一',
      a3: '2号仓库',
      a4: '待出库',
      a5: '240',
      a6: '-',
      a7: '张三',
      a8: '2023-05-11 10:15:42',
      a9: '2023-05-15',
    },
    {
      id: 9,
      a1: 'DB20230513552552',
      a2: '供应商二',
      a3: '直营2号店',
      a4: '待出库',
      a5: '900',
      a6: '-',
      a7: '李四',
      a8: '2023-05-13 08:55:33',
      a9: '2023-05-15',
    },
    {
      id: 10,
      a1: 'DB20230513852680',
      a2: '供应商一',
      a3: '2号仓库',
      a4: '待出库',
      a5: '100',
      a6: '-',
      a7: '李四',
      a8: '2023-05-13 18:25:32',
      a9: '2023-05-14',
    },
    {
      id: 11,
      a1: 'DB20230514109211',
      a2: '2号仓库',
      a3: '加盟1号店',
      a4: '已出库',
      a5: '1700',
      a6: '1550',
      a7: '李四',
      a8: '2023-05-13 15:22:01',
      a9: '2023-05-15',
    },
    {
      id: 12,
      a1: 'DB20230515500677',
      a2: '1号仓库',
      a3: '直营1号店',
      a4: '已出库',
      a5: '2200',
      a6: '2200',
      a7: '李四',
      a8: '2023-05-13 15:22:01',
      a9: '2023-05-20',
    },
  ];
  return mock(pagerResponseWrap(req, data));
};

export const outTable3List = (req: any) => {
  const data = [
    {
      id: 1,
      a1: 'CT20230505957284',
      a2: '直营1号店',
      a3: '待出库',
      a4: '2',
      a5: '-',
      a6: '张三',
      a7: '2023-05-05 14:35:53',
    },
    {
      id: 2,
      a1: 'CT20230505592047',
      a2: '直营1号店',
      a3: '待出库',
      a4: '1',
      a5: '-',
      a6: '张三',
      a7: '2023-05-05 17:34:11',
    },
    {
      id: 3,
      a1: 'CT20230505484901',
      a2: '加盟2号店',
      a3: '待出库',
      a4: '5',
      a5: '-',
      a6: '张三',
      a7: '2023-05-07 10:23:41',
    },
    {
      id: 4,
      a1: 'CT20230509089996',
      a2: '加盟2号店',
      a3: '待出库',
      a4: '106',
      a5: '-',
      a6: '李四',
      a7: '2023-05-09 11:42:09',
    },
    {
      id: 5,
      a1: 'CT20230507426528',
      a2: '加盟2号店',
      a3: '待出库',
      a4: '9',
      a5: '-',
      a6: '张三',
      a7: '2023-05-07 21:10:55',
    },
    {
      id: 6,
      a1: 'CT20230509002521',
      a2: '加盟2号店',
      a3: '待出库',
      a4: '30',
      a5: '-',
      a6: '张三',
      a7: '2023-05-09 09:18:05',
    },
    {
      id: 7,
      a1: 'CT20230509002507',
      a2: '直营2号店',
      a3: '待出库',
      a4: '54',
      a5: '-',
      a6: '李四',
      a7: '2023-05-08 09:28:57',
    },
    {
      id: 8,
      a1: 'CT20230509421087',
      a2: '加盟2号店',
      a3: '待出库',
      a4: '100',
      a5: '-',
      a6: '张三',
      a7: '2023-05-09 18:18:40',
    },
    {
      id: 9,
      a1: 'CT20230509002521',
      a2: '直营1号店',
      a3: '待出库',
      a4: '12',
      a5: '-',
      a6: '张三',
      a7: '2023-05-09 09:51:31',
    },
    {
      id: 10,
      a1: 'CT20230510723560',
      a2: '直营1号店',
      a3: '待出库',
      a4: '12',
      a5: '-',
      a6: '李四',
      a7: '2023-05-19 16:40:51',
    },
    {
      id: 11,
      a1: 'CT20230520100397',
      a2: '加盟1号店',
      a3: '已出库',
      a4: '100',
      a5: '100',
      a6: '李四',
      a7: '2023-05-20 19:20:31',
    },
  ];
  return mock(pagerResponseWrap(req, data));
};

export const outTable4List = (req: any) => {
  const data = [
    {
      id: 1,
      a1: 'PX20230505484901',
      a2: '直营1号店',
      a3: '直营2号店',
      a4: '待出库',
      a5: '100',
      a6: '-',
      a7: '张三',
      a8: '2023-05-05 14:35:53',
      a9: '2023-05-07',
    },
    {
      id: 2,
      a1: 'PX20230506014509',
      a2: '供应商三',
      a3: '加盟1号店',
      a4: '待出库',
      a5: '400',
      a6: '-',
      a7: '李四',
      a8: '2023-05-05 17:34:11',
      a9: '2023-05-07',
    },
    {
      id: 3,
      a1: 'PX20230507005621',
      a2: '供应商一',
      a3: '加盟2号店',
      a4: '待出库',
      a5: '1350',
      a6: '-',
      a7: '张三',
      a8: '2023-05-07 10:23:41',
      a9: '2023-05-07',
    },
    {
      id: 4,
      a1: 'PX20230507484901',
      a2: '供应商二',
      a3: '3号仓库',
      a4: '待出库',
      a5: '600',
      a6: '-',
      a7: '李四',
      a8: '2023-05-09 11:42:09',
      a9: '2023-05-07',
    },
    {
      id: 5,
      a1: 'PX20230501252599',
      a2: '供应商三',
      a3: '加盟1号店',
      a4: '待出库',
      a5: '52100',
      a6: '-',
      a7: '张三',
      a8: '2023-05-09 09:59:02',
      a9: '2023-05-07',
    },
    {
      id: 6,
      a1: 'PX20230511484971',
      a2: '供应商二',
      a3: '直营1号店',
      a4: '待出库',
      a5: '890',
      a6: '-',
      a7: '张三',
      a8: '2023-05-11 19:25:45',
      a9: '2023-05-12',
    },
    {
      id: 7,
      a1: 'PX20230525424910',
      a2: '供应商一',
      a3: '直营1号店',
      a4: '待出库',
      a5: '400',
      a6: '-',
      a7: '张三',
      a8: '2023-05-25 14:35:07',
      a9: '2023-05-27',
    },
    {
      id: 8,
      a1: 'PX20230511477201',
      a2: '供应商一',
      a3: '2号仓库',
      a4: '待出库',
      a5: '240',
      a6: '-',
      a7: '张三',
      a8: '2023-05-11 10:15:42',
      a9: '2023-05-15',
    },
    {
      id: 9,
      a1: 'PX20230513552552',
      a2: '供应商二',
      a3: '直营2号店',
      a4: '待出库',
      a5: '900',
      a6: '-',
      a7: '李四',
      a8: '2023-05-13 08:55:33',
      a9: '2023-05-15',
    },
    {
      id: 10,
      a1: 'PX20230513852680',
      a2: '供应商一',
      a3: '2号仓库',
      a4: '待出库',
      a5: '100',
      a6: '-',
      a7: '李四',
      a8: '2023-05-13 18:25:32',
      a9: '2023-05-14',
    },
    {
      id: 11,
      a1: 'PX20230514109211',
      a2: '2号仓库',
      a3: '加盟1号店',
      a4: '已出库',
      a5: '1700',
      a6: '1550',
      a7: '李四',
      a8: '2023-05-13 15:22:01',
      a9: '2023-05-15',
    },
    {
      id: 12,
      a1: 'PX20230515500677',
      a2: '1号仓库',
      a3: '直营1号店',
      a4: '已出库',
      a5: '2200',
      a6: '2200',
      a7: '李四',
      a8: '2023-05-13 15:22:01',
      a9: '2023-05-20',
    },
  ];
  return mock(pagerResponseWrap(req, data));
};

export const outDetailList = (req: any) => {
  const data = [
    {
      id: 1,
      a1: 'CK20230505484901',
      a2: '供应商一',
      a3: '调拨出库',
      a4: '100',
      a5: '¥3490.31',
      a6: '¥5905.68',
      a7: '2023-05-05 14:35:53',
      a8: '张三',
    },
    {
      id: 2,
      a1: 'CK20230510672990',
      a2: '加盟1号店',
      a3: '配销出库',
      a4: '350',
      a5: '¥25100.00',
      a6: '¥31850.00',
      a7: '2023-05-05 13:40:12',
      a8: '李四',
    },
    {
      id: 3,
      a1: 'CK20230505488510',
      a2: '加盟1号店',
      a3: '调拨出库',
      a4: '350',
      a5: '¥25100.00',
      a6: '¥31850.00',
      a7: '2023-05-05 13:40:12',
      a8: '李四',
    },
    {
      id: 4,
      a1: 'CK20230507123542',
      a2: '1号仓库',
      a3: '门店销售出库',
      a4: '4000',
      a5: '¥825100.00',
      a6: '¥988860.00',
      a7: '2023-05-07 17:23:00',
      a8: '张三',
    },
    {
      id: 5,
      a1: 'CK20230509488510',
      a2: '2号仓库',
      a3: '调拨出库',
      a4: '500',
      a5: '¥95100.00',
      a6: '¥178860.00',
      a7: '2023-05-09 07:33:20',
      a8: '张三',
    },
    {
      id: 6,
      a1: 'CK20230509578861',
      a2: '加盟1号店',
      a3: '采购退货出库',
      a4: '300',
      a5: '¥5900.00',
      a6: '¥8790.00',
      a7: '2023-05-10 17:59:30',
      a8: '李四',
    },
    {
      id: 7,
      a1: 'CK20230505488588',
      a2: '加盟1号店',
      a3: '配销出库',
      a4: '320',
      a5: '¥22100.80',
      a6: '¥36850.00',
      a7: '2023-05-06 11:12:12',
      a8: '张三',
    },
    {
      id: 8,
      a1: 'CK20230519875904',
      a2: '直营1号店',
      a3: '盘亏出库',
      a4: '20',
      a5: '¥120.00',
      a6: '¥260.00',
      a7: '2023-05-19 17:33:20',
      a8: '李四',
    },
    {
      id: 9,
      a1: 'CK20230411920086',
      a2: '2号仓库',
      a3: '配销出库',
      a4: '9010',
      a5: '¥254800.00',
      a6: '¥472790.00',
      a7: '2023-04-11 12:42:31',
      a8: '张三',
    },
    {
      id: 10,
      a1: 'CK20230518864591',
      a2: '加盟2号店',
      a3: '调拨出库',
      a4: '90',
      a5: '¥51800.00',
      a6: '¥69000.00',
      a7: '2023-05-18 17:02:10',
      a8: '李四',
    },
  ];
  return mock(pagerResponseWrap(req, data));
};

export default mock;
