/**
 * @desc 积点充值审批
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.AuditRechargeDTO;

export const init = false;

export type ParamsAndBody = { data: defs.AuditRechargeDTO };

export function request(
  data: defs.AuditRechargeDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`points/auditRecharge`, {
    data,
    method: 'POST',
    ...options,
  });
}
