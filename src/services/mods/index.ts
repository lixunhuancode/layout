import * as adminSysLog from './adminSysLog';
import * as adminUser from './adminUser';
import * as asyncTask from './asyncTask';
import * as auth from './auth';
import * as backendAftersale from './backendAftersale';
import * as backendOrder from './backendOrder';
import * as businessTypeBackend from './businessTypeBackend';
import * as carBrandBackend from './carBrandBackend';
import * as carBrandFront from './carBrandFront';
import * as carTypeBackend from './carTypeBackend';
import * as carTypeFront from './carTypeFront';
import * as cardLabelBackend from './cardLabelBackend';
import * as cart from './cart';
import * as cartItem from './cartItem';
import * as cashier from './cashier';
import * as categoryBackend from './categoryBackend';
import * as commonBackend from './commonBackend';
import * as commonFront from './commonFront';
import * as countryArea from './countryArea';
import * as coupon from './coupon';
import * as couponBackend from './couponBackend';
import * as couponPublishBatch from './couponPublishBatch';
import * as couponTemplate from './couponTemplate';
import * as dictBackend from './dictBackend';
import * as dictFront from './dictFront';
import * as frontOrder from './frontOrder';
import * as mallBackend from './mallBackend';
import * as mallFront from './mallFront';
import * as memberBenefitsBackend from './memberBenefitsBackend';
import * as memberBenefitsFront from './memberBenefitsFront';
import * as memberLabelBackend from './memberLabelBackend';
import * as pageFront from './pageFront';
import * as pageHomeBackend from './pageHomeBackend';
import * as pageRecommendBackend from './pageRecommendBackend';
import * as pageRecommendFront from './pageRecommendFront';
import * as pageTextFront from './pageTextFront';
import * as pageTextServiceBackend from './pageTextServiceBackend';
import * as pageTextTopicBackend from './pageTextTopicBackend';
import * as pay from './pay';
import * as paymentCode from './paymentCode';
import * as paymentCodeBackend from './paymentCodeBackend';
import * as permission from './permission';
import * as point from './point';
import * as productApproval from './productApproval';
import * as productBrandManagement from './productBrandManagement';
import * as productFront from './productFront';
import * as productLabelBackend from './productLabelBackend';
import * as productManagementBackend from './productManagementBackend';
import * as productMktBackend from './productMktBackend';
import * as productRecycleBackend from './productRecycleBackend';
import * as productSalesBackend from './productSalesBackend';
import * as productStock from './productStock';
import * as propertyBackend from './propertyBackend';
import * as propertyFront from './propertyFront';
import * as qrCode from './qrCode';
import * as rechargeReasonBackend from './rechargeReasonBackend';
import * as role from './role';
import * as scoreBackend from './scoreBackend';
import * as searchKeywordBackend from './searchKeywordBackend';
import * as searchKeywordFront from './searchKeywordFront';
import * as serviceAssuranceBackend from './serviceAssuranceBackend';
import * as serviceAssuranceFront from './serviceAssuranceFront';
import * as shortLink from './shortLink';
import * as singleSignOn from './singleSignOn';
import * as sms from './sms';
import * as stock from './stock';
import * as supplier from './supplier';
import * as supplierBackend from './supplierBackend';
import * as user from './user';
import * as userFront from './userFront';
import * as userJob from './userJob';
import * as versionBackend from './versionBackend';
import * as versionFront from './versionFront';
import * as wechatCode from './wechatCode';

(window as any).API = {
  adminSysLog,
  adminUser,
  asyncTask,
  auth,
  backendAftersale,
  backendOrder,
  businessTypeBackend,
  carBrandBackend,
  carBrandFront,
  carTypeBackend,
  carTypeFront,
  cardLabelBackend,
  cart,
  cartItem,
  cashier,
  categoryBackend,
  commonBackend,
  commonFront,
  countryArea,
  coupon,
  couponBackend,
  couponPublishBatch,
  couponTemplate,
  dictBackend,
  dictFront,
  frontOrder,
  mallBackend,
  mallFront,
  memberBenefitsBackend,
  memberBenefitsFront,
  memberLabelBackend,
  pageFront,
  pageHomeBackend,
  pageRecommendBackend,
  pageRecommendFront,
  pageTextFront,
  pageTextServiceBackend,
  pageTextTopicBackend,
  pay,
  paymentCode,
  paymentCodeBackend,
  permission,
  point,
  productApproval,
  productBrandManagement,
  productFront,
  productLabelBackend,
  productManagementBackend,
  productMktBackend,
  productRecycleBackend,
  productSalesBackend,
  productStock,
  propertyBackend,
  propertyFront,
  qrCode,
  rechargeReasonBackend,
  role,
  scoreBackend,
  searchKeywordBackend,
  searchKeywordFront,
  serviceAssuranceBackend,
  serviceAssuranceFront,
  shortLink,
  singleSignOn,
  sms,
  stock,
  supplier,
  supplierBackend,
  user,
  userFront,
  userJob,
  versionBackend,
  versionFront,
  wechatCode,
};
