export default {
  path: 'activityProductSearch',
  name: 'marketing.activityProductSearch',
  component: () => import('./list/index.vue'),
  meta: {
    locale: '商品营销',
    sort: 2,
  },
};
