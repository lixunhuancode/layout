export default [
  {
    path: 'storeQuery',
    name: 'inventory.storeQuery',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '库存查询',
      sort: 3,
    },
  },
];
