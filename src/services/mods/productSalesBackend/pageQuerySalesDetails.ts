/**
   * @desc 分页查询销售明细
分页查询销售明细
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryProductSalesDTO;

export const init = new defs.ProductSalesVO();

export type ParamsAndBody = { data: defs.QueryProductSalesDTO };

export function request(
  data: defs.QueryProductSalesDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/productSales/pageQuerySalesDetails`, {
    data,
    method: 'POST',
    ...options,
  });
}
