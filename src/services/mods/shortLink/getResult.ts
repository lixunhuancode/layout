/**
 * @desc getResult
 */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = '';

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`link/result`, {
    method: 'GET',
    ...options,
  });
}
