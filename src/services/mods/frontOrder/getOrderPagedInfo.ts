/**
   * @desc 用户订单列表查询
用户订单列表查询
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.OrderListQueryDTO;

export const init = new defs.OrderDetailInfoVO();

export type ParamsAndBody = { data: defs.OrderListQueryDTO };

export function request(
  data: defs.OrderListQueryDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`front/order/getOrderPagedInfo`, {
    data,
    method: 'POST',
    ...options,
  });
}
