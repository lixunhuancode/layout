export default [
  {
    path: 'version/list',
    name: 'setting.version.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '版本管理',
      requiresAuth: false,
    },
  },
];
