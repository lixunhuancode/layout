export default [
  {
    path: 'savings/list',
    name: 'finance.savings-settlement.list',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '储值结算',
    },
  },
  {
    path: 'savings/add',
    name: 'finance.savings-settlement.add',
    component: () => import('./addOrEdit/index.vue'),
    meta: {
      locale: '创建结算单',
      hideInMenu: true,
    },
  },
  {
    path: 'savings/detail',
    name: 'finance.savings-settlement.detail',
    component: () => import('./detail/index.vue'),
    meta: {
      locale: '查看',
      hideInMenu: true,
    },
  },
];
