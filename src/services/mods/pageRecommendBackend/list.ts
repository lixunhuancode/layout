/**
 * @desc 获取页面入口推荐列表(个人中心)
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryPageRecommendDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.QueryPageRecommendDTO };

export function request(
  data: defs.QueryPageRecommendDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/cms/pageRecommend/list`, {
    data,
    method: 'POST',
    ...options,
  });
}
