/**
 * @desc 添加会员权益
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.MemberBenefitsDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.MemberBenefitsDTO };

export function request(
  data: defs.MemberBenefitsDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/cms/memberBenefits/create`, {
    data,
    method: 'POST',
    ...options,
  });
}
