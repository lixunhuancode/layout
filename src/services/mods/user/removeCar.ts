/**
   * @desc 删除车辆信息
删除车辆信息
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** id */
  id: number;
}

export const init = false;

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`webapi/user/removeCar/${params.id}`, {
    params,
    method: 'GET',
    ...options,
  });
}
