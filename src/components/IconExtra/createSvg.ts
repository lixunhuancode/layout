import {
  defineComponent,
  createElementBlock,
  normalizeClass,
  normalizeStyle,
  computed,
  createElementVNode,
} from 'vue';

const prefixCls = 'arco-icon';
const opt = Object.prototype.toString;
const isNumber = (obj: any) => {
  return opt.call(obj) === '[object Number]';
};

type IPath = {
  tag: string;
  path: {
    [key: string]: unknown;
  };
};

export default (name: string, child: IPath[]) => {
  return defineComponent({
    name,
    props: {
      size: {
        type: [Number, String],
      },
      strokeWidth: {
        type: Number,
        default: 4,
      },
      strokeLinecap: {
        type: String,
        default: 'butt',
        validator: (value: string) => {
          return ['butt', 'round', 'square'].includes(value);
        },
      },
      strokeLinejoin: {
        type: String,
        default: 'miter',
        validator: (value: string) => {
          return ['arcs', 'bevel', 'miter', 'miter-clip', 'round'].includes(
            value
          );
        },
      },
      rotate: Number,
      spin: Boolean,
    },
    setup(props) {
      const cls = computed(() => [
        prefixCls,
        name,
        { [`${prefixCls}-spin`]: props.spin },
      ]);

      const innerStyle = computed(() => {
        const styles: { fontSize?: any; transform?: any } = {};
        if (props.size) {
          styles.fontSize = isNumber(props.size)
            ? `${props.size}px`
            : props.size;
        }
        if (props.rotate) {
          styles.transform = `rotate(${props.rotate}deg)`;
        }
        return styles;
      });
      return {
        cls,
        innerStyle,
      };
    },
    render(ctx: any) {
      const pathDefault = {
        fill: 'currentColor',
        stroke: 'none',
      };
      return createElementBlock(
        'svg',
        {
          'viewBox': '0 0 1024 1024',
          'fill': 'currentColor',
          'xmlns': 'http://www.w3.org/2000/svg',
          'stroke': 'currentColor',
          'class': normalizeClass(ctx.cls),
          'style': normalizeStyle(ctx.innerStyle),
          'stroke-width': ctx.strokeWidth,
          'stroke-linecap': ctx.strokeLinecap,
          'stroke-linejoin': ctx.strokeLinejoin,
        },
        child.map((item: IPath) => {
          return createElementVNode(
            item.tag,
            { ...pathDefault, ...item.path },
            null,
            -1
          );
        }),
        14,
        ['stroke-width', 'stroke-linecap', 'stroke-linejoin']
      );
    },
  });
};
