/**
 * @desc 添加
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.BaseCarTypeDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.BaseCarTypeDTO };

export function request(
  data: defs.BaseCarTypeDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/carType/add`, {
    data,
    method: 'POST',
    ...options,
  });
}
