export default {
  path: '/permission',
  name: 'permission',
  component: () => import('./index.vue'),
  meta: {
    title: '权限',
    hideInMenu: true,
  },
};
