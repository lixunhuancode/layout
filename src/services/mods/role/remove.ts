/**
 * @desc 删除
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** id */
  id: number;
}

export const init = new defs.ResponseModel();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`admin/role/delete/${params.id}`, {
    params,
    method: 'DELETE',
    ...options,
  });
}
