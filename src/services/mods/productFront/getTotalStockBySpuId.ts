/**
   * @desc 根据spuId获取商品总库存K:spuId V:总库存
根据spuId获取商品总库存K:spuId V:总库存
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = Array<string>;

export const init = undefined;

export type ParamsAndBody = { data: Array<string> };

export function request(data: Array<string>, options: AxiosRequestConfig) {
  return axiosHandle(`front/product/getTotalStockBySpuId`, {
    data,
    method: 'POST',
    ...options,
  });
}
