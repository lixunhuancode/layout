/**
   * @desc 分页查询商品信息
分页查询商品信息
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryProductDTO;

export const init = new defs.ProductWithSkuVO();

export type ParamsAndBody = { data: defs.QueryProductDTO };

export function request(
  data: defs.QueryProductDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/productManagement/pageQueryProduct`, {
    data,
    method: 'POST',
    ...options,
  });
}
