/**
   * @desc 批量设置安全库存
批量设置安全库存
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** 安全库存 */
  safeStock?: number;
}

export type RequestBody = Array<string>;

export const init = false;

export type ParamsAndBody = { params: Params; data: Array<string> };

export function request(
  { params, data }: { params: Params; data: Array<string> },
  options: AxiosRequestConfig,
) {
  return axiosHandle(
    `backend/productStock/batchSetSafeStock/${params?.safeStock}`,
    {
      params,
      data,
      method: 'POST',
      ...options,
    },
  );
}
