export default {
  path: 'level',
  name: 'member.level',
  component: () => import('@/components/routeView.vue'),
  meta: {
    locale: '会员等级',
    icon: 'icon-c-info',
    sort: 2,
  },
  children: [
    {
      path: 'list',
      name: 'member.level.list',
      component: () => import('./list/index.vue'),
      meta: {
        locale: '等级设置',
        sort: 1,
      },
    },
    {
      path: 'edit/:id',
      name: 'member.level.edit',
      component: () => import('./edit/index.vue'),
      meta: {
        locale: '编辑等级',
        hideInMenu: true,
      },
    },
    {
      path: 'setting',
      name: 'member.level.setting',
      component: () => import('./setting/index.vue'),
      meta: {
        sort: 2,
        locale: '成长值规则',
      },
    },
    {
      path: 'record',
      name: 'member.level.record',
      component: () => import('./record/index.vue'),
      meta: {
        sort: 3,
        locale: '成长值明细',
      },
    },
  ],
};
