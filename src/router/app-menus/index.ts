import { useUserStore } from '@/store';
import { appRoutes, appExternalRoutes } from '../routes';

// 递归匹配有权限的路由
const filterTreeArray = (tree: any, bList: any) => {
  console.log({ bList });
  return tree
    .filter((item: any) => {
      return bList.some((pms: any) => item.name === pms.key);
    })
    .map((item: any) => {
      item = { ...item };
      const obj = bList.filter((pms: any) => item.name === pms.key);
      if (obj.length) {
        item.sort = obj[0].sort;
      }

      if (item.children) {
        item.children = filterTreeArray(item.children, bList);
      }
      return item;
    });
};

// 递归排序
const sortTree = (tree: any) => {
  const list = tree.sort((a1: any, a2: any) => {
    const sort1 = a1.meta.sort || 999;
    const sort2 = a2.meta.sort || 999;
    return sort1 - sort2;
  });
  list.map((item: any) => {
    if (item.children) {
      item.children = sortTree(item.children);
    }
    return item;
  });
  return list;
};

const { userinfo } = useUserStore();
// 权限过滤

console.log({ sdfasdfaL: userinfo });
// const routerList = filterTreeArray(appRoutes, userinfo.permissions);
const routerList = appRoutes;
// 对菜单树排序
const sortList = sortTree(routerList);
const mixinRoutes = [...sortList, ...appExternalRoutes];

const appMenus = mixinRoutes.map((el) => {
  const { name, path, meta, redirect, children } = el;
  return {
    name,
    path,
    meta,
    redirect,
    children,
  };
});
const appClientMenus = () => {
  return appMenus;
};
export default appClientMenus;
