/**
 * @desc 当前登录用户信息
 */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = new defs.ResponseModel();

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`admin/auth/me`, {
    method: 'GET',
    ...options,
  });
}
