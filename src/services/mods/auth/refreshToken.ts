/**
 * @desc 刷新token
 */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = new defs.ResponseModel();

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`admin/auth/refresh/token`, {
    method: 'POST',
    ...options,
  });
}
