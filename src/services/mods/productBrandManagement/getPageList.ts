/**
   * @desc 分页查询品牌列表
分页查询品牌列表
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryBrandDTO;

export const init = new defs.ProductBrandVO();

export type ParamsAndBody = { data: defs.QueryBrandDTO };

export function request(data: defs.QueryBrandDTO, options: AxiosRequestConfig) {
  return axiosHandle(`backend/brand/getPageList`, {
    data,
    method: 'POST',
    ...options,
  });
}
