import { defineStore } from 'pinia';
import cloneDeep from 'lodash/cloneDeep';
import staticDict from './staticDict';
import type { DictState, DictMap, DictOption } from './types';

export default defineStore('dictionary', {
  state: (): DictState => ({
    codes: [...staticDict.codes],
    dict: { ...staticDict.dict },
  }),
  getters: {
    getData(state) {
      return (code: string): DictOption[] => {
        return cloneDeep(state.dict[code]?.data || []);
      };
    },
    getDictValItem(state) {
      return (code: string, value: string | number | undefined) => {
        if (!value && value !== 0) {
          return {} as DictOption;
        }
        return state.dict[code]?.valMap[value];
      };
    },
    getLabel() {
      return (code: string, value?: string | number): string | undefined => {
        if (!value && value !== 0) {
          return '';
        }
        const item = this.getDictValItem(code, value);
        return item?.label as string;
      };
    },
  },
  actions: {
    async query(payload: string[]) {
      const list: DictMap = {};
      let reqCodes: string[] = [];
      const codes = [...this.codes];
      try {
        if (!Array.isArray(payload)) {
          throw new Error('字典查询参数错误！');
        }

        reqCodes = payload.filter((code: string) => {
          if (!codes.includes(code)) {
            return true;
          }
          return false;
        });

        if (!reqCodes.length) {
          return;
        }

        reqCodes.forEach((code) => {
          list[code] = {
            loading: true,
            data: [],
            code,
            name: '',
            valMap: {},
          };
        });

        this.$patch((state) => {
          state.dict = { ...state.dict, ...list };
        });

        const res = await API.dictBackend.getListByCodes.request(reqCodes);

        res.data = res.data || [];

        res.data.forEach((item) => {
          if (item.code && item.dictDetailVOS && item.dictDetailVOS.length) {
            const code = item.code as string;
            codes.push(code);
            list[code].loading = false;
            list[code].name = item.name as string;
            item.dictDetailVOS.forEach((op) => {
              op.label = op.label as string;
              op.value = op.value as string;
              const dictOp = { ...op, value: op.value, label: op.label };

              list[code].data.push({ ...dictOp });
              list[code].valMap[dictOp.value] = { ...dictOp };
            });
          }
        });

        this.$patch((state) => {
          state.dict = { ...state.dict, ...list };
          state.codes = codes;
        });
      } catch (e) {
        console.warn(e);
        reqCodes.forEach((code) => {
          list[code].loading = false;
        });
      }
    },
  },
});
