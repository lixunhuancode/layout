declare namespace ListLayout {
  type SearchForm = Record<string, any>;
  type Column = TableColumnData & { hide?: false };
  type SizeProps = 'mini' | 'small' | 'medium' | 'large';
  type listInstance = ComponentInternalInstance & { emitter?: any };
}
