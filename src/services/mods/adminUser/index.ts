/**
 * @description 后端用户管理
 */
import * as add from './add';
import * as remove from './remove';
import * as detail from './detail';
import * as list from './list';
import * as resetPassword from './resetPassword';
import * as update from './update';

export { add, remove, detail, list, resetPassword, update };
