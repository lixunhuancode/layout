/**
 * @desc 获取支付方式列表
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** orderNo */
  orderNo?: string;
}

export const init = new defs.ResponseModel();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`front/cashier/listPayWay`, {
    params,
    method: 'GET',
    ...options,
  });
}
