export default {
  name: '测试',
  code: 'order',
  resource: [
    'order',
    'order.management',
    'order.management.list',
    'order.after-sale.list',
    'order.management.detail',
    'order.after-sale.detail',
    'order.performance',
    'order.deliver-goods.list',
    'order.intra-city-service.list',
    'order.pick-order-store.list',
    'order.deliver-goods.detail',
    'order.pick-order-store.detail',
  ],
};
