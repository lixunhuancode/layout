/**
 * @description 商品管理后台接口【商品库存】
 */
import * as batchSetSafeStock from './batchSetSafeStock';
import * as batchUpdateStock from './batchUpdateStock';
import * as chooseSkuSetStock from './chooseSkuSetStock';
import * as getNoStockTotal from './getNoStockTotal';
import * as getSkuTotal from './getSkuTotal';
import * as importSkuSetStock from './importSkuSetStock';
import * as pageQueryBatchManageStock from './pageQueryBatchManageStock';
import * as pageQuerySkuList from './pageQuerySkuList';
import * as pageQueryStockRecords from './pageQueryStockRecords';
import * as pageQueryStockWarn from './pageQueryStockWarn';
import * as setSafeStock from './setSafeStock';
import * as syncExportSkuList from './syncExportSkuList';
import * as sendMsg from './sendMsg';
import * as updateAllSkuCacheStock from './updateAllSkuCacheStock';
import * as updateCacheStock from './updateCacheStock';
import * as updateStatus from './updateStatus';

export {
  batchSetSafeStock,
  batchUpdateStock,
  chooseSkuSetStock,
  getNoStockTotal,
  getSkuTotal,
  importSkuSetStock,
  pageQueryBatchManageStock,
  pageQuerySkuList,
  pageQueryStockRecords,
  pageQueryStockWarn,
  setSafeStock,
  syncExportSkuList,
  sendMsg,
  updateAllSkuCacheStock,
  updateCacheStock,
  updateStatus,
};
