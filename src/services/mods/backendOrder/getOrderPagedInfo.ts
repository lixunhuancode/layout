/**
   * @desc 分页获取订单列表
分页获取订单列表
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.OrderListQueryDTO;

export const init = undefined;

export type ParamsAndBody = { data: defs.OrderListQueryDTO };

export function request(
  data: defs.OrderListQueryDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/order/getOrderPagedInfo`, {
    data,
    method: 'POST',
    ...options,
  });
}
