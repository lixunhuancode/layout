/**
 * @desc 上传文件到公网访问桶
 */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = new defs.ResponseModel();

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`backend/cms/common/uploadFile`, {
    method: 'POST',
    ...options,
  });
}
