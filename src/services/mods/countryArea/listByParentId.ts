/**
 * @desc 通过parentId获取地址列表，parentId为空返回省级列表
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** parentId */
  parentId?: number;
}

export const init = new defs.ResponseModel();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`backend/countryArea/list`, {
    params,
    method: 'GET',
    ...options,
  });
}
