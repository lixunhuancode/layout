export default [
  {
    path: 'management/list',
    name: 'products.management.list',
    component: () => import('@/views/Products/Management/List/index.vue'),
    meta: {
      locale: '商品管理',
      requiresAuth: false,
    },
  },
  {
    path: 'management/form',
    name: 'products.management.add',
    component: () => import('@/views/Products/Management/Form/index.vue'),
    meta: {
      hideInMenu: true,
      locale: '新增商品',
      requiresAuth: false,
    },
  },
  {
    path: 'management/form/:id',
    name: 'products.management.edit',
    component: () => import('@/views/Products/Management/Form/index.vue'),
    meta: {
      hideInMenu: true,
      locale: '编辑商品',
      requiresAuth: false,
    },
  },
  {
    path: 'management/detail/:id',
    name: 'products.management.detail',
    component: () => import('@/views/Products/Management/Detail/index.vue'),
    meta: {
      hideInMenu: true,
      locale: '商品详情',
    },
  },
];
