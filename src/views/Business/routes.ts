import { DEFAULT_LAYOUT } from '@/router/routes/base';
import Supplier from './Supplier/routes';
import MoneyReceived from './MoneyReceived/routes';

export default {
  path: '/business',
  name: 'business',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '商户',
    requiresAuth: false,
    icon: 'icon-c-business',
    order: 1,
  },
  children: [...Supplier, ...MoneyReceived],
};
