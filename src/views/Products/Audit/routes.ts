export default [
  {
    path: 'audit/list',
    name: 'products.audit.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '审核管理',
    },
  },
  {
    path: 'management/detail/:id',
    name: 'products.audit.detail',
    component: () => import('@/views/Products/Management/Detail/index.vue'),
    meta: {
      hideInMenu: true,
      locale: '商品详情',
    },
  },
];
