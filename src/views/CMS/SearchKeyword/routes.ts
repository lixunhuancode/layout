export default [
  {
    path: 'search-keyword/list',
    name: 'cms.search-keyword.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '搜索管理',
    },
  },
];
