type ObjectMap<Key extends string | number | symbol = any, Value = any> = {
  [key in Key]: Value;
};

declare namespace defs {
  export class AdminAsyncTask {
    /** 业务数据内容json串 */
    businessContent?: string;

    /** createTime */
    createTime?: string;

    /** 文件名称 */
    fileName?: string;

    /** 处理后文件路径地址 */
    fileUrl?: string;

    /** finishTime */
    finishTime?: string;

    /** 业务请求header */
    header?: string;

    /** 唯一id */
    id?: number;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 备注 */
    remark?: string;

    /** 处理状态（0：待处理，1：处理中，2：已完成，3：失败） */
    status?: number;

    /** 任务名称 */
    taskName?: string;

    /** 任务类型 */
    taskType?: string;

    /** 上传文件路径地址 */
    uploadFileUrl?: string;

    /** 是否删除（1 ：正常 ，-1： 删除） */
    validity?: number;
  }

  export class AdminSysLog {
    /** 管理员ID */
    adminId?: number;

    /** 管理员 */
    adminName?: string;

    /** 发生时间 */
    createTime?: string;

    /** id */
    id?: number;

    /** IP地址 */
    ip?: string;

    /** 操作的功能名称 */
    operationName?: string;

    /** 操作标签方便归类查询 */
    operationTag?: string;

    /** 请求参数 */
    requestParams?: string;

    /** 操作url */
    requestUrl?: string;

    /** 操作结果 */
    result?: string;
  }

  export class AdminSysLogQueryDTO {
    /** 用户名 */
    adminName?: string;

    /** 操作时间 */
    createEndTime?: string;

    /** 操作时间 */
    createStartTime?: string;

    /** 页码(>=1) */
    pageIndex?: number;

    /** pageSize */
    pageSize?: number;
  }

  export class AdminUserDTO {
    /** 数据权限 */
    dataResources?: Array<defs.AdminUserDataResourceDTO>;

    /** 是否拥有全部数据权限 1是 0 否 */
    hasAllDataResource?: number;

    /** 用户名id，修改时必须 */
    id?: number;

    /** 姓名 */
    name?: string;

    /** 密码 */
    password?: string;

    /** 备注 */
    remark?: string;

    /** 角色 */
    roleIds?: Array<number>;

    /** 1启用 2禁用 */
    status?: number;

    /** 管理员类型1超管 2普通管理员 */
    type?: number;

    /** 用户名 */
    username?: string;
  }

  export class AdminUserDataResourceDTO {
    /** 资源唯一标识 */
    resource?: string;

    /** 资源名称 */
    resourceName?: string;

    /** 资源类型：1、供应商 */
    resourceType?: number;
  }

  export class AdminUserQueryDTO {
    /** 姓名 */
    name?: string;

    /** 页码(>=1) */
    pageIndex?: number;

    /** pageSize */
    pageSize?: number;

    /** 更新时间 */
    updateEndTime?: string;

    /** 更新人 */
    updateName?: string;

    /** 更新时间 */
    updateStartTime?: string;

    /** 用户名 */
    username?: string;
  }

  export class AdminUserVO {
    /** 创建人 */
    createName?: string;

    /** 添加时间 */
    createTime?: string;

    /** 数据权限 */
    dataResources?: Array<defs.AdminUserDataResourceDTO>;

    /** 是否拥有全部数据权限 1是 0 否 */
    hasAllDataResource?: number;

    /** id */
    id?: number;

    /** 登录时间 */
    loginAt?: string;

    /** 姓名 */
    name?: string;

    /** 权限项不包含数据权限 */
    permissions?: Array<defs.Permission>;

    /** 备注 */
    remark?: string;

    /** 角色 */
    roles?: Array<defs.RoleVO>;

    /** 1启用 2禁用 */
    status?: number;

    /** 管理员类型1超管 2普通管理员 */
    type?: number;

    /** 更新人 */
    updateName?: string;

    /** 修改时间 */
    updateTime?: string;

    /** 用户名 */
    username?: string;
  }

  export class AftersaleApplyDTO {
    /** 售后类型(1:退货退款 2:仅退款) */
    aftersaleType?: number;

    /** 退货原因类型(1:不喜欢/买错了 2:不合适(尺码型号不合适) 3:发错货(颜色/尺码/型号不对) 4:缺少件(缺少相关附件) 5:质量问题 6:其他) */
    applyReasonType?: number;

    /** 订单商品行id */
    orderDetailIds?: Array<number>;

    /** 订单id */
    orderId?: number;

    /** 备注 */
    remark?: string;

    /** 退回方式(1:快递退回 2:退回门店 3: 无需退回) */
    returnType?: number;
  }

  export class AftersaleAuditDTO {
    /** 审核备注 */
    auditRemark?: string;

    /** 审核结果(1:通过 2:驳回) */
    auditResult?: number;

    /** 售后单id集合 */
    ids?: Array<number>;
  }

  export class AftersaleDetailInfoVO {
    /** 售后数量 */
    aftersaleCount?: number;

    /** 售后单号 */
    aftersaleNo?: string;

    /** 售后收货地址 */
    aftersaleReceiveAddr?: string;

    /** 售后收货人 */
    aftersaleReceiveName?: string;

    /** 售后收货联系电话 */
    aftersaleReceivePhone?: string;

    /** 售后状态码(20000：待审核 20040：已完成(审核通过) 20050：已关闭(审核驳回)) */
    aftersaleStatusCode?: number;

    /** 售后状态名称 */
    aftersaleStatusName?: string;

    /** 售后类型(1：退货退款 2：仅退款) */
    aftersaleType?: number;

    /** 售后申请日期 */
    applyDate?: string;

    /** 申请原因类型描述 */
    applyReasonDesc?: string;

    /** 申请原因备注 */
    applyReasonRemark?: string;

    /** 申请原因类型((1:不喜欢/买错了 2:不合适(尺码型号不合适) 3:发错货(颜色/尺码/型号不对) 4:缺少件(缺少相关附件) 5:质量问题 6:其他)) */
    applyReasonType?: number;

    /** 售后申请时间 */
    applyTime?: string;

    /** 售后单审核人id */
    auditId?: number;

    /** 售后单审核人 */
    auditName?: string;

    /** 售后单审核备注 */
    auditRemark?: string;

    /** 售后单审核时间 */
    auditTime?: string;

    /** 审核关闭原因 */
    closeReason?: string;

    /** 审核关闭时间 */
    closeTime?: string;

    /** 售后单创建人id */
    createdId?: number;

    /** 售后单创建人 */
    createdName?: string;

    /** 配送方式 */
    deliveryType?: number;

    /** 运费 */
    fare?: number;

    /** 主键id */
    id?: number;

    /** 用户id */
    memberId?: number;

    /** 用户名 */
    memberName?: string;

    /** 用户手机号 */
    memberPhone?: string;

    /** 操作时间 */
    operTime?: string;

    /** 售后商品信息 */
    orderAftersaleDetailInfos?: Array<defs.OrderAftersaleDetailInfoVO>;

    /** 订单id */
    orderId?: number;

    /** 下单时间 */
    orderTime?: string;

    /** 订单类型 */
    orderType?: number;

    /** 订单号 */
    orgOrderNo?: string;

    /** 应付金额 */
    payAmount?: number;

    /** 应付积分 */
    payPoints?: number;

    /** 支付方式 */
    payType?: number;

    /** 收货/提货地址 */
    receiveAddr?: string;

    /** 收/提货人 */
    receiveName?: string;

    /** 收/提货人电话 */
    receivePhone?: string;

    /** 收货时间 */
    receiveTime?: string;

    /** 售后单退款状态(0:未发起 1:退款中 2:退款失败 3:退款成功 4:线下已退款) */
    refundStatus?: number;

    /** 售后单退款时间 */
    refundTime?: string;

    /** 售后备注 */
    remark?: string;

    /** 退货金额 */
    returnAmount?: number;

    /** 退货积分 */
    returnPoints?: number;

    /** 退回方式(1：快递退回 2：送回门店 3：无需退回) */
    returnType?: number;

    /** 供应商id */
    supplierId?: number;

    /** 供应商名称 */
    supplierName?: string;

    /** 供应商简称 */
    supplierShortName?: string;
  }

  export class AftersaleListQueryDTO {
    /** 售后单号 */
    aftersaleNo?: string;

    /** 售后状态(20000:待审核 20040：已完成(审核通过) 20050：已关闭(审核驳回)) */
    aftersaleStatusCodes?: Array<number>;

    /** 售后类型(1:退货退款 2:仅退款) */
    aftersaleType?: number;

    /** 售后原因类型 */
    applyReasonType?: number;

    /** 申请结束时间 */
    applyTimeEnd?: string;

    /** 申请开始时间 */
    applyTimeStart?: string;

    /** 审核人 */
    auditName?: string;

    /** 审核结束时间 */
    auditTimeEnd?: string;

    /** 审核开始时间 */
    auditTimeStart?: string;

    /** 发起人 */
    createdName?: string;

    /** 用户姓名 */
    memberName?: string;

    /** 手机号 */
    memberPhone?: string;

    /** 订单号 */
    orgOrderNo?: string;

    /** 当前页 */
    pageIndex?: number;

    /** 每页大小 */
    pageSize?: number;

    /** 供应商id列表 */
    supplierIds?: Array<number>;
  }

  export class AsyncTaskDTO {
    /** 业务数据内容json串 */
    businessContent?: string;

    /** 业务请求header */
    header?: string;

    /** 任务类型("SUPPLIER_LIST_EXPORT:供应商列表导出","ORDER_LIST_EXPORT:订单列表导出","AFTERSALE_LIST_EXPORT:售后列表导出","SALEDETAIL_LIST_EXPORT:交易明细列表导出","SALESTATISTICS_LIST_EXPORT:交易统计列表导出") */
    taskType?:
      | 'SUPPLIER_LIST_EXPORT'
      | 'SUPPLIER_LIST_IMPORT'
      | 'SUPPLIER_PAYMENT_CODE_EXPORT'
      | 'COUPON_LIST_EXPORT'
      | 'COUPON_PUBLISH_LIST_EXPORT'
      | 'PRODUCT_LIST_EXPORT'
      | 'PRODUCT_RECYCLE_LIST_EXPORT'
      | 'PRODUCT_SALES_LIST_EXPORT'
      | 'PRODUCT_SALES_DETAILS_LIST_EXPORT'
      | 'STOCK_RECORDS_LIST_EXPORT'
      | 'STOCK_WARN_LIST_EXPORT'
      | 'ORDER_LIST_EXPORT'
      | 'AFTERSALE_LIST_EXPORT'
      | 'SALEDETAIL_LIST_EXPORT'
      | 'SALESTATISTICS_LIST_EXPORT'
      | 'CUSTOMER_LIST_EXPORT'
      | 'CUSTOMER_IMPORT'
      | 'CUSTOMER_RECHARGE_LIST_EXPORT'
      | 'CUSTOMER_RECHARGE_REPORT_EXPORT'
      | 'CUSTOMER_CAR_LIST_EXPORT'
      | 'CUSTOMER_CAR_IMPORT';

    /** 上传文件路径地址(导入任务使用) */
    uploadFileUrl?: string;
  }

  export class AuditRechargeDTO {
    /** 审批备注 */
    auditRemark?: string;

    /** 1通过,2驳回 */
    auditStatus?: number;

    /** 批次号 */
    batchNo?: number;
  }

  export class BaseBusinessTypeDTO {
    /** createTime */
    createTime?: string;

    /** 主键ID */
    id?: number;

    /** 业务类型名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 备注 */
    remark?: string;

    /** 是否删除 -1:删除 1:正常 */
    validity?: number;
  }

  export class BaseBusinessTypeVO {
    /** createTime */
    createTime?: string;

    /** 主键ID */
    id?: number;

    /** 业务类型名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 备注 */
    remark?: string;

    /** 是否删除 -1:删除 1:正常 */
    validity?: number;
  }

  export class BaseCarBrandDTO {
    /** createTime */
    createTime?: string;

    /** 图标url */
    icon?: string;

    /** 主键ID */
    id?: number;

    /** 名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 备注 */
    remark?: string;

    /** 是否删除 -1:删除 1:正常 */
    validity?: number;
  }

  export class BaseCarBrandVO {
    /** 车型列表 */
    carTypeList?: Array<defs.BaseCarTypeObject>;

    /** createTime */
    createTime?: string;

    /** 图标url */
    icon?: string;

    /** 主键ID */
    id?: number;

    /** 名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 备注 */
    remark?: string;

    /** 是否删除 -1:删除 1:正常 */
    validity?: number;
  }

  export class BaseCarTypeDTO {
    /** 车型品牌ID */
    brandId?: number;

    /** createTime */
    createTime?: string;

    /** 主键ID */
    id?: number;

    /** 名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 备注 */
    remark?: string;

    /** 是否删除 -1:删除 1:正常 */
    validity?: number;
  }

  export class BaseCarTypeObject {
    /** 车型品牌ID */
    brandId?: number;

    /** createTime */
    createTime?: string;

    /** 主键ID */
    id?: number;

    /** 名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 备注 */
    remark?: string;

    /** 是否删除 -1:删除 1:正常 */
    validity?: number;
  }

  export class BaseCarTypeVO {
    /** 车型品牌ID */
    brandId?: number;

    /** 车型品牌 */
    brandName?: string;

    /** createTime */
    createTime?: string;

    /** icon */
    icon?: string;

    /** 主键ID */
    id?: number;

    /** 名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 备注 */
    remark?: string;

    /** 是否删除 -1:删除 1:正常 */
    validity?: number;
  }

  export class BaseCardLabelDTO {
    /** createTime */
    createTime?: string;

    /** 主键ID */
    id?: number;

    /** 名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 备注 */
    remark?: string;

    /** 是否删除 -1:删除 1:正常 */
    validity?: boolean;
  }

  export class BaseCardLabelVO {
    /** createTime */
    createTime?: string;

    /** 主键ID */
    id?: number;

    /** 名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 备注 */
    remark?: string;

    /** 是否删除 -1:删除 1:正常 */
    validity?: boolean;
  }

  export class BaseDTO {
    /** 开始时间 */
    beginTime?: string;

    /** 结束时间 */
    endTime?: string;

    /** 排序方式 */
    orderBy?: string;

    /** 页码 */
    pageIndex?: number;

    /** 页面大小 */
    pageSize?: number;

    /** 排序字段 */
    sortBy?: string;
  }

  export class BaseMemberLabelDTO {
    /** createTime */
    createTime?: string;

    /** 主键ID */
    id?: number;

    /** 名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 备注 */
    remark?: string;

    /** 是否删除 -1:删除 1:正常 */
    validity?: number;
  }

  export class BaseMemberLabelVO {
    /** createTime */
    createTime?: string;

    /** 主键ID */
    id?: number;

    /** 名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 备注 */
    remark?: string;

    /** 是否删除 -1:删除 1:正常 */
    validity?: number;
  }

  export class BaseRechargeReasonDTO {
    /** code */
    code?: string;

    /** createTime */
    createTime?: string;

    /** 主键id */
    id?: number;

    /** 名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;
  }

  export class BaseRechargeReasonVO {
    /** code */
    code?: string;

    /** createTime */
    createTime?: string;

    /** 主键id */
    id?: number;

    /** 名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;
  }

  export class BaseRefundRequest {
    /** 订单号 */
    orderNo?: string;

    /** 支付系统业务流水号 */
    payNo?: string;

    /** 退款金额,不支持部分退款 */
    refundAmount?: number;

    /** 退款订单号 */
    refundOrderNo?: string;

    /** 退款积分 */
    refundPoints?: number;

    /** 退款原因 */
    refundReason?: string;
  }

  export class CarListVO {
    /** 购买时间 */
    buyTime?: string;

    /** 车系 */
    carModel?: number;

    /** 车系 */
    carModelName?: string;

    /** 行驶证图片 */
    driveLicense?: string;

    /** 行驶里程数 */
    driveMileage?: string;

    /** 发动机号 */
    engineNo?: string;

    /**  车架号 */
    frameNo?: string;

    /** id */
    id?: number;

    /** 所属用户 */
    owner?: string;

    /** 用户手机号 */
    phone?: string;

    /** 车牌 */
    plate?: string;

    /** 车牌号-市 */
    plateCity?: string;

    /** 车牌号 */
    plateNo?: string;

    /** 车牌号-省 */
    plateProvince?: string;

    /** 备注 */
    remark?: string;

    /** 赠送原因 */
    sendReason?: string;

    /** 用户id */
    userId?: number;

    /** 用户姓名 */
    userName?: string;
  }

  export class CartItemDTO {
    /** 商品数量 */
    count?: number;

    /** SKU ID */
    skuId?: string;

    /** 供应商id */
    supplierId?: string;
  }

  export class CartItemProductDto {
    /** 数 */
    count?: number;

    /** skuId */
    skuId?: string;
  }

  export class CartItemVO {
    /** cartId */
    cartId?: number;

    /** count */
    count?: number;

    /** createTime */
    createTime?: string;

    /** itemId */
    itemId?: number;

    /** operTime */
    operTime?: string;

    /** price */
    price?: number;

    /** 规格名称 */
    propertyValueName?: string;

    /** 上下架状态 0: 下架 1: 上架 2: 发布中(审核中) 3: 待上架 */
    putOnShelves?: number;

    /** score */
    score?: number;

    /** skuId */
    skuId?: string;

    /** spuId */
    spuId?: string;

    /** spu名称 */
    spuName?: string;

    /** 商品spu图片地址(封面) */
    spuPictureUrl?: string;

    /** stock */
    stock?: number;

    /** supplierId */
    supplierId?: string;

    /** 供应商名称 */
    supplierName?: string;

    /** 商品行总价 */
    totalPrice?: number;

    /** 商品行积分 */
    totalScore?: number;

    /** 商品状态 1:正常 0:回收站 -1:删除 */
    validity?: number;
  }

  export class CartPriceVO {
    /** 总价 */
    totalPrice?: number;

    /** 积分 */
    totalScore?: number;
  }

  export class CartVO {
    /** 购物车id */
    cartId?: number;

    /** 购物车商品列表 */
    cartItemVOList?: Array<defs.CartItemVO>;

    /** 总件数 */
    totalCount?: number;

    /** 用户唯一ID */
    uniqueId?: string;
  }

  export class CashierPayDto {
    /** 订单号 */
    orderNo?: string;

    /** 支付密码 */
    password?: string;

    /** payAmount */
    payAmount?: number;

    /** payPoints */
    payPoints?: number;

    /** 支付方式 */
    payWay?: number;

    /** 收款码名称 */
    rcvQrCodeName?: string;

    /** supplierId */
    supplierId?: string;

    /** 交易类型 1APP支付 2用户扫二维码支付 3扫用户付款码支付 */
    tradeType?: number;
  }

  export class CategoryDTO {
    /** 品类编码 */
    categoryCode?: string;

    /** 品类名称 */
    categoryName?: string;

    /** 上级品类编码 */
    parentCode?: string;

    /** 品类备注 */
    remark?: string;
  }

  export class CategoryVO {
    /** 品类编码 */
    categoryCode?: string;

    /** 品类名称 */
    categoryName?: string;

    /** 子品类 */
    child?: Array<defs.CategoryVO>;

    /** 创建时间 */
    createTime?: string;

    /** 是否已关联营销分类 1:是 0:否 */
    existMkt?: number;

    /** 品类层级 */
    level?: number;

    /** 操作时间 */
    operTime?: string;

    /** 父级品类编码 */
    parentCode?: string;

    /** 商品数量 */
    productCount?: number;

    /** 备注 */
    remark?: string;
  }

  export class ChangePasswordDTO {
    /** 用户id */
    id?: number;

    /** 密码 */
    newPassword?: string;

    /** 密码 */
    password?: string;
  }

  export class ChangeStatusDTO {
    /** 主键ID */
    id?: number;

    /** 状态 0:禁用 1:启用 */
    status?: 'ENABLED' | 'DISABLED';
  }

  export class CmsPageItem {
    /** 结束时间 */
    endTime?: string;

    /** 模块配置内容标识 */
    itemCode?: string;

    /** 模块配置内容数据 */
    itemData?: string;

    /** 模块配置内容ID */
    itemId?: number;

    /** 模块配置内容顺序 */
    itemSort?: number;

    /** 模块标识 */
    moduleCode?: string;

    /** 模块id */
    moduleId?: number;

    /** 模块标签 商品product 图文image 导航nav 活动activity */
    moduleTag?: string;

    /** 页面ID */
    pageId?: number;

    /** 页面版本号 */
    pageVersionId?: number;

    /** 开始时间 */
    startTime?: string;
  }

  export class CountryAreaObject {
    /** code */
    code?: string;

    /** depth */
    depth?: number;

    /** id */
    id?: number;

    /** initial */
    initial?: string;

    /** latitude */
    latitude?: string;

    /** longitude */
    longitude?: string;

    /** name */
    name?: string;

    /** parentId */
    parentId?: number;

    /** parentName */
    parentName?: string;

    /** rootId */
    rootId?: number;

    /** zipcode */
    zipcode?: string;
  }

  export class CouponObject {
    /** 失效日期 */
    activeEndTime?: string;

    /** 消费时间 */
    consumeTime?: string;

    /** 优惠券编码 */
    couponCode?: number;

    /** customerId */
    customerId?: number;

    /** 用户手姓名 */
    customerName?: string;

    /** 用户手机号 */
    customerPhone?: string;

    /** 使用说明 */
    description?: string;

    /** id */
    id?: number;

    /** 卡券名称 */
    name?: string;

    /** 发放批次id */
    publishBatchId?: number;

    /** 领取时间 */
    receiveTime?: string;

    /** 优惠券状态AVAILABLE可用、CONSUMED已核销、EXPIRED已过期 DISCARD已废弃 */
    status?: string;

    /** 供应商id */
    supplierId?: number;

    /** 供应商名称 */
    supplierName?: string;

    /** 券模板id */
    templateId?: number;

    /** updateId */
    updateId?: number;

    /** updateTime */
    updateTime?: string;

    /** 核销码 */
    verifyCode?: string;

    /** 核销人姓名 */
    verifyName?: string;

    /** 核销备注 */
    verifyRemark?: string;

    /** 核销方式 1.后端核销 2.小程序核销 */
    verifyType?: number;

    /** 核销员账号 */
    verifyUsername?: string;
  }

  export class CouponPublishBatchAuditDTO {
    /** 审核意见 */
    auditRemark?: string;

    /** 0待审核1通过2驳回 */
    auditStatus?: number;

    /** 发放批次id */
    id?: number;
  }

  export class CouponPublishBatchCustomerDTO {
    /** 姓名 */
    name?: string;

    /** 手机号 */
    phone?: string;
  }

  export class CouponPublishBatchDTO {
    /** 卡券 */
    couponTemplates?: Array<defs.CouponPublishBatchTemplateDTO>;

    /** 用户 */
    customers?: Array<defs.CouponPublishBatchCustomerDTO>;

    /** 上传附件 */
    files?: Array<defs.CouponPublishBatchFileDTO>;

    /** 批次id 修改时需要 */
    id?: number;

    /** 备注 */
    remark?: string;

    /** 标题 */
    title?: string;
  }

  export class CouponPublishBatchFileDTO {
    /** 文件名 */
    name?: string;

    /** 文件url */
    url?: string;
  }

  export class CouponPublishBatchListVO {
    /** 审核人名字 */
    auditName?: string;

    /** 0待审核1通过2驳回 */
    auditStatus?: number;

    /** 审核时间 */
    auditTime?: string;

    /** 创建人名字 */
    createName?: string;

    /** 创建时间 */
    createTime?: string;

    /** 人数 */
    headcount?: number;

    /** id */
    id?: number;

    /** 标题 */
    title?: string;
  }

  export class CouponPublishBatchObject {
    /** 审核人id */
    auditId?: number;

    /** 审核人名字 */
    auditName?: string;

    /** 审核意见 */
    auditRemark?: string;

    /** 0待审核1通过2驳回 */
    auditStatus?: number;

    /** 审核时间 */
    auditTime?: string;

    /** 卡券 */
    couponTemplates?: Array<defs.CouponPublishBatchTemplateVO>;

    /** 创建人id */
    createId?: number;

    /** 创建人名字 */
    createName?: string;

    /** 创建时间 */
    createTime?: string;

    /** 用户 */
    customers?: Array<defs.CouponPublishBatchCustomerDTO>;

    /** 上传附件 json数组 */
    files?: Array<defs.CouponPublishBatchFileDTO>;

    /** 上传附件 json数组 */
    filesJson?: string;

    /** 人数 */
    headcount?: number;

    /** id */
    id?: number;

    /** 发放优惠券数量 */
    publishQuantity?: number;

    /** 0待发放 1发放成功 2发放失败 */
    publishStatus?: number;

    /** 备注 */
    remark?: string;

    /** 标题 */
    title?: string;

    /** updateId */
    updateId?: number;

    /** updateTime */
    updateTime?: string;
  }

  export class CouponPublishBatchQueryDTO {
    /** 审核时间止 */
    auditEndTime?: string;

    /** 审核时间起 */
    auditStartTime?: string;

    /** 0待审核1通过2驳回 */
    auditStatus?: number;

    /** 创建时间止 */
    createEndTime?: string;

    /** 创建时间起 */
    createStartTime?: string;

    /** id */
    id?: number;

    /** 页码(>=1) */
    pageIndex?: number;

    /** pageSize */
    pageSize?: number;

    /** 标题 */
    title?: string;
  }

  export class CouponPublishBatchTemplateDTO {
    /** 发放数量 */
    quantity?: number;

    /** 券模板id */
    templateId?: number;
  }

  export class CouponPublishBatchTemplateVO {
    /** 有效期类型1.领取后*个月内有效 2.领取后*天内有效 */
    activeDateType?: number;

    /** 参见类型，例如当类型为1时存1表示1一个月内有效，类型为2时表示一天内有效 */
    activeDateValue?: number;

    /** 卡券名称 */
    name?: string;

    /** 发放数量 */
    quantity?: number;

    /** 供应商id */
    supplierId?: number;

    /** 供应商名称 */
    supplierName?: string;

    /** 券模板id */
    templateId?: number;
  }

  export class CouponQueryDTO {
    /** 消费时间止 */
    consumeEndTime?: string;

    /** 消费时间起 */
    consumeStartTime?: string;

    /** 优惠券编码 */
    couponCode?: number;

    /** 用户手姓名 */
    customerName?: string;

    /** 用户手机号 */
    customerPhone?: string;

    /** 卡券名称 */
    name?: string;

    /** 页码(>=1) */
    pageIndex?: number;

    /** pageSize */
    pageSize?: number;

    /** 发放时间止 */
    publishEndTime?: string;

    /** 发放时间起 */
    publishStartTime?: string;

    /** 优惠券状态AVAILABLE可用、CONSUMED已核销、EXPIRED已过期 DISCARD已废弃 */
    status?: string;

    /** 供应商ids */
    supplierIds?: Array<number>;

    /** 券模板id */
    templateId?: number;

    /** 核销方式 1.后端核销 2.小程序核销 */
    verifyType?: number;
  }

  export class CouponTemplateDTO {
    /** 有效期类型1.领取后*个月内有效 2.领取后*天内有效 */
    activeDateType?: number;

    /** 参见类型，例如当类型为1时存1表示1一个月内有效，类型为2时表示一天内有效 */
    activeDateValue?: number;

    /** 使用说明 */
    description?: string;

    /** 券模板id 修改时需要 */
    id?: number;

    /** 卡券名称 */
    name?: string;

    /** 状态0启用 1禁用 */
    status?: number;

    /** 供应商id */
    supplierId?: number;

    /** 供应商名称 */
    supplierName?: string;

    /** 优惠券标签id */
    tags?: Array<number>;

    /** 卡券类型 1、兑换券 */
    type?: number;
  }

  export class CouponTemplateQueryDTO {
    /** 创建时间止 */
    createEndTime?: string;

    /** 创建时间起 */
    createStartTime?: string;

    /** 卡券id */
    id?: number;

    /** 卡券名称 */
    name?: string;

    /** 页码(>=1) */
    pageIndex?: number;

    /** pageSize */
    pageSize?: number;

    /** 状态0启用 1禁用 */
    status?: number;

    /** 供应商id */
    supplierId?: number;

    /** 标签id */
    tagIds?: Array<number>;

    /** 卡券类型 1、兑换券 */
    type?: number;
  }

  export class CouponTemplateVO {
    /** 有效期类型1.领取后*个月内有效 2.领取后*天内有效 */
    activeDateType?: number;

    /** 参见类型，例如当类型为1时存1表示1一个月内有效，类型为2时表示一天内有效 */
    activeDateValue?: number;

    /** createId */
    createId?: number;

    /** createName */
    createName?: string;

    /** createTime */
    createTime?: string;

    /** 使用说明 */
    description?: string;

    /** 券模板id 修改时需要 */
    id?: number;

    /** 卡券名称 */
    name?: string;

    /** 下发数量 */
    publishQuantity?: number;

    /** 状态0启用 1禁用 */
    status?: number;

    /** 供应商id */
    supplierId?: number;

    /** 供应商名称 */
    supplierName?: string;

    /** 优惠券标签id */
    tags?: Array<number>;

    /** 卡券类型 1、兑换券 */
    type?: number;

    /** updateId */
    updateId?: number;

    /** updateTime */
    updateTime?: string;
  }

  export class CouponVerifyDTO {
    /** 优惠券编码 */
    couponCode?: number;

    /** 核销码 */
    verifyCode?: string;

    /** 核销备注 */
    verifyRemark?: string;
  }

  export class CreateOrUpdateLabelDTO {
    /** 标签code */
    labelCode?: string;

    /** 标签名称 */
    labelName?: string;

    /** 图标地址 */
    labelPictureUrl?: string;

    /** 备注 */
    remark?: string;
  }

  export class CreateOrUpdateMktDTO {
    /** 分类编码列表 */
    categoryCodeList?: Array<string>;

    /** 营销分类Id */
    id?: number;

    /** 营销分类编码 */
    mktCode?: string;

    /** 分类名称 */
    mktName?: string;

    /** 图标地址 */
    mktPictureUrl?: string;

    /** 排序 */
    mktSort?: number;

    /** 父类编码 */
    parentMktCode?: string;

    /** 父类名称 */
    parentMktName?: string;

    /** 备注 */
    remark?: string;
  }

  export class CreateOrUpdateProductDTO {
    /** 品牌id */
    brandId?: number;

    /** 品类编码 */
    categoryCode?: string;

    /** 配送方式 0:无需配送 1:快递 2:自提 3:快递或自提 */
    deliveryType?: number;

    /** 商品描述 */
    description?: string;

    /** 详细参数 */
    detailedParametersDTOList?: Array<defs.DetailedParametersDTO>;

    /** 商品标签列表 */
    labelCodeList?: Array<string>;

    /** 商品图片列表 */
    pictureUrlList?: Array<string>;

    /** 商品sku信息 */
    productSkuDTOList?: Array<defs.ProductSkuDTO>;

    /** 商品备注 */
    remark?: string;

    /** 服务保障列表 */
    serviceCodeList?: Array<string>;

    /** spuId */
    spuId?: string;

    /** spu名称 */
    spuName?: string;

    /** 商品spu图片地址(封面) */
    spuPictureUrl?: string;

    /** 商品视屏地址 */
    spuViewUrl?: string;

    /** 供应商编码 */
    supplierId?: string;

    /** 供应商名称 */
    supplierName?: string;

    /** 供应商简称 */
    supplierShortName?: string;
  }

  export class CreateQRCodeDTO {
    /** 参数 */
    params?: object;

    /** 跳转地址 */
    path?: string;

    /** 二维码的宽度，单位 px。最小 280px，最大 1280px(默认430) */
    width?: number;
  }

  export class CreateRechargeBatchDTO {
    /** 附件（逗号分隔） */
    files?: string;

    /** 充值账号及金额 */
    rechargeAccount?: Array<defs.RechargeAccount>;

    /** 充值描述 */
    rechargeReason?: string;

    /** 备注 */
    remark?: string;

    /** 标题 */
    title?: string;

    /** 类型:1充值,2赠送 */
    type?: number;
  }

  export class CreateWechatQRCodeDTO {
    /** 参数 */
    params?: object;

    /** 小程序页面链接 */
    path?: string;

    /** 二维码的宽度，单位 px。最小 280px，最大 1280px(默认430) */
    width?: number;
  }

  export class DetailedParameterVO {
    /** 参数名称 */
    parametersName?: string;

    /** 参数值 */
    parametersValue?: string;
  }

  export class DetailedParametersDTO {
    /** 参数名称 */
    parametersName?: string;

    /** 参数值 */
    parametersValue?: string;
  }

  export class DiscardDTO {
    /** 优惠券编码 */
    couponCode?: number;
  }

  export class File {
    /** absolute */
    absolute?: boolean;

    /** absoluteFile */
    absoluteFile?: defs.File;

    /** absolutePath */
    absolutePath?: string;

    /** canonicalFile */
    canonicalFile?: defs.File;

    /** canonicalPath */
    canonicalPath?: string;

    /** directory */
    directory?: boolean;

    /** executable */
    executable?: boolean;

    /** file */
    file?: boolean;

    /** freeSpace */
    freeSpace?: number;

    /** hidden */
    hidden?: boolean;

    /** lastModified */
    lastModified?: number;

    /** name */
    name?: string;

    /** parent */
    parent?: string;

    /** parentFile */
    parentFile?: defs.File;

    /** path */
    path?: string;

    /** readable */
    readable?: boolean;

    /** totalSpace */
    totalSpace?: number;

    /** usableSpace */
    usableSpace?: number;

    /** writable */
    writable?: boolean;
  }

  export class GetchemeDTO {
    /** 通过scheme码进入的小程序页面路径，必须是已经发布的小程序存在的页面，不可携带query。path为空时会跳转小程序主页。 */
    path?: string;

    /** 通过scheme码进入小程序时的query，最大1024个字符，只支持数字，大小写英文以及部分特殊字符：!#$&'()*+,/:;=?@-._~ */
    query?: string;
  }

  export class GoodsInfoDTO {
    /** 数量 */
    count?: number;

    /** 商品skuid */
    skuId?: string;
  }

  export class IdDTO {
    /** ID */
    id?: number;
  }

  export class IdLongDTO {
    /** ID */
    id?: number;
  }

  export class InputStream {}

  export class LoginDTO {
    /** 密码 */
    password?: string;

    /** 用户名 */
    username?: string;
  }

  export class Map<T0 = any, T1 = any> {}

  export class MemberBenefitsBasicVO {
    /** 描述 */
    benefitDesc?: string;

    /** 权益名称 */
    benefitName?: string;

    /** 图标 */
    icon?: string;

    /** ID */
    id?: number;

    /** 等级编码 */
    levelCode?: string;

    /** 排序 */
    sort?: number;
  }

  export class MemberBenefitsDTO {
    /** 描述 */
    benefitDesc?: string;

    /** 权益名称 */
    benefitName?: string;

    /** 图标 */
    icon?: string;

    /** ID */
    id?: number;
  }

  export class MemberBenefitsVO {
    /** 描述 */
    benefitDesc?: string;

    /** 权益名称 */
    benefitName?: string;

    /** 创建时间 */
    createTime?: string;

    /** 图标 */
    icon?: string;

    /** ID */
    id?: number;

    /** 等级编码 */
    levelCode?: string;

    /** 操作人ID */
    operId?: number;

    /** 操作人 */
    operName?: string;

    /** 更新时间 */
    operTime?: string;

    /** 排序 */
    sort?: number;
  }

  export class MiniAppBindMobileDTO {
    /** encryptedData */
    encryptedData?: string;

    /** iv */
    iv?: string;

    /** openId */
    openId?: string;

    /** 微信获取手机号code */
    phoneCode?: string;
  }

  export class ModifyCarDTO {
    /** 购买时间 */
    buyTime?: string;

    /** 车品牌 */
    carBrand?: number;

    /** 车系 */
    carModel?: number;

    /** 行驶证图片 */
    driveLicense?: string;

    /** 行驶里程数 */
    driveMileage?: string;

    /** 发动机号 */
    engineNo?: string;

    /** 车架号 */
    frameNo?: string;

    /** id,更新传 */
    id?: number;

    /** 车牌号-市 */
    plateCity?: string;

    /** 车牌号 */
    plateNo?: string;

    /** 车牌号-省 */
    plateProvince?: string;

    /** 备注 */
    remark?: string;

    /** 赠送币 */
    sendPoint?: number;

    /** 赠送原因 */
    sendReason?: string;

    /** 车辆所属用户id */
    userId?: number;
  }

  export class ModifyRechargeBatchDTO {
    /** 附件（逗号分隔） */
    files?: string;

    /** id */
    id?: number;

    /** 充值账号及金额 */
    rechargeAccount?: Array<defs.RechargeAccount>;

    /** 充值描述 */
    rechargeReason?: string;

    /** 备注 */
    remark?: string;

    /** 标题 */
    title?: string;

    /** 类型:1充值,2赠送 */
    type?: number;
  }

  export class ModifyUserDTO {
    /** 生日 */
    birthday?: string;

    /** 车辆信息 */
    carInfo?: Array<defs.ModifyCarDTO>;

    /** id,更新传 */
    id?: number;

    /**  身份证号 */
    identNo?: string;

    /** 会员等级:general普通会员，senior高级会员 */
    memberLevel?: string;

    /** 手机号 */
    phone?: string;

    /** 备注 */
    remark?: string;

    /** 性别:F男，M女 */
    sex?: string;

    /** 状态,1启用，2禁用 */
    status?: number;

    /** 标签(逗号分隔) */
    tags?: string;

    /** 真实姓名 */
    userName?: string;
  }

  export class ModifyUserPhoneDTO {
    /** 新手机号 */
    newPhone?: string;

    /** 短信验证码 */
    smsCode?: string;

    /** 用户id */
    userId?: number;
  }

  export class MyCouponListVO {
    /** 失效日期 */
    activeEndTime?: string;

    /** 使用说明 */
    description?: string;

    /** 卡券名称 */
    name?: string;

    /** 发放批次id */
    publishBatchId?: number;

    /** 数量 */
    quantity?: number;

    /** 券模板id */
    templateId?: number;

    /** 优惠券状态AVAILABLE可用、CONSUMED已核销、INVALID已失效（已过期+废弃） */
    viewStatus?: string;
  }

  export class MyCouponQueryDTO {
    /** 页码(>=1) */
    pageIndex?: number;

    /** pageSize */
    pageSize?: number;

    /** 优惠券状态AVAILABLE可用、CONSUMED已核销、INVALID已失效（已过期+废弃） */
    viewStatus?: string;
  }

  export class OrderAftersaleDetailInfoVO {
    /** 订单商品购买数量 */
    count?: number;

    /** 订单商品行优惠金额 */
    discountAmount?: number;

    /** 订单商品一级品类id */
    firstCategoryId?: string;

    /** 售后商品详情id */
    id?: number;

    /** 订单商品主图图片地址 */
    mainPictureUrl?: string;

    /** 订单商品详情id */
    orderDetailId?: number;

    /** 订单商品id */
    productId?: string;

    /** 订单商品名称 */
    productName?: string;

    /** 退货商品金额 */
    returnAmount?: number;

    /** 退货商品数量 */
    returnCount?: number;

    /** 退货商品积分 */
    returnPoints?: number;

    /** 订单商品行金额 */
    saleAmount?: number;

    /** 订单商品行积分单价 */
    salePoint?: number;

    /** 订单商品行积分总额 */
    salePoints?: number;

    /** 订单商品销售单价 */
    salePrice?: number;

    /** 订单商品skuid */
    skuId?: string;

    /** 订单商品sku名称 */
    skuName?: string;

    /** 订单商品行uuid */
    uuid?: string;
  }

  export class OrderCancelDTO {
    /** 取消原因 */
    cancelReason?: string;

    /** 订单id */
    id?: number;
  }

  export class OrderCreateDTO {
    /** 渠道来源 */
    channelSource?: string;

    /** 收货/提货地址市编码 */
    cityCode?: string;

    /** 收货/提货地址区/县编码 */
    countyCode?: string;

    /** 配送类型 0：无需配送 1：快递 2：自提 */
    deliveryType?: number;

    /** 是否购物车下单 0：否(直接购买) 1：是 */
    isCart?: number;

    /** 下单商品列表 */
    products?: Array<defs.GoodsInfoDTO>;

    /** 收货/提货地址省编码 */
    provinceCode?: string;

    /** 收货/自提地址 */
    receiveAddr?: string;

    /** 收货/提货人 */
    receiveName?: string;

    /** 收货/提货联系电话 */
    receivePhone?: string;

    /** 备注 */
    remark?: string;

    /** 收货/提货地址街道编码(四级地址编码) */
    streetCode?: string;

    /** 供应商id */
    supplierId?: number;
  }

  export class OrderDetailDTO {
    /** sku购买数量 */
    count?: number;

    /** sku id */
    skuId?: string;

    /** sku名称 */
    skuName?: string;

    /** 商品id */
    spuId?: string;

    /** 商品名称 */
    spuName?: string;
  }

  export class OrderDetailInfoVO {
    /** 订单后台状态名称 */
    backendStatusName?: string;

    /** 取消原因 */
    cancelReason?: string;

    /** 取消退款状态 */
    cancelRefundStatus?: number;

    /** 取消时间 */
    cancelTime?: string;

    /** 下单终端 */
    channelSource?: string;

    /** 发货时间 */
    deliveryTime?: string;

    /** 配送方式(0:无需配送 1:快递 2:自提) */
    deliveryType?: number;

    /** 优惠总金额=商品优惠总金额+运费优惠总金额 */
    discountAmount?: number;

    /** 运费 */
    fare?: number;

    /** 运费优惠总金额 */
    fareDiscountAmount?: number;

    /** 订单前台状态名称 */
    frontStatusName?: string;

    /** 主键id */
    id?: number;

    /** 是否已评价(0:未评价 1:已评价) */
    isComment?: number;

    /** 收货/提货地址纬度 */
    latitude?: string;

    /** 收货/提货地址经度 */
    longitude?: string;

    /** 用户id */
    memberId?: number;

    /** 用户名 */
    memberName?: string;

    /** 用户手机号 */
    memberPhone?: string;

    /** 订单总计金额=订单商品总计金额+运费 */
    orderAmount?: number;

    /** 下单日期 */
    orderDate?: string;

    /** 订单确认状态(0:) */
    orderStatus?: number;

    /** 下单时间 */
    orderTime?: string;

    /** 订单类型(1:商城订单 2:收款码订单) */
    orderType?: number;

    /** 订单号 */
    orgOrderNo?: string;

    /** 第三方订单号 */
    orgThirdNo?: string;

    /** 订单商品原价总计金额 */
    originalAmount?: number;

    /** 父订单号 */
    pOrgOrderNo?: string;

    /** 订单应付金额=订单总计金额-优惠总金额 */
    payAmount?: number;

    /** 订单应付积分 */
    payPoints?: number;

    /** 支付时间 */
    payTime?: string;

    /** 支付方式(1:积分 2:微信) */
    payType?: number;

    /** 商品优惠总金额 */
    productDiscountAmount?: number;

    /** 订单商品信息 */
    products?: Array<defs.OrderDetailVO>;

    /** 收/提货地址 */
    receiveAddr?: string;

    /** 收/提货地址编码 */
    receiveAddrCode?: string;

    /** 收货截止时间 */
    receiveEndTime?: string;

    /** 收/提货人 */
    receiveName?: string;

    /** 收/提货人电话 */
    receivePhone?: string;

    /** 收货时间 */
    receiveTime?: string;

    /** 备注 */
    remark?: string;

    /** 订单商品总计金额 */
    saleAmount?: number;

    /** 订单状态码 */
    statusCode?: number;

    /** 供应商id */
    supplierId?: number;

    /** 供应商名称 */
    supplierName?: string;

    /** 供应商名称简称 */
    supplierShortName?: string;

    /** 支付超时时间(秒) */
    termOfValidity?: number;
  }

  export class OrderDetailVO {
    /** 售后状态(0:未售后 1:售后中 2:售后完成) */
    aftersaleStatus?: number;

    /** 品牌id */
    brandId?: string;

    /** 品牌名称 */
    brandName?: string;

    /** 品类id */
    categoryId?: string;

    /** 品类名称 */
    categoryName?: string;

    /** 购买数量 */
    count?: number;

    /** 商品行优惠券明细 */
    couponCodeDetail?: string;

    /** 商品行优惠金额 */
    discountAmount?: number;

    /** 商品行优惠明细 */
    discountAmountDetail?: string;

    /** 一级品类id */
    firstCategoryId?: string;

    /** 一级品类名称 */
    firstCategoryName?: string;

    /** 订单详情id */
    id?: number;

    /** 是否已评价 */
    isComment?: number;

    /** 商品主图 */
    mainPictureUrl?: string;

    /** 订单id */
    orderId?: number;

    /** 商品行原始总金额=商品原始单价*购买数量 */
    originalAmount?: number;

    /** 商品原始单价 */
    originalPrice?: number;

    /** 商品id */
    productId?: string;

    /** 名称 */
    productName?: string;

    /** 已售后数量 */
    returnCount?: number;

    /** 商品行销售总金额=商品销售单价*购买数量 */
    saleAmount?: number;

    /** 单商品积分 */
    salePoint?: number;

    /** 商品行积分=单商品积分*购买数量 */
    salePoints?: number;

    /** 商品销售单价 */
    salePrice?: number;

    /** skuid */
    skuId?: string;

    /** sku名称 */
    skuName?: string;

    /** uuid */
    uuid?: string;

    /** 核销渠道(1:小程序核销 2:后台核销) */
    verifyChannel?: number;

    /** 核销码 */
    verifyCode?: string;

    /** 核销人id */
    verifyId?: number;

    /** 核销人 */
    verifyName?: string;

    /** 核销备注 */
    verifyRemark?: number;

    /** 核销状态(0:未核销 1:已核销 2:已取消 3:无需核销) */
    verifyStatus?: number;

    /** 核销时间 */
    verifyTime?: string;

    /** 核销类型(1:核销码核销 2:强制核销) */
    verifyType?: number;
  }

  export class OrderInfoDTO {
    /** 订单号 */
    orderNo?: string;

    /** 商品行 */
    productInfos?: Array<defs.OrderDetailDTO>;

    /** 供应商id */
    supplierId?: string;
  }

  export class OrderItem {
    /** asc */
    asc?: boolean;

    /** column */
    column?: string;
  }

  export class OrderListQueryDTO {
    /** 配送方式 */
    deliveryType?: number;

    /** 用户id */
    memberId?: number;

    /** 用户名 */
    memberName?: string;

    /** 手机号 */
    memberPhone?: string;

    /** 是否需要屏蔽敏感信息 */
    needShieldSensitiveInfo?: boolean;

    /** 订单结束时间 */
    orderTimeEnd?: string;

    /** 订单开始时间 */
    orderTimeStart?: string;

    /** 订单类型 */
    orderType?: number;

    /** 订单号 */
    orgOrderNo?: string;

    /** 订单号列表 */
    orgOrderNos?: Array<string>;

    /** 当前页 */
    pageIndex?: number;

    /** 每页数据大小 */
    pageSize?: number;

    /** 订单状态 */
    statusCodes?: Array<number>;

    /** 供应商id列表 */
    supplierIds?: Array<number>;
  }

  export class OrderStatusCountVO {
    /** 已完成订单数 */
    completeCount?: number;

    /** 待支付订单数 */
    waitPayCount?: number;

    /** 待自提订单数 */
    waitSelfDeliveryCount?: number;
  }

  export class OrderTraceVO {
    /** 订单后台状态名称 */
    backendStatusName?: string;

    /** 订单前台状态名称 */
    frontStatusName?: string;

    /** 主键id */
    id?: number;

    /** 操作时间 */
    operTime?: string;

    /** 操作人id */
    operatorId?: number;

    /** 操作人姓名 */
    operatorName?: string;

    /** 操作人电话 */
    operatorPhone?: string;

    /** 操作人类型（1：前台用户 2：后台用户 3：系统） */
    operatorType?: number;

    /** 订单id */
    orderId?: number;

    /** 操作描述 */
    orderTraceLog?: string;

    /** 订单状态码 */
    statusCode?: number;
  }

  export class Page<T0 = any> {
    /** countId */
    countId?: string;

    /** current */
    current?: number;

    /** hitCount */
    hitCount?: boolean;

    /** maxLimit */
    maxLimit?: number;

    /** optimizeCountSql */
    optimizeCountSql?: boolean;

    /** orders */
    orders?: Array<defs.OrderItem>;

    /** pages */
    pages?: number;

    /** records */
    records: Array<T0>;

    /** searchCount */
    searchCount?: boolean;

    /** size */
    size?: number;

    /** total */
    total?: number;
  }

  export class PageContentDTO {
    /** 页面背景色 */
    backgroundColor?: string;

    /** 页面背景图片 */
    backgroundImage?: string;

    /** 页面封面图片 */
    cover?: string;

    /** 页面模块数据 */
    items?: Array<defs.PageItemDTO>;

    /** 页面ID */
    pageId?: number;

    /** 页面名称 */
    title?: string;

    /** 页面顶部图标 */
    topIcon?: string;
  }

  export class PageDTO {
    /** 背景色 */
    backgroundColor?: string;

    /** 背景图片 */
    backgroundImage?: string;

    /** 页面封面图片 */
    cover?: string;

    /** ID */
    id?: number;

    /** 描述 */
    pageDesc?: string;

    /** 标题 */
    title?: string;

    /** 页面顶部图标 */
    topIcon?: string;
  }

  export class PageIdDTO {
    /** ID */
    id?: number;
  }

  export class PageItemBasicVO {
    /** 结束时间 */
    endTime?: string;

    /** 模块配置内容标识 */
    itemCode?: string;

    /** 模块配置内容数据 */
    itemData?: string;

    /** 模块配置内容ID */
    itemId?: number;

    /** 模块标识 */
    moduleCode?: string;

    /** 模块标签 商品product 图文image 导航nav 活动activity */
    moduleTag?: string;

    /** 开始时间 */
    startTime?: string;
  }

  export class PageItemDTO {
    /** 结束时间 */
    endTime?: string;

    /** 模块内容标识 */
    itemCode?: string;

    /** 模块内容数据 */
    itemData?: string;

    /** 模块内容ID */
    itemId?: number;

    /** 模块内容顺序 */
    itemSort?: number;

    /** 模块标识 */
    moduleCode?: string;

    /** 模块ID */
    moduleId?: number;

    /** 模块标签 商品product 图文image 导航nav 活动activity */
    moduleTag?: string;

    /** 开始时间 */
    startTime?: string;
  }

  export class PageRecommendBasicVO {
    /** 图标 */
    icon?: string;

    /** ID */
    id?: number;

    /** 位置编码 */
    locationCode?: string;

    /** 路径 */
    path?: string;

    /** 排序 */
    sort?: number;

    /** 启用状态 1启用 0禁用 */
    status?: number;

    /** 入口名称 */
    title?: string;
  }

  export class PageRecommendDTO {
    /** 图标 */
    icon?: string;

    /** ID */
    id?: number;

    /** 位置编码 */
    locationCode?: string;

    /** 路径 */
    path?: string;

    /** 排序 */
    sort?: number;

    /** 启用状态 1启用 0禁用 */
    status?: number;

    /** 名称 */
    title?: string;
  }

  export class PageRecommendVO {
    /** 创建时间 */
    createTime?: string;

    /** 图标 */
    icon?: string;

    /** ID */
    id?: number;

    /** 位置编码 */
    locationCode?: string;

    /** 操作人ID */
    operId?: number;

    /** 操作人 */
    operName?: string;

    /** 更新时间 */
    operTime?: string;

    /** 路径 */
    path?: string;

    /** 排序 */
    sort?: number;

    /** 启用状态 1启用 0禁用 */
    status?: number;

    /** 入口名称 */
    title?: string;
  }

  export class PageTextDTO {
    /** 内容 */
    content?: string;

    /** ID */
    id?: number;

    /** 链接 */
    linkUrl?: string;

    /** 页面类型 1富文本 2小程序 */
    pageType?: number;

    /** 短链接 */
    shortUrl?: string;

    /** 标题 */
    title?: string;
  }

  export class PageTextVO {
    /** 小程序appId */
    appId?: string;

    /** 自定义内容 */
    content?: string;

    /** 创建时间 */
    createTime?: string;

    /** 页面地址 */
    hrefUrl?: string;

    /** 内容 */
    html?: string;

    /** ID */
    id?: number;

    /** 是否删除 0否,1是 */
    isDel?: number;

    /** 链接 */
    linkUrl?: string;

    /** 操作人ID */
    operId?: number;

    /** 操作人 */
    operName?: string;

    /** 更新时间 */
    operTime?: string;

    /** 页面编码 price价格说明 score如何获取积分 privac隐私说明 */
    pageCode?: string;

    /** 页面类型 1富文本页 2小程序 */
    pageType?: number;

    /** 模板内容 */
    resource?: string;

    /** 短链接 */
    shortUrl?: string;

    /** 页面模板标识 home首页 topic专题 service服务 */
    templateCode?: string;

    /** 页面模板ID */
    templateId?: number;

    /** 页面模板类型 page配置页面 text富文本页 */
    templateType?: string;

    /** 标题 */
    title?: string;
  }

  export class PageVO {
    /** 审核状态 1审核通过 0审核不通过 */
    auditStatus?: number;

    /** 背景色 */
    backgroundColor?: string;

    /** 背景图片 */
    backgroundImage?: string;

    /** 页面封面图片 */
    cover?: string;

    /** 创建时间 */
    createTime?: string;

    /** ID */
    id?: number;

    /** 是否删除 0否,1是 */
    isDel?: number;

    /** 发布上线时间 */
    onlineTime?: string;

    /** 操作人ID */
    operId?: number;

    /** 操作人 */
    operName?: string;

    /** 更新时间 */
    operTime?: string;

    /** 描述 */
    pageDesc?: string;

    /** 页面版本 */
    pageVersionId?: number;

    /** 页面状态： 1待提交审核  2待审核 3审核通过 4审核不通过 */
    status?: number;

    /** 页面模板标识 home首页 topic专题 service服务 */
    templateCode?: string;

    /** 页面模板ID */
    templateId?: number;

    /** 页面模板类型 page配置页面 text富文本页 */
    templateType?: string;

    /** 标题 */
    title?: string;

    /** 页面顶部图标 */
    topIcon?: string;
  }

  export class PageWithItemsBasicVO {
    /** 背景色 */
    backgroundColor?: string;

    /** 背景图片 */
    backgroundImage?: string;

    /** 页面封面图片 */
    cover?: string;

    /** ID */
    id?: number;

    /** pageItems */
    pageItems?: Array<defs.PageItemBasicVO>;

    /** 页面版本 */
    pageVersionId?: number;

    /** 页面模板标识 home首页 topic专题 service服务 */
    templateCode?: string;

    /** 页面模板类型 page配置页面 text富文本页 */
    templateType?: string;

    /** 标题 */
    title?: string;

    /** 页面顶部图标 */
    topIcon?: string;
  }

  export class PageWithItemsVO {
    /** 审核状态 1审核通过 0审核不通过 */
    auditStatus?: number;

    /** 背景色 */
    backgroundColor?: string;

    /** 背景图片 */
    backgroundImage?: string;

    /** 页面封面图片 */
    cover?: string;

    /** 创建时间 */
    createTime?: string;

    /** ID */
    id?: number;

    /** 是否删除 0否,1是 */
    isDel?: number;

    /** 发布上线时间 */
    onlineTime?: string;

    /** 操作人ID */
    operId?: number;

    /** 操作人 */
    operName?: string;

    /** 更新时间 */
    operTime?: string;

    /** 描述 */
    pageDesc?: string;

    /** pageItems */
    pageItems?: Array<defs.CmsPageItem>;

    /** 页面版本 */
    pageVersionId?: number;

    /** 页面状态： 1待提交审核  2待审核 3审核通过 4审核不通过 */
    status?: number;

    /** 页面模板标识 home首页 topic专题 service服务 */
    templateCode?: string;

    /** 页面模板ID */
    templateId?: number;

    /** 页面模板类型 page配置页面 text富文本页 */
    templateType?: string;

    /** 标题 */
    title?: string;

    /** 页面顶部图标 */
    topIcon?: string;
  }

  export class Pagination<T0 = any> {
    /** 总页数 */
    pageCount?: number;

    /** 当前页 */
    pageIndex?: number;

    /** 每页记录数 */
    pageSize?: number;

    /** 总记录数 */
    recordCount?: number;

    /** 当前页的集合 */
    result: Array<T0>;
  }

  export class PaymentCodeDTOObject {
    /** 收款地点 */
    address?: string;

    /** 收款码CODE */
    code?: string;

    /** 创建时间 */
    createTime?: string;

    /** 主键 */
    id?: number;

    /** 收款码名称 */
    name?: string;

    /** 操作人id */
    operId?: number;

    /** 操作人名称 */
    operName?: string;

    /** 操作时间 */
    operTime?: string;

    /** 备注 */
    remark?: string;

    /** 收款码图片地址 */
    resUrl?: string;

    /** 状态 0:禁用 1:启用 */
    status?: number;

    /** 供应商ID */
    supplierId?: number;

    /** 是否删除 -1:删除 1:正常 */
    validity?: number;
  }

  export class PaymentCodeVOObject {
    /** 收款地点 */
    address?: string;

    /** 收款码CODE */
    code?: string;

    /** 创建时间 */
    createTime?: string;

    /** 主键 */
    id?: number;

    /** 收款码名称 */
    name?: string;

    /** 操作人id */
    operId?: number;

    /** 操作人名称 */
    operName?: string;

    /** 操作时间 */
    operTime?: string;

    /** 备注 */
    remark?: string;

    /** 收款码图片地址 */
    resUrl?: string;

    /** 状态 0:禁用 1:启用 */
    status?: number;

    /** 编码 */
    supplierCode?: string;

    /** 供应商ID */
    supplierId?: number;

    /** 供应商名称 */
    supplierName?: string;

    /** 供应商简称 */
    supplierShortName?: string;

    /** 是否删除 -1:删除 1:正常 */
    validity?: number;
  }

  export class PaymentVO {
    /** 余额 */
    balance?: number;

    /** 支付方式描述 */
    description?: string;

    /** 不可用原因 */
    disableReason?: string;

    /** 图标 */
    icon?: string;

    /** 支付方式名称 */
    name?: string;

    /** 支付方式 1积分 */
    payWay?: number;

    /** 是否置灰标识 */
    showFlag?: boolean;
  }

  export class Permission {
    /** api的http请求方法 */
    apiMethod?: string;

    /** 后端接口api */
    apiPath?: string;

    /** children */
    children?: Array<defs.Permission>;

    /** 创建时间 */
    createTime?: string;

    /** 0正常1删除 */
    deleted?: number;

    /** 描述 */
    description?: string;

    /** id */
    id?: number;

    /** 权限项标识 */
    key?: string;

    /** 权限名 */
    name?: string;

    /** 父级ID */
    pid?: number;

    /** 排序 */
    sort?: number;

    /** 1菜单 2按钮 */
    type?: number;
  }

  export class PointNoticeVO {
    /** 当前积点 */
    currentPoint?: number;

    /** 增加积点 */
    point?: number;
  }

  export class PointRecordListDTO {
    /** 页码 */
    pageIndex?: number;

    /** 每页显示条数 */
    pageSize?: number;

    /** 交易类型:1收入,2支出,不传全部 */
    tradeType?: number;
  }

  export class PointTrandVO {
    /** 失败信息 */
    msg?: string;

    /** 剩余积点 */
    point?: number;

    /** 状态：true成功，false失败 */
    status?: boolean;

    /** 交易流水 */
    transNo?: string;
  }

  export class PointTransInfoDTO {
    /** 售后单号 */
    aftersaleNo?: string;

    /** 订单号 */
    orderNo?: string;

    /** 交易积点 */
    point?: number;

    /** 备注 */
    remark?: string;

    /** 交易方 */
    tradeCompany?: string;

    /** 交易类型:1赠送,2充值,3商城消费,4消费售后 */
    tradeType?: 'SEND' | 'RECHARGE' | 'CONSUME' | 'AFTERSALE';

    /** 交易流水 */
    transNo?: string;

    /** 用户标识 */
    userId?: number;
  }

  export class ProductApprovalDTO {
    /** 驳回原因 */
    refuseReason?: string;

    /** 商品id列表 */
    spuIdList?: Array<string>;

    /** 审核状态  0: 待审核 1: 驳回 2: 通过 */
    status?: number;
  }

  export class ProductBrandDTO {
    /** 品牌英文名称 */
    brandEnglishName?: string;

    /** 品牌名称 */
    brandName?: string;

    /** 品牌ID */
    id?: number;

    /** 备注 */
    remark?: string;
  }

  export class ProductBrandVO {
    /** 品牌英文名称 */
    brandEnglishName?: string;

    /** 品牌名称 */
    brandName?: string;

    /** 创建时间 */
    createTime?: string;

    /** 品牌ID */
    id?: number;

    /** 更新时间 */
    operTime?: string;

    /** 备注 */
    remark?: string;
  }

  export class ProductDetailsVO {
    /** 品牌英文名 */
    brandEnglishName?: string;

    /** 品牌id */
    brandId?: number;

    /** 品牌名称 */
    brandName?: string;

    /** 品类编码 */
    categoryCode?: string;

    /** 品类全称 */
    categoryFullName?: string;

    /** 品类名称 */
    categoryName?: string;

    /** 配送方式 0:无需配送 1:快递 2:自提 3:快递或自提 */
    deliveryType?: number;

    /** 商品描述 */
    description?: string;

    /** 详细参数 */
    detailedParameterVOList?: Array<defs.DetailedParameterVO>;

    /** 商品标签编码列表 */
    labelCodeList?: Array<string>;

    /** 上架时间 */
    onShelvesBeginTime?: string;

    /** 下架时间 */
    onShelvesEndTime?: string;

    /** 商品图片列表 */
    pictureUrlList?: Array<string>;

    /** 商品标签列表 */
    productLabelVOList?: Array<defs.ProductLabelVO>;

    /** 规格组列表 */
    productPropertyVOList?: Array<defs.ProductPropertyVO>;

    /** sku列表 */
    productSkuVOList?: Array<defs.ProductSkuVO>;

    /** 上下架状态 0: 下架 1: 上架 2: 发布中 3: 待上架 */
    putOnShelves?: number;

    /** 商品备注 */
    remark?: string;

    /** 服务保障编码列表 */
    serviceCodeList?: Array<string>;

    /** spuId */
    spuId?: string;

    /** spu名称 */
    spuName?: string;

    /** 商品spu图片地址(封面) */
    spuPictureUrl?: string;

    /** 商品视屏地址 */
    spuViewUrl?: string;

    /** 供应商编码 */
    supplierId?: string;

    /** 供应商名称 */
    supplierName?: string;

    /** 供应商简称 */
    supplierShortName?: string;

    /** 商品总库存 */
    totalStock?: number;
  }

  export class ProductLabelVO {
    /** 标签code */
    labelCode?: string;

    /** 标签名称 */
    labelName?: string;

    /** 图标地址 */
    labelPictureUrl?: string;

    /** 状态 0:禁用 1:启用 */
    labelStatus?: number;

    /** 更新时间 */
    operTime?: string;

    /** 备注 */
    remark?: string;
  }

  export class ProductLogVO {
    /** 创建时间(操作时间) */
    createTime?: string;

    /** 事件描述 */
    eventDesc?: string;

    /** 操作来源 1:商品管理 2:商品回收站 3:审核管理 4:库存管理 */
    eventSrc?: number;

    /** 事件类型 */
    eventType?: number;

    /** 操作人名称 */
    operName?: string;

    /** 上下架状态 0: 下架 1: 上架 2: 发布中(审核中) 3: 待上架 */
    putOnShelves?: number;

    /** spuId */
    spuId?: string;
  }

  export class ProductMktVO {
    /** 品类列表 */
    categoryVOList?: Array<defs.CategoryVO>;

    /** 主键ID */
    id?: number;

    /** 营销分类编码 */
    mktCode?: string;

    /** 分类名称 */
    mktName?: string;

    /** 图标地址 */
    mktPictureUrl?: string;

    /** 排序 */
    mktSort?: number;

    /** 状态 0:禁用 1:启用 */
    mktStatus?: number;

    /** 父类编码 */
    parentMktCode?: string;

    /** 父类名称 */
    parentMktName?: string;

    /** 备注 */
    remark?: string;
  }

  export class ProductPropertyDTO {
    /** 规格组ID */
    id?: number;

    /** 规格组名称 */
    name?: string;

    /** 规格组备注 */
    remark?: string;

    /** 规格值列表 */
    values?: Array<defs.ProductPropertyValueDTO>;
  }

  export class ProductPropertyVO {
    /** 创建时间 */
    createTime?: string;

    /** 规格组ID */
    id?: number;

    /** 规格组KEY */
    key?: string;

    /** 规格组名称 */
    name?: string;

    /** 更新时间 */
    operTime?: string;

    /** 规格组备注 */
    remark?: string;

    /** 规格值集合 */
    values?: Array<defs.ProductPropertyValueVO>;
  }

  export class ProductPropertyValueDTO {
    /** 规格值ID */
    id?: number;

    /** 规格值 */
    value?: string;
  }

  export class ProductPropertyValueVO {
    /** 规格值ID */
    id?: number;

    /** 规格值KEY */
    key?: string;

    /** 规格组ID */
    propertyId?: number;

    /** 规格值 */
    value?: string;
  }

  export class ProductSalesVO {
    /** 品牌id */
    brandId?: number;

    /** 品牌名称 */
    brandName?: string;

    /** 品类编码 */
    categoryCode?: string;

    /** 品类名称 */
    categoryName?: string;

    /** 交易完成时间 */
    finishTime?: string;

    /** 销售数量 */
    salesNumber?: number;

    /** 交易价格 */
    salesPrice?: number;

    /** 交易积分 */
    salesScore?: number;

    /** 销售类型 1:交易 2:售后 */
    salesType?: number;

    /** skuId */
    skuId?: string;

    /** spuId */
    spuId?: string;

    /** spu名称 */
    spuName?: string;

    /** 供应商编码 */
    supplierId?: string;

    /** 供应商名称 */
    supplierName?: string;

    /** 交易号(交易-订单号 售后-售后单号) */
    tradeNo?: string;
  }

  export class ProductSkuDTO {
    /** 最小购买数量 */
    minPurchaseQty?: number;

    /** 吊牌价 */
    msrp?: number;

    /** 价格策略 1:积分 2:售价 3: 积分+售价 */
    priceStrategy?: number;

    /** 规格信息Map<规格组key,规格值key> */
    propertyValueMap?: ObjectMap<any, string>;

    /** 规格名称 */
    propertyValueName?: string;

    /** 售价 */
    salePrice?: number;

    /** 积分 */
    score?: number;

    /** skuId */
    skuId?: string;

    /** 可售库存 */
    stock?: number;
  }

  export class ProductSkuVO {
    /** 最小购买数量 */
    minPurchaseQty?: number;

    /** 吊牌价 */
    msrp?: number;

    /** 价格策略 1:积分 2:售价 3: 积分+售价 */
    priceStrategy?: number;

    /** 规格信息Map<规格组key,规格值key> */
    propertyValueMap?: string;

    /** 规格名称 */
    propertyValueName?: string;

    /** 售价 */
    salePrice?: number;

    /** 积分 */
    score?: number;

    /** skuId */
    skuId?: string;

    /** sku销量 */
    skuSales?: number;

    /** 顺序(从1开始,  顺序为1的是默认sku) */
    skuSort?: number;

    /** spuId */
    spuId?: string;

    /** 可售库存 */
    stock?: number;
  }

  export class ProductWithApprovalVO {
    /** 申请人姓名 */
    applicantName?: string;

    /** 申请时间 */
    applicantTime?: string;

    /** 审核人姓名 */
    approvalName?: string;

    /** 审核时间 */
    approvalTime?: string;

    /** 品牌id */
    brandId?: number;

    /** 品牌名称 */
    brandName?: string;

    /** 品类编码 */
    categoryCode?: string;

    /** 品类全名 */
    categoryFullName?: string;

    /** 品类名称 */
    categoryName?: string;

    /** 一级品类编码 */
    firstCategoryCode?: string;

    /** 一级品类名称 */
    firstCategoryName?: string;

    /** 最小购买数量 */
    minPurchaseQty?: number;

    /** 吊牌价 */
    msrp?: number;

    /** 新增库存值 */
    newStock?: number;

    /** 上架时间 */
    onShelvesBeginTime?: string;

    /** 下架时间 */
    onShelvesEndTime?: string;

    /** 操作时间 */
    operTime?: string;

    /** 价格策略 1:积分 2:售价 3: 积分+售价 */
    priceStrategy?: number;

    /** 标签列表 */
    productLabelVOList?: Array<defs.ProductLabelVO>;

    /** 规格信息Map<规格组key,规格值key> */
    propertyValueMap?: string;

    /** 规格名称 */
    propertyValueName?: string;

    /** 上下架状态 0: 下架 1: 上架 2: 发布中(审核中) 3: 待上架 */
    putOnShelves?: number;

    /** 驳回原因 */
    refuseReason?: string;

    /** 售价 */
    salePrice?: number;

    /** 积分 */
    score?: number;

    /** skuId */
    skuId?: string;

    /** sku销量 */
    skuSales?: number;

    /** 顺序(从1开始,  顺序为1的是默认sku) */
    skuSort?: number;

    /** spuId */
    spuId?: string;

    /** spu名称 */
    spuName?: string;

    /** 商品spu图片地址(封面) */
    spuPictureUrl?: string;

    /** 审核状态 0:待审核 1:驳回 2:通过 */
    status?: number;

    /** 可售库存 */
    stock?: number;

    /** 供应商编码 */
    supplierId?: string;

    /** 供应商名称 */
    supplierName?: string;

    /** 供应商简称 */
    supplierShortName?: string;

    /** 商品状态 1:正常 0:回收站 -1:删除 */
    validity?: number;
  }

  export class ProductWithSkuVO {
    /** 品牌id */
    brandId?: number;

    /** 品牌名称 */
    brandName?: string;

    /** 品类编码 */
    categoryCode?: string;

    /** 品类全名 */
    categoryFullName?: string;

    /** 品类名称 */
    categoryName?: string;

    /** 一级品类编码 */
    firstCategoryCode?: string;

    /** 一级品类名称 */
    firstCategoryName?: string;

    /** 最小购买数量 */
    minPurchaseQty?: number;

    /** 吊牌价 */
    msrp?: number;

    /** 新增库存值 */
    newStock?: number;

    /** 上架时间 */
    onShelvesBeginTime?: string;

    /** 下架时间 */
    onShelvesEndTime?: string;

    /** 操作时间 */
    operTime?: string;

    /** 价格策略 1:积分 2:售价 3: 积分+售价 */
    priceStrategy?: number;

    /** 标签列表 */
    productLabelVOList?: Array<defs.ProductLabelVO>;

    /** 规格信息Map<规格组key,规格值key> */
    propertyValueMap?: string;

    /** 规格名称 */
    propertyValueName?: string;

    /** 上下架状态 0: 下架 1: 上架 2: 发布中(审核中) 3: 待上架 */
    putOnShelves?: number;

    /** 售价 */
    salePrice?: number;

    /** 积分 */
    score?: number;

    /** skuId */
    skuId?: string;

    /** sku销量 */
    skuSales?: number;

    /** 顺序(从1开始,  顺序为1的是默认sku) */
    skuSort?: number;

    /** spuId */
    spuId?: string;

    /** spu名称 */
    spuName?: string;

    /** 商品spu图片地址(封面) */
    spuPictureUrl?: string;

    /** 可售库存 */
    stock?: number;

    /** 供应商编码 */
    supplierId?: string;

    /** 供应商名称 */
    supplierName?: string;

    /** 供应商简称 */
    supplierShortName?: string;

    /** 商品状态 1:正常 0:回收站 -1:删除 */
    validity?: number;
  }

  export class PublicDto {
    /** 页码 */
    pageIndex?: number;

    /** 每页显示条数 */
    pageSize?: number;
  }

  export class QueryBatchManageStockDTO {
    /** 开始时间 */
    beginTime?: string;

    /** 缓存Id */
    cacheId?: string;

    /** 结束时间 */
    endTime?: string;

    /** 页码 */
    pageIndex?: number;

    /** 页面大小 */
    pageSize?: number;
  }

  export class QueryBrandDTO {
    /** 开始时间 */
    beginTime?: string;

    /** 品牌英文名称 */
    brandEnglishName?: string;

    /** 品牌名称 */
    brandName?: string;

    /** 结束时间 */
    endTime?: string;

    /** 品牌ID */
    id?: number;

    /** 页码 */
    pageIndex?: number;

    /** 页面大小 */
    pageSize?: number;
  }

  export class QueryCarListDTO {
    /** 购买时间结束 */
    buyEndTime?: string;

    /** 购买时间开始 */
    buyStartTime?: string;

    /** 车系 */
    carModels?: Array<number>;

    /** 车架号 */
    frameNo?: string;

    /** id */
    id?: number;

    /** 页码 */
    pageIndex?: number;

    /** 每页显示条数 */
    pageSize?: number;

    /** 用户手机号 */
    phone?: string;

    /** 车牌号-市 */
    plateCity?: string;

    /** 车牌号 */
    plateNo?: string;

    /** 车牌号-省 */
    plateProvince?: string;

    /** 状态 */
    status?: number;
  }

  export class QueryCarTypeDTO {
    /** 开始时间 */
    beginTime?: string;

    /** 车型品牌ID */
    brandId?: number;

    /** 结束时间 */
    endTime?: string;

    /** 名称 */
    name?: string;

    /** 排序方式 */
    orderBy?: string;

    /** 页码 */
    pageIndex?: number;

    /** 页面大小 */
    pageSize?: number;

    /** 排序字段 */
    sortBy?: string;
  }

  export class QueryCategoryDTO {
    /** 开始时间 */
    beginTime?: string;

    /** 品类编码 */
    categoryCode?: string;

    /** 品类名称 */
    categoryName?: string;

    /** 结束时间 */
    endTime?: string;

    /** 页码 */
    pageIndex?: number;

    /** 页面大小 */
    pageSize?: number;
  }

  export class QueryDictDTO {
    /** 开始时间 */
    beginTime?: string;

    /** 编码 */
    code?: string;

    /** 结束时间 */
    endTime?: string;

    /** 名称 */
    name?: string;

    /** 排序方式 */
    orderBy?: string;

    /** 页码 */
    pageIndex?: number;

    /** 页面大小 */
    pageSize?: number;

    /** 排序字段 */
    sortBy?: string;
  }

  export class QueryMemberBenefitsDTO {
    /** 权益名称 */
    benefitName?: string;

    /** ID */
    id?: number;

    /** 操作人 */
    operName?: string;

    /** 更新时间结束 */
    operTimeEnd?: string;

    /** 更新时间开始 */
    operTimeStart?: string;
  }

  export class QueryPageDTO {
    /** ID */
    id?: number;

    /** 操作人 */
    operName?: string;

    /** 更新时间结束 */
    operTimeEnd?: string;

    /** 更新时间开始 */
    operTimeStart?: string;

    /** 页码 */
    pageIndex?: number;

    /** 页面大小 */
    pageSize?: number;

    /** startRow */
    startRow?: number;

    /** 标题 */
    title?: string;
  }

  export class QueryPageRecommendDTO {
    /** ID */
    id?: number;

    /** 位置编码 个人中心常用功能：myCommon */
    locationCode?: string;

    /** 操作人 */
    operName?: string;

    /** 更新时间结束 */
    operTimeEnd?: string;

    /** 更新时间开始 */
    operTimeStart?: string;

    /** 启用状态 1启用 0禁用 */
    status?: number;

    /** 入口名称 */
    title?: string;
  }

  export class QueryPageRecommendFrontDTO {
    /** 位置编码 个人中心常用功能：myCommon */
    locationCode?: string;
  }

  export class QueryPageTextDTO {
    /** ID */
    id?: number;

    /** ID或标题 */
    keyword?: string;

    /** 操作人 */
    operName?: string;

    /** 更新时间结束 */
    operTimeEnd?: string;

    /** 更新时间开始 */
    operTimeStart?: string;

    /** 页面编码 price价格说明 score如何获取积分 privac隐私说明 */
    pageCode?: string;

    /** 页码 */
    pageIndex?: number;

    /** 页面大小 */
    pageSize?: number;

    /** 页面类型 */
    pageType?: number;

    /** startRow */
    startRow?: number;

    /** 标题 */
    title?: string;
  }

  export class QueryPaymentCodeDTO {
    /** 开始时间 */
    beginTime?: string;

    /** 结束时间 */
    endTime?: string;

    /** 操作时间结束 */
    operEndTime?: string;

    /** 更新人名称 */
    operName?: string;

    /** 操作时间开始 */
    operStartTime?: string;

    /** 排序方式 */
    orderBy?: string;

    /** 页码 */
    pageIndex?: number;

    /** 页面大小 */
    pageSize?: number;

    /** 排序字段 */
    sortBy?: string;

    /** 状态 0:禁用 1:启用 */
    status?: number;

    /** 供应商IDs */
    supplierIds?: Array<number>;

    /** 查询信息，模糊匹配，名称/编码 */
    supplierInfo?: string;

    /** 是否删除 -1:删除 1:正常 */
    validity?: number;
  }

  export class QueryPointRecordDTO {
    /** 结束时间 */
    end?: string;

    /** 单号 */
    orderNo?: string;

    /** 页码 */
    pageIndex?: number;

    /** 每页显示条数 */
    pageSize?: number;

    /** 手机号 */
    phone?: string;

    /** 积点来源 1线上,2线下 */
    sourceType?: number;

    /** 开始时间 */
    start?: string;

    /** 交易类型:1充值,2赠送,3商城消费,4消费售后 */
    tradeType?: number;

    /** 交易类型:1充值,2赠送,3商城消费,4消费售后 */
    tradeTypes?: Array<number>;

    /** 用户id或姓名 */
    userName?: string;
  }

  export class QueryProductDTO {
    /** 申请人名称 */
    applicantName?: string;

    /** 申请时间(始) */
    applicantTimeBegin?: string;

    /** 申请时间(止) */
    applicantTimeEnd?: string;

    /** 审核人名称 */
    approvalName?: string;

    /** 审核状态列表 0:待审核 1:驳回 2:通过 */
    approvalStatusList?: Array<number>;

    /** 审核时间(始) */
    approvalTimeBegin?: string;

    /** 审核时间(止) */
    approvalTimeEnd?: string;

    /** 开始时间 */
    beginTime?: string;

    /** 品牌Id列表 */
    brandIdList?: Array<number>;

    /** 品类列表 */
    categoryCodeList?: Array<string>;

    /** 结束时间 */
    endTime?: string;

    /** 商品标签列表 */
    labelCodeList?: Array<string>;

    /** 营销分类列表 */
    mktCodeList?: Array<string>;

    /** 下架时间(始) */
    offShelvesTimeBegin?: string;

    /** 下架时间(止) */
    offShelvesTimeEnd?: string;

    /** 上架时间(始) */
    onShelvesTimeBegin?: string;

    /** 上架时间(止) */
    onShelvesTimeEnd?: string;

    /** 更新时间(始) */
    operTimeBegin?: string;

    /** 更新时间(止) */
    operTimeEnd?: string;

    /** 页码 */
    pageIndex?: number;

    /** 页面大小 */
    pageSize?: number;

    /** 商品查询 名称/编号(spuId)/skuId */
    productQuery?: string;

    /** 上下架状态 0:下架 1:上架 2:发布中 3:待上架 */
    putOnShelvesList?: Array<number>;

    /** 积分区间(大) */
    scoreBig?: number;

    /** 积分区间(小) */
    scoreSmall?: number;

    /** sku列表 */
    skuIdList?: Array<string>;

    /** spu列表 */
    spuIdList?: Array<string>;

    /** 库存区间(大) */
    stockBig?: number;

    /** 出入库记录操作人名称 */
    stockRecordsOperName?: string;

    /** 出入库记录订单号 */
    stockRecordsOrderNo?: string;

    /** 出入库记录开始时间 */
    stockRecordsTimeBegin?: string;

    /** 出入库记录结束时间 */
    stockRecordsTimeEnd?: string;

    /** 库存区间(小) */
    stockSmall?: number;

    /** 库存预警标志( 默认查所有列表 1:无货预警 2:低于安全库存预警) */
    stockWarnFlag?: number;

    /** 供应商列表 */
    supplierIdList?: Array<string>;

    /** 使用类型列表(1.商品核销 2.手动出库 3.售后入库 4.手动入库) */
    useTypeList?: Array<number>;
  }

  export class QueryProductLabelDTO {
    /** 开始时间 */
    beginTime?: string;

    /** 结束时间 */
    endTime?: string;

    /** 标签code */
    labelCode?: string;

    /** 标签名称 */
    labelName?: string;

    /** 状态 0:禁用 1:启用 */
    labelStatus?: number;

    /** 页码 */
    pageIndex?: number;

    /** 页面大小 */
    pageSize?: number;
  }

  export class QueryProductLogDTO {
    /** 开始时间 */
    beginTime?: string;

    /** 结束时间 */
    endTime?: string;

    /** 页码 */
    pageIndex?: number;

    /** 页面大小 */
    pageSize?: number;

    /** 商品查询 名称/编号(spuId) */
    productQuery?: string;

    /** 上下架状态 0:下架 1:上架 2:发布中 3:待上架 */
    putOnShelvesList?: Array<number>;

    /** 供应商列表 */
    supplierIdList?: Array<string>;
  }

  export class QueryProductMktDTO {
    /** 开始时间 */
    beginTime?: string;

    /** 结束时间 */
    endTime?: string;

    /** 营销分类编码 */
    mktCode?: string;

    /** 分类名称 */
    mktName?: string;

    /** 状态 0:禁用 1:启用 */
    mktStatus?: number;

    /** 页码 */
    pageIndex?: number;

    /** 页面大小 */
    pageSize?: number;

    /** 查询营销分类(名称或编码) */
    queryMkt?: string;
  }

  export class QueryProductSalesDTO {
    /** 开始时间 */
    beginTime?: string;

    /** 品牌Id列表 */
    brandIdList?: Array<number>;

    /** 品类列表 */
    categoryCodeList?: Array<string>;

    /** 结束时间 */
    endTime?: string;

    /** 页码 */
    pageIndex?: number;

    /** 页面大小 */
    pageSize?: number;

    /** 商品查询 名称/编号(spuId)/skuId */
    productQuery?: string;

    /** skuId列表 */
    skuIdList?: Array<string>;

    /** spuId列表 */
    spuIdList?: Array<string>;

    /** 供应商列表 */
    supplierIdList?: Array<string>;

    /** 交易号(交易-订单号 售后-售后单号) */
    tradeNo?: string;
  }

  export class QueryPropertyDTO {
    /** 开始时间 */
    beginTime?: string;

    /** 结束时间 */
    endTime?: string;

    /** 规格组ID */
    id?: number;

    /** 规格组名称 */
    name?: string;

    /** 页码 */
    pageIndex?: number;

    /** 页面大小 */
    pageSize?: number;
  }

  export class QuerySearchKeywordDTO {
    /** ID */
    id?: number;

    /** 关键词 */
    keyword?: string;

    /** 操作人 */
    operName?: string;

    /** 更新时间结束 */
    operTimeEnd?: string;

    /** 更新时间开始 */
    operTimeStart?: string;

    /** 启用状态 1启用 0禁用 */
    status?: number;
  }

  export class QueryServiceAssuranceDTO {
    /** 操作人 */
    operName?: string;

    /** 更新时间结束 */
    operTimeEnd?: string;

    /** 更新时间开始 */
    operTimeStart?: string;

    /** 页码 */
    pageIndex?: number;

    /** 页面大小 */
    pageSize?: number;

    /** 权益名称 */
    serviceCode?: string;

    /** startRow */
    startRow?: number;

    /** 供应商ID */
    supperId?: number;
  }

  export class QueryStockDTO {
    /** skuId */
    skuId?: string;

    /** 供应商id */
    supplierId?: string;
  }

  export class QuerySupplierDTO {
    /** 开始时间 */
    beginTime?: string;

    /** 业务类型 */
    businessType?: Array<number>;

    /** 创建时间结束 */
    createEndTime?: string;

    /** 创建时间开始 */
    createStartTime?: string;

    /** 结束时间 */
    endTime?: string;

    /** 主键IDs */
    ids?: Array<number>;

    /** 排序方式 */
    orderBy?: string;

    /** 页码 */
    pageIndex?: number;

    /** 页面大小 */
    pageSize?: number;

    /** 是否启用收款码（1:启用，0:不启用） */
    paymentCodeEnable?: number;

    /** 结算方式 1:经销，2:联营，3扣点 */
    settlementMode?: number;

    /** 排序字段 */
    sortBy?: string;

    /** 状态 0:禁用 1:启用 */
    status?: number;

    /** 查询信息，模糊匹配，名称/编码 */
    supplierInfo?: string;

    /** 是否删除 -1:删除 1:正常 */
    validity?: number;
  }

  export class QueryUserListDTO {
    /** 注册结束时间 */
    endTime?: string;

    /** 会员等级:1普通会员，2高级会员 */
    memberLevel?: string;

    /** 页码 */
    pageIndex?: number;

    /** 每页显示条数 */
    pageSize?: number;

    /** 手机号 */
    phone?: string;

    /** 注册开始时间 */
    startTime?: string;

    /** 状态,1启用，2禁用 */
    status?: number;

    /** 标签 */
    tags?: string;

    /** 用户id */
    userId?: number;

    /** 姓名 */
    userName?: string;
  }

  export class QueryUserPointRecordDTO {
    /** 结束时间 */
    end?: string;

    /** 页码 */
    pageIndex?: number;

    /** 每页显示条数 */
    pageSize?: number;

    /** 开始时间 */
    start?: string;

    /** 交易类型:1赠送,2充值,3商城消费,4消费售后 */
    tradeType?: number;

    /** 用户id */
    userId?: number;
  }

  export class RechargeAccount {
    /** 用户手机号 */
    phone?: string;

    /** 充值金额 */
    point?: number;

    /** 用户id */
    userId?: number;

    /** 用户姓名 */
    userName?: string;
  }

  export class RechargeDetailVO {
    /** 审批备注 */
    auditRemark?: string;

    /** 审批状态:0未审批，1已审批,2驳回 */
    auditStatus?: number;

    /** 审批时间 */
    auditTime?: string;

    /** 审批人 */
    auditUser?: string;

    /** batchNo */
    batchNo?: number;

    /** 创建时间 */
    createTime?: string;

    /** 创建人 */
    createUser?: string;

    /** 附件（逗号分隔） */
    files?: string;

    /** id */
    id?: number;

    /** 充值账号及金额 */
    rechargeAccount?: Array<defs.RechargeAccount>;

    /** 充值描述 */
    rechargeReason?: string;

    /** 发放状态：0未审核未发放,1审核通过未发放,2发放中,3发放完成 */
    sendStatus?: number;

    /** 发放成功人数 */
    successMember?: number;

    /** 标题 */
    title?: string;

    /** 批次发放人数 */
    totalMember?: number;

    /** 批次总发放积分 */
    totalPoint?: number;

    /** 类型:1充值,2赠送 */
    type?: number;
  }

  export class RechargePageDTO {
    /** 申请结束时间 */
    applyEndTime?: string;

    /** 申请开始时间 */
    applyStartTime?: string;

    /** 审核结束时间 */
    auditEndTime?: string;

    /** 审核开始时间 */
    auditStartTime?: string;

    /** 审批状态:0未审批，1已审批,2驳回 */
    auditStatus?: number;

    /** 批次号 */
    batchNo?: number;

    /** id */
    id?: number;

    /** 页码 */
    pageIndex?: number;

    /** 每页显示条数 */
    pageSize?: number;

    /** 标题 */
    title?: string;

    /** 1充值2赠送 */
    type?: number;
  }

  export class ResetPasswordDTO {
    /** 用户名id */
    id?: number;

    /** 用户名id */
    password?: string;
  }

  export class Resource {
    /** description */
    description?: string;

    /** file */
    file?: defs.File;

    /** filename */
    filename?: string;

    /** inputStream */
    inputStream?: defs.InputStream;

    /** open */
    open?: boolean;

    /** readable */
    readable?: boolean;

    /** uri */
    uri?: defs.URI;

    /** url */
    url?: defs.URL;
  }

  export class ResponseModel<T0 = any> {
    /** code */
    code?: string;

    /** data */
    data: T0;

    /** message */
    message?: string;

    /** msg */
    msg?: string;
  }

  export class RoleDTO {
    /** 描述 */
    description?: string;

    /** 角色id 修改时需要 */
    id?: number;

    /** 角色名 */
    name?: string;

    /** 权限 */
    permissions?: Array<number>;
  }

  export class RoleListVO {
    /** 创建人 */
    createName?: string;

    /** 添加时间 */
    createTime?: string;

    /** 描述 */
    description?: string;

    /** 角色id 修改时需要 */
    id?: number;

    /** 角色名 */
    name?: string;

    /** 更新人 */
    updateName?: string;

    /** 修改时间 */
    updateTime?: string;
  }

  export class RoleQueryDTO {
    /** 名称 */
    name?: string;

    /** 页码(>=1) */
    pageIndex?: number;

    /** pageSize */
    pageSize?: number;

    /** 更新时间 */
    updateEndTime?: string;

    /** 更新人 */
    updateName?: string;

    /** 更新时间 */
    updateStartTime?: string;
  }

  export class RoleVO {
    /** 角色id */
    id?: number;

    /** 角色名 */
    name?: string;
  }

  export class SaleDetailListQueryDTO {
    /** 当前页 */
    pageIndex?: number;

    /** 每页大小 */
    pageSize?: number;

    /** 交易单号 */
    saleNo?: string;

    /** 交易结束时间 */
    saleTimeEnd?: string;

    /** 交易开始时间 */
    saleTimeStart?: string;

    /** 交易类型(1:交易 2:售后 3:交易冲正) */
    saleType?: number;

    /** 供应商id列表 */
    supplierIds?: Array<number>;
  }

  export class SalesCountVO {
    /** 销售总量 */
    salesNumberTotal?: number;

    /** 销售总价格 */
    salesPriceTotal?: number;

    /** 销售总积分 */
    salesScoreTotal?: number;
  }

  export class SearchKeywordBasicVO {
    /** ID */
    id?: number;

    /** 关键词 */
    keyword?: string;

    /** 排序 */
    sort?: number;

    /** 启用状态 */
    status?: number;
  }

  export class SearchKeywordDTO {
    /** ID */
    id?: number;

    /** 关键词 */
    keyword?: string;

    /** 排序 */
    sort?: number;

    /** 启用状态 */
    status?: number;
  }

  export class SearchKeywordVO {
    /** 创建时间 */
    createTime?: string;

    /** ID */
    id?: number;

    /** 关键词 */
    keyword?: string;

    /** 操作人ID */
    operId?: number;

    /** 操作人 */
    operName?: string;

    /** 更新时间 */
    operTime?: string;

    /** 排序 */
    sort?: number;

    /** 启用状态 */
    status?: number;
  }

  export class SearchProductDTO {
    /** 开始时间 */
    beginTime?: string;

    /** 结束时间 */
    endTime?: string;

    /** 商品关键词 */
    keyWord?: string;

    /** 价格筛选 最大价格 */
    maxSalePrice?: number;

    /** 积分筛选 最大积分 */
    maxScore?: number;

    /** 价格筛选 最小价格 */
    minSalePrice?: number;

    /** 积分筛选 最小积分 */
    minScore?: number;

    /** 营销分类列表 */
    mktCodeList?: Array<string>;

    /** 页码 */
    pageIndex?: number;

    /** 页面大小 */
    pageSize?: number;

    /** 排序字段【积分:score 价格:salePrice 销量:productSales】 */
    sortField?: string;

    /** 排序方式 升序:ASC 降序:DESC 默认升序(ASC) */
    sortType?: string;

    /** 供应商列表 */
    supplierIdList?: Array<string>;
  }

  export class SearchProductVO {
    /** 品牌英文名 */
    brandEnglishName?: string;

    /** 品牌id */
    brandId?: string;

    /** 品牌名称 */
    brandName?: string;

    /** 品类编码 */
    categoryCode?: string;

    /** 品类名称 */
    categoryName?: string;

    /** 吊牌价 */
    msrp?: number;

    /** 商品标签信息 */
    productLabelVOList?: Array<defs.ProductLabelVO>;

    /** 销量 */
    productSales?: number;

    /** 售价 */
    salePrice?: number;

    /** 积分 */
    score?: number;

    /** skuId */
    skuId?: string;

    /** skuId */
    spuId?: string;

    /** spu名称 */
    spuName?: string;

    /** 商品spu图片地址(封面) */
    spuPictureUrl?: string;

    /** 供应商编码 */
    supplierId?: string;

    /** 供应商名称 */
    supplierName?: string;

    /** 商品总库存 */
    totalStock?: number;
  }

  export class ServiceAssuranceBasicVO {
    /** ID */
    id?: number;

    /** 服务保障编码 */
    serviceCode?: string;

    /** 描述 */
    serviceDesc?: string;

    /** 名称 */
    serviceName?: string;

    /** 供应商编码 */
    supperId?: number;

    /** 供应商名称 */
    supperName?: string;
  }

  export class ServiceAssuranceDTO {
    /** ID */
    id?: number;

    /** 描述 */
    serviceDesc?: string;

    /** 名称 */
    serviceName?: string;

    /** 供应商ID */
    supperId?: number;

    /** 供应商名称 */
    supperName?: string;
  }

  export class ServiceAssuranceVO {
    /** 创建时间 */
    createTime?: string;

    /** ID */
    id?: number;

    /** 操作人ID */
    operId?: number;

    /** 操作人 */
    operName?: string;

    /** 更新时间 */
    operTime?: string;

    /** 服务保障编码 */
    serviceCode?: string;

    /** 描述 */
    serviceDesc?: string;

    /** 名称 */
    serviceName?: string;

    /** 供应商编码 */
    supperId?: number;

    /** 供应商名称 */
    supperName?: string;
  }

  export class SettingDictDTO {
    /** 编码 */
    code?: string;

    /** createTime */
    createTime?: string;

    /** 字典值列表 */
    dictDetailDTOS?: Array<defs.SettingDictDetailDTO>;

    /** 主键ID */
    id?: number;

    /** 名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 备注 */
    remark?: string;
  }

  export class SettingDictDetailDTO {
    /** createTime */
    createTime?: string;

    /** 字典ID */
    dictId?: number;

    /** 扩展字段 */
    extend?: string;

    /** 主键ID */
    id?: number;

    /** 名称 */
    label?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 备注 */
    remark?: string;

    /** 排序 */
    sort?: string;

    /** 值 */
    value?: string;
  }

  export class SettingDictDetailObject {
    /** createTime */
    createTime?: string;

    /** dictId */
    dictId?: number;

    /** 扩展字段 */
    extend?: string;

    /** 主键ID */
    id?: number;

    /** label */
    label?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 备注 */
    remark?: string;

    /** sort */
    sort?: string;

    /** value */
    value?: string;
  }

  export class SettingDictDetailVO {
    /** createTime */
    createTime?: string;

    /** dictId */
    dictId?: number;

    /** 扩展字段 */
    extend?: string;

    /** 主键ID */
    id?: number;

    /** label */
    label?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 备注 */
    remark?: string;

    /** sort */
    sort?: string;

    /** value */
    value?: string;
  }

  export class SettingDictVO {
    /** 编码 */
    code?: string;

    /** 字段数 */
    count?: number;

    /** createTime */
    createTime?: string;

    /** 字典值列表 */
    dictDetailVOS?: Array<defs.SettingDictDetailVO>;

    /** 主键ID */
    id?: number;

    /** 名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 备注 */
    remark?: string;
  }

  export class SettingMallDTO {
    /** createTime */
    createTime?: string;

    /** 首页描述 */
    homeDesc?: string;

    /** 主键ID */
    id?: number;

    /** 图片 */
    imgUrl?: string;

    /** logo图片 */
    logo?: string;

    /** 小程序描述 */
    miniAppDesc?: string;

    /** 商场名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 积分名称 */
    scoreName?: string;

    /** 开放登录 0:禁用 1:启用 */
    validity?: number;
  }

  export class SettingMallVO {
    /** createTime */
    createTime?: string;

    /** 首页描述 */
    homeDesc?: string;

    /** 主键ID */
    id?: number;

    /** 图片 */
    imgUrl?: string;

    /** logo图片 */
    logo?: string;

    /** 小程序描述 */
    miniAppDesc?: string;

    /** 商场名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 积分名称 */
    scoreName?: string;

    /** 开放登录 0:禁用 1:启用 */
    validity?: number;
  }

  export class SettingScoreDTO {
    /** createTime */
    createTime?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 积分名称 */
    scoreName?: string;

    /** 积分规则列表 */
    scores?: Array<defs.SettingScoreObject>;
  }

  export class SettingScoreObject {
    /** 会员等级code */
    code?: string;

    /** createTime */
    createTime?: string;

    /** 会员等级名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 积分 */
    score?: number;
  }

  export class SettingScoreVO {
    /** 商城信息 */
    mall?: defs.SettingMallVO;

    /** 积分规则列表 */
    scores?: Array<defs.SettingScoreObject>;
  }

  export class SettingVersionDTO {
    /** createTime */
    createTime?: string;

    /** 主键ID */
    id?: number;

    /** 名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 描述 */
    remark?: string;
  }

  export class SettingVersionObject {
    /** createTime */
    createTime?: string;

    /** 主键ID */
    id?: number;

    /** 名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 描述 */
    remark?: string;
  }

  export class ShortLinkVO {
    /** 长链接 */
    longLink?: string;

    /** 短链接 */
    shortLink?: string;
  }

  export class SkuStockVO {
    /** 库存数量 */
    count?: number;

    /** skuId */
    skuId?: string;

    /** 供应商id */
    supplierId?: string;
  }

  export class SmsSendDTO {
    /** 手机号 */
    mobile?: string;

    /** 短信类型:2:重新绑定会员卡发送短信 */
    smsType?: number;
  }

  export class SmsVerifyDTO {
    /** 手机号 */
    mobile?: string;

    /** 验证码类型！ */
    smsType?: number;

    /** 验证码,验证时必传 */
    verificationCode?: string;
  }

  export class StatusChangeDTO {
    /** ID */
    id?: number;

    /** 启用状态 1启用 0禁用 */
    status?: number;
  }

  export class StockItemDTO {
    /** count */
    count?: number;

    /** skuId */
    skuId?: string;
  }

  export class StockListVO {
    /** propertyValueName */
    propertyValueName?: string;

    /** 上下架状态 0: 下架 1: 上架 2: 发布中 3: 待上架 */
    putOnShelves?: number;

    /** putOnShelvesName */
    putOnShelvesName?: string;

    /** score */
    score?: number;

    /** skuId */
    skuId?: string;

    /** spuId */
    spuId?: string;

    /** spuName */
    spuName?: string;

    /** stock */
    stock?: number;

    /** supplierName */
    supplierName?: string;
  }

  export class StockRecordsWithProductVO {
    /** 品牌英文名 */
    brandEnglishName?: string;

    /** 品牌id */
    brandId?: number;

    /** 品牌名称 */
    brandName?: string;

    /** 品类编码 */
    categoryCode?: string;

    /** 品类全称 */
    categoryFullName?: string;

    /** 品类名称 */
    categoryName?: string;

    /** 创建时间 */
    createTime?: string;

    /** 配送方式 0:无需配送 1:快递 2:自提 3:快递或自提 */
    deliveryType?: number;

    /** 商品描述 */
    description?: string;

    /** 商品标签编码列表 */
    labelCodeList?: Array<string>;

    /** 剩余库存 */
    leftStock?: number;

    /** 上架时间 */
    onShelvesBeginTime?: string;

    /** 下架时间 */
    onShelvesEndTime?: string;

    /** 操作人姓名 */
    operName?: string;

    /** 订单号 */
    orderNo?: string;

    /** 规格名称 */
    propertyValueName?: string;

    /** 上下架状态 0: 下架 1: 上架 2: 发布中 3: 待上架 */
    putOnShelves?: number;

    /** 商品备注 */
    remark?: string;

    /** 服务保障编码列表 */
    serviceCodeList?: Array<string>;

    /** skuId */
    skuId?: string;

    /** spuId */
    spuId?: string;

    /** spu名称 */
    spuName?: string;

    /** 商品spu图片地址(封面) */
    spuPictureUrl?: string;

    /** 商品视屏地址 */
    spuViewUrl?: string;

    /** 库存值(可为负) */
    stock?: number;

    /** 供应商编码 */
    supplierId?: string;

    /** 供应商名称 */
    supplierName?: string;

    /** 供应商简称 */
    supplierShortName?: string;

    /** 使用类型(1.商品核销 2.手动出库 3.售后入库 4.手动入库) */
    useType?: number;
  }

  export class StockWarnWithProductVO {
    /** 品牌英文名 */
    brandEnglishName?: string;

    /** 品牌id */
    brandId?: number;

    /** 品牌名称 */
    brandName?: string;

    /** 品类编码 */
    categoryCode?: string;

    /** 品类全称 */
    categoryFullName?: string;

    /** 品类名称 */
    categoryName?: string;

    /** 配送方式 0:无需配送 1:快递 2:自提 3:快递或自提 */
    deliveryType?: number;

    /** 商品描述 */
    description?: string;

    /** 商品标签编码列表 */
    labelCodeList?: Array<string>;

    /** 锁定库存 */
    lockStock?: number;

    /** 吊牌价 */
    msrp?: number;

    /** 上架时间 */
    onShelvesBeginTime?: string;

    /** 下架时间 */
    onShelvesEndTime?: string;

    /** 规格名称 */
    propertyValueName?: string;

    /** 上下架状态 0: 下架 1: 上架 2: 发布中 3: 待上架 */
    putOnShelves?: number;

    /** 商品备注 */
    remark?: string;

    /** 安全库存 */
    safeStock?: number;

    /** 售价 */
    salePrice?: number;

    /** 积分 */
    score?: number;

    /** 服务保障编码列表 */
    serviceCodeList?: Array<string>;

    /** skuId */
    skuId?: string;

    /** spuId */
    spuId?: string;

    /** spu名称 */
    spuName?: string;

    /** 商品spu图片地址(封面) */
    spuPictureUrl?: string;

    /** 商品视屏地址 */
    spuViewUrl?: string;

    /** 可售库存 */
    stock?: number;

    /** 库存预警 0:关闭 1:开启 */
    stockWarn?: number;

    /** 供应商编码 */
    supplierId?: string;

    /** 供应商名称 */
    supplierName?: string;

    /** 供应商简称 */
    supplierShortName?: string;
  }

  export class SupplierBusinessTypeObject {
    /** 业务类型ID */
    businessTypeId?: number;

    /** 主键ID */
    id?: number;
  }

  export class SupplierDTO {
    /** 所属行业IDS */
    businessTypes?: Array<defs.SupplierBusinessTypeObject>;

    /** 编码 */
    code?: string;

    /** 联系人地址 */
    contactAddress?: string;

    /** 联系人 */
    contactName?: string;

    /** 联系人电话 */
    contactPhone?: string;

    /** 客服电话 */
    customerPhone?: string;

    /** 自提点列表 */
    deliveryAddresses?: Array<defs.SupplierDeliveryAddressDTO>;

    /** 主键ID */
    id?: number;

    /** 介绍 */
    intro?: string;

    /** 供应商名称 */
    name?: string;

    /** 是否启用收款码（1:启用，0:不启用） */
    paymentCodeEnable?: number;

    /** 备注 */
    remark?: string;

    /** 结算方式 1:经销，2:联营，3扣点 */
    settlementMode?: number;

    /** 供应商简称 */
    shortName?: string;

    /** 状态 0:禁用 1:启用 */
    status?: number;

    /** 核销账号列表 */
    verifyAccounts?: Array<defs.SupplierVerifyAccountObject>;

    /** 预警手机号 */
    warningMobile?: string;
  }

  export class SupplierDeliveryAddressDTO {
    /** 详细地址 */
    address?: string;

    /** 区编码 */
    area?: string;

    /** 区编码name */
    areaName?: string;

    /** 市编码 */
    city?: string;

    /** 市编码name */
    cityName?: string;

    /** 国家编码 */
    country?: string;

    /** 国家编码name */
    countryName?: string;

    /** createTime */
    createTime?: string;

    /** 主键ID */
    id?: number;

    /** 纬度 */
    latitude?: string;

    /** 经度 */
    longitude?: string;

    /** 自提点名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 省编码 */
    province?: string;

    /** 省编码name */
    provinceName?: string;

    /** 状态 0:禁用 1:启用 */
    status?: number;

    /** 供应商ID */
    supplierId?: number;

    /** 服务电话 */
    tel?: string;

    /** 是否删除 -1:删除 1:正常 */
    validity?: number;
  }

  export class SupplierDeliveryAddressObject {
    /** 详细地址 */
    address?: string;

    /** 区编码 */
    area?: string;

    /** 市编码 */
    city?: string;

    /** 国家编码 */
    country?: string;

    /** createTime */
    createTime?: string;

    /** 主键ID */
    id?: number;

    /** 纬度 */
    latitude?: string;

    /** 经度 */
    longitude?: string;

    /** 自提点名称 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 省编码 */
    province?: string;

    /** 状态 0:禁用 1:启用 */
    status?: number;

    /** 供应商ID */
    supplierId?: number;

    /** 服务电话 */
    tel?: string;

    /** 是否删除 -1:删除 1:正常 */
    validity?: number;
  }

  export class SupplierDetailVO {
    /** 所属行业IDS */
    businessTypes?: Array<defs.SupplierBusinessTypeObject>;

    /** 编码 */
    code?: string;

    /** 联系人地址 */
    contactAddress?: string;

    /** 联系人 */
    contactName?: string;

    /** 联系人电话 */
    contactPhone?: string;

    /** 创建时间 */
    createTime?: string;

    /** 客服电话 */
    customerPhone?: string;

    /** 自提点列表 */
    deliveryAddresses?: Array<defs.SupplierDeliveryAddressDTO>;

    /** 主键ID */
    id?: number;

    /** 介绍 */
    intro?: string;

    /** 供应商名称 */
    name?: string;

    /** 操作人id */
    operId?: number;

    /** 操作人名称 */
    operName?: string;

    /** 操作时间 */
    operTime?: string;

    /** 是否启用收款码（1:启用，0:不启用） */
    paymentCodeEnable?: number;

    /** 备注 */
    remark?: string;

    /** 结算方式 1:经销，2:联营，3扣点 */
    settlementMode?: number;

    /** 供应商简称 */
    shortName?: string;

    /** 状态 0:禁用 1:启用 */
    status?: number;

    /** 是否删除 -1:删除 1:正常 */
    validity?: number;

    /** 核销账号列表 */
    verifyAccounts?: Array<defs.SupplierVerifyAccountObject>;

    /** 预警手机号 */
    warningMobile?: string;
  }

  export class SupplierVO {
    /** 编码 */
    code?: string;

    /** 联系人地址 */
    contactAddress?: string;

    /** 联系人 */
    contactName?: string;

    /** 联系人电话 */
    contactPhone?: string;

    /** 创建时间 */
    createTime?: string;

    /** 客服电话 */
    customerPhone?: string;

    /** 自提点（启用） */
    deliveryAddresses?: defs.SupplierDeliveryAddressObject;

    /** 主键ID */
    id?: number;

    /** 介绍 */
    intro?: string;

    /** 供应商名称 */
    name?: string;

    /** 操作人id */
    operId?: number;

    /** 操作人名称 */
    operName?: string;

    /** 操作时间 */
    operTime?: string;

    /** 是否启用收款码（1:启用，0:不启用） */
    paymentCodeEnable?: number;

    /** 备注 */
    remark?: string;

    /** 结算方式 1:经销，2:联营，3扣点 */
    settlementMode?: number;

    /** 供应商简称 */
    shortName?: string;

    /** 状态 0:禁用 1:启用 */
    status?: number;

    /** 是否删除 -1:删除 1:正常 */
    validity?: number;

    /** 预警手机号 */
    warningMobile?: string;
  }

  export class SupplierVerifyAccountObject {
    /** createTime */
    createTime?: string;

    /** 主键ID */
    id?: number;

    /** 核销账号（手机号） */
    mobile?: string;

    /** 姓名 */
    name?: string;

    /** operId */
    operId?: number;

    /** operName */
    operName?: string;

    /** operTime */
    operTime?: string;

    /** 供应商ID */
    supplierId?: number;

    /** 是否删除 -1:删除 1:正常 */
    validity?: number;
  }

  export class TokenVO {
    /** 过期时间秒 */
    expireIn?: number;

    /** 刷新token */
    refreshToken?: string;

    /** token */
    token?: string;
  }

  export class TreeNode<T0 = any> {
    /** children */
    children?: Array<defs.TreeNode<defs.ProductMktVO>>;

    /** node */
    node: T0;
  }

  export class URI {
    /** absolute */
    absolute?: boolean;

    /** authority */
    authority?: string;

    /** fragment */
    fragment?: string;

    /** host */
    host?: string;

    /** opaque */
    opaque?: boolean;

    /** path */
    path?: string;

    /** port */
    port?: number;

    /** query */
    query?: string;

    /** rawAuthority */
    rawAuthority?: string;

    /** rawFragment */
    rawFragment?: string;

    /** rawPath */
    rawPath?: string;

    /** rawQuery */
    rawQuery?: string;

    /** rawSchemeSpecificPart */
    rawSchemeSpecificPart?: string;

    /** rawUserInfo */
    rawUserInfo?: string;

    /** scheme */
    scheme?: string;

    /** schemeSpecificPart */
    schemeSpecificPart?: string;

    /** userInfo */
    userInfo?: string;
  }

  export class URL {
    /** authority */
    authority?: string;

    /** content */
    content?: object;

    /** defaultPort */
    defaultPort?: number;

    /** deserializedFields */
    deserializedFields?: defs.URLStreamHandler;

    /** file */
    file?: string;

    /** host */
    host?: string;

    /** path */
    path?: string;

    /** port */
    port?: number;

    /** protocol */
    protocol?: string;

    /** query */
    query?: string;

    /** ref */
    ref?: string;

    /** serializedHashCode */
    serializedHashCode?: number;

    /** userInfo */
    userInfo?: string;
  }

  export class URLStreamHandler {}

  export class UpdateCartItemVO {
    /** 商品数量 */
    count?: number;

    /** 购物车商品项唯一id */
    itemId?: number;

    /** skuId */
    skuId?: string;
  }

  export class UpdateCouponTemplateStatusDTO {
    /** 券模板id 修改时需要 */
    id?: number;

    /** 状态0启用 1禁用 */
    status?: number;
  }

  export class UserCar {
    /** buyTime */
    buyTime?: string;

    /** carBrand */
    carBrand?: number;

    /** carBrandName */
    carBrandName?: string;

    /** carModel */
    carModel?: number;

    /** carModelName */
    carModelName?: string;

    /** createTime */
    createTime?: string;

    /** driveLicense */
    driveLicense?: string;

    /** driveMileage */
    driveMileage?: string;

    /** engineNo */
    engineNo?: string;

    /** frameNo */
    frameNo?: string;

    /** icon */
    icon?: string;

    /** id */
    id?: number;

    /** plateCity */
    plateCity?: string;

    /** plateNo */
    plateNo?: string;

    /** plateProvince */
    plateProvince?: string;

    /** remark */
    remark?: string;

    /** sendPoint */
    sendPoint?: number;

    /** sendReason */
    sendReason?: string;

    /** updateTime */
    updateTime?: string;

    /** userId */
    userId?: number;
  }

  export class UserCarListVO {
    /** 购买时间 */
    buyTime?: string;

    /** 车系 */
    carModel?: string;

    /** 车系名称 */
    carModelName?: string;

    /** id */
    id?: number;

    /** 车牌号-市 */
    plateCity?: string;

    /** 车牌号 */
    plateNo?: string;

    /** 车牌号-省 */
    plateProvince?: string;
  }

  export class UserInfo {
    /** birthday */
    birthday?: string;

    /** carNum */
    carNum?: number;

    /** createTime */
    createTime?: string;

    /** 是否是员工,1是，0否 */
    employer?: number;

    /** forzePoint */
    forzePoint?: number;

    /** id */
    id?: number;

    /** identNo */
    identNo?: string;

    /** memberLevel */
    memberLevel?: string;

    /** memberLevelBenefit */
    memberLevelBenefit?: string;

    /** nickname */
    nickname?: string;

    /** openid */
    openid?: string;

    /** phone */
    phone?: string;

    /** point */
    point?: number;

    /** regChannel */
    regChannel?: number;

    /** remark */
    remark?: string;

    /** sex */
    sex?: string;

    /** status */
    status?: number;

    /** statusName */
    statusName?: string;

    /** tags */
    tags?: string;

    /** tagsName */
    tagsName?: Array<string>;

    /** tagsNames */
    tagsNames?: string;

    /** unionid */
    unionid?: string;

    /** uniqueId */
    uniqueId?: number;

    /** userName */
    userName?: string;

    /** userType */
    userType?: number;
  }

  export class UserInfoVO {
    /** 车辆数量 */
    carNum?: number;

    /** 是否是员工,1是，0否 */
    employer?: number;

    /** 会员等级:1普通会员，2高级会员 */
    memberLevel?: string;

    /** 昵称 */
    nickname?: string;

    /** 用户电话 */
    phone?: string;

    /** 账户余额 */
    point?: number;

    /** 用户车辆信息 */
    userCarList?: Array<defs.UserCar>;

    /** 用户名 */
    userName?: string;
  }

  export class UserLoginVO {
    /** 是否是新用户 */
    isNew?: boolean;

    /** true:需要绑定手机号 */
    needBind?: boolean;

    /** 提示信息 */
    notice?: string;

    /** openid */
    openid?: string;

    /** 用户登录成功后返回ticket */
    ticket?: string;
  }

  export class UserPointRecordListVO {
    /** 交易后积点 */
    afterPoint?: number;

    /** batchNo */
    batchNo?: number;

    /** createTime */
    createTime?: string;

    /** 交易前积点 */
    currentPoint?: number;

    /** 交易积分(有正负) */
    flowPoint?: number;

    /** orderNo */
    orderNo?: string;

    /** phone */
    phone?: string;

    /** remark */
    remark?: string;

    /** 交易方 */
    tradeCompany?: string;

    /** tradeType */
    tradeType?: number;

    /** userId */
    userId?: number;

    /** userName */
    userName?: string;
  }

  export class UserPointVO {
    /** list */
    list?: defs.Page<defs.UserPointRecordListVO>;

    /** point */
    point?: number;
  }

  export class UserRechargeBatch {
    /** auditRemark */
    auditRemark?: string;

    /** auditStatus */
    auditStatus?: number;

    /** auditStatusName */
    auditStatusName?: string;

    /** auditTime */
    auditTime?: string;

    /** auditUser */
    auditUser?: string;

    /** batchNo */
    batchNo?: number;

    /** createTime */
    createTime?: string;

    /** createUser */
    createUser?: string;

    /** files */
    files?: string;

    /** id */
    id?: number;

    /** 充值描述 */
    rechargeReason?: string;

    /** 备注 */
    remark?: string;

    /** sendStatus */
    sendStatus?: number;

    /** successMember */
    successMember?: number;

    /** title */
    title?: string;

    /** totalMember */
    totalMember?: number;

    /** totalPoint */
    totalPoint?: number;

    /** type */
    type?: number;

    /** typeName */
    typeName?: string;
  }

  export class UserSmsLoginDTO {
    /** 手机号 */
    mobile?: string;

    /** 短信验证码 */
    veriftyCode?: string;
  }

  export class VerifiedCouponListVO {
    /** 消费时间 */
    consumeTime?: string;

    /** 卡券名称 */
    name?: string;

    /** 卡券类型 1、兑换券 */
    type?: number;

    /** 核销码 */
    verifyCode?: string;

    /** 核销人姓名 */
    verifyName?: string;
  }

  export class VerifyDTO {
    /** 订单详情id */
    orderDetailId?: number;

    /** 强制核销说明(强制核销必传) */
    remark?: string;

    /** 核销码(核销码核销时必传) */
    verifyCode?: string;

    /** 核销类型(1:核销码核销 2:强制核销) */
    verifyType?: number;
  }

  export class WechatGenerateUrlDTO {
    /** 云开发静态网站自定义 H5 配置参数，可配置中转的云开发 H5 页面。不填默认用官方 H5 页面 */
    cloud_base?: string;

    /** 到期失效的URL Link的失效间隔天数。生成的到期失效URL Link在该间隔时间到达前有效。最长间隔天数为365天。expire_type 为 1 必填 */
    expire_interval?: number;

    /** 到期失效的 URL Link 的失效时间，为 Unix 时间戳。生成的到期失效 URL Link 在该时间前有效。最长有效期为1年。expire_type 为 0 必填 */
    expire_time?: number;

    /** 失效类型，失效时间：0，失效间隔天数：1 */
    expire_type?: number;

    /** 生成的 URL Link 类型，到期失效：true，永久有效：false */
    is_expire?: boolean;

    /** URL Link 进入的小程序页面路径，必须是已经发布的小程序存在的页面，不可携带 query 。path 为空时会跳转小程序主页 */
    path?: string;

    /** 通过 URL Link 进入小程序时的query，最大1024个字符，只支持数字，大小写英文以及部分特殊字符：!#$&'()*+,/:;=?@-._~ */
    query?: string;
  }
}

declare namespace API {
  /**
   * 后端日志
   */
  export namespace adminSysLog {
    /**
     * 列表
     * /admin/log/detail
     */
    export namespace detail {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.AdminSysLog>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 列表
     * /admin/log/list
     */
    export namespace list {
      export type RequestBody = defs.AdminSysLogQueryDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.AdminSysLog>
      >;

      export const init: Response;

      export function request(
        data: defs.AdminSysLogQueryDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 后端用户管理
   */
  export namespace adminUser {
    /**
     * 添加
     * /admin/user/add
     */
    export namespace add {
      export type RequestBody = defs.AdminUserDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.AdminUserDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 删除
     * /admin/user/delete/{id}
     */
    export namespace remove {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 详情
     * /admin/user/detail
     */
    export namespace detail {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.AdminUserVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 列表
     * /admin/user/list
     */
    export namespace list {
      export type RequestBody = defs.AdminUserQueryDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.AdminUserVO>
      >;

      export const init: Response;

      export function request(
        data: defs.AdminUserQueryDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 重置密码
     * /admin/user/resetPassword
     */
    export namespace resetPassword {
      export type RequestBody = defs.ResetPasswordDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.ResetPasswordDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 修改
     * /admin/user/update
     */
    export namespace update {
      export type RequestBody = defs.AdminUserDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.AdminUserDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 后台管理-异步任务接口（导入/导出）
   */
  export namespace asyncTask {
    /**
     * 任务添加
     * /admin/async-task/add
     */
    export namespace addTask {
      export type RequestBody = defs.AsyncTaskDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.AsyncTaskDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 任务查询
     * /admin/async-task/list
     */
    export namespace list {
      export class Params {
        /** taskType, SHIPMENT_TASK:发货任务导出 */
        taskType?:
          | 'SUPPLIER_LIST_EXPORT'
          | 'SUPPLIER_LIST_IMPORT'
          | 'SUPPLIER_PAYMENT_CODE_EXPORT'
          | 'COUPON_LIST_EXPORT'
          | 'COUPON_PUBLISH_LIST_EXPORT'
          | 'PRODUCT_LIST_EXPORT'
          | 'PRODUCT_RECYCLE_LIST_EXPORT'
          | 'PRODUCT_SALES_LIST_EXPORT'
          | 'PRODUCT_SALES_DETAILS_LIST_EXPORT'
          | 'STOCK_RECORDS_LIST_EXPORT'
          | 'STOCK_WARN_LIST_EXPORT'
          | 'ORDER_LIST_EXPORT'
          | 'AFTERSALE_LIST_EXPORT'
          | 'SALEDETAIL_LIST_EXPORT'
          | 'SALESTATISTICS_LIST_EXPORT'
          | 'CUSTOMER_LIST_EXPORT'
          | 'CUSTOMER_IMPORT'
          | 'CUSTOMER_RECHARGE_LIST_EXPORT'
          | 'CUSTOMER_RECHARGE_REPORT_EXPORT'
          | 'CUSTOMER_CAR_LIST_EXPORT'
          | 'CUSTOMER_CAR_IMPORT';
      }

      export type Response = defs.ResponseModel<Array<defs.AdminAsyncTask>>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 后端用户认证
   */
  export namespace auth {
    /**
     * 修改密码
     * /admin/auth/changePassword
     */
    export namespace changePassword {
      export type RequestBody = defs.ChangePasswordDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.ChangePasswordDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 登录
     * /admin/auth/login
     */
    export namespace login {
      export type RequestBody = defs.LoginDTO;

      export type Response = defs.ResponseModel<defs.TokenVO>;

      export const init: Response;

      export function request(
        data: defs.LoginDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 退出
     * /admin/auth/logout
     */
    export namespace logout {
      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 当前登录用户信息
     * /admin/auth/me
     */
    export namespace me {
      export type Response = defs.ResponseModel<defs.AdminUserVO>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 刷新token
     * /admin/auth/refresh/token
     */
    export namespace refreshToken {
      export type Response = defs.ResponseModel<defs.TokenVO>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }
  }

  /**
   * 售后单后台接口
   */
  export namespace backendAftersale {
    /**
        * 申请售后
申请售后
        * /backend/aftersale/applyAftersale
        */
    export namespace applyAftersale {
      export type RequestBody = defs.AftersaleApplyDTO;

      export type Response = string;

      export const init: Response;

      export function request(
        data: defs.AftersaleApplyDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 批量审核售后单
批量审核售后单
        * /backend/aftersale/batchAuditAftersale
        */
    export namespace batchAuditAftersale {
      export type RequestBody = defs.AftersaleAuditDTO;

      export type Response = number;

      export const init: Response;

      export function request(
        data: defs.AftersaleAuditDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 根据售后id查询售后单详细信息
根据售后id查询售后单详细信息
        * /backend/aftersale/getAftersaleByAftersaleNo/{aftersaleNo}
        */
    export namespace getAftersaleByAftersaleNo {
      export class Params {
        /** aftersaleNo */
        aftersaleNo: string;
      }

      export type Response = defs.AftersaleDetailInfoVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 根据售后id查询售后单详细信息
根据售后id查询售后单详细信息
        * /backend/aftersale/getAftersaleById/{id}
        */
    export namespace getAftersaleById {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.AftersaleDetailInfoVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 分页获取售后单列表
分页获取售后单列表
        * /backend/aftersale/getAftersalePagedInfo
        */
    export namespace getAftersalePagedInfo {
      export type RequestBody = defs.AftersaleListQueryDTO;

      export type Response = defs.AftersaleDetailInfoVO;

      export const init: Response;

      export function request(
        data: defs.AftersaleListQueryDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 订单后台接口
   */
  export namespace backendOrder {
    /**
        * 取消订单
取消订单
        * /backend/order/cancel
        */
    export namespace cancel {
      export type RequestBody = defs.OrderCancelDTO;

      export type Response = number;

      export const init: Response;

      export function request(
        data: defs.OrderCancelDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 根据订单id查询订单详细信息
根据订单id查询订单详细信息
        * /backend/order/getOrderById/{id}
        */
    export namespace getOrderById {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.OrderDetailInfoVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 获取订单操作日志
获取订单操作日志
        * /backend/order/getOrderOperateLog/{id}
        */
    export namespace getOrderOperateLog {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.OrderTraceVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 分页获取订单列表
分页获取订单列表
        * /backend/order/getOrderPagedInfo
        */
    export namespace getOrderPagedInfo {
      export type RequestBody = defs.OrderListQueryDTO;

      export type Response = object;

      export const init: Response;

      export function request(
        data: defs.OrderListQueryDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 分页获取交易明细列表
分页获取交易明细列表
        * /backend/order/getSaleDetailPagedInfo
        */
    export namespace getSaleDetailPagedInfo {
      export type RequestBody = defs.SaleDetailListQueryDTO;

      export type Response = object;

      export const init: Response;

      export function request(
        data: defs.SaleDetailListQueryDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 分页获取交易统计列表
分页获取交易统计列表
        * /backend/order/getSaleStatisticsPagedInfo
        */
    export namespace getSaleStatisticsPagedInfo {
      export type RequestBody = defs.SaleDetailListQueryDTO;

      export type Response = object;

      export const init: Response;

      export function request(
        data: defs.SaleDetailListQueryDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 核销订单
核销订单
        * /backend/order/verify
        */
    export namespace verify {
      export type RequestBody = defs.VerifyDTO;

      export type Response = number;

      export const init: Response;

      export function request(
        data: defs.VerifyDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 后端业务类型管理
   */
  export namespace businessTypeBackend {
    /**
     * 添加
     * /backend/businessType/add
     */
    export namespace add {
      export type RequestBody = defs.BaseBusinessTypeDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.BaseBusinessTypeDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取所有有效状态业务类型
     * /backend/businessType/all
     */
    export namespace getAll {
      export type Response = defs.ResponseModel<Array<defs.BaseBusinessTypeVO>>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 删除
     * /backend/businessType/delete/{id}
     */
    export namespace remove {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过id获取详细信息
     * /backend/businessType/detail/{id}
     */
    export namespace getDetailById {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.BaseBusinessTypeVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取供应商业务类型列表（分页）
     * /backend/businessType/list
     */
    export namespace getPageInfo {
      export type RequestBody = defs.BaseDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.BaseBusinessTypeVO>
      >;

      export const init: Response;

      export function request(
        data: defs.BaseDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 编辑
     * /backend/businessType/update
     */
    export namespace update {
      export type RequestBody = defs.BaseBusinessTypeDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.BaseBusinessTypeDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 后端车型品牌管理
   */
  export namespace carBrandBackend {
    /**
     * 添加
     * /backend/carBrand/add
     */
    export namespace add {
      export type RequestBody = defs.BaseCarBrandDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.BaseCarBrandDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取所有有效状态车型品牌
     * /backend/carBrand/all
     */
    export namespace getAll {
      export type Response = defs.ResponseModel<Array<defs.BaseCarBrandVO>>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 获取所有有效状态车型品牌和车型树
     * /backend/carBrand/brandAndType/tree
     */
    export namespace brandAndTypeTree {
      export type Response = defs.ResponseModel<Array<defs.BaseCarBrandVO>>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 删除
     * /backend/carBrand/delete/{id}
     */
    export namespace remove {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过id获取详细信息
     * /backend/carBrand/detail/{id}
     */
    export namespace getDetailById {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.BaseCarBrandVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取车型品牌列表（分页）
     * /backend/carBrand/list
     */
    export namespace getPageInfo {
      export type RequestBody = defs.BaseDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.BaseCarBrandVO>
      >;

      export const init: Response;

      export function request(
        data: defs.BaseDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过ids获取列表
     * /backend/carBrand/listByIds
     */
    export namespace getListByIds {
      export type RequestBody = Array<number>;

      export type Response = defs.ResponseModel<Array<defs.BaseCarBrandVO>>;

      export const init: Response;

      export function request(
        data: Array<number>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 编辑
     * /backend/carBrand/update
     */
    export namespace update {
      export type RequestBody = defs.BaseCarBrandDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.BaseCarBrandDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 前端-车型品牌
   */
  export namespace carBrandFront {
    /**
     * 获取所有有效状态车型品牌
     * /front/carBrand/all
     */
    export namespace getAll {
      export type Response = defs.ResponseModel<Array<defs.BaseCarBrandVO>>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }
  }

  /**
   * 后端车型管理
   */
  export namespace carTypeBackend {
    /**
     * 添加
     * /backend/carType/add
     */
    export namespace add {
      export type RequestBody = defs.BaseCarTypeDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.BaseCarTypeDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取所有有效状态车型
     * /backend/carType/all
     */
    export namespace getAll {
      export type Response = defs.ResponseModel<Array<defs.BaseCarTypeVO>>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 删除
     * /backend/carType/delete/{id}
     */
    export namespace remove {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过id获取详细信息
     * /backend/carType/detail/{id}
     */
    export namespace getDetailById {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.BaseCarTypeVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取车型列表通过品牌ID
     * /backend/carType/getListByBrandId
     */
    export namespace getListByBrandId {
      export class Params {
        /** brandId */
        brandId: number;
      }

      export type Response = defs.ResponseModel<Array<defs.BaseCarTypeVO>>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取车型列表（分页）
     * /backend/carType/list
     */
    export namespace getPageInfo {
      export type RequestBody = defs.QueryCarTypeDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.BaseCarTypeVO>
      >;

      export const init: Response;

      export function request(
        data: defs.QueryCarTypeDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过ids获取列表
     * /backend/carType/listByIds
     */
    export namespace getListByIds {
      export type RequestBody = Array<number>;

      export type Response = defs.ResponseModel<Array<defs.BaseCarTypeVO>>;

      export const init: Response;

      export function request(
        data: Array<number>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 编辑
     * /backend/carType/update
     */
    export namespace update {
      export type RequestBody = defs.BaseCarTypeDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.BaseCarTypeDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 前端-车型管理
   */
  export namespace carTypeFront {
    /**
     * 获取所有有效状态车型
     * /front/carType/all
     */
    export namespace getAll {
      export type Response = defs.ResponseModel<Array<defs.BaseCarTypeVO>>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }
  }

  /**
   * 后端卡券标签管理
   */
  export namespace cardLabelBackend {
    /**
     * 添加
     * /backend/cardLabel/add
     */
    export namespace add {
      export type RequestBody = defs.BaseCardLabelDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.BaseCardLabelDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取所有有效状态卡券标签
     * /backend/cardLabel/all
     */
    export namespace getAll {
      export type Response = defs.ResponseModel<Array<defs.BaseCardLabelVO>>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 删除
     * /backend/cardLabel/delete/{id}
     */
    export namespace remove {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过id获取详细信息
     * /backend/cardLabel/detail/{id}
     */
    export namespace getDetailById {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.BaseCardLabelVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取卡券标签列表（分页）
     * /backend/cardLabel/list
     */
    export namespace getPageInfo {
      export type RequestBody = defs.BaseDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.BaseCardLabelVO>
      >;

      export const init: Response;

      export function request(
        data: defs.BaseDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过ids获取列表
     * /backend/cardLabel/listByIds
     */
    export namespace getListByIds {
      export type RequestBody = Array<number>;

      export type Response = defs.ResponseModel<Array<defs.BaseCardLabelVO>>;

      export const init: Response;

      export function request(
        data: Array<number>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 编辑
     * /backend/cardLabel/update
     */
    export namespace update {
      export type RequestBody = defs.BaseCardLabelDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.BaseCardLabelDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 购物车接口
   */
  export namespace cart {
    /**
     * 获取购物车信息(硬刷，会拿回商品实时信息)
     * /front/cart/getCartByList
     */
    export namespace getCartByList {
      export type RequestBody = Array<string>;

      export type Response = defs.CartVO;

      export const init: Response;

      export function request(
        data: Array<string>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取购物车数量
     * /front/cart/getCartCount
     */
    export namespace getCartCount {
      export type Response = number;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 获取购物车价格
     * /front/cart/getCartPrice
     */
    export namespace getCartPrice {
      export type RequestBody = Array<string>;

      export type Response = defs.CartPriceVO;

      export const init: Response;

      export function request(
        data: Array<string>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 购物车商品接口
   */
  export namespace cartItem {
    /**
        * 添加商品到购物车（商品详情页用，实时校验库存等信息）
添加商品到购物车
        * /front/cartItem/addCartItem
        */
    export namespace addCartItem {
      export type RequestBody = defs.CartItemDTO;

      export type Response = number;

      export const init: Response;

      export function request(
        data: defs.CartItemDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 添加商品到购物车（购物车内用，独立增加数量，不做校验）
添加商品到购物车
        * /front/cartItem/addCartItemUnValidate
        */
    export namespace addCartItemUnValidate {
      export type RequestBody = defs.CartItemDTO;

      export type Response = defs.CartItemProductDto;

      export const init: Response;

      export function request(
        data: defs.CartItemDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 删除购物车中的商品
     * /front/cartItem/deleteCartItems
     */
    export namespace deleteCartItem {
      export type RequestBody = Array<string>;

      export type Response = object;

      export const init: Response;

      export function request(
        data: Array<string>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 更新购物车商品sku
     * /front/cartItem/updateCartItem
     */
    export namespace updateCartItem {
      export type RequestBody = defs.UpdateCartItemVO;

      export type Response = number;

      export const init: Response;

      export function request(
        data: defs.UpdateCartItemVO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 收银台接口
   */
  export namespace cashier {
    /**
     * 获取支付方式列表
     * /front/cashier/listPayWay
     */
    export namespace listPayWay {
      export class Params {
        /** orderNo */
        orderNo?: string;
      }

      export type Response = defs.ResponseModel<Array<defs.PaymentVO>>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 统一支付接口
     * /front/cashier/pay
     */
    export namespace pay {
      export type RequestBody = defs.CashierPayDto;

      export type Response = object;

      export const init: Response;

      export function request(
        data: defs.CashierPayDto,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 商品管理后台接口【品类管理接口】
   */
  export namespace categoryBackend {
    /**
        * 创建品类
创建品类
        * /backend/category/createCategory
        */
    export namespace createCategory {
      export type RequestBody = defs.CategoryDTO;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: defs.CategoryDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 删除品类
删除品类
        * /backend/category/deleteCategory/{categoryCode}
        */
    export namespace deleteCategory {
      export class Params {
        /** 品类编码 */
        categoryCode: string;
      }

      export type Response = boolean;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 查询品类信息
查询品类信息
        * /backend/category/getCategory/{categoryCode}
        */
    export namespace getCategory {
      export class Params {
        /** 品类编码 */
        categoryCode: string;
      }

      export type Response = defs.CategoryVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 查询品类列表
查询品类列表
        * /backend/category/getCategoryList
        */
    export namespace getCategoryList {
      export type Response = Array<defs.CategoryVO>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
        * 分页查询品类列表
分页查询品类列表
        * /backend/category/getPageList
        */
    export namespace getPageList {
      export type RequestBody = defs.QueryCategoryDTO;

      export type Response = defs.CategoryVO;

      export const init: Response;

      export function request(
        data: defs.QueryCategoryDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 更新品类
更新品类
        * /backend/category/updateCategory
        */
    export namespace updateCategory {
      export type RequestBody = defs.CategoryDTO;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: defs.CategoryDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 内容管理-公共类接口
   */
  export namespace commonBackend {
    /**
     * 上传文件到公网访问桶
     * /backend/cms/common/uploadFile
     */
    export namespace uploadFile {
      export type Response = defs.ResponseModel<string>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 上传文件到私有桶
     * /backend/cms/common/uploadFilePrivate
     */
    export namespace uploadFilePrivate {
      export type Response = defs.ResponseModel<string>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 上传图片到公网访问桶
     * /backend/cms/common/uploadImage
     */
    export namespace uploadImage {
      export type Response = defs.ResponseModel<string>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }
  }

  /**
   * 内容管理-公共类接口-前台接口
   */
  export namespace commonFront {
    /**
     * 上传图片到公网访问桶
     * /front/cms/common/uploadImage
     */
    export namespace uploadImage {
      export type Response = defs.ResponseModel<string>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }
  }

  /**
   * 后端-地址查询
   */
  export namespace countryArea {
    /**
     * 通过parentId获取地址列表，parentId为空返回省级列表
     * /backend/countryArea/list
     */
    export namespace listByParentId {
      export class Params {
        /** parentId */
        parentId?: number;
      }

      export type Response = defs.ResponseModel<Array<defs.CountryAreaObject>>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 前端优惠券
   */
  export namespace coupon {
    /**
     * 通过核销码获取优惠券详情
     * /coupon/getDetailByVerifyCode
     */
    export namespace getDetailByVerifyCode {
      export class Params {
        /** verifyCode */
        verifyCode: string;
      }

      export type Response = defs.ResponseModel<defs.CouponObject>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取核销码
     * /coupon/getVerifyCode
     */
    export namespace getVerifyCode {
      export class Params {
        /** publishBatchId */
        publishBatchId: number;
        /** templateId */
        templateId: number;
      }

      export type Response = defs.ResponseModel<string>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 列表
     * /coupon/myCouponList
     */
    export namespace myCouponList {
      export type RequestBody = defs.MyCouponQueryDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.MyCouponListVO>
      >;

      export const init: Response;

      export function request(
        data: defs.MyCouponQueryDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 已核销列表
     * /coupon/verifiedList
     */
    export namespace verifiedList {
      export class Params {
        /** pageIndex */
        pageIndex: number;
        /** pageSize */
        pageSize: number;
      }

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.VerifiedCouponListVO>
      >;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 核销
     * /coupon/verify
     */
    export namespace verify {
      export type RequestBody = defs.CouponVerifyDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.CouponVerifyDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 优惠券管理
   */
  export namespace couponBackend {
    /**
     * 废弃
     * /admin/coupon/discard
     */
    export namespace discard {
      export type RequestBody = defs.DiscardDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.DiscardDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 强制核销优惠券
     * /admin/coupon/forceVerify
     */
    export namespace forceVerify {
      export type RequestBody = defs.CouponVerifyDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.CouponVerifyDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过核销码获取优惠券详情
     * /admin/coupon/getDetailByVerifyCode
     */
    export namespace getDetailByVerifyCode {
      export class Params {
        /** verifyCode */
        verifyCode: string;
      }

      export type Response = defs.ResponseModel<defs.CouponObject>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 列表
     * /admin/coupon/list
     */
    export namespace list {
      export type RequestBody = defs.CouponQueryDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.CouponObject>
      >;

      export const init: Response;

      export function request(
        data: defs.CouponQueryDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 核销
     * /admin/coupon/verify
     */
    export namespace verify {
      export type RequestBody = defs.CouponVerifyDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.CouponVerifyDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 优惠券发放
   */
  export namespace couponPublishBatch {
    /**
     * 添加
     * /admin/coupon/publishBatch/add
     */
    export namespace add {
      export type RequestBody = defs.CouponPublishBatchDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.CouponPublishBatchDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 审核
     * /admin/coupon/publishBatch/audit
     */
    export namespace audit {
      export type RequestBody = defs.CouponPublishBatchAuditDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.CouponPublishBatchAuditDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 删除
     * /admin/coupon/publishBatch/delete/{id}
     */
    export namespace remove {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 详情
     * /admin/coupon/publishBatch/detail
     */
    export namespace detail {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.CouponPublishBatchObject>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 模板下载
     * /admin/coupon/publishBatch/downloadTemplate
     */
    export namespace downloadTemplate {
      export type Response = any;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 列表
     * /admin/coupon/publishBatch/list
     */
    export namespace list {
      export type RequestBody = defs.CouponPublishBatchQueryDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.CouponPublishBatchListVO>
      >;

      export const init: Response;

      export function request(
        data: defs.CouponPublishBatchQueryDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 模板解析
     * /admin/coupon/publishBatch/parseTemplate
     */
    export namespace parseTemplate {
      export type Response = defs.ResponseModel<
        Array<defs.CouponPublishBatchCustomerDTO>
      >;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 修改
     * /admin/coupon/publishBatch/update
     */
    export namespace update {
      export type RequestBody = defs.CouponPublishBatchDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.CouponPublishBatchDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 优惠券模板管理
   */
  export namespace couponTemplate {
    /**
     * 添加
     * /admin/coupon/template/add
     */
    export namespace add {
      export type RequestBody = defs.CouponTemplateDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.CouponTemplateDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 详情
     * /admin/coupon/template/detail
     */
    export namespace detail {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.CouponTemplateVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 列表
     * /admin/coupon/template/list
     */
    export namespace list {
      export type RequestBody = defs.CouponTemplateQueryDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.CouponTemplateVO>
      >;

      export const init: Response;

      export function request(
        data: defs.CouponTemplateQueryDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 修改
     * /admin/coupon/template/update
     */
    export namespace update {
      export type RequestBody = defs.CouponTemplateDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.CouponTemplateDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 修改状态
     * /admin/coupon/template/updateStatus
     */
    export namespace updateStatus {
      export type RequestBody = defs.UpdateCouponTemplateStatusDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.UpdateCouponTemplateStatusDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 后端字典管理
   */
  export namespace dictBackend {
    /**
     * 添加
     * /backend/dict/add
     */
    export namespace add {
      export type RequestBody = defs.SettingDictDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.SettingDictDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 删除
     * /backend/dict/delete/{id}
     */
    export namespace remove {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过id获取详细信息
     * /backend/dict/detail/{id}
     */
    export namespace getDetailById {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.SettingDictVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过code,label获取字典信息
     * /backend/dict/detailByCodeLabel
     */
    export namespace getDetailByCodeLabel {
      export class Params {
        /** code */
        code: string;
        /** label */
        label?: string;
      }

      export type Response = defs.ResponseModel<
        Array<defs.SettingDictDetailObject>
      >;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取导入模版
     * /backend/dict/importTemplate
     */
    export namespace getImportTemplate {
      export class Params {
        /** label */
        label: string;
      }

      export type Response = defs.ResponseModel<
        Array<defs.SettingDictDetailObject>
      >;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取字典列表（分页）
     * /backend/dict/list
     */
    export namespace getPageInfo {
      export type RequestBody = defs.QueryDictDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.SettingDictVO>
      >;

      export const init: Response;

      export function request(
        data: defs.QueryDictDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过codes获取列表
     * /backend/dict/listByCodes
     */
    export namespace getListByCodes {
      export type RequestBody = Array<string>;

      export type Response = defs.ResponseModel<Array<defs.SettingDictVO>>;

      export const init: Response;

      export function request(
        data: Array<string>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 编辑
     * /backend/dict/update
     */
    export namespace update {
      export type RequestBody = defs.SettingDictDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.SettingDictDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 前端-字典管理
   */
  export namespace dictFront {
    /**
     * 通过id获取详细信息
     * /front/dict/detail/{id}
     */
    export namespace getDetailById {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.SettingDictVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过code,label获取字典信息
     * /front/dict/detailByCodeLabel
     */
    export namespace getDetailByCodeLabel {
      export class Params {
        /** code */
        code: string;
        /** label */
        label?: string;
      }

      export type Response = defs.ResponseModel<
        Array<defs.SettingDictDetailObject>
      >;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过codes获取列表
     * /front/dict/listByCodes
     */
    export namespace getListByCodes {
      export type RequestBody = Array<string>;

      export type Response = defs.ResponseModel<Array<defs.SettingDictVO>>;

      export const init: Response;

      export function request(
        data: Array<string>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 订单前台接口
   */
  export namespace frontOrder {
    /**
        * 取消订单
取消订单
        * /front/order/cancel
        */
    export namespace cancel {
      export type RequestBody = defs.OrderCancelDTO;

      export type Response = number;

      export const init: Response;

      export function request(
        data: defs.OrderCancelDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 创建订单
创建订单
        * /front/order/createOrder
        */
    export namespace createOrder {
      export type RequestBody = defs.OrderCreateDTO;

      export type Response = string;

      export const init: Response;

      export function request(
        data: defs.OrderCreateDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 根据订单id查询订单详细信息
根据订单id查询订单详细信息
        * /front/order/getOrderById/{id}
        */
    export namespace getOrderById {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.OrderDetailInfoVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 根据订单号查询订单详细信息
根据订单号查询订单详细信息
        * /front/order/getOrderByOrgOrderNo/{orgOrderNo}
        */
    export namespace getOrderByOrgOrderNo {
      export class Params {
        /** orgOrderNo */
        orgOrderNo: string;
      }

      export type Response = defs.OrderDetailInfoVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 用户订单列表查询
用户订单列表查询
        * /front/order/getOrderPagedInfo
        */
    export namespace getOrderPagedInfo {
      export type RequestBody = defs.OrderListQueryDTO;

      export type Response = defs.OrderDetailInfoVO;

      export const init: Response;

      export function request(
        data: defs.OrderListQueryDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 获取用户不同状态订单数量
获取用户不同状态订单数量
        * /front/order/getOrderStatusCount
        */
    export namespace getOrderStatusCount {
      export type Response = defs.OrderStatusCountVO;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
        * 获取核销记录
获取核销记录
        * /front/order/getVerifyRecord
        */
    export namespace getVerifyRecord {
      export type RequestBody = defs.PublicDto;

      export type Response = defs.OrderDetailInfoVO;

      export const init: Response;

      export function request(
        data: defs.PublicDto,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 核销订单
核销订单
        * /front/order/verify
        */
    export namespace verify {
      export type RequestBody = defs.VerifyDTO;

      export type Response = number;

      export const init: Response;

      export function request(
        data: defs.VerifyDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 后端商城设置管理
   */
  export namespace mallBackend {
    /**
     * 通过id获取详细信息
     * /backend/mall/detail
     */
    export namespace getDetail {
      export type Response = defs.ResponseModel<defs.SettingMallVO>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 编辑
     * /backend/mall/update
     */
    export namespace update {
      export type RequestBody = defs.SettingMallDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.SettingMallDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 前端-商城设置管理
   */
  export namespace mallFront {
    /**
     * 通过id获取详细信息
     * /front/mall/detail
     */
    export namespace getDetail {
      export type Response = defs.ResponseModel<defs.SettingMallVO>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }
  }

  /**
   * 内容管理-会员权益-后台接口
   */
  export namespace memberBenefitsBackend {
    /**
     * 添加会员权益
     * /backend/cms/memberBenefits/create
     */
    export namespace create {
      export type RequestBody = defs.MemberBenefitsDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.MemberBenefitsDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 删除会员权益
     * /backend/cms/memberBenefits/delete
     */
    export namespace remove {
      export type RequestBody = defs.IdDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.IdDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 查询会员权益详情
     * /backend/cms/memberBenefits/detail/{id}
     */
    export namespace detail {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.MemberBenefitsVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取会员权益列表
     * /backend/cms/memberBenefits/list
     */
    export namespace list {
      export type RequestBody = defs.QueryMemberBenefitsDTO;

      export type Response = defs.ResponseModel<Array<defs.MemberBenefitsVO>>;

      export const init: Response;

      export function request(
        data: defs.QueryMemberBenefitsDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 编辑会员权益
     * /backend/cms/memberBenefits/update
     */
    export namespace update {
      export type RequestBody = defs.MemberBenefitsDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.MemberBenefitsDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 内容管理-会员权益-前台接口
   */
  export namespace memberBenefitsFront {
    /**
     * 查询会员权益详情
     * /front/cms/memberBenefits/detail/{id}
     */
    export namespace detail {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.MemberBenefitsBasicVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取会员权益
     * /front/cms/memberBenefits/list
     */
    export namespace list {
      export type Response = defs.ResponseModel<
        Array<defs.MemberBenefitsBasicVO>
      >;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }
  }

  /**
   * 后端会员标签管理
   */
  export namespace memberLabelBackend {
    /**
     * 添加
     * /backend/memberLabel/add
     */
    export namespace add {
      export type RequestBody = defs.BaseMemberLabelDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.BaseMemberLabelDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取所有有效状态会员标签
     * /backend/memberLabel/all
     */
    export namespace getAll {
      export type Response = defs.ResponseModel<Array<defs.BaseMemberLabelVO>>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 删除
     * /backend/memberLabel/delete/{id}
     */
    export namespace remove {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过id获取详细信息
     * /backend/memberLabel/detail/{id}
     */
    export namespace getDetailById {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.BaseMemberLabelVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取会员标签列表（分页）
     * /backend/memberLabel/list
     */
    export namespace getPageInfo {
      export type RequestBody = defs.BaseDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.BaseMemberLabelVO>
      >;

      export const init: Response;

      export function request(
        data: defs.BaseDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过ids获取列表
     * /backend/memberLabel/listByIds
     */
    export namespace getListByIds {
      export type RequestBody = Array<number>;

      export type Response = defs.ResponseModel<Array<defs.BaseMemberLabelVO>>;

      export const init: Response;

      export function request(
        data: Array<number>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 编辑
     * /backend/memberLabel/update
     */
    export namespace update {
      export type RequestBody = defs.BaseMemberLabelDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.BaseMemberLabelDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 内容管理-首页管理-前台接口
   */
  export namespace pageFront {
    /**
     * 清楚页面缓存
     * /front/cms/page/clearCache
     */
    export namespace clearCache {
      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 查询页面配置内容
     * /front/cms/page/home
     */
    export namespace home {
      export type Response = defs.ResponseModel<defs.PageWithItemsBasicVO>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }
  }

  /**
   * 内容管理-首页管理-后台接口
   */
  export namespace pageHomeBackend {
    /**
     * 新增首页
     * /backend/cms/page/home/create
     */
    export namespace create {
      export type RequestBody = defs.PageDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.PageDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 删除首页
     * /backend/cms/page/home/delete
     */
    export namespace remove {
      export type RequestBody = defs.PageIdDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.PageIdDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 查询首页详情
     * /backend/cms/page/home/detail/{id}
     */
    export namespace detail {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.PageVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 查询页面配置内容
     * /backend/cms/page/home/detailWithItems/{id}
     */
    export namespace detailWithItems {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.PageWithItemsVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取首页列表
     * /backend/cms/page/home/list
     */
    export namespace list {
      export type RequestBody = defs.QueryPageDTO;

      export type Response = defs.ResponseModel<defs.Pagination<defs.PageVO>>;

      export const init: Response;

      export function request(
        data: defs.QueryPageDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 装修页面-首页
     * /backend/cms/page/home/saveContent
     */
    export namespace saveContent {
      export type RequestBody = defs.PageContentDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.PageContentDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 发布页面-首页
     * /backend/cms/page/home/setOnline
     */
    export namespace setOnline {
      export type RequestBody = defs.IdLongDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.IdLongDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 编辑首页
     * /backend/cms/page/home/update
     */
    export namespace update {
      export type RequestBody = defs.PageDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.PageDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 设为当前页面-首页
     * /backend/cms/page/home/usePage
     */
    export namespace usePage {
      export type RequestBody = defs.IdLongDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.IdLongDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 内容管理-个人中心-后台接口
   */
  export namespace pageRecommendBackend {
    /**
     * 添加页面入口推荐(个人中心)
     * /backend/cms/pageRecommend/create
     */
    export namespace create {
      export type RequestBody = defs.PageRecommendDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.PageRecommendDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 删除页面入口推荐(个人中心)
     * /backend/cms/pageRecommend/delete
     */
    export namespace remove {
      export type RequestBody = defs.IdDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.IdDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取页面入口推荐列表(个人中心)
     * /backend/cms/pageRecommend/list
     */
    export namespace list {
      export type RequestBody = defs.QueryPageRecommendDTO;

      export type Response = defs.ResponseModel<Array<defs.PageRecommendVO>>;

      export const init: Response;

      export function request(
        data: defs.QueryPageRecommendDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 启用禁用页面入口推荐(个人中心)
     * /backend/cms/pageRecommend/statusChange
     */
    export namespace statusChange {
      export type RequestBody = defs.StatusChangeDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.StatusChangeDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 编辑页面入口推荐(个人中心)
     * /backend/cms/pageRecommend/update
     */
    export namespace update {
      export type RequestBody = defs.PageRecommendDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.PageRecommendDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 内容管理-个人中心-前台接口
   */
  export namespace pageRecommendFront {
    /**
     * 获取页面功能推荐(个人中心)推荐
     * /front/cms/pageRecommend/list
     */
    export namespace list {
      export type RequestBody = defs.QueryPageRecommendFrontDTO;

      export type Response = defs.ResponseModel<
        Array<defs.PageRecommendBasicVO>
      >;

      export const init: Response;

      export function request(
        data: defs.QueryPageRecommendFrontDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 内容管理-专题/服务内容管理-前台页面
   */
  export namespace pageTextFront {
    /**
     * 服务内容详情
     * /cms/pageText/service/{pageCode}
     */
    export namespace detailByPageCode {
      export class Params {
        /** pageCode */
        pageCode: string;
      }

      export type Response = any;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 专题详情
     * /cms/pageText/topic/{id}
     */
    export namespace detailById {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = any;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 内容管理-服务内容-后台接口
   */
  export namespace pageTextServiceBackend {
    /**
     * 查询服务内容详情
     * /backend/cms/pageText/service/detail/{id}
     */
    export namespace detail {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.PageTextVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取服务内容列表
     * /backend/cms/pageText/service/list
     */
    export namespace list {
      export type RequestBody = defs.QueryPageTextDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.PageTextVO>
      >;

      export const init: Response;

      export function request(
        data: defs.QueryPageTextDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 编辑服务内容
     * /backend/cms/pageText/service/update
     */
    export namespace update {
      export type RequestBody = defs.PageTextDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.PageTextDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 内容管理-专题管理-后台接口
   */
  export namespace pageTextTopicBackend {
    /**
     * 添加专题
     * /backend/cms/pageText/topic/create
     */
    export namespace create {
      export type RequestBody = defs.PageTextDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.PageTextDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 删除专题
     * /backend/cms/pageText/topic/delete
     */
    export namespace remove {
      export type RequestBody = defs.IdLongDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.IdLongDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 查询专题详情
     * /backend/cms/pageText/topic/detail/{id}
     */
    export namespace detail {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.PageTextVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取专题列表
     * /backend/cms/pageText/topic/list
     */
    export namespace list {
      export type RequestBody = defs.QueryPageTextDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.PageTextVO>
      >;

      export const init: Response;

      export function request(
        data: defs.QueryPageTextDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取专题列表
     * /backend/cms/pageText/topic/search
     */
    export namespace search {
      export type RequestBody = defs.QueryPageTextDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.PageTextVO>
      >;

      export const init: Response;

      export function request(
        data: defs.QueryPageTextDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 编辑专题
     * /backend/cms/pageText/topic/update
     */
    export namespace update {
      export type RequestBody = defs.PageTextDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.PageTextDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 支付接口
   */
  export namespace pay {
    /**
     * 统一退款
     * /backend/pay/refund
     */
    export namespace refund {
      export type RequestBody = defs.BaseRefundRequest;

      export type Response = defs.ResponseModel<number>;

      export const init: Response;

      export function request(
        data: defs.BaseRefundRequest,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 前端-收款码相关
   */
  export namespace paymentCode {
    /**
     * 通过收款码id获取详细信息
     * /front/payment-code/detail/{id}
     */
    export namespace getDetailById {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.PaymentCodeVOObject>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过code获取详细信息
     * /front/payment-code/detailByCode/{code}
     */
    export namespace getDetailByCode {
      export class Params {
        /** code */
        code: string;
      }

      export type Response = defs.ResponseModel<defs.PaymentCodeVOObject>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 后端收款码管理
   */
  export namespace paymentCodeBackend {
    /**
     * 添加
     * /backend/payment-code/add
     */
    export namespace add {
      export type RequestBody = defs.PaymentCodeDTOObject;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.PaymentCodeDTOObject,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 改变状态
     * /backend/payment-code/changeStatus
     */
    export namespace postChangeStatus {
      export type RequestBody = defs.ChangeStatusDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.ChangeStatusDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 删除
     * /backend/payment-code/delete/{id}
     */
    export namespace remove {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过id获取详细信息
     * /backend/payment-code/detail/{id}
     */
    export namespace getDetailById {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.PaymentCodeVOObject>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过id获取详细信息
     * /backend/payment-code/getNewPaymentCode
     */
    export namespace getNewPaymentCode {
      export type Response = defs.ResponseModel<string>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 获取供应商收款码列表（分页）
     * /backend/payment-code/list
     */
    export namespace getPageInfo {
      export type RequestBody = defs.QueryPaymentCodeDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.PaymentCodeVOObject>
      >;

      export const init: Response;

      export function request(
        data: defs.QueryPaymentCodeDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 编辑
     * /backend/payment-code/update
     */
    export namespace update {
      export type RequestBody = defs.PaymentCodeDTOObject;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.PaymentCodeDTOObject,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 后端权限管理
   */
  export namespace permission {
    /**
     * 添加
     * /admin/permission/add
     */
    export namespace add {
      export type RequestBody = defs.Permission;

      export type Response = defs.ResponseModel<number>;

      export const init: Response;

      export function request(
        data: defs.Permission,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 删除
     * /admin/permission/delete/{id}
     */
    export namespace remove {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 列表
     * /admin/permission/list
     */
    export namespace list {
      export type Response = defs.ResponseModel<Array<defs.Permission>>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 列表树
     * /admin/permission/tree
     */
    export namespace tree {
      export type Response = defs.ResponseModel<Array<defs.Permission>>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 修改
     * /admin/permission/update
     */
    export namespace update {
      export type RequestBody = defs.Permission;

      export type Response = defs.ResponseModel<number>;

      export const init: Response;

      export function request(
        data: defs.Permission,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 积点相关接口
   */
  export namespace point {
    /**
     * 积点充值审批
     * /points/auditRecharge
     */
    export namespace auditRecharge {
      export type RequestBody = defs.AuditRechargeDTO;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: defs.AuditRechargeDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 删除充值批次
     * /points/delRecharge/{id}
     */
    export namespace delRecharge {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = boolean;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 积点充值修改
     * /points/modifyRecharge
     */
    export namespace modifyRecharge {
      export type RequestBody = defs.ModifyRechargeBatchDTO;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: defs.ModifyRechargeBatchDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 积点变动记录
     * /points/pointListPage
     */
    export namespace ListPage {
      export type RequestBody = defs.QueryPointRecordDTO;

      export type Response = defs.Page;

      export const init: Response;

      export function request(
        data: defs.QueryPointRecordDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 积点充值
     * /points/recharge
     */
    export namespace recharge {
      export type RequestBody = defs.CreateRechargeBatchDTO;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: defs.CreateRechargeBatchDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 积点充值详情查看
     * /points/rechargeDetail/{id}
     */
    export namespace postRechargeDetailById {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.RechargeDetailVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 会员充值列表
     * /points/rechargePage
     */
    export namespace rechargePage {
      export type RequestBody = defs.RechargePageDTO;

      export type Response = defs.UserRechargeBatch;

      export const init: Response;

      export function request(
        data: defs.RechargePageDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 积点交易
     * /points/trial
     */
    export namespace trial {
      export type RequestBody = defs.PointTransInfoDTO;

      export type Response = defs.PointTrandVO;

      export const init: Response;

      export function request(
        data: defs.PointTransInfoDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 积点交易结果查询
     * /points/trialResult/{transNo}
     */
    export namespace trialResult {
      export class Params {
        /** transNo */
        transNo: string;
      }

      export type Response = defs.PointTrandVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 商品管理后台接口【上下架管理接口】
   */
  export namespace productApproval {
    /**
        * 批量审核
批量审核
        * /backend/spu/approval
        */
    export namespace approval {
      export type RequestBody = defs.ProductApprovalDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.ProductApprovalDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 批量下架
批量下架
        * /backend/spu/offShelf
        */
    export namespace offShelf {
      export type RequestBody = Array<string>;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: Array<string>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 批量上架
批量上架
        * /backend/spu/onShelf
        */
    export namespace onShelf {
      export type RequestBody = Array<string>;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: Array<string>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 审核列表
审核列表
        * /backend/spu/shelfList
        */
    export namespace shelfList {
      export type RequestBody = defs.QueryProductDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.ProductWithApprovalVO>
      >;

      export const init: Response;

      export function request(
        data: defs.QueryProductDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 商品管理后台接口【品牌管理接口】
   */
  export namespace productBrandManagement {
    /**
        * 创建品牌
创建品牌
        * /backend/brand/createBrand
        */
    export namespace createBrand {
      export type RequestBody = defs.ProductBrandDTO;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: defs.ProductBrandDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 删除品牌
删除品牌
        * /backend/brand/deleteBrand/{brandId}
        */
    export namespace deleteBrand {
      export class Params {
        /** 品牌ID */
        brandId: number;
      }

      export type Response = boolean;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 查询品牌信息
查询品牌信息
        * /backend/brand/getBrandById/{brandId}
        */
    export namespace getBrandById {
      export class Params {
        /** 品牌ID */
        brandId: number;
      }

      export type Response = defs.ProductBrandVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 查询品牌列表
查询品牌列表
        * /backend/brand/getBrandList
        */
    export namespace getBrandList {
      export type Response = Array<defs.ProductBrandVO>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
        * 分页查询品牌列表
分页查询品牌列表
        * /backend/brand/getPageList
        */
    export namespace getPageList {
      export type RequestBody = defs.QueryBrandDTO;

      export type Response = defs.ProductBrandVO;

      export const init: Response;

      export function request(
        data: defs.QueryBrandDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 更新品牌状态
更新品牌状态
        * /backend/brand/operateBrandStatus/{brandId}/{status}
        */
    export namespace operateBrandStatus {
      export class Params {
        /** 品牌ID */
        brandId: number;
        /** 状态 0:禁用 1:启用 */
        status: number;
      }

      export type Response = boolean;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 更新品牌
更新品牌
        * /backend/brand/updateBrand
        */
    export namespace updateBrand {
      export type RequestBody = defs.ProductBrandDTO;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: defs.ProductBrandDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 商品前台接口【商品信息】
   */
  export namespace productFront {
    /**
        * 获取营销分类树
获取营销分类树
        * /front/product/getMktTree
        */
    export namespace getMktTree {
      export class Params {
        /** 状态 1-启用 0-禁用 默认查启用 */
        status?: number;
      }

      export type Response = defs.ProductMktVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 根据spuId获取商品总库存K:spuId V:总库存
根据spuId获取商品总库存K:spuId V:总库存
        * /front/product/getTotalStockBySpuId
        */
    export namespace getTotalStockBySpuId {
      export type RequestBody = Array<string>;

      export type Response = object;

      export const init: Response;

      export function request(
        data: Array<string>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 根据spuId查询商品详情信息
根据spuId查询商品详情信息
        * /front/product/queryProductDetails/{spuId}
        */
    export namespace queryProductDetails {
      export class Params {
        /** spuId */
        spuId: string;
      }

      export type Response = defs.ProductDetailsVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 搜索商品
搜索商品
        * /front/product/searchProduct
        */
    export namespace searchProduct {
      export type RequestBody = defs.SearchProductDTO;

      export type Response = defs.SearchProductVO;

      export const init: Response;

      export function request(
        data: defs.SearchProductDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 商品管理后台接口【商品标签】
   */
  export namespace productLabelBackend {
    /**
        * 创建标签
创建标签
        * /backend/productLabel/createLabel
        */
    export namespace createLabel {
      export type RequestBody = defs.CreateOrUpdateLabelDTO;

      export type Response = string;

      export const init: Response;

      export function request(
        data: defs.CreateOrUpdateLabelDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 删除标签
删除标签
        * /backend/productLabel/deleteLabel/{labelCode}
        */
    export namespace deleteLabel {
      export class Params {
        /** labelCode */
        labelCode: string;
      }

      export type Response = boolean;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 查询可用的标签列表
查询可用的标签列表
        * /backend/productLabel/labelList
        */
    export namespace labelList {
      export type Response = defs.ProductLabelVO;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
        * 分页查询标签列表
分页查询标签列表
        * /backend/productLabel/pageQueryLabel
        */
    export namespace pageQueryLabel {
      export type RequestBody = defs.QueryProductLabelDTO;

      export type Response = defs.ProductLabelVO;

      export const init: Response;

      export function request(
        data: defs.QueryProductLabelDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 查询标签详情
查询标签详情
        * /backend/productLabel/queryLabelDetails/{labelCode}
        */
    export namespace queryLabelDetails {
      export class Params {
        /** labelCode */
        labelCode: string;
      }

      export type Response = defs.ProductLabelVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 编辑标签
编辑标签
        * /backend/productLabel/updateLabel
        */
    export namespace updateLabel {
      export type RequestBody = defs.CreateOrUpdateLabelDTO;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: defs.CreateOrUpdateLabelDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 修改标签状态
修改标签状态
        * /backend/productLabel/updateStatus/{labelCode}
        */
    export namespace updateStatus {
      export class Params {
        /** labelCode */
        labelCode: string;
      }

      export type Response = boolean;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 商品管理后台接口【商品管理】
   */
  export namespace productManagementBackend {
    /**
        * 创建商品
创建商品
        * /backend/productManagement/createProduct
        */
    export namespace createProduct {
      export type RequestBody = defs.CreateOrUpdateProductDTO;

      export type Response = string;

      export const init: Response;

      export function request(
        data: defs.CreateOrUpdateProductDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 删除商品(单个删除传一个spuId)
删除商品(单个删除传一个spuId)
        * /backend/productManagement/deleteProduct
        */
    export namespace deleteProduct {
      export type RequestBody = Array<string>;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: Array<string>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 分页查询商品信息
分页查询商品信息
        * /backend/productManagement/pageQueryProduct
        */
    export namespace pageQueryProduct {
      export type RequestBody = defs.QueryProductDTO;

      export type Response = defs.ProductWithSkuVO;

      export const init: Response;

      export function request(
        data: defs.QueryProductDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 分页查询商品操作日志
分页查询商品操作日志
        * /backend/productManagement/pageQueryProductLog
        */
    export namespace pageQueryProductLog {
      export type RequestBody = defs.QueryProductLogDTO;

      export type Response = defs.ProductLogVO;

      export const init: Response;

      export function request(
        data: defs.QueryProductLogDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 分页查询商品Sku列表
分页查询商品Sku列表
        * /backend/productManagement/pageQuerySku
        */
    export namespace pageQuerySkuList {
      export type RequestBody = defs.QueryProductDTO;

      export type Response = defs.ProductWithSkuVO;

      export const init: Response;

      export function request(
        data: defs.QueryProductDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 根据skuId查询商品信息
根据skuId查询商品信息
        * /backend/productManagement/queryProductBySkuIdList
        */
    export namespace queryProductBySkuIdList {
      export type RequestBody = Array<string>;

      export type Response = defs.ProductWithSkuVO;

      export const init: Response;

      export function request(
        data: Array<string>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 查询商品详情信息
查询商品详情信息
        * /backend/productManagement/queryProductDetails/{spuId}
        */
    export namespace queryProductDetails {
      export class Params {
        /** spuId */
        spuId: string;
      }

      export type Response = defs.ProductDetailsVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 根据spuId查询商品信息(带默认sku信息)
根据spuId查询商品信息(带默认sku信息)
        * /backend/productManagement/queryProductWithDefSkuBySpuId
        */
    export namespace queryProductWithDefSkuBySpuId {
      export type RequestBody = Array<string>;

      export type Response = defs.ProductWithSkuVO;

      export const init: Response;

      export function request(
        data: Array<string>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 手动同步上架商品到ES
手动同步上架商品到ES
        * /backend/productManagement/syncOnShelvesProduct
        */
    export namespace syncOnShelvesProduct {
      export type RequestBody = Array<string>;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: Array<string>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 修改商品
修改商品
        * /backend/productManagement/updateProduct
        */
    export namespace updateProduct {
      export type RequestBody = defs.CreateOrUpdateProductDTO;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: defs.CreateOrUpdateProductDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 商品管理后台接口【营销分类】
   */
  export namespace productMktBackend {
    /**
        * 创建营销分类
创建营销分类
        * /backend/productMkt/createMkt
        */
    export namespace createMkt {
      export type RequestBody = defs.CreateOrUpdateMktDTO;

      export type Response = string;

      export const init: Response;

      export function request(
        data: defs.CreateOrUpdateMktDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 根据Id删除营销分类
根据Id删除营销分类
        * /backend/productMkt/deleteMkt/{id}
        */
    export namespace deleteMkt {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = boolean;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 获取营销分类树
获取营销分类树
        * /backend/productMkt/getMktTree/{status}
        */
    export namespace getMktTree {
      export class Params {
        /** status */
        status: number;
      }

      export type Response = defs.ProductMktVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 分页查询营销分类列表
分页查询营销分类列表
        * /backend/productMkt/pageQueryMkt
        */
    export namespace pageQueryMkt {
      export type RequestBody = defs.QueryProductMktDTO;

      export type Response = defs.ProductMktVO;

      export const init: Response;

      export function request(
        data: defs.QueryProductMktDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 查询下级营销分类列表
查询下级营销分类列表
        * /backend/productMkt/queryChildMktList/{parentMktCode}
        */
    export namespace queryChildMktList {
      export class Params {
        /** parentMktCode */
        parentMktCode: string;
        /** 状态 1-启用 0-禁用 默认查启用 */
        status?: number;
      }

      export type Response = defs.ProductMktVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 根据Id查询营销分类详情
根据Id查询营销分类详情
        * /backend/productMkt/queryMktDetails/{id}
        */
    export namespace queryMktDetails {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ProductMktVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 编辑营销分类
编辑营销分类
        * /backend/productMkt/updateMkt
        */
    export namespace updateMkt {
      export type RequestBody = defs.CreateOrUpdateMktDTO;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: defs.CreateOrUpdateMktDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 根据Id修改营销分类状态
根据Id修改营销分类状态
        * /backend/productMkt/updateStatus/{id}
        */
    export namespace updateStatus {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = boolean;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 商品管理后台接口【商品回收站】
   */
  export namespace productRecycleBackend {
    /**
        * 删除回收站商品(单个删除传一个spuId)
删除回收站商品(单个删除传一个spuId)
        * /backend/productRecycle/deleteRecycleProduct
        */
    export namespace deleteRecycleProduct {
      export type RequestBody = Array<string>;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: Array<string>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 分页查询回收站商品信息
分页查询回收站商品信息
        * /backend/productRecycle/pageQueryRecycleProduct
        */
    export namespace pageQueryProduct {
      export type RequestBody = defs.QueryProductDTO;

      export type Response = defs.ProductWithSkuVO;

      export const init: Response;

      export function request(
        data: defs.QueryProductDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 查询回收站商品详情
查询回收站商品详情
        * /backend/productRecycle/queryRecycleProductDetails/{spuId}
        */
    export namespace queryRecycleProductDetails {
      export class Params {
        /** spuId */
        spuId: string;
      }

      export type Response = defs.ProductDetailsVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 还原回收站商品(单个还原传一个spuId)
还原回收站商品(单个还原传一个spuId)
        * /backend/productRecycle/restoreRecycleProduct
        */
    export namespace restoreRecycleProduct {
      export type RequestBody = Array<string>;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: Array<string>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 商品管理后台接口【商品销售报表】
   */
  export namespace productSalesBackend {
    /**
        * 分页查询销售报表
分页查询销售报表
        * /backend/productSales/pageQuerySales
        */
    export namespace pageQuerySales {
      export type RequestBody = defs.QueryProductSalesDTO;

      export type Response = defs.ProductSalesVO;

      export const init: Response;

      export function request(
        data: defs.QueryProductSalesDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 分页查询销售明细
分页查询销售明细
        * /backend/productSales/pageQuerySalesDetails
        */
    export namespace pageQuerySalesDetails {
      export type RequestBody = defs.QueryProductSalesDTO;

      export type Response = defs.ProductSalesVO;

      export const init: Response;

      export function request(
        data: defs.QueryProductSalesDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 销售数量统计
销售数量统计
        * /backend/productSales/salesCount
        */
    export namespace salesCount {
      export type RequestBody = defs.QueryProductSalesDTO;

      export type Response = defs.SalesCountVO;

      export const init: Response;

      export function request(
        data: defs.QueryProductSalesDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 商品管理后台接口【商品库存】
   */
  export namespace productStock {
    /**
        * 批量设置安全库存
批量设置安全库存
        * /backend/productStock/batchSetSafeStock/{safeStock}
        */
    export namespace batchSetSafeStock {
      export class Params {
        /** 安全库存 */
        safeStock?: number;
      }

      export type RequestBody = Array<string>;

      export type Response = boolean;

      export const init: Response;

      export function request(
        paramsAndBody: { params: Params; data: Array<string> },
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 批量修改库存(提交保存)
批量修改库存(提交保存)
        * /backend/productStock/batchUpdateStock/{cacheId}
        */
    export namespace batchUpdateStock {
      export class Params {
        /** 缓存Id */
        cacheId?: string;
      }

      export type Response = boolean;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 选择商品设置库存(最大限制1000)
选择商品设置库存(最大限制1000)
        * /backend/productStock/chooseSkuSetStock
        */
    export namespace chooseSkuSetStock {
      export type RequestBody = ObjectMap<any, number>;

      export type Response = string;

      export const init: Response;

      export function request(
        data: ObjectMap<any, number>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 查询无货预警数量
查询无货预警数量
        * /backend/productStock/getNoStockTotal
        */
    export namespace getNoStockTotal {
      export type RequestBody = defs.QueryProductDTO;

      export type Response = number;

      export const init: Response;

      export function request(
        data: defs.QueryProductDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 获取Sku总数
获取Sku总数
        * /backend/productStock/getSkuTotal
        */
    export namespace getSkuTotal {
      export type RequestBody = defs.QueryProductDTO;

      export type Response = number;

      export const init: Response;

      export function request(
        data: defs.QueryProductDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 导入商品设置库存(最大限制1000)
选择商品设置库存(最大限制1000)
        * /backend/productStock/importSkuSetStock
        */
    export namespace importSkuSetStock {
      export type Response = string;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
        * 分页查询批量管理库存列表
分页查询批量管理库存列表
        * /backend/productStock/pageQueryBatchManageStock
        */
    export namespace pageQueryBatchManageStock {
      export type RequestBody = defs.QueryBatchManageStockDTO;

      export type Response = defs.ProductWithSkuVO;

      export const init: Response;

      export function request(
        data: defs.QueryBatchManageStockDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 分页查询商品Sku列表
分页查询商品Sku列表
        * /backend/productStock/pageQuerySkuList
        */
    export namespace pageQuerySkuList {
      export type RequestBody = defs.QueryProductDTO;

      export type Response = defs.ProductWithSkuVO;

      export const init: Response;

      export function request(
        data: defs.QueryProductDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 分页查询商品出入库记录
分页查询商品出入库记录
        * /backend/productStock/pageQueryStockRecords
        */
    export namespace pageQueryStockRecords {
      export type RequestBody = defs.QueryProductDTO;

      export type Response = defs.StockRecordsWithProductVO;

      export const init: Response;

      export function request(
        data: defs.QueryProductDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 分页查询库存预警列表
分页查询库存预警列表
        * /backend/productStock/pageQueryStockWarn
        */
    export namespace pageQueryStockWarn {
      export type RequestBody = defs.QueryProductDTO;

      export type Response = defs.StockWarnWithProductVO;

      export const init: Response;

      export function request(
        data: defs.QueryProductDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 设置安全库存
设置安全库存
        * /backend/productStock/setSafeStock/{skuId}/{safeStock}
        */
    export namespace setSafeStock {
      export class Params {
        /** skuId */
        skuId?: string;
        /** 安全库存 */
        safeStock?: number;
      }

      export type Response = boolean;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 同步导出商品Sku列表(最多1000条)
同步导出商品Sku列表(最多1000条)
        * /backend/productStock/syncExportSkuList
        */
    export namespace syncExportSkuList {
      export type RequestBody = defs.QueryProductDTO;

      export type Response = defs.StockListVO;

      export const init: Response;

      export function request(
        data: defs.QueryProductDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 测试库存预警短信
测试库存预警短信
        * /backend/productStock/test/sendMsg
        */
    export namespace sendMsg {
      export type Response = boolean;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
        * 修改该缓存下的全部Sku库存
修改该缓存下的全部Sku库存
        * /backend/productStock/updateAllSkuCacheStock/{cacheId}/{stock}
        */
    export namespace updateAllSkuCacheStock {
      export class Params {
        /** 缓存Id */
        cacheId?: string;
        /** 库存值 */
        stock?: number;
      }

      export type Response = boolean;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 批量修改Sku缓存中的库存(单个修改传一个Map键值对)
批量修改Sku缓存中的库存(单个修改传一个Map键值对)
        * /backend/productStock/updateCacheStock/{cacheId}
        */
    export namespace updateCacheStock {
      export class Params {
        /** 缓存Id */
        cacheId?: string;
      }

      export type RequestBody = ObjectMap<any, number>;

      export type Response = boolean;

      export const init: Response;

      export function request(
        paramsAndBody: { params: Params; data: ObjectMap<any, number> },
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 修改库存预警状态
修改库存预警状态
        * /backend/productStock/updateStockWarn/{stockWarn}
        */
    export namespace updateStatus {
      export class Params {
        /** 状态:0:关闭 1:开启 */
        stockWarn?: number;
      }

      export type RequestBody = Array<string>;

      export type Response = boolean;

      export const init: Response;

      export function request(
        paramsAndBody: { params: Params; data: Array<string> },
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 商品管理后台接口【规格管理接口】
   */
  export namespace propertyBackend {
    /**
        * 新增规格组
新增规格组
        * /backend/property/add
        */
    export namespace addProperty {
      export type RequestBody = defs.ProductPropertyDTO;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: defs.ProductPropertyDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 删除规格组
删除规格组
        * /backend/property/delete/{propertyId}
        */
    export namespace deleteProperty {
      export class Params {
        /** 规格组ID */
        propertyId: number;
      }

      export type Response = boolean;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 通过KEY批量查询规格组列表
通过KEY批量查询规格组列表
        * /backend/property/getByKeyBatch
        */
    export namespace getByKeyBatch {
      export type RequestBody = Array<string>;

      export type Response = defs.ProductPropertyVO;

      export const init: Response;

      export function request(
        data: Array<string>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 查询规格组
查询规格组
        * /backend/property/getPropertyDetail/{propertyId}
        */
    export namespace getPropertyDetail {
      export class Params {
        /** 规格组ID */
        propertyId: number;
      }

      export type Response = defs.ProductPropertyVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 修改规格组
修改规格组
        * /backend/property/modify
        */
    export namespace modifyProperty {
      export type RequestBody = defs.ProductPropertyDTO;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: defs.ProductPropertyDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 查询规格组列表
查询规格组列表
        * /backend/property/query/list
        */
    export namespace getProductPropertyList {
      export type Response = defs.ProductPropertyVO;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
        * 分页查询规格组列表
分页查询规格组列表
        * /backend/property/query/page
        */
    export namespace queryPropertyList {
      export type RequestBody = defs.QueryPropertyDTO;

      export type Response = defs.ProductPropertyVO;

      export const init: Response;

      export function request(
        data: defs.QueryPropertyDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 规格前端接口
   */
  export namespace propertyFront {
    /**
        * 通过KEY批量查询规格组列表
通过KEY批量查询规格组列表
        * /front/property/getByKeyBatch
        */
    export namespace getByKeyBatch {
      export type RequestBody = Array<string>;

      export type Response = defs.ProductPropertyVO;

      export const init: Response;

      export function request(
        data: Array<string>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 二维码相关相关接口
   */
  export namespace qrCode {
    /**
     * 生成通用普通二维码
     * /qrCode/createQRCode
     */
    export namespace createQRCode {
      export type RequestBody = defs.CreateQRCodeDTO;

      export type Response = any;

      export const init: Response;

      export function request(
        data: defs.CreateQRCodeDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 生成微信小程序二维码
     * /qrCode/createWechatQRCode
     */
    export namespace createWechatQRCode {
      export type RequestBody = defs.CreateWechatQRCodeDTO;

      export type Response = any;

      export const init: Response;

      export function request(
        data: defs.CreateWechatQRCodeDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取短信跳转url
     * /qrCode/generateUrl
     */
    export namespace generateUrl {
      export type RequestBody = defs.WechatGenerateUrlDTO;

      export type Response = string;

      export const init: Response;

      export function request(
        data: defs.WechatGenerateUrlDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取短信跳转scheme码
     * /qrCode/generatescheme
     */
    export namespace generatescheme {
      export type RequestBody = defs.GetchemeDTO;

      export type Response = string;

      export const init: Response;

      export function request(
        data: defs.GetchemeDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取分享信息
     * /qrCode/getInfo/{qk}
     */
    export namespace checkPoints {
      export class Params {
        /** qk */
        qk: string;
      }

      export type Response = object;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 后端充值原因管理
   */
  export namespace rechargeReasonBackend {
    /**
     * 添加
     * /backend/rechargeReason/add
     */
    export namespace add {
      export type RequestBody = defs.BaseRechargeReasonDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.BaseRechargeReasonDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取所有有效状态充值原因
     * /backend/rechargeReason/all
     */
    export namespace getAll {
      export type Response = defs.ResponseModel<
        Array<defs.BaseRechargeReasonVO>
      >;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 删除
     * /backend/rechargeReason/delete/{id}
     */
    export namespace remove {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过id获取详细信息
     * /backend/rechargeReason/detail/{id}
     */
    export namespace getDetailById {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.BaseRechargeReasonVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取充值原因列表（分页）
     * /backend/rechargeReason/list
     */
    export namespace getPageInfo {
      export type RequestBody = defs.BaseDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.BaseRechargeReasonVO>
      >;

      export const init: Response;

      export function request(
        data: defs.BaseDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过ids获取列表
     * /backend/rechargeReason/listByIds
     */
    export namespace getListByIds {
      export type RequestBody = Array<number>;

      export type Response = defs.ResponseModel<
        Array<defs.BaseRechargeReasonVO>
      >;

      export const init: Response;

      export function request(
        data: Array<number>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 编辑
     * /backend/rechargeReason/update
     */
    export namespace update {
      export type RequestBody = defs.BaseRechargeReasonDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.BaseRechargeReasonDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 后端角色管理
   */
  export namespace role {
    /**
     * 添加
     * /admin/role/add
     */
    export namespace add {
      export type RequestBody = defs.RoleDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.RoleDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 删除
     * /admin/role/delete/{id}
     */
    export namespace remove {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 详情
     * /admin/role/detail
     */
    export namespace detail {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.RoleDTO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 列表
     * /admin/role/list
     */
    export namespace list {
      export type RequestBody = defs.RoleQueryDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.RoleListVO>
      >;

      export const init: Response;

      export function request(
        data: defs.RoleQueryDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 修改
     * /admin/role/update
     */
    export namespace update {
      export type RequestBody = defs.RoleDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.RoleDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 后端积分设置管理
   */
  export namespace scoreBackend {
    /**
     * 通过id获取详细信息
     * /backend/score/detail
     */
    export namespace getDetail {
      export type Response = defs.ResponseModel<defs.SettingScoreVO>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 编辑
     * /backend/score/update
     */
    export namespace update {
      export type RequestBody = defs.SettingScoreDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.SettingScoreDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 内容管理-搜索关键词-后台接口
   */
  export namespace searchKeywordBackend {
    /**
     * 添加搜索关键词
     * /backend/cms/searchKeyword/create
     */
    export namespace createKeyword {
      export type RequestBody = defs.SearchKeywordDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.SearchKeywordDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 删除搜索关键词
     * /backend/cms/searchKeyword/delete
     */
    export namespace deleteKeyword {
      export type RequestBody = defs.IdDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.IdDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取关键词列表
     * /backend/cms/searchKeyword/list
     */
    export namespace list {
      export type RequestBody = defs.QuerySearchKeywordDTO;

      export type Response = defs.ResponseModel<Array<defs.SearchKeywordVO>>;

      export const init: Response;

      export function request(
        data: defs.QuerySearchKeywordDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 编辑搜索关键词
     * /backend/cms/searchKeyword/update
     */
    export namespace updateKeyword {
      export type RequestBody = defs.SearchKeywordDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.SearchKeywordDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 内容管理-搜索关键词-前台接口
   */
  export namespace searchKeywordFront {
    /**
     * 获取关键词列表
     * /front/cms/searchKeyword/list
     */
    export namespace list {
      export type Response = defs.ResponseModel<
        Array<defs.SearchKeywordBasicVO>
      >;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }
  }

  /**
   * 内容管理-服务保障-后台接口
   */
  export namespace serviceAssuranceBackend {
    /**
     * 添加服务保障
     * /backend/cms/serviceAssurance/create
     */
    export namespace create {
      export type RequestBody = defs.ServiceAssuranceDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.ServiceAssuranceDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 删除服务保障
     * /backend/cms/serviceAssurance/delete
     */
    export namespace remove {
      export type RequestBody = defs.IdLongDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.IdLongDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 查询服务保障详情
     * /backend/cms/serviceAssurance/detail/{id}
     */
    export namespace detail {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.ServiceAssuranceVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 根据服务保障编号查询服务保障详情
     * /backend/cms/serviceAssurance/detailByCode/{code}
     */
    export namespace detailByCode {
      export class Params {
        /** code */
        code: string;
      }

      export type Response = defs.ResponseModel<defs.ServiceAssuranceBasicVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取服务保障列表
     * /backend/cms/serviceAssurance/list
     */
    export namespace list {
      export type RequestBody = defs.QueryServiceAssuranceDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.ServiceAssuranceVO>
      >;

      export const init: Response;

      export function request(
        data: defs.QueryServiceAssuranceDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 根据服务保障编号查询服务保障列表
     * /backend/cms/serviceAssurance/queryByCodes
     */
    export namespace queryByCodes {
      export type RequestBody = Array<string>;

      export type Response = defs.ResponseModel<
        Array<defs.ServiceAssuranceBasicVO>
      >;

      export const init: Response;

      export function request(
        data: Array<string>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 编辑服务保障
     * /backend/cms/serviceAssurance/update
     */
    export namespace update {
      export type RequestBody = defs.ServiceAssuranceDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.ServiceAssuranceDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 内容管理-服务保障-前台接口
   */
  export namespace serviceAssuranceFront {
    /**
     * 查询服务保障详情
     * /front/cms/serviceAssurance/detail/{id}
     */
    export namespace detail {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.ServiceAssuranceBasicVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 根据服务保障编号查询服务保障详情
     * /front/cms/serviceAssurance/detailByCode/{code}
     */
    export namespace detailByCode {
      export class Params {
        /** code */
        code: string;
      }

      export type Response = defs.ResponseModel<defs.ServiceAssuranceBasicVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 根据服务保障编号查询服务保障列表
     * /front/cms/serviceAssurance/queryByCodes
     */
    export namespace queryByCodes {
      export type RequestBody = Array<string>;

      export type Response = defs.ResponseModel<
        Array<defs.ServiceAssuranceBasicVO>
      >;

      export const init: Response;

      export function request(
        data: Array<string>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 内容管理-短链接
   */
  export namespace shortLink {
    /**
     * 创建短域名
     * /link/create
     */
    export namespace createShortLink {
      export type RequestBody = String;

      export type Response = defs.ResponseModel<defs.ShortLinkVO>;

      export const init: Response;

      export function request(
        data: String,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * getResult
     * /link/result
     */
    export namespace getResult {
      export type Response = string;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * getResult
     * /link/result
     */
    export namespace headResult {
      export type Response = string;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * getResult
     * /link/result
     */
    export namespace postResult {
      export type Response = string;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * getResult
     * /link/result
     */
    export namespace putResult {
      export type Response = string;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * getResult
     * /link/result
     */
    export namespace deleteResult {
      export type Response = string;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * getResult
     * /link/result
     */
    export namespace optionsResult {
      export type Response = string;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * getResult
     * /link/result
     */
    export namespace patchResult {
      export type Response = string;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 跳转到原始地址
     * /link/{code}
     */
    export namespace redirect {
      export class Params {
        /** code */
        code: string;
      }

      export type Response = string;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 跳转到原始地址
     * /link/{code}
     */
    export namespace headByCode {
      export class Params {
        /** code */
        code: string;
      }

      export type Response = string;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 跳转到原始地址
     * /link/{code}
     */
    export namespace postByCode {
      export class Params {
        /** code */
        code: string;
      }

      export type Response = string;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 跳转到原始地址
     * /link/{code}
     */
    export namespace putByCode {
      export class Params {
        /** code */
        code: string;
      }

      export type Response = string;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 跳转到原始地址
     * /link/{code}
     */
    export namespace deleteByCode {
      export class Params {
        /** code */
        code: string;
      }

      export type Response = string;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 跳转到原始地址
     * /link/{code}
     */
    export namespace optionsByCode {
      export class Params {
        /** code */
        code: string;
      }

      export type Response = string;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 跳转到原始地址
     * /link/{code}
     */
    export namespace patchByCode {
      export class Params {
        /** code */
        code: string;
      }

      export type Response = string;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 用户注册登录相关接口
   */
  export namespace singleSignOn {
    /**
        * 退出登录
并退出登录
        * /sso/logout
        */
    export namespace logout {
      export type Response = boolean;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
        * 微信用户绑定手机号
微信用户绑定手机号
        * /sso/miniBindMobile
        */
    export namespace miniBindMobile {
      export type RequestBody = defs.MiniAppBindMobileDTO;

      export type Response = defs.UserLoginVO;

      export const init: Response;

      export function request(
        data: defs.MiniAppBindMobileDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 小程序注册
小程序注册
        * /sso/miniLogin/{code}
        */
    export namespace miniLogin {
      export class Params {
        /** code */
        code: string;
      }

      export type Response = defs.UserLoginVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 短信登录
短信登录
        * /sso/smsLogin
        */
    export namespace smsLogin {
      export type RequestBody = defs.UserSmsLoginDTO;

      export type Response = string;

      export const init: Response;

      export function request(
        data: defs.UserSmsLoginDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 解锁openid并退出登录
解锁openid并退出登录
        * /sso/unlockout
        */
    export namespace unlockout {
      export type Response = boolean;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }
  }

  /**
   * 短信相关接口
   */
  export namespace sms {
    /**
     * verifySmsCode
     * /sms/getWechatAccessToken
     */
    export namespace verifySmsCode {
      export class Params {
        /** reflash */
        reflash: boolean;
      }

      export type Response = object;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 发送短信验证码(通用)
1:短信验证码登录,2:绑定会员卡,3:绑定手机号
        * /sms/send/sendMsg
        */
    export namespace sendMsg {
      export type RequestBody = defs.SmsSendDTO;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: defs.SmsSendDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 验证验证码(通用)
1:短信验证码登录,2:绑定会员卡号,3:绑定手机号
        * /sms/verify/verifySmsCode
        */
    export namespace postVerifyVerifySmsCode {
      export type RequestBody = defs.SmsVerifyDTO;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: defs.SmsVerifyDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 库存管理后台接口
   */
  export namespace stock {
    /**
        * 批量修改库存
批量修改库存
        * /backend/stock/batchUpdateStock
        */
    export namespace batchUpdateStock {
      export type RequestBody = Array<defs.StockItemDTO>;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: Array<defs.StockItemDTO>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 取消库存测试
取消库存测试
        * /backend/stock/cancelStock
        */
    export namespace cancelStock {
      export type RequestBody = defs.OrderInfoDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.OrderInfoDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 确认库存
确认库存
        * /backend/stock/confirmStock
        */
    export namespace confirmStock {
      export type RequestBody = defs.OrderInfoDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.OrderInfoDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 预占测试
预占测试
        * /backend/stock/occupyStock
        */
    export namespace occupyStock {
      export type RequestBody = defs.OrderInfoDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.OrderInfoDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 查询库存
查询库存
        * /backend/stock/queryStockList
        */
    export namespace queryStockList {
      export type RequestBody = Array<defs.QueryStockDTO>;

      export type Response = defs.ResponseModel<Array<defs.SkuStockVO>>;

      export const init: Response;

      export function request(
        data: Array<defs.QueryStockDTO>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 前端-供应商管理
   */
  export namespace supplier {
    /**
     * 通过id获取详细信息
     * /front/supplier/detail/{id}
     */
    export namespace getDetailById {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.SupplierVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 后端供应商管理
   */
  export namespace supplierBackend {
    /**
     * 添加
     * /backend/supplier/add
     */
    export namespace add {
      export type RequestBody = defs.SupplierDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.SupplierDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 查询所有供应商列表（不分页）
     * /backend/supplier/all
     */
    export namespace queryAllList {
      export type RequestBody = defs.QuerySupplierDTO;

      export type Response = defs.ResponseModel<Array<defs.SupplierVO>>;

      export const init: Response;

      export function request(
        data: defs.QuerySupplierDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 查询权限范围内的所有启用状态供应商列表
     * /backend/supplier/auth/list
     */
    export namespace queryAuthList {
      export type Response = defs.ResponseModel<Array<defs.SupplierVO>>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 改变状态
     * /backend/supplier/changeStatus
     */
    export namespace postChangeStatus {
      export type RequestBody = defs.ChangeStatusDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.ChangeStatusDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 删除
     * /backend/supplier/delete/{id}
     */
    export namespace remove {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过id获取详细信息
     * /backend/supplier/detail/{id}
     */
    export namespace getDetailById {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.SupplierVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过id获取编辑页详细信息
     * /backend/supplier/editDetail/{id}
     */
    export namespace getEditDetailById {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.SupplierDetailVO>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取供应商列表（分页）
     * /backend/supplier/list
     */
    export namespace getPageInfo {
      export type RequestBody = defs.QuerySupplierDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.SupplierVO>
      >;

      export const init: Response;

      export function request(
        data: defs.QuerySupplierDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过ids获取列表
     * /backend/supplier/listByIds
     */
    export namespace getListByIds {
      export type RequestBody = Array<number>;

      export type Response = defs.ResponseModel<Array<defs.SupplierVO>>;

      export const init: Response;

      export function request(
        data: Array<number>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 编辑
     * /backend/supplier/update
     */
    export namespace update {
      export type RequestBody = defs.SupplierDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.SupplierDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 用户后端接口
   */
  export namespace user {
    /**
        * 获取车辆信息
获取车辆信息
        * /webapi/user/carInfo/{id}
        */
    export namespace carInfo {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.UserCarListVO;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 获取车辆信息
获取车辆信息
        * /webapi/user/carListByUser/{userId}
        */
    export namespace carList {
      export class Params {
        /** userId */
        userId: number;
      }

      export type Response = defs.UserCar;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 积分变动记录
     * /webapi/user/changePhone
     */
    export namespace ListPage {
      export type RequestBody = defs.ModifyUserPhoneDTO;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: defs.ModifyUserPhoneDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 根据用户id或手机号查询用户信息
根据用户id或手机号查询用户信息
        * /webapi/user/getUser
        */
    export namespace getUser {
      export class Params {
        /** userId */
        userId?: number;
        /** phone */
        phone?: string;
      }

      export type Response = defs.UserInfo;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 获取用户信息
获取用户信息
        * /webapi/user/infoById/{id}
        */
    export namespace infoById {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.UserInfo;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 更新/新增用户信息
更新/新增用户信息
        * /webapi/user/modifyUserInfo
        */
    export namespace modifyUserInfo {
      export type RequestBody = defs.ModifyUserDTO;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: defs.ModifyUserDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 积分变动记录
     * /webapi/user/pointListPage
     */
    export namespace postPointListPage {
      export type RequestBody = defs.QueryUserPointRecordDTO;

      export type Response = defs.UserPointRecordListVO;

      export const init: Response;

      export function request(
        data: defs.QueryUserPointRecordDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 删除车辆信息
删除车辆信息
        * /webapi/user/removeCar/{id}
        */
    export namespace removeCar {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = boolean;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 更新车辆信息
更新车辆信息
        * /webapi/user/updateCarInfo
        */
    export namespace updateCarInfo {
      export type RequestBody = defs.ModifyCarDTO;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: defs.ModifyCarDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 车辆列表
车辆列表
        * /webapi/user/userCarList
        */
    export namespace userCarList {
      export type RequestBody = defs.QueryCarListDTO;

      export type Response = defs.CarListVO;

      export const init: Response;

      export function request(
        data: defs.QueryCarListDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 用户列表
用户列表
        * /webapi/user/userList
        */
    export namespace userList {
      export type RequestBody = defs.QueryUserListDTO;

      export type Response = defs.UserInfo;

      export const init: Response;

      export function request(
        data: defs.QueryUserListDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 用户前台接口
   */
  export namespace userFront {
    /**
        * 获取车辆信息
获取车辆信息
        * /front/user/carInfo/{id}
        */
    export namespace carInfo {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.UserCar;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 获取车辆信息
获取车辆信息
        * /front/user/carList
        */
    export namespace carList {
      export type Response = defs.UserCarListVO;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
        * 获取用户信息
获取用户信息
        * /front/user/info
        */
    export namespace info {
      export type Response = defs.UserInfoVO;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
        * 获取提示信息
获取提示信息
        * /front/user/notice
        */
    export namespace notice {
      export type Response = defs.PointNoticeVO;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 积点变动记录
     * /front/user/pointListPage
     */
    export namespace ListPage {
      export type RequestBody = defs.PointRecordListDTO;

      export type Response = defs.UserPointVO;

      export const init: Response;

      export function request(
        data: defs.PointRecordListDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
        * 更新车辆信息
更新车辆信息
        * /front/user/updateCarInfo
        */
    export namespace updateCarInfo {
      export type RequestBody = defs.ModifyCarDTO;

      export type Response = boolean;

      export const init: Response;

      export function request(
        data: defs.ModifyCarDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 定时任务
   */
  export namespace userJob {
    /**
     * 积点充值
     * /userJob/doRecharge
     */
    export namespace doRechange {
      export class Params {
        /** batchNo */
        batchNo?: number;
      }

      export type Response = boolean;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 后端版本管理
   */
  export namespace versionBackend {
    /**
     * 添加
     * /backend/version/add
     */
    export namespace add {
      export type RequestBody = defs.SettingVersionDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.SettingVersionDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取所有有效状态版本
     * /backend/version/all
     */
    export namespace getAll {
      export type Response = defs.ResponseModel<
        Array<defs.SettingVersionObject>
      >;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }

    /**
     * 删除
     * /backend/version/delete/{id}
     */
    export namespace remove {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过id获取详细信息
     * /backend/version/detail/{id}
     */
    export namespace getDetailById {
      export class Params {
        /** id */
        id: number;
      }

      export type Response = defs.ResponseModel<defs.SettingVersionObject>;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取版本列表（分页）
     * /backend/version/list
     */
    export namespace getPageInfo {
      export type RequestBody = defs.BaseDTO;

      export type Response = defs.ResponseModel<
        defs.Pagination<defs.SettingVersionObject>
      >;

      export const init: Response;

      export function request(
        data: defs.BaseDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 通过ids获取列表
     * /backend/version/listByIds
     */
    export namespace getListByIds {
      export type RequestBody = Array<number>;

      export type Response = defs.ResponseModel<
        Array<defs.SettingVersionObject>
      >;

      export const init: Response;

      export function request(
        data: Array<number>,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 编辑
     * /backend/version/update
     */
    export namespace update {
      export type RequestBody = defs.SettingVersionDTO;

      export type Response = defs.ResponseModel<boolean>;

      export const init: Response;

      export function request(
        data: defs.SettingVersionDTO,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }

  /**
   * 前端-版本管理
   */
  export namespace versionFront {
    /**
     * 获取最新版本详细信息
     * /front/version/latest/detail
     */
    export namespace getLatestDetail {
      export type Response = defs.ResponseModel<defs.SettingVersionObject>;

      export const init: Response;

      export function request(options?: AxiosRequestConfig): Promise<Response>;
    }
  }

  /**
   * 微信二维码
   */
  export namespace wechatCode {
    /**
     * 生成微信小程序二维码
     * /wechatCode/createQRCode
     */
    export namespace createQRCode {
      export class Params {
        /** 小程序页面链接 */
        path?: string;
        /** 参数(Map(string,string))JSON后传输(不需要可为空) */
        params?: string;
        /** 二维码的宽度，单位 px。最小 280px，最大 1280px(默认430) */
        width?: ref;
      }

      export type Response = any;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }

    /**
     * 获取分享信息
     * /wechatCode/getInfo/{qk}
     */
    export namespace checkPoints {
      export class Params {
        /** qk */
        qk: string;
      }

      export type Response = object;

      export const init: Response;

      export function request(
        params: Params,
        options?: AxiosRequestConfig,
      ): Promise<Response>;
    }
  }
}
