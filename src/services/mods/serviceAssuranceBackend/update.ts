/**
 * @desc 编辑服务保障
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.ServiceAssuranceDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.ServiceAssuranceDTO };

export function request(
  data: defs.ServiceAssuranceDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/cms/serviceAssurance/update`, {
    data,
    method: 'POST',
    ...options,
  });
}
