import master from './master';
import directSale from './direct-sale';
import franchisee from './franchisee';
// import order from './order';

const roles = [master, directSale, franchisee];

export const rolesMap = roles.reduce((pre: any, item) => {
  pre[item.code] = { ...item };
  return pre;
}, {});

export default roles;
