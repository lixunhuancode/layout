/**
 * @desc 获取分享信息
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** qk */
  qk: string;
}

export const init = undefined;

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`qrCode/getInfo/${params.qk}`, {
    params,
    method: 'GET',
    ...options,
  });
}
