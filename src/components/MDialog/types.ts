import type { Component, AsyncComponentLoader } from 'vue';
// AsyncComponentLoader<Component>
export interface OpenOptions {
  component: AsyncComponentLoader<Component>;
  [key: string]: any;
  title?: string;
  class?: string;
  data?: () => any;
}

export type Open = (options: OpenOptions) => Promise<any>;

export interface DialogHandle {
  open: Open;
  close: (a?: any) => any;
  app?: any;
  installed?: boolean;
}

export interface DialogItemProps {
  componentId: string;
  dialogPromise: {
    resolve: (a: any) => any;
    reject: (a: any) => any;
  };
  close: (id: string) => void;
  data?: () => any;
  title?: string;
  class?: string;
}

export interface ComponentMap {
  [key: string]: Component;
}

export interface IDialogItem {
  id: string;
  itemProps: DialogItemProps;
}

export type DialogStore = { open: Open };

export type UseDialog = () => DialogStore;
