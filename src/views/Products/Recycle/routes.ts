export default [
  {
    path: 'recycle/list',
    name: 'products.recycle.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '商品回收站',
    },
  },
  {
    path: 'recycle/detail/:id',
    name: 'products.recycle.detail',
    component: () => import('./Detail/index.vue'),
    meta: {
      hideInMenu: true,
      locale: '商品回收站-详情',
    },
  },
];
