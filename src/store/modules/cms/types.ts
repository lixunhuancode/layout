export interface ListItem {
  /**
   * 标题 */
  title?: string;
  /**
   * 跳转链接 */
  hrefUrl?: string;
  /**
   * 跳转链接类型 */
  hrefType?: string;
  /**
   * 图片地址 */
  imageUrl?: string;
  /**
   * 显示时间 */
  showTime?: Array<string>;
  /**
   * 商品跳转地址 */
  spuViewUrl?: string;

  /**
   * 自定义商品名 */
  customSpuName?: string;
  /**
   * 自定义商品描述 */
  customSpuDesc?: string;
  /**
   * 积分 */
  score?: number;
  /**
   * 商品skuId */
  skuId?: string;
  /**
   * 弹窗code */
  popCode?: string;
}
export interface ItemDataType {
  /**
   * 标题 */
  title?: string;
  /**
   * 副标题 */
  titleSub?: string;
  /**
   * 跳转链接 */
  hrefUrl?: string;
  /**
   * 跳转链接类型 */
  hrefType?: string;
  /**
   * 图片地址 */
  imageUrl?: string;
  /**
   * 更多地址 */
  moreUrl?: string;
  /**
   * 图片一行列数 */
  column?: number;
  /**
   * 推荐规则 */
  recommend?: string;
  orderBy?: string;
  /**
   * 列表 */
  list?: Array<ListItem>;
}

export type CmsPageItem = {
  /**
   * 结束时间 */
  endTime?: string;
  /**
   * 模块配置内容标识 */
  itemCode?: string;
  /**
   * 模块配置内容数据 */
  itemData?: string | ItemDataType;
  /**
   * 模块配置内容ID */
  itemId?: number;
  /**
   * 模块配置内容顺序 */
  itemSort?: number;
  /**
   * 模块标识 */
  moduleCode?: string;
  /**
   * 模块id */
  moduleId?: number;
  /**
   * 模块标签 商品product 图文image 导航nav 活动activity */
  moduleTag?: string;
  /**
   * 页面ID */
  pageId?: number;
  /**
   * 页面版本号 */
  pageVersionId?: number;
  /**
   * 开始时间 */
  startTime?: string;
};

export type PageWithItemsVO = {
  /**
   * 审核状态 1审核通过 0审核不通过 */
  auditStatus?: number;
  /**
   * 背景色 */
  backgroundColor?: string;
  /**
   * 背景图片 */
  backgroundImage?: string;
  /**
   * 创建时间 */
  createTime?: string;
  /**
   * ID */
  id?: number;
  /**
   * 是否删除 0否,1是 */
  isDel?: number;
  /**
   * 发布上线时间 */
  onlineTime?: string;
  /**
   * 操作人ID */
  operId?: number;
  /**
   * 操作人 */
  operName?: string;
  /**
   * 更新时间 */
  operTime?: string;
  /**
   * 描述 */
  pageDesc?: string;
  pageItems?: CmsPageItem[];
  /**
   * 页面版本 */
  pageVersionId?: number;
  /**
   * 页面状态： 1待提交审核  2待审核 3审核通过 4审核不通过 */
  status?: number;
  /**
   * 页面模板标识 home首页 topic专题 service服务 */
  templateCode?: string;
  /**
   * 页面模板ID */
  templateId?: number;
  /**
   * 页面模板类型 page配置页面 text富文本页 */
  templateType?: string;
  /**
   * 标题 */
  title?: string;
  /**
   * 页面顶部图标 */
  topIcon?: string;
  /**
   * 页面封面图片 */
  cover?: string;
};
