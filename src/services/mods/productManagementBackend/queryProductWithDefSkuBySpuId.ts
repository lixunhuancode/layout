/**
   * @desc 根据spuId查询商品信息(带默认sku信息)
根据spuId查询商品信息(带默认sku信息)
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = Array<string>;

export const init = new defs.ProductWithSkuVO();

export type ParamsAndBody = { data: Array<string> };

export function request(data: Array<string>, options: AxiosRequestConfig) {
  return axiosHandle(
    `backend/productManagement/queryProductWithDefSkuBySpuId`,
    {
      data,
      method: 'POST',
      ...options,
    },
  );
}
