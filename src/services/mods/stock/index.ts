/**
 * @description 库存管理后台接口
 */
import * as batchUpdateStock from './batchUpdateStock';
import * as cancelStock from './cancelStock';
import * as confirmStock from './confirmStock';
import * as occupyStock from './occupyStock';
import * as queryStockList from './queryStockList';

export {
  batchUpdateStock,
  cancelStock,
  confirmStock,
  occupyStock,
  queryStockList,
};
