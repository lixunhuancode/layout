/**
   * @desc 编辑营销分类
编辑营销分类
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.CreateOrUpdateMktDTO;

export const init = false;

export type ParamsAndBody = { data: defs.CreateOrUpdateMktDTO };

export function request(
  data: defs.CreateOrUpdateMktDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/productMkt/updateMkt`, {
    data,
    method: 'POST',
    ...options,
  });
}
