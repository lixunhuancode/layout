/**
 * @description 商品管理后台接口【品类管理接口】
 */
import * as createCategory from './createCategory';
import * as deleteCategory from './deleteCategory';
import * as getCategory from './getCategory';
import * as getCategoryList from './getCategoryList';
import * as getPageList from './getPageList';
import * as updateCategory from './updateCategory';

export {
  createCategory,
  deleteCategory,
  getCategory,
  getCategoryList,
  getPageList,
  updateCategory,
};
