/**
 * @description 前端优惠券
 */
import * as getDetailByVerifyCode from './getDetailByVerifyCode';
import * as getVerifyCode from './getVerifyCode';
import * as myCouponList from './myCouponList';
import * as verifiedList from './verifiedList';
import * as verify from './verify';

export {
  getDetailByVerifyCode,
  getVerifyCode,
  myCouponList,
  verifiedList,
  verify,
};
