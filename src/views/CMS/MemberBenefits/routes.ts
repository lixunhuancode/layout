export default [
  {
    path: 'member-benefits/list',
    name: 'cms.member-benefits.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '会员权益',
    },
  },
  {
    path: 'member-benefits/form',
    name: 'cms.member-benefits.add',
    component: () => import('./Form/index.vue'),
    meta: {
      hideInMenu: true,
      locale: '新增会员权益',
    },
  },
  {
    path: 'member-benefits/form/:id',
    name: 'cms.member-benefits.edit',
    component: () => import('./Form/index.vue'),
    meta: {
      hideInMenu: true,
      locale: '编辑会员权益',
    },
  },
];
