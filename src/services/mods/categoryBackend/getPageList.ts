/**
   * @desc 分页查询品类列表
分页查询品类列表
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryCategoryDTO;

export const init = new defs.CategoryVO();

export type ParamsAndBody = { data: defs.QueryCategoryDTO };

export function request(
  data: defs.QueryCategoryDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/category/getPageList`, {
    data,
    method: 'POST',
    ...options,
  });
}
