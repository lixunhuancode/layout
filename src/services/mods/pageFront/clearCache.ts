/**
 * @desc 清楚页面缓存
 */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = new defs.ResponseModel();

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`front/cms/page/clearCache`, {
    method: 'GET',
    ...options,
  });
}
