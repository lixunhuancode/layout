/**
   * @desc 删除规格组
删除规格组
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** 规格组ID */
  propertyId: number;
}

export const init = false;

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`backend/property/delete/${params.propertyId}`, {
    params,
    method: 'DELETE',
    ...options,
  });
}
