export default [
  {
    path: 'detail/list',
    name: 'coupon.detail.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '卡券明细',
      requiresAuth: false,
    },
  },
];
