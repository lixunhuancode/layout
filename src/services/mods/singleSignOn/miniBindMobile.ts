/**
   * @desc 微信用户绑定手机号
微信用户绑定手机号
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.MiniAppBindMobileDTO;

export const init = new defs.UserLoginVO();

export type ParamsAndBody = { data: defs.MiniAppBindMobileDTO };

export function request(
  data: defs.MiniAppBindMobileDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`sso/miniBindMobile`, {
    data,
    method: 'POST',
    ...options,
  });
}
