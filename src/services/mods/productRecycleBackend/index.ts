/**
 * @description 商品管理后台接口【商品回收站】
 */
import * as deleteRecycleProduct from './deleteRecycleProduct';
import * as pageQueryProduct from './pageQueryProduct';
import * as queryRecycleProductDetails from './queryRecycleProductDetails';
import * as restoreRecycleProduct from './restoreRecycleProduct';

export {
  deleteRecycleProduct,
  pageQueryProduct,
  queryRecycleProductDetails,
  restoreRecycleProduct,
};
