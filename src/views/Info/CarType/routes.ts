export default [
  {
    path: 'car-type/list',
    name: 'info.car-type.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '车型管理',
      requiresAuth: false,
      isHidden: true,
      hideInMenu: true,
    },
  },
];
