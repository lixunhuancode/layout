/**
   * @desc 取消库存测试
取消库存测试
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.OrderInfoDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.OrderInfoDTO };

export function request(data: defs.OrderInfoDTO, options: AxiosRequestConfig) {
  return axiosHandle(`backend/stock/cancelStock`, {
    data,
    method: 'POST',
    ...options,
  });
}
