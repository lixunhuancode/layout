export default {
  path: 'payment',
  name: 'finance.payment',
  component: () => import('./list/index.vue'),
  meta: {
    locale: '支付方式',
  }
};
