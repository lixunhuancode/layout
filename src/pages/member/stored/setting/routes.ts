export default [
  {
    path: 'list',
    name: 'setting.list',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '储值配置',
    },
  },
  {
    path: 'edit',
    name: 'setting.edit',
    component: () => import('./form/index.vue'),
    meta: {
      hideInMenu: true,
      locale: '编辑储值配置',
    },
  },
];
