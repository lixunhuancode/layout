/**
    * @desc 获取用户信息
获取用户信息
    */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = new defs.UserInfoVO();

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`front/user/info`, {
    method: 'GET',
    ...options,
  });
}
