/**
 * @desc 列表
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.RoleQueryDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.RoleQueryDTO };

export function request(data: defs.RoleQueryDTO, options: AxiosRequestConfig) {
  return axiosHandle(`admin/role/list`, {
    data,
    method: 'POST',
    ...options,
  });
}
