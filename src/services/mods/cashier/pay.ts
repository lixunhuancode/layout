/**
 * @desc 统一支付接口
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.CashierPayDto;

export const init = undefined;

export type ParamsAndBody = { data: defs.CashierPayDto };

export function request(data: defs.CashierPayDto, options: AxiosRequestConfig) {
  return axiosHandle(`front/cashier/pay`, {
    data,
    method: 'POST',
    ...options,
  });
}
