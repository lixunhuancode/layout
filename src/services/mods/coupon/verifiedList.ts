/**
 * @desc 已核销列表
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** pageIndex */
  pageIndex: number;
  /** pageSize */
  pageSize: number;
}

export const init = new defs.ResponseModel();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`coupon/verifiedList`, {
    params,
    method: 'GET',
    ...options,
  });
}
