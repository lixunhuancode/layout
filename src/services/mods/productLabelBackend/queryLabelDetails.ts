/**
   * @desc 查询标签详情
查询标签详情
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** labelCode */
  labelCode: string;
}

export const init = new defs.ProductLabelVO();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(
    `backend/productLabel/queryLabelDetails/${params.labelCode}`,
    {
      params,
      method: 'GET',
      ...options,
    },
  );
}
