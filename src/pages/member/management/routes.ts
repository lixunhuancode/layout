export default {
  path: 'management',
  name: 'member.management',
  component: () => import('@/components/routeView.vue'),
  meta: {
    locale: '会员管理',
    sort: 1,
  },
  children: [
    {
      path: 'list',
      name: 'member.management.list',
      component: () => import('./list.vue'),
      meta: {
        sort: 1,
        locale: '会员列表',
      },
    },
    {
      path: 'form',
      name: 'member.management.form',
      component: () => import('./form.vue'),
      meta: {
        locale: '新增会员',
        hideInMenu: true,
      },
    },
    {
      path: 'detail/:id',
      name: 'member.management.detail',
      component: () => import('./detail.vue'),
      meta: {
        locale: '会员详情',
        hideInMenu: true,
      },
    },
    {
      path: 'tag/list',
      name: 'member.tagManage.list',
      component: () => import('./tag/list.vue'),
      meta: {
        sort: 2,
        locale: '标签管理',
      },
    },
    {
      path: 'tag/form/:id',
      name: 'member.tagManage.form',
      component: () => import('./tag/form.vue'),
      meta: {
        locale: '新增标签',
        hideInMenu: true,
      },
    },
  ],
};
