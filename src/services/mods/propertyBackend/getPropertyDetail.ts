/**
   * @desc 查询规格组
查询规格组
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** 规格组ID */
  propertyId: number;
}

export const init = new defs.ProductPropertyVO();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(
    `backend/property/getPropertyDetail/${params.propertyId}`,
    {
      params,
      method: 'GET',
      ...options,
    },
  );
}
