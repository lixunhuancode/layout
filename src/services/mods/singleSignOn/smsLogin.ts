/**
   * @desc 短信登录
短信登录
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.UserSmsLoginDTO;

export const init = '';

export type ParamsAndBody = { data: defs.UserSmsLoginDTO };

export function request(
  data: defs.UserSmsLoginDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`sso/smsLogin`, {
    data,
    method: 'POST',
    ...options,
  });
}
