/**
 * @description 优惠券模板管理
 */
import * as add from './add';
import * as detail from './detail';
import * as list from './list';
import * as update from './update';
import * as updateStatus from './updateStatus';

export { add, detail, list, update, updateStatus };
