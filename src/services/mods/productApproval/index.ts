/**
 * @description 商品管理后台接口【上下架管理接口】
 */
import * as approval from './approval';
import * as offShelf from './offShelf';
import * as onShelf from './onShelf';
import * as shelfList from './shelfList';

export { approval, offShelf, onShelf, shelfList };
