import { DEFAULT_LAYOUT } from '@/router/routes/base';
import Management from './Management/routes';
import Category from './Category/routes';
import MarketingCategory from './MarketingCategory/routes';
import Recycle from './Recycle/routes';
import Audit from './Audit/routes';
import Property from './Property/routes';

export default {
  path: '/products',
  name: 'products',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '商品',
    requiresAuth: false,
    icon: 'icon-c-products',
    order: 4,
  },
  children: [
    ...Management,
    ...Recycle,
    ...Audit,
    ...Category,
    ...MarketingCategory,
    ...Property,
  ],
};
