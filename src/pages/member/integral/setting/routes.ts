export default [
  {
    path: 'setting',
    name: 'member.integral.setting',
    component: () => import('./index.vue'),
    meta: {
      locale: '积分设置',
    },
  },
  {
    path: 'general',
    name: 'member.integral.general',
    component: () => import('./general/index.vue'),
    meta: {
      locale: '积分通用规则设置',
      hideInMenu: true,
    },
  },
  {
    path: 'consume',
    name: 'member.integral.consume',
    component: () => import('./consume/index.vue'),
    meta: {
      locale: '积分消耗规则设置',
      hideInMenu: true,
    },
  },
];
