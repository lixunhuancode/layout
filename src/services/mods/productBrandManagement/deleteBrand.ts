/**
   * @desc 删除品牌
删除品牌
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** 品牌ID */
  brandId: number;
}

export const init = false;

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`backend/brand/deleteBrand/${params.brandId}`, {
    params,
    method: 'DELETE',
    ...options,
  });
}
