/**
 * @desc 积点变动记录
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.PointRecordListDTO;

export const init = new defs.UserPointVO();

export type ParamsAndBody = { data: defs.PointRecordListDTO };

export function request(
  data: defs.PointRecordListDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`front/user/pointListPage`, {
    data,
    method: 'POST',
    ...options,
  });
}
