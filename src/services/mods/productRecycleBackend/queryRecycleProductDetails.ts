/**
   * @desc 查询回收站商品详情
查询回收站商品详情
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** spuId */
  spuId: string;
}

export const init = new defs.ProductDetailsVO();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(
    `backend/productRecycle/queryRecycleProductDetails/${params.spuId}`,
    {
      params,
      method: 'GET',
      ...options,
    },
  );
}
