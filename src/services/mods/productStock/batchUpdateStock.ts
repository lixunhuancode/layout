/**
   * @desc 批量修改库存(提交保存)
批量修改库存(提交保存)
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** 缓存Id */
  cacheId?: string;
}

export const init = false;

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(
    `backend/productStock/batchUpdateStock/${params?.cacheId}`,
    {
      params,
      method: 'POST',
      ...options,
    },
  );
}
