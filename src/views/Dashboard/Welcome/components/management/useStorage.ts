import { ref } from 'vue';

const MENU_LOCAL_KEY = 'operation.shortcut';
const MENU_LOCAL_KEY_ROUTER = 'operation.shortcut.router';

export type Tree = {
  iconName: string | undefined;
  title: string;
  key: string;
  path: string;
  children: Tree[] | undefined;
};

const localMenuRoute = window.localStorage.getItem(MENU_LOCAL_KEY_ROUTER);
const menuDataRoute = localMenuRoute ? JSON.parse(localMenuRoute) : [];
const menuRouteList = ref(menuDataRoute as unknown as Tree[]);

export default function useStorage() {
  // 获取快捷菜单数据
  const localMenu = window.localStorage.getItem(MENU_LOCAL_KEY);
  const menuData = localMenu ? JSON.parse(localMenu) : [];
  const menuList = ref(menuData as unknown as string[]);

  const saveMenuList = () => {
    window.localStorage.setItem(MENU_LOCAL_KEY, JSON.stringify(menuList.value));
  };

  const saveRouteMenu = (menuRoute: Tree[]) => {
    const data = menuRoute.filter((item) => item.key.includes('.'));
    window.localStorage.setItem(MENU_LOCAL_KEY_ROUTER, JSON.stringify(data));
    menuRouteList.value = data;
  };

  return {
    menuList,
    menuRouteList,
    saveMenuList,
    saveRouteMenu,
  };
}
