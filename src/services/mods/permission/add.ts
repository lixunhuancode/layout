/**
 * @desc 添加
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.Permission;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.Permission };

export function request(data: defs.Permission, options: AxiosRequestConfig) {
  return axiosHandle(`admin/permission/add`, {
    data,
    method: 'POST',
    ...options,
  });
}
