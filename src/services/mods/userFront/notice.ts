/**
    * @desc 获取提示信息
获取提示信息
    */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = new defs.PointNoticeVO();

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`front/user/notice`, {
    method: 'GET',
    ...options,
  });
}
