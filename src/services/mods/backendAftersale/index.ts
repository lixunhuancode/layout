/**
 * @description 售后单后台接口
 */
import * as applyAftersale from './applyAftersale';
import * as batchAuditAftersale from './batchAuditAftersale';
import * as getAftersaleByAftersaleNo from './getAftersaleByAftersaleNo';
import * as getAftersaleById from './getAftersaleById';
import * as getAftersalePagedInfo from './getAftersalePagedInfo';

export {
  applyAftersale,
  batchAuditAftersale,
  getAftersaleByAftersaleNo,
  getAftersaleById,
  getAftersalePagedInfo,
};
