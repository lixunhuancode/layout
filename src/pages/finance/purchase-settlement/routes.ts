export default [
  {
    path: 'purchase/list',
    name: 'finance.purchase-settlement.list',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '采购结算',
    },
  },
  {
    path: 'purchase/add',
    name: 'finance.purchase-settlement.add',
    component: () => import('./addOrEdit/index.vue'),
    meta: {
      locale: '创建结算单',
      hideInMenu: true,
    },
  },
  {
    path: 'purchase/detail',
    name: 'finance.purchase-settlement.detail',
    component: () => import('./detail/index.vue'),
    meta: {
      locale: '查看',
      hideInMenu: true,
    },
  },
];
