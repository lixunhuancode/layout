export default [
  {
    path: 'buypoints/list',
    name: 'statistics.buypoints.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '积分充值报表',
    },
  },
];
