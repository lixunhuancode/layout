/**
 * @description 商品前台接口【商品信息】
 */
import * as getMktTree from './getMktTree';
import * as getTotalStockBySpuId from './getTotalStockBySpuId';
import * as queryProductDetails from './queryProductDetails';
import * as searchProduct from './searchProduct';

export { getMktTree, getTotalStockBySpuId, queryProductDetails, searchProduct };
