export default {
  path: 'distribution',
  name: 'purchase.distribution',
  component: () => import('@/components/routeView.vue'),
  meta: {
    locale: '配货管理',
    icon: 'icon-c-info',
    sort: 10,
  },
  children: [
    {
      path: 'toc-sscr/list',
      name: 'purchase.toc-sscr.list',
      component: () => import('./toc-sscr/list/index.vue'),
      meta: {
        sort: 1,
        locale: '加盟配销',
      },
    },
    {
      path: 'toc-sscr/form',
      name: 'purchase.toc-sscr.form',
      component: () => import('./toc-sscr/form/index.vue'),
      meta: {
        locale: '新建配销单',
        hideInMenu: true,
      },
    },
    {
      path: 'toc-sscr/form/:id',
      name: 'purchase.toc-sscr.update',
      component: () => import('./toc-sscr/form/index.vue'),
      meta: {
        locale: '编辑配销单',
        hideInMenu: true,
      },
    },
    {
      path: 'toc-sscr/detail/:id',
      name: 'purchase.toc-sscr.detail',
      component: () => import('./toc-sscr/form/index.vue'),
      meta: {
        locale: '配销单详情',
        hideInMenu: true,
      },
    },
    {
      path: 'toc-sscr-return/list',
      name: 'purchase.toc-sscr-return.list',
      component: () => import('./toc-sscr-return/list/index.vue'),
      meta: {
        sort: 2,
        locale: '配销退货',
      },
    },
    {
      path: 'toc-sscr-return/form',
      name: 'purchase.toc-sscr-return.form',
      component: () => import('./toc-sscr-return/form/index.vue'),
      meta: {
        locale: '新建配销退货申请单',
        hideInMenu: true,
      },
    },
    {
      path: 'toc-sscr-return/form/:id',
      name: 'purchase.toc-sscr-return.update',
      component: () => import('./toc-sscr-return/form/index.vue'),
      meta: {
        locale: '编辑配销退货申请单',
        hideInMenu: true,
      },
    },
    {
      path: 'toc-sscr-return/detail/:id',
      name: 'purchase.toc-sscr-return.detail',
      component: () => import('./toc-sscr-return/form/index.vue'),
      meta: {
        locale: '配销退货申请详情',
        hideInMenu: true,
      },
    },
    {
      path: 'batch/list',
      name: 'purchase.distribution.batch.list',
      component: () => import('./batch/list/index.vue'),
      meta: {
        sort: 3,
        locale: '批量分货',
      },
    },
    {
      path: 'batch/form',
      name: 'purchase.distribution.batch.form',
      component: () => import('./batch/form/index.vue'),
      meta: {
        locale: '新建批量分货单',
        hideInMenu: true,
      },
    },
    {
      path: 'batch/form/:id',
      name: 'purchase.distribution.batch.update',
      component: () => import('./batch/form/index.vue'),
      meta: {
        locale: '编辑批量分货单',
        hideInMenu: true,
      },
    },
    {
      path: 'batch/detail/:id',
      name: 'purchase.distribution.batch.detail',
      component: () => import('./batch/form/index.vue'),
      meta: {
        locale: '批量分货详情',
        hideInMenu: true,
      },
    },
    {
      path: 'transfer/list',
      name: 'purchase.transfer.list',
      component: () => import('./transfer/list/index.vue'),
      meta: {
        sort: 4,
        locale: '调拨管理',
      },
    },
    {
      path: 'transfer/form',
      name: 'purchase.transfer.form',
      component: () => import('./transfer/form/index.vue'),
      meta: {
        locale: '新建调拨单',
        hideInMenu: true,
      },
    },
    {
      path: 'transfer/form/:id',
      name: 'purchase.transfer.update',
      component: () => import('./transfer/form/index.vue'),
      meta: {
        locale: '编辑调拨单',
        hideInMenu: true,
      },
    },
    {
      path: 'transfer/detail/:id',
      name: 'purchase.transfer.detail',
      component: () => import('./transfer/form/index.vue'),
      meta: {
        locale: '调拨单详情',
        hideInMenu: true,
      },
    },
  ],
};
