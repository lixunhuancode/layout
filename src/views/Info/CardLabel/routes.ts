export default [
  {
    path: 'card-label/list',
    name: 'info.card-label.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '卡券标签',
      requiresAuth: false,
    },
  },
];
