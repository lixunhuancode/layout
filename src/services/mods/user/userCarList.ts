/**
   * @desc 车辆列表
车辆列表
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryCarListDTO;

export const init = new defs.CarListVO();

export type ParamsAndBody = { data: defs.QueryCarListDTO };

export function request(
  data: defs.QueryCarListDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`webapi/user/userCarList`, {
    data,
    method: 'POST',
    ...options,
  });
}
