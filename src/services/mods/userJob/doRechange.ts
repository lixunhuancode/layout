/**
 * @desc 积点充值
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** batchNo */
  batchNo?: number;
}

export const init = false;

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`userJob/doRecharge`, {
    params,
    method: 'GET',
    ...options,
  });
}
