import type { Open, DialogStore } from './types';

const store: DialogStore = {
  async open(a) {
    return a;
  },
};

export const setStoreOpen = (open: Open) => {
  store.open = open;
};

export default store;
