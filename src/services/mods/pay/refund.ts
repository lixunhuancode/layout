/**
 * @desc 统一退款
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.BaseRefundRequest;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.BaseRefundRequest };

export function request(
  data: defs.BaseRefundRequest,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/pay/refund`, {
    data,
    method: 'POST',
    ...options,
  });
}
