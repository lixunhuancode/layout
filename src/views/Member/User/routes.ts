export default [
  {
    path: 'user/list',
    name: 'member.user.list',
    component: () => import('@/views/Member/User/List/index.vue'),
    meta: {
      locale: '会员管理',
      requiresAuth: false,
    },
  },
  {
    path: 'user/formAdd',
    name: 'member.user.formAdd',
    component: () => import('@/views/Member/User/Form/index.vue'),
    meta: {
      locale: '新增会员',
      requiresAuth: false,
      hideInMenu: true,
    },
  },
  {
    path: 'user/formEdit/:id',
    name: 'member.user.formEdit',
    component: () => import('@/views/Member/User/Form/index.vue'),
    meta: {
      locale: '编辑会员',
      requiresAuth: false,
      hideInMenu: true,
    },
  },
  {
    path: 'user/detail/:id',
    name: 'member.user.detail',
    component: () => import('@/views/Member/User/Detail/index.vue'),
    meta: {
      locale: '客户详情',
      requiresAuth: false,
      hideInMenu: true,
    },
  },
  {
    path: 'recharge/form',
    name: 'member.user.recharge.form',
    component: () => import('@/views/Member/Recharge/Form/index.vue'),
    meta: {
      locale: '会员充值',
      requiresAuth: false,
      hideInMenu: true,
    },
  },
];
