export default {
  path: 'statement',
  name: 'finance.statement',
  component: () => import('./list/index.vue'),
  meta: {
    locale: '进销存报表',
  }
};
