/**
 * @desc 通过ids获取列表
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = Array<number>;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: Array<number> };

export function request(data: Array<number>, options: AxiosRequestConfig) {
  return axiosHandle(`backend/rechargeReason/listByIds`, {
    data,
    method: 'POST',
    ...options,
  });
}
