export default [
  {
    path: 'service-assurance/list',
    name: 'cms.service-assurance.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '服务保障',
      requiresAuth: false,
    },
  },
  {
    path: 'service-assurance/form',
    name: 'cms.service-assurance.add',
    component: () => import('./Form/index.vue'),
    meta: {
      hideInMenu: true,
      locale: '新增服务保障',
      requiresAuth: false,
    },
  },
  {
    path: 'service-assurance/form/:id',
    name: 'cms.service-assurance.edit',
    component: () => import('./Form/index.vue'),
    meta: {
      hideInMenu: true,
      locale: '编辑服务保障',
      requiresAuth: false,
    },
  },
];
