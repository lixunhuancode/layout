// 读取elcel文件
import * as XLSX from 'xlsx';

const readExcel = (file: Blob) => {
  return new Promise((resolve, reject) => {
    let istrue = false;
    const importdata = {
      sheet1: [],
    };
    const fileReader = new FileReader();
    fileReader.readAsBinaryString(file);
    fileReader.onload = (ev) => {
      const data = ev.target?.result;
      const workbook = XLSX.read(data, { type: 'binary' });
      Object.keys(workbook.Sheets).forEach((sheet: string) => {
        const sheetArray = XLSX.utils.sheet_to_json(workbook.Sheets[sheet]);
        if (sheetArray.length) {
          istrue = true;
          importdata[sheet] = sheetArray;
        }
      });
      if (istrue) {
        resolve(importdata.sheet1);
      } else {
        reject(new Error('文件解析错误'));
      }
    };
  });
};

export default readExcel;
