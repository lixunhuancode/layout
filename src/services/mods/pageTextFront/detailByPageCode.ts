/**
 * @desc 服务内容详情
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** pageCode */
  pageCode: string;
}

export const init = undefined;

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`cms/pageText/service/${params.pageCode}`, {
    params,
    method: 'GET',
    ...options,
  });
}
