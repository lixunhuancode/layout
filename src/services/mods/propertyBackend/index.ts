/**
 * @description 商品管理后台接口【规格管理接口】
 */
import * as addProperty from './addProperty';
import * as deleteProperty from './deleteProperty';
import * as getByKeyBatch from './getByKeyBatch';
import * as getPropertyDetail from './getPropertyDetail';
import * as modifyProperty from './modifyProperty';
import * as getProductPropertyList from './getProductPropertyList';
import * as queryPropertyList from './queryPropertyList';

export {
  addProperty,
  deleteProperty,
  getByKeyBatch,
  getPropertyDetail,
  modifyProperty,
  getProductPropertyList,
  queryPropertyList,
};
