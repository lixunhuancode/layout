export default [
  {
    path: 'role/list',
    name: 'setting.role.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '角色管理',
      requiresAuth: false,
    },
  },
];
