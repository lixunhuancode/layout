export default [
  {
    path: 'car-manage/list',
    name: 'member.carManage.list',
    component: () => import('@/views/Member/CarManage/List/index.vue'),
    meta: {
      locale: '车辆管理',
      requiresAuth: false,
      isHidden: true,
      hideInMenu: true,
    },
  },
];
