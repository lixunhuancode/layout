/**
   * @desc 创建品类
创建品类
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.CategoryDTO;

export const init = false;

export type ParamsAndBody = { data: defs.CategoryDTO };

export function request(data: defs.CategoryDTO, options: AxiosRequestConfig) {
  return axiosHandle(`backend/category/createCategory`, {
    data,
    method: 'POST',
    ...options,
  });
}
