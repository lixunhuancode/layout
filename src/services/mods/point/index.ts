/**
 * @description 积点相关接口
 */
import * as auditRecharge from './auditRecharge';
import * as delRecharge from './delRecharge';
import * as modifyRecharge from './modifyRecharge';
import * as ListPage from './ListPage';
import * as recharge from './recharge';
import * as postRechargeDetailById from './postRechargeDetailById';
import * as rechargePage from './rechargePage';
import * as trial from './trial';
import * as trialResult from './trialResult';

export {
  auditRecharge,
  delRecharge,
  modifyRecharge,
  ListPage,
  recharge,
  postRechargeDetailById,
  rechargePage,
  trial,
  trialResult,
};
