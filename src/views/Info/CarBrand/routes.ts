export default [
  {
    path: 'car-brand/list',
    name: 'info.car-brand.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '车型品牌',
      requiresAuth: false,
      isHidden: true,
      hideInMenu: true,
    },
  },
];
