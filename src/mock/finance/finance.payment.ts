import {
  successResponseWrap,
  mock,
  pagerResponseWrap,
} from '@/utils/setup-mock';

export const shopList = [
  {
    id: 1,
    a1: '直营1号店',
    a2: '265.98',
    a3: '28362.86',
    a4: '67265.98',
    a5: '0',
    a6: '10283.23',
    a7: '283.23',
  },
  {
    id: 2,
    a1: '直营2号店',
    a2: '125.98',
    a3: '28362.86',
    a4: '67265.98',
    a5: '0',
    a6: '10283.18',
    a7: '7878.23',
  },
  {
    id: 3,
    a1: '加盟1号店',
    a2: '235.38',
    a3: '28262.26',
    a4: '17265.98',
    a5: '0',
    a6: '20283.23',
    a7: '9454.23',
  },
  {
    id: 4,
    a1: '加盟2号店',
    a2: '35.98',
    a3: '2162.86',
    a4: '6765.21',
    a5: '0',
    a6: '10243.13',
    a7: '2813.23',
  },
];

export const list = (req: any) => {
  return mock(pagerResponseWrap(req, shopList));
};

export const all = () => {
  return mock(successResponseWrap(shopList));
};

export default mock;
