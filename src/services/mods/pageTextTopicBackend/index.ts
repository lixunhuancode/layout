/**
 * @description 内容管理-专题管理-后台接口
 */
import * as create from './create';
import * as remove from './remove';
import * as detail from './detail';
import * as list from './list';
import * as search from './search';
import * as update from './update';

export { create, remove, detail, list, search, update };
