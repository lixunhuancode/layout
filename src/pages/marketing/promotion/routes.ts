export default [
  {
    path: 'promotion/list',
    name: 'marketing.promotion.list',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '满减促销',
    },
  },
  {
    path: 'promotion/add',
    name: 'marketing.promotion.add',
    component: () => import('./form/index.vue'),
    meta: {
      locale: '新建活动',
      hideInMenu: true,
    },
  },
  {
    path: 'promotion/edit',
    name: 'marketing.promotion.edit',
    component: () => import('./form/index.vue'),
    meta: {
      locale: '编辑活动',
      hideInMenu: true,
    },
  },
  {
    path: 'promotion/detail',
    name: 'marketing.promotion.detail',
    component: () => import('./details/index.vue'),
    meta: {
      locale: '活动详情',
      hideInMenu: true,
    },
  },
  {
    path: 'promotion/statement',
    name: 'marketing.promotion.statement',
    component: () => import('./statement/index.vue'),
    meta: {
      locale: '统计数据',
      hideInMenu: true,
    },
  },
];
