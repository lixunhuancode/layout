/**
 * @desc 获取专题列表
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryPageTextDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.QueryPageTextDTO };

export function request(
  data: defs.QueryPageTextDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/cms/pageText/topic/list`, {
    data,
    method: 'POST',
    ...options,
  });
}
