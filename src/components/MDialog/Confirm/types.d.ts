import { VNode } from 'vue';

export type IField = {
  name: string;
  field: string;
  type: string;
  placeholder?: string;
  rules?: [];
};

export interface ConfirmData {
  content: VNode | string;
  title?: string;
  dialogTitle?: string;
  type?: 'info' | 'success' | 'warning' | 'error';
  fields?: IField[];
  submitText?: string;
  hideCancel?: boolean;
}

export type ConfirmFn = (options: ConfirmData | string) => Promise<any>;
