export default [
  {
    path: 'markReceipt',
    name: 'cashier.markReceipt',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '标记收款',
    },
  },
];

