import Mock from 'mockjs';

import './user';
import './order';
import './member';
import './message-box';
import './finance/join-settlement';
import './finance/payment';
import './finance/purchase-settlement';
import './finance/savings-settlement';
import './finance/statement';

Mock.setup({
  timeout: '600-1000',
});
