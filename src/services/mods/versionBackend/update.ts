/**
 * @desc 编辑
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.SettingVersionDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.SettingVersionDTO };

export function request(
  data: defs.SettingVersionDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/version/update`, {
    data,
    method: 'POST',
    ...options,
  });
}
