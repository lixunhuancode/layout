/**
   * @desc 发送短信验证码(通用)
1:短信验证码登录,2:绑定会员卡,3:绑定手机号
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.SmsSendDTO;

export const init = false;

export type ParamsAndBody = { data: defs.SmsSendDTO };

export function request(data: defs.SmsSendDTO, options: AxiosRequestConfig) {
  return axiosHandle(`sms/send/sendMsg`, {
    data,
    method: 'POST',
    ...options,
  });
}
