/**
 * @desc 积分变动记录
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryUserPointRecordDTO;

export const init = new defs.UserPointRecordListVO();

export type ParamsAndBody = { data: defs.QueryUserPointRecordDTO };

export function request(
  data: defs.QueryUserPointRecordDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`webapi/user/pointListPage`, {
    data,
    method: 'POST',
    ...options,
  });
}
