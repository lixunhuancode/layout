export default {
  path: 'management',
  name: 'order.management',
  component: () => import('@/components/routeView.vue'),
  meta: {
    locale: '订单管理',
  },
  children: [
    {
      path: 'list',
      name: 'order.management.list',
      component: () => import('./list/index.vue'),
      meta: {
        sort: 1,
        locale: '订单列表',
      },
    },
    {
      path: 'detail/:id',
      name: 'order.management.detail',
      component: () => import('./detail/index.vue'),
      meta: {
        locale: '订单详情',
        hideInMenu: true,
      },
    },
    {
      path: 'after-sale/list',
      name: 'order.after-sale.list',
      component: () => import('./after-sale/list/index.vue'),
      meta: {
        sort: 2,
        locale: '售后列表',
      },
    },
    {
      path: 'after-sale/detail/:id',
      name: 'order.after-sale.detail',
      component: () => import('./after-sale/detail/index.vue'),
      meta: {
        locale: '售后详情',
        hideInMenu: true,
      },
    },
  ],
};
