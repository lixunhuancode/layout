/**
   * @desc 根据订单id查询订单详细信息
根据订单id查询订单详细信息
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** id */
  id: number;
}

export const init = new defs.OrderDetailInfoVO();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`front/order/getOrderById/${params.id}`, {
    params,
    method: 'GET',
    ...options,
  });
}
