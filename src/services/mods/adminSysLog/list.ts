/**
 * @desc 列表
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.AdminSysLogQueryDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.AdminSysLogQueryDTO };

export function request(
  data: defs.AdminSysLogQueryDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`admin/log/list`, {
    data,
    method: 'POST',
    ...options,
  });
}
