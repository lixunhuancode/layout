/**
   * @desc 获取营销分类树
获取营销分类树
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** 状态 1-启用 0-禁用 默认查启用 */
  status?: number;
}

export const init = new defs.ProductMktVO();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`front/product/getMktTree`, {
    params,
    method: 'GET',
    ...options,
  });
}
