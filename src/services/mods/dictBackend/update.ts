/**
 * @desc 编辑
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.SettingDictDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.SettingDictDTO };

export function request(
  data: defs.SettingDictDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/dict/update`, {
    data,
    method: 'POST',
    ...options,
  });
}
