/**
 * @desc 生成微信小程序二维码
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.CreateWechatQRCodeDTO;

export const init = undefined;

export type ParamsAndBody = { data: defs.CreateWechatQRCodeDTO };

export function request(
  data: defs.CreateWechatQRCodeDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`qrCode/createWechatQRCode`, {
    data,
    method: 'POST',
    ...options,
  });
}
