export default [
  {
    path: 'join/list',
    name: 'finance.join-settlement.list',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '加盟结算',
    },
  },
  {
    path: 'join/add',
    name: 'finance.join-settlement.add',
    component: () => import('./addOrEdit/index.vue'),
    meta: {
      locale: '创建结算单',
      hideInMenu: true,
    },
  },
  {
    path: 'join/detail',
    name: 'finance.join-settlement.detail',
    component: () => import('./detail/index.vue'),
    meta: {
      locale: '查看',
      hideInMenu: true,
    },
  },
];
