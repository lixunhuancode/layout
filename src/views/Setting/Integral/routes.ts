export default [
  {
    path: 'integral/list',
    name: 'setting.integral.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '积分设置',
      requiresAuth: false,
    },
  },
];
