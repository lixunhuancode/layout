const staticDictList = {
  a1: {
    name: '履约方式',
    data: [
      { label: '快递发货', value: 3, color: 'gray' },
      { label: '到店自提', value: 2, color: 'orange' },
      { label: '同城配送', value: 1, color: 'green' },
    ],
  },
  a2: {
    name: '订单来源',
    data: [
      { label: '微信商城', value: 3 },
      { label: '门店Pos', value: 2 },
    ],
  },
  a3: {
    name: '是否退款',
    data: [
      { label: '有', value: 3 },
      { label: '无', value: 2 },
    ],
  },
  a4: {
    name: '售后原因',
    data: [
      { label: '不喜欢', value: 1 },
      { label: '质量问题', value: 2 },
      { label: '七天无理由退款', value: 3 },
    ],
  },
  a5: {
    name: '售后类型',
    data: [
      { label: '退货退款', value: 1 },
      { label: '仅退款', value: 2 },
    ],
  },
  a6: {
    name: '退回方式',
    data: [
      { label: '快递退回', value: 1 },
      { label: '退回门店', value: 2 },
    ],
  },
  a7: {
    name: '快递公司',
    data: [
      { label: '顺丰', value: 1 },
      { label: '圆通', value: 2 },
      { label: '韵达', value: 3 },
    ],
  },
  a8: {
    name: '同城配送公司',
    data: [
      { label: '达达', value: 1 },
      { label: '顺丰同城', value: 2 },
      { label: '美团跑腿', value: 3 },
    ],
  },
  pageType: {
    name: '页面类型',
    code: 'pageType',
    data: [
      { label: '富文本', value: 1 },
      { label: '小程序', value: 2 },
    ],
  },
  orderTypes: {
    name: '订单类别',
    data: [
      { label: '线上', value: 1, color: 'orange' },
      { label: '线下', value: 2, color: 'orange' },
      { label: '批发', value: 3, color: 'orange' },
      { label: '团购', value: 4, color: 'green' },
    ],
  },
  tradeTypes: {
    name: '交易类型',
    data: [
      { label: '正向', value: 1, color: 'orange' },
      { label: '售后', value: 2, color: 'orange' },
    ],
  },
  afterSaleStatus: {
    name: '售后状态',
    data: [
      { label: '待审核', value: 1, color: 'orange' },
      { label: '待收货', value: 2, color: 'red' },
      { label: '已完成', value: 4, color: 'green' },
      { label: '已取消/已关闭', value: 3, color: 'gray' },
    ],
  },
  orderStatus: {
    name: '订单状态',
    data: [
      { label: '待支付', value: 1, color: 'orange' },
      { label: '待发货', value: 2, color: 'orange' },
      { label: '待收货/待自提', value: 3, color: 'orange' },
      { label: '已完成', value: 4, color: 'green' },
      { label: '已取消', value: 5, color: 'gray' },
    ],
  },
  deliverGoodsStatus: {
    name: '快递发货状态',
    data: [
      { label: '待发货', value: 1, color: 'orange' },
      { label: '已发货', value: 2, color: 'green' },
    ],
  },
  payType: {
    name: '支付方式',
    data: [
      { label: '微信支付', value: 3 },
      { label: '支付宝', value: 2 },
    ],
  },
  pickOrderStoreStatus: {
    name: '自提订单状态',
    data: [
      { label: '待自提', value: 1 },
      { label: '已自提', value: 2 },
    ],
  },
  intraCityService: {
    name: '同城配送订单状态',
    data: [
      { label: '待发货', value: 1 },
      { label: '待骑手接单', value: 2 },
      { label: '待取货', value: 3 },
      { label: '配送中', value: 4 },
    ],
  },
  productResources: {
    name: '商品来源',
    data: [
      { label: '自建', value: 1 },
      { label: '商品库', value: 2 },
    ],
  },
  shippingTypes: {
    name: '配送方式',
    data: [
      { label: '门店自提', value: 1 },
      { label: '快递发货', value: 2 },
      { label: '同城配送', value: 3 },
    ],
  },
  productStatus: {
    name: '商品状态',
    data: [
      { label: '草稿', value: 1, color: 'gray' },
      { label: '启用', value: 2, color: 'green' },
      { label: '禁用', value: 3, color: 'red' },
    ],
  },
  marketingChannel: {
    name: '销售渠道',
    data: [
      { label: '网店渠道', value: 1 },
      { label: '门店渠道', value: 2 },
    ],
  },
  deliveryStatus: {
    name: '下发状态',
    data: [
      { label: '可售', value: 1 },
      { label: '上架', value: 2 },
    ],
  },
  auditProductStatus: {
    name: '商品审核状态',
    data: [
      { label: '待审核', value: 1, color: 'orange' },
      { label: '已通过', value: 2, color: 'green' },
      { label: '已驳回', value: 3, color: 'red' },
    ],
  },
  productTypes: {
    name: '商品类型',
    data: [
      { label: '实物商品', value: 1 },
      { label: '虚拟商品', value: 2 },
    ],
  },
  deliveryMethod: {
    name: '发货方式',
    data: [
      { label: '物流发货', value: 1 },
      { label: '无物流发货', value: 2 },
    ],
  },
  status2: {
    name: '通用状态2',
    data: [
      { label: '需要', value: 1, color: 'green' },
      { label: '不需要', value: 2, color: 'red' },
    ],
  },
  status: {
    name: '通用状态',
    data: [
      { label: '启用', value: 1, color: 'green' },
      { label: '禁用', value: 2, color: 'red' },
    ],
  },
  addressOptions: {
    name: '地址',
    data: [
      { label: '重庆市', value: '重庆市', color: 'green' },
      { label: '四川省', value: '四川省', color: 'red' },
    ],
  },
  mallProductStatus: {
    name: '店铺商品状态',
    data: [
      { label: '销售中', value: 1, color: 'green' },
      { label: '已售罄', value: 2, color: 'red' },
      { label: '仓库中', value: 3, color: 'orange' },
    ],
  },
  dadjapType: {
    name: '调整价格类型',
    data: [
      { label: '建议零售价', value: 1, color: 'green' },
      { label: '销售价', value: 2, color: 'red' },
    ],
  },
  dadjapStatus: {
    name: '调整价格类型',
    data: [
      { label: '草稿', value: 1, color: 'blue' },
      { label: '待审核', value: 2, color: 'orange' },
      { label: '已完成', value: 3, color: 'green' },
      { label: '已关闭', value: 4, color: 'gray' },
    ],
  },
  businessPattern: {
    name: '经营方式',
    data: [
      { label: '经销', value: 1, color: 'blue' },
      { label: '代销', value: 2, color: 'orange' },
      { label: '联营', value: 3, color: 'green' },
    ],
  },
  businessModel: {
    name: '经营模式',
    data: [
      { label: '直营', value: 1, color: 'orange' },
      { label: '加盟', value: 2, color: 'green' },
    ],
  },
  clearingForm: {
    name: '结算方式',
    data: [
      { label: '先货后款', value: 1, color: 'blue' },
      { label: '先款后货', value: 2, color: 'orange' },
    ],
  },
  purchaseReceiptStatus: {
    name: '采购进货单状态',
    data: [
      { label: '待提交', value: 1, color: 'blue' },
      { label: '待收货', value: 2, color: 'orange' },
      { label: '部分收货', value: 3, color: 'orange' },
      { label: '已完成', value: 4, color: 'green' },
      { label: '已关闭', value: 5, color: 'gray' },
    ],
  },
  purchaseReturnStatus: {
    name: '采购退货单状态',
    data: [
      { label: '待提交', value: 1, color: 'blue' },
      { label: '待退货', value: 2, color: 'orange' },
      { label: '部分退货', value: 3, color: 'orange' },
      { label: '已完成', value: 4, color: 'green' },
      { label: '已关闭', value: 5, color: 'gray' },
    ],
  },
  pullSgnal: {
    name: '要货申请状态',
    data: [
      { label: '待提交', value: 1, color: 'blue' },
      { label: '待审核', value: 2, color: 'orange' },
      { label: '待发货', value: 3, color: 'orange' },
      { label: '部分发货', value: 4, color: 'green' },
      { label: '已完成', value: 5, color: 'gray' },
      { label: '已关闭', value: 5, color: 'gray' },
    ],
  },
  tocSscrStatus: {
    name: '配销单状态',
    data: [
      { label: '待提交', value: 1, color: 'blue' },
      { label: '待发货', value: 2, color: 'orange' },
      { label: '部分发货', value: 3, color: 'green' },
      { label: '待收货', value: 4, color: 'orange' },
      { label: '已完成', value: 5, color: 'gray' },
      { label: '已关闭', value: 6, color: 'gray' },
    ],
  },
  transferStatus: {
    name: '调拨单状态',
    data: [
      { label: '待提交', value: 1, color: 'blue' },
      { label: '待仓库出库', value: 2, color: 'orange' },
      { label: '部分出库', value: 3, color: 'orange' },
      { label: '待门店入库', value: 4, color: 'orange' },
      { label: '已完成', value: 5, color: 'green' },
      { label: '已关闭', value: 6, color: 'gray' },
    ],
  },
  tocSscrReturnStatus: {
    name: '配销单状态',
    data: [
      { label: '待提交', value: 1, color: 'blue' },
      { label: '待审核', value: 2, color: 'blue' },
      { label: '待退货', value: 3, color: 'orange' },
      { label: '已完成', value: 4, color: 'green' },
      { label: '已关闭', value: 5, color: 'gray' },
    ],
  },
  activityType: {
    name: '活动类型',
    data: [
      { label: '全部', value: null, color: 'blue' },
      { label: '优惠券', value: 2, color: 'blue' },
      { label: '限时降价', value: 3, color: 'orange' },
      { label: '满减促销', value: 4, color: 'green' },
    ],
  },
  batchDistributionStatus: {
    name: '批量分货状态',
    data: [
      { label: '待提交', value: 1, color: 'blue' },
      { label: '已完成', value: 2, color: 'green' },
      { label: '已关闭', value: 3, color: 'gray' },
    ],
  },
  cmsLinkTypes: {
    name: 'cms链接类型',
    data: [
      { label: '商品', value: 1, color: 'red' },
      { label: '专题', value: 2, color: 'cyan' },
      { label: '首页', value: 3, color: 'orange' },
      { label: '服务内容', value: 4, color: 'green' },
      { label: '分类', value: 5, color: 'gray' },
    ],
  },
  b1: {
    name: '结算状态',
    data: [
      { label: '结算中', value: 3, color: 'gray' },
      { label: '已完成', value: 2, color: 'green' },
      { label: '已作废', value: 1, color: 'orange' },
    ],
  },
  b2: {
    name: '流程状态',
    data: [
      { label: '全部', value: null, color: 'gray' },
      { label: '付款方确认', value: 1, color: 'green' },
      { label: '收款方确认', value: 2, color: 'green' },
      { label: '付款方打款', value: 3, color: 'orange' },
      { label: '收款方收打款', value: 4, color: 'gray' },
    ],
  },
  b3: {
    name: '销售渠道',
    data: [
      { label: '全部', value: null, color: 'gray' },
      { label: '加盟店', value: 1, color: 'green' },
      { label: '直营店', value: 2, color: 'green' },
    ],
  },
  b4: {
    name: '毛利',
    data: [
      { label: '全部', value: null, color: 'gray' },
      { label: '负毛利', value: 1, color: 'orange' },
    ],
  },
  b5: {
    name: '商品分类',
    data: [
      { label: '全部', value: null, color: 'gray' },
      { label: '裤子', value: 1, color: 'green' },
      { label: '衣服', value: 2, color: 'green' },
    ],
  },
  b6: {
    name: '品牌',
    data: [
      { label: '全部', value: null, color: 'gray' },
      { label: '耐克', value: 1, color: 'green' },
    ],
  },
  b7: {
    name: '商品成本',
    data: [
      { label: '全部', value: null, color: 'gray' },
      { label: '含税', value: 1, color: 'green' },
      { label: '不含税', value: 2, color: 'green' },
    ],
  },
  b8: {
    name: '商品筛选条件',
    data: [
      { label: '商品名称', value: 1, color: 'green' },
      { label: '商品条码', value: 2, color: 'green' },
      { label: '商品编码', value: 3, color: 'green' },
    ],
  },
  b9: {
    name: '单据状态',
    data: [
      { label: '全部', value: null, color: 'gray' },
      { label: '待入库', value: 1, color: 'orange' },
      { label: '已入库', value: 2, color: 'green' },
    ],
  },
  b10: {
    name: '入库明细状态',
    data: [
      { label: '已完成', value: 1, color: 'green' },
      { label: '已作废', value: 2, color: 'orange' },
      { label: '草稿', value: 3, color: 'gray' },
    ],
  },
  b11: {
    name: '入库原因',
    data: [
      { label: '其他', value: 1, color: 'green' },
      { label: '1', value: 2, color: 'orange' },
      { label: '2', value: 3, color: 'gray' },
    ],
  },
  b12: {
    name: '出库原因',
    data: [
      { label: '试吃', value: 1, color: 'green' },
      { label: '领用', value: 2, color: 'orange' },
      { label: '报损', value: 3, color: 'gray' },
      { label: '其他', value: 3, color: 'gray' },
    ],
  },
  b13: {
    name: '盘点单据状态',
    data: [
      { label: '全部', value: null, color: 'gray' },
      { label: '盘点中', value: 1, color: 'orange' },
      { label: '已完成', value: 2, color: 'green' },
      { label: '已作废', value: 3, color: 'gray' },
    ],
  },
  b14: {
    name: '盘点类型',
    data: [
      { label: '全部', value: null, color: 'gray' },
      { label: '日常盘点', value: 1, color: 'orange' },
      { label: '任务盘点', value: 2, color: 'green' },
    ],
  },
  b15: {
    name: '盘点任务状态',
    data: [
      { label: '待盘点', value: 1, color: 'gray' },
      { label: '盘点中', value: 2, color: 'orange' },
      { label: '已完成', value: 3, color: 'green' },
    ],
  },
  b16: {
    name: '库存预警状态',
    data: [
      { label: '全部', value: null, color: 'gray' },
      { label: '低库存', value: 2, color: 'orange' },
      { label: '正常', value: 1, color: 'gray' },
      { label: '高库存', value: 3, color: 'green' },
    ],
  },
  b17: {
    name: '盘存状态',
    data: [
      { label: '盘盈', value: 2, color: 'orange' },
      { label: '盘亏', value: 1, color: 'gray' },
      { label: '盘平', value: 3, color: 'green' },
    ],
  },
  couponType: {
    name: '券类型',
    data: [
      { label: '全部', value: null, color: 'orange' },
      { label: '满减券', value: 1, color: 'gray' },
      { label: '兑礼券', value: 2, color: 'green' },
    ],
  },
  couponStatus: {
    name: '券状态',
    data: [
      { label: '全部', value: null, color: 'orange' },
      { label: '待审核', value: 2, color: 'green' },
      { label: '已驳回', value: 3, color: 'green' },
      { label: '待上架', value: 4, color: 'green' },
      { label: '已上架', value: 5, color: 'green' },
      { label: '已下架', value: 6, color: 'green' },
      { label: '发券中', value: 7, color: 'green' },
      { label: '已结束', value: 8, color: 'green' },
    ],
  },
  groundingType: {
    name: '上架方式',
    data: [
      { label: '审核通过自动上架', value: 1, color: 'gray' },
      { label: '审核通过手动上架', value: 2, color: 'green' },
    ],
  },
  couponChannel: {
    name: '发券渠道',
    data: [
      { label: '线下门店', value: 1, color: 'gray' },
      { label: '线上商城', value: 2, color: 'green' },
    ],
  },
  publicStatus: {
    name: '通用是否',
    data: [
      { label: '是', value: 1, color: 'green' },
      { label: '否', value: 2, color: 'orange' },
    ],
  },
  sendCouponType: {
    name: '发券类型',
    data: [
      { label: '单用户发券', value: 1, color: 'orange' },
      { label: '多用户批量发券', value: 2, color: 'gray' },
      { label: '按会员标签发券', value: 3, color: 'green' },
    ],
  },
  neckCouponType: {
    name: '领券类型',
    data: [
      { label: '免费领取', value: 1, color: 'gray' },
      { label: '积分兑换', value: 2, color: 'green' },
      { label: '购买领取', value: 3, color: 'green' },
    ],
  },
  useStoreType: {
    name: '用券门店',
    data: [
      { label: '所有门店可用', value: 1, color: 'green' },
      { label: '自定义可用门店', value: 2, color: 'orange' },
    ],
  },
  useProductType: {
    name: '用券商品限制',
    data: [
      { label: '所有商品可用', value: 1, color: 'green' },
      { label: '限定用券商品', value: 2, color: 'orange' },
      { label: '限定不可用券商品', value: 3, color: 'orange' },
    ],
  },
  useTimeType: {
    name: '用券有效期类型',
    data: [
      { label: '固定时间段', value: 1, color: 'green' },
      { label: '浮动时间段', value: 2, color: 'orange' },
      { label: '领券当天有效', value: 3, color: 'orange' },
      { label: '领券当月有效', value: 4, color: 'orange' },
    ],
  },
  activitySource: {
    name: '活动来源',
    data: [
      { label: '全部', value: null, color: 'orange' },
      { label: '总部创建', value: 1, color: 'gray' },
      { label: '直营门店', value: 2, color: 'green' },
      { label: '加盟门店', value: 3, color: 'green' },
    ],
  },
  activityStatus: {
    name: '活动状态',
    data: [
      { label: '全部', value: null, color: 'orange' },
      { label: '审核中', value: 1, color: 'gray' },
      { label: '待上架', value: 2, color: 'green' },
      { label: '已上架', value: 3, color: 'green' },
      { label: '已下架', value: 4, color: 'green' },
      { label: '已驳回', value: 5, color: 'orange' },
      { label: '发券中', value: 6, color: 'green' },
      { label: '已结束', value: 7, color: 'green' },
    ],
  },
  activityUseCoupon: {
    name: '用券叠加',
    data: [
      { label: '不限制', value: 1, color: 'gray' },
      { label: '限制，活动商品不能使用优惠券', value: 2, color: 'green' },
    ],
  },
  activityStoreType: {
    name: '活动门店',
    data: [
      { label: '所有门店可用', value: 1, color: 'green' },
      { label: '自定义可用门店', value: 2, color: 'orange' },
    ],
  },
  backGoodsType: {
    name: '退货退券',
    data: [
      { label: '退券', value: 1, color: 'green' },
      { label: '不退券', value: 2, color: 'orange' },
    ],
  },
  memberStatus: {
    name: '会员状态',
    data: [
      { label: '未激活', value: 1, color: 'gray' },
      { label: '激活', value: 2, color: 'green' },
      { label: '限制购买', value: 3, color: 'red' },
    ],
  },
  memberChannel: {
    name: '会员渠道',
    data: [
      { label: '后台添加', value: 1, color: 'green' },
      { label: '微信商城', value: 2, color: 'green' },
    ],
  },
  memberLevel: {
    name: '会员等级',
    data: [
      { label: '全部', value: null, color: 'green' },
      { label: '普通会员', value: 1, color: 'green' },
      { label: '白银会员', value: 2, color: 'green' },
      { label: '黄金会员', value: 3, color: 'green' },
      { label: '钻石会员', value: 4, color: 'green' },
    ],
  },
  integralPointRechargeType: {
    name: '类型',
    data: [
      { label: '全部', value: null, color: 'gray' },
      { label: '单人', value: 1, color: 'green' },
      { label: '批量', value: 2, color: 'green' },
    ],
  },
  integralPointRechargeStatus: {
    name: '状态',
    data: [
      { label: '全部', value: null, color: 'gray' },
      { label: '审批中', value: 1, color: 'orange' },
      { label: '已通过', value: 2, color: 'green' },
      { label: '已驳回', value: 3, color: 'red' },
    ],
  },
  pointsApprovalStatus: {
    name: '状态',
    data: [
      { label: '待审批', value: 1, color: 'green' },
      { label: '已通过', value: 2, color: 'green' },
      { label: '已驳回', value: 3, color: 'green' },
    ],
  },
  storedOutIncomeStatus: {
    name: '出入账状态',
    data: [
      { label: '全部', value: null, color: 'gray' },
      { label: '出账', value: 1, color: 'green' },
      { label: '入账', value: 2, color: 'green' },
    ],
  },
  storedType: {
    name: '储值明细类型',
    data: [
      { label: '全部', value: null, color: 'gray' },
      { label: '储值', value: 1, color: 'green' },
      { label: '储值退回', value: 2, color: 'green' },
      { label: '消费', value: 3, color: 'green' },
      { label: '消费冲正', value: 4, color: 'green' },
      { label: '售后', value: 5, color: 'green' },
    ],
  },
  sourceChannel: {
    name: '来源渠道',
    data: [
      { label: '全部', value: null, color: 'gray' },
      { label: '微信小程序', value: 1, color: 'green' },
      { label: '收银POS', value: 2, color: 'green' },
    ],
  },
  storedValueStatus: {
    name: '状态',
    data: [
      { label: '已完成', value: 1, color: 'green' },
      { label: '已退款', value: 2, color: 'red' },
    ],
  },
  equityCardStatus: {
    name: '状态',
    data: [
      { label: '已开启', value: 1, color: 'green' },
      { label: '未开启', value: 0, color: 'red' },
    ],
  },
  purchaseApplyStatus: {
    name: '状态',
    data: [
      { label: '待审核', value: 1, color: 'orange' },
      { label: '待配货', value: 2, color: 'orange' },
      { label: '部分配货', value: 3, color: 'orange' },
      { label: '已完成', value: 4, color: 'green' },
      { label: '已关闭', value: 5, color: 'gray' },
    ],
  },
  memberTagsType: {
    name: '标签类型',
    data: [{ label: '手动标签', value: 1, color: 'green' }],
  },
  levelType: {
    name: '成长值类型',
    data: [
      { label: '消费', value: 1, color: 'green' },
      { label: '消费冲正', value: 2, color: 'green' },
      { label: '售后', value: 3, color: 'green' },
      { label: '完善资料', value: 4, color: 'green' },
      { label: '绑定手机号', value: 5, color: 'green' },
      { label: '过期', value: 6, color: 'green' },
    ],
  },
  levelOperType: {
    name: '成长值操作类型',
    data: [
      { label: '后台', value: 1, color: 'green' },
      { label: '前端用户', value: 2, color: 'green' },
      { label: '系统处理', value: 3, color: 'green' },
    ],
  },
  integralDetailsType: {
    name: '积分流水类型',
    data: [
      { label: '充值', value: 1, color: 'green' },
      { label: '充值退回', value: 2, color: 'green' },
      { label: '消费', value: 3, color: 'green' },
      { label: '消费', value: 4, color: 'green' },
      { label: '消费冲正', value: 5, color: 'green' },
      { label: '售后', value: 6, color: 'green' },
      { label: '积分抵扣', value: 7, color: 'green' },
    ],
  },
  paymentStatus: {
    name: '缴款单状态',
    data: [
      { label: '已审核', value: 1, color: 'green' },
      { label: '未审核', value: 0, color: 'orange' },
    ],
  },
};

const genDictStoreState = (list) => {
  const state = list.reduce(
    (pre, item) => {
      item.valMap = item.data.reduce((valMap, op) => {
        valMap[op.value] = op;
        return valMap;
      }, {});

      pre.dict[item.code] = item;
      pre.codes.push(item.code);
      return pre;
    },
    { dict: {}, codes: [] }
  );
  return state;
};

export default genDictStoreState(
  Object.keys(staticDictList).map((code) => {
    return {
      code,
      ...staticDictList[code],
    };
  })
);
