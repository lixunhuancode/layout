export default {
  path: 'equityCard',
  name: 'equityCard',
  component: () => import('@/components/routeView.vue'),
  meta: {
    locale: '设置',
  },
  children: [
    {
      path: 'list',
      name: 'equityCard.list',
      component: () => import('./list/index.vue'),
      meta: {
        locale: '权益设置',
      },
    },
  ],
};
