/**
 * @desc 生成微信小程序二维码
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** 小程序页面链接 */
  path?: string;
  /** 参数(Map(string,string))JSON后传输(不需要可为空) */
  params?: string;
  /** 二维码的宽度，单位 px。最小 280px，最大 1280px(默认430) */
  width?: ref;
}

export const init = undefined;

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`wechatCode/createQRCode`, {
    params,
    method: 'GET',
    ...options,
  });
}
