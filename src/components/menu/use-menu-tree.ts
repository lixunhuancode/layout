import { computed } from 'vue';
import { RouteRecordRaw, RouteRecordNormalized } from 'vue-router';
// import usePermission from '@/hooks/permission';
import appClientMenus from '@/router/app-menus';
import { cloneDeep } from 'lodash';

export default function useMenuTree() {
  // const permission = usePermission();
  const appRoute = computed(() => {
    return appClientMenus();
  });

  const menuTree = computed(() => {
    const copyRouter = cloneDeep(appRoute.value) as RouteRecordNormalized[];

    function travel(_routes: RouteRecordRaw[], layer: number) {
      if (!_routes) return null;

      // const routes2 = _routes.sort((a, b) => {
      //   const aSort = a.meta?.sort as number;
      //   const bSort = a.meta?.sort as number;
      //   return aSort - bSort;
      // });

      // console.log({ _routes, routes2 });
      const collector: any = _routes
        .filter((item) => !item.meta?.hideInMenu)
        .map((element) => {
          // no access
          // if (!permission.accessRouter(element)) {
          //   return null;
          // }

          // leaf node
          if (element.meta?.hideChildrenInMenu || !element.children) {
            element.children = [];
            return element;
          }

          // route filter hideInMenu true
          element.children = element.children.filter(
            (x) => x.meta?.hideInMenu !== true
          );

          // Associated child node
          const subItem = travel(element.children, layer + 1);

          if (subItem.length) {
            element.children = subItem;
            return element;
          }
          // the else logic
          if (layer > 1) {
            element.children = subItem;
            return element;
          }

          if (element.meta?.hideInMenu === false) {
            return element;
          }

          return null;
        });
      return collector.filter(Boolean);
    }
    return travel(copyRouter, 0);
  });

  return {
    menuTree,
  };
}
