/**
   * @desc 设置安全库存
设置安全库存
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** skuId */
  skuId?: string;
  /** 安全库存 */
  safeStock?: number;
}

export const init = false;

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(
    `backend/productStock/setSafeStock/${params?.skuId}/${params?.safeStock}`,
    {
      params,
      method: 'POST',
      ...options,
    },
  );
}
