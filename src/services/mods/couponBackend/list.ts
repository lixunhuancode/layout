/**
 * @desc 列表
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.CouponQueryDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.CouponQueryDTO };

export function request(
  data: defs.CouponQueryDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`admin/coupon/list`, {
    data,
    method: 'POST',
    ...options,
  });
}
