import { DEFAULT_LAYOUT } from '@/router/routes/base';
import Welcome from './Welcome/routes';

export default {
  path: '/dashboard',
  name: 'dashboard',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '工作台',
    requiresAuth: false,
    order: 0,
    hideInMenu: true,
  },
  children: [...Welcome],
};
