/**
 * @description 内容管理-搜索关键词-后台接口
 */
import * as createKeyword from './createKeyword';
import * as deleteKeyword from './deleteKeyword';
import * as list from './list';
import * as updateKeyword from './updateKeyword';

export { createKeyword, deleteKeyword, list, updateKeyword };
