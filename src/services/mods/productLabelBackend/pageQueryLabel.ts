/**
   * @desc 分页查询标签列表
分页查询标签列表
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.QueryProductLabelDTO;

export const init = new defs.ProductLabelVO();

export type ParamsAndBody = { data: defs.QueryProductLabelDTO };

export function request(
  data: defs.QueryProductLabelDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/productLabel/pageQueryLabel`, {
    data,
    method: 'POST',
    ...options,
  });
}
