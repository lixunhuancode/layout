import { DEFAULT_LAYOUT } from '@/router/routes/base';
import Home from './Home/routes';
import ServiceAssurance from './ServiceAssurance/routes';
import PageRecommend from './PageRecommend/routes';
import Topic from './Topic/routes';
import Link from './link/routes';
import ServiceContent from './ServiceContent/routes';
import SearchKeyword from './SearchKeyword/routes';
import MemberBenefits from './MemberBenefits/routes';

export default {
  path: '/cms',
  name: 'cms',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '内容',
    requiresAuth: false,
    icon: 'icon-c-cms',
    order: 7,
  },
  children: [
    ...Home,
    ...PageRecommend,
    ...Topic,
    ...Link,
    ...ServiceContent,
    ...SearchKeyword,
    ...ServiceAssurance,
    ...MemberBenefits,
  ],
};
