/**
 * @desc 添加
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.BaseCarBrandDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.BaseCarBrandDTO };

export function request(
  data: defs.BaseCarBrandDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/carBrand/add`, {
    data,
    method: 'POST',
    ...options,
  });
}
