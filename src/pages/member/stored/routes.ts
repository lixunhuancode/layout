import setting from './setting/routes';
import search from './search/routes';
import details from './details/routes';

export default {
  path: 'stored',
  name: 'member.stored',
  component: () => import('@/components/routeView.vue'),
  meta: {
    locale: '会员储值',
    icon: 'icon-c-info',
    sort: 4,
  },
  children: [...setting, ...search, ...details],
};
