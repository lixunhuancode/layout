export default {
  name: '总部',
  code: 'master',
  resource: [
    'dashboard',
    'dashboard.welcome',
    'product.management.audit.list',
    'product.management.category',
    'product.management.unit',
    'product.management.brand',
    'product.management.basic.form',
    'product.management.basic.update',
    'product.management.basic.detail',
    'product.management',
    'product.management.basic',
    'product.management.channel.list',
    'product',
    'product.guide',
    'product.mall',
    'product.mall.product.list',
    'product.price',
    'product.price.list',
    'product.price.form',
    'product.price.detail',
    'product.price.price-record',
    'order',
    'order.management',
    'order.management.list',
    'order.after-sale.list',
    'order.management.detail',
    'order.after-sale.detail',
    'order.performance',
    'order.deliver-goods.list',
    'order.intra-city-service.list',
    'order.pick-order-store.list',
    'order.deliver-goods.detail',
    'order.pick-order-store.detail',
    'member',
    'member.management',
    'member.management.list',
    'member.tagManage.list',
    'member.management.form',
    'member.management.detail',
    'member.tagManage.form',
    'member.level',
    'member.level.list',
    'member.level.setting',
    'member.level.record',
    'member.level.edit',
    'integral',
    'member.integral.recharge.list',
    'member.integral.recharge.detail',
    'member.integral.recharge.batch',
    'member.integral.details',
    'member.integral.approval.list',
    'member.integral.approval.detail',
    'member.integral.setting',
    'member.integral.general',
    'member.integral.consume',
    'member.stored',
    'member.stored.details',
    'setting.list',
    'setting.edit',
    'member.stored.search.list',
    'member.stored.search.detail',
    'marketing',
    'marketing.marketingSite',
    'marketing.activityProductSearch',
    'marketing.activities',
    'marketing.coupon.list',
    'marketing.storeCoupon',
    'marketing.coupon.add',
    'marketing.coupon.edit',
    'marketing.coupon.detail',
    'marketing.coupon.statement',
    'marketing.timePrice.list',
    'marketing.timePrice.add',
    'marketing.timePrice.edit',
    'marketing.timePrice.detail',
    'marketing.timePrice.statement',
    'marketing.promotion.list',
    'marketing.promotion.add',
    'marketing.promotion.edit',
    'marketing.promotion.detail',
    'marketing.promotion.statement',
    'cms',
    'cms.home.list',
    'cms.topic.list',
    'cms.link.list',
    'cms.home.add',
    'cms.home.edit',
    'cms.topic.add',
    'cms.topic.edit',
    'purchase',
    'purchase.guide',
    'purchase.demand',
    'purchase.apply.audit',
    'purchase.distribution.list',
    'purchase.apply.form',
    'purchase.apply.update',
    'purchase.apply.detail',
    'purchase.distribution.form',
    'purchase.distribution.update',
    'purchase.distribution.detail',
    'purchase.outsourcing',
    'purchase.receipt.list',
    'purchase.return.list',
    'purchase.supplier.list',
    'purchase.receipt.form',
    'purchase.receipt.update',
    'purchase.receipt.detail',
    'purchase.return.form',
    'purchase.return.update',
    'purchase.return.detail',
    'purchase.supplier.form',
    'purchase.supplier.update',
    'purchase.distribution',
    'purchase.toc-sscr.list',
    'purchase.toc-sscr-return.list',
    'purchase.distribution.batch.list',
    'purchase.transfer.list',
    'purchase.toc-sscr.form',
    'purchase.toc-sscr.update',
    'purchase.toc-sscr.detail',
    'purchase.toc-sscr-return.form',
    'purchase.toc-sscr-return.update',
    'purchase.toc-sscr-return.detail',
    'purchase.distribution.batch.form',
    'purchase.distribution.batch.update',
    'purchase.distribution.batch.detail',
    'purchase.transfer.form',
    'purchase.transfer.update',
    'purchase.transfer.detail',
    'inventory',
    'inventory.guide',
    'inventory.query',
    'inventory.warning',
    'inventory.warehousing',
    'inventory.warehousing.in.add',
    'inventory.warehousing.in-details.list',
    'inventory.warehousing.out.list',
    'inventory.warehousing.out-details.list',
    'inventory.warehousing.in.list',
    'inventory.warehousing.in-details.add',
    'inventory.warehousing.in-details.detail',
    'inventory.warehousing.out.add',
    'inventory.warehousing.out-details.add',
    'inventory.warehousing.out-details.detail',
    'inventory.check',
    'inventory.physical.list',
    'inventory.physical.add',
    'inventory.physical.edit',
    'inventory.physical.input',
    'inventory.physical.detail',
    'inventory.check.task.list',
    'inventory.check.task.add',
    'inventory.check.task.detail',
    'bi',
    'bi.sales-report.statistical',
    'bi.sales-analysis',
    'finance',
    'finance.settlement',
    'finance.join-settlement.list',
    'finance.join-settlement.add',
    'finance.join-settlement.detail',
    'finance.savings-settlement.list',
    'finance.savings-settlement.add',
    'finance.savings-settlement.detail',
    'finance.purchase-settlement.list',
    'finance.purchase-settlement.add',
    'finance.purchase-settlement.detail',
    'integralSettlement.collect.list',
    'integralSettlement.final.list',
    'integralSettlement.final.form',
    'integralSettlement.final.detail',
    'finance.report',
    'finance.statement',
    'finance.payment',
    'cashier',
    'cashier.payment',
    'cashier.markReceipt',
    'cashier.pos',
    'setting',
    'setting.organizationManage',
    'setting.organizationManage.organization',
    'setting.organizationManage.department',
    'setting.organizationManage.organization.store.add',
    'setting.organizationManage.organization.warehouse.add',
    'setting.organizationManage.organization.store.detail',
    'setting.organizationManage.organization.warehouse.detail',
    'setting.organizationManage.department.add',
    'setting.organizationManage.department.edit',
    'setting.staffAccount',
    'setting.staffAccount.account',
    'setting.staffAccount.account.form',
    'setting.staffAccount.account.update',
    'setting.staffAccount.role',
    'setting.staffAccount.role.form',
    'setting.staffAccount.role.update',
  ],
};
