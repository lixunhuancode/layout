/**
 * @desc 登录
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.LoginDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.LoginDTO };

export function request(data: defs.LoginDTO, options: AxiosRequestConfig) {
  return axiosHandle(`admin/auth/login`, {
    data,
    method: 'POST',
    ...options,
  });
}
