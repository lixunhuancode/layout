export default {
  path: 'price',
  name: 'product.price',
  component: () => import('@/components/routeView.vue'),
  meta: {
    locale: '价格管理',
  },
  children: [
    {
      path: 'list',
      name: 'product.price.list',
      component: () => import('./list/index.vue'),
      meta: {
        locale: '调价管理',
      },
    },
    {
      path: 'form',
      name: 'product.price.form',
      component: () => import('./form/index.vue'),
      meta: {
        locale: '新建调价单',
        hideInMenu: true,
      },
    },
    {
      path: 'detail/:id',
      name: 'product.price.detail',
      component: () => import('./form/index.vue'),
      meta: {
        locale: '查看调价单',
        hideInMenu: true,
      },
    },
    {
      path: 'price-record',
      name: 'product.price.price-record',
      component: () => import('./price-record/index.vue'),
      meta: {
        locale: '调价记录',
      },
    },
  ],
};
