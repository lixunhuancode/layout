export default [
  {
    path: 'home/list',
    name: 'cms.home.list',
    component: () => import('@/pages/cms/home/list/index.vue'),
    meta: {
      locale: '首页装修',
      sort: 1,
    },
  },
  {
    path: 'home/form',
    name: 'cms.home.add',
    component: () => import('@/pages/cms/home/form/index.vue'),
    meta: {
      hideInMenu: true,
      locale: '新增首页',
    },
  },
  {
    path: 'home/form/:id',
    name: 'cms.home.edit',
    component: () => import('@/pages/cms/home/form/index.vue'),
    meta: {
      hideInMenu: true,
      locale: '编辑首页',
    },
  },
];
