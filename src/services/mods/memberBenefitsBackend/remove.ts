/**
 * @desc 删除会员权益
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.IdDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.IdDTO };

export function request(data: defs.IdDTO, options: AxiosRequestConfig) {
  return axiosHandle(`backend/cms/memberBenefits/delete`, {
    data,
    method: 'POST',
    ...options,
  });
}
