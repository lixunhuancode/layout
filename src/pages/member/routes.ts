/*
 * @Author: shanshan.hu
 * @FilePath: \liansuobao-admin-html\src\pages\member\routes.ts
 */
import { DEFAULT_LAYOUT } from '@/router/routes/base';
import management from './management/routes';
import level from './level/routes';
import integral from './integral/routes';
import stored from './stored/routes';

export default {
  path: '/member',
  name: 'member',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '会员',
    icon: 'icon-c-member',
    sort: 4,
  },
  children: [management, level, integral, stored],
};
