import { pagerResponseWrap, mock } from '@/utils/setup-mock';

export const list = (req: any) => {
  console.log({ req });
  const data = [
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      createTime: '2023-05-11 00:00:00',
      num: '22',
      a3: '333',
    },
  ];
  return mock(pagerResponseWrap(req, data));
};

export const getOrderList = (req: any) => {
  console.log({ req });
  const data: any[] = [
    {
      orderNo: 'S1651468950219972608',
      type: '1',
      a2: '22',
      a3: '333',
      a4: '22',
      a5: '333',
      a6: '22',
      a7: '333',
    },
    {
      orderNo: 'S1651468950219972608',
      type: '1',
      a2: '22',
      a3: '333',
      a4: '22',
      a5: '333',
      a6: '22',
      a7: '333',
    },
  ];
  return mock(pagerResponseWrap(req, data));
};

export default mock;
