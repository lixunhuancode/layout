/**
 * @desc 编辑
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.SettingMallDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.SettingMallDTO };

export function request(
  data: defs.SettingMallDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/mall/update`, {
    data,
    method: 'POST',
    ...options,
  });
}
