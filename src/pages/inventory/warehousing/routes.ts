export default {
  path: 'warehousing',
  name: 'inventory.warehousing',
  component: () => import('@/components/routeView.vue'),
  meta: {
    locale: '出入库管理',
    icon: 'icon-relation',
    sort: 9,
  },
  children: [
    {
      path: 'in/list',
      name: 'inventory.warehousing.in.list',
      component: () => import('./in/list/index.vue'),
      meta: {
        locale: '入库管理',
      },
    },
    {
      path: 'in/add',
      name: 'inventory.warehousing.in.add',
      component: () => import('./in/form/index.vue'),
      meta: {
        sort: 1,
        locale: '采购入库',
        hideInMenu: true,
      },
    },
    {
      path: 'in-details/list',
      name: 'inventory.warehousing.in-details.list',
      component: () => import('./in-details/list/index.vue'),
      meta: {
        sort: 2,
        locale: '入库明细',
      },
    },
    {
      path: 'in-details/add',
      name: 'inventory.warehousing.in-details.add',
      component: () => import('./in-details/form/index.vue'),
      meta: {
        locale: '新建入库单',
        hideInMenu: true,
      },
    },
    {
      path: 'in-details/detail/:id',
      name: 'inventory.warehousing.in-details.detail',
      component: () => import('./in-details/detail/index.vue'),
      meta: {
        locale: '入库明细详情',
        hideInMenu: true,
      },
    },
    {
      path: 'out/list',
      name: 'inventory.warehousing.out.list',
      component: () => import('./out/list/index.vue'),
      meta: {
        sort: 3,
        locale: '出库管理',
      },
    },
    {
      path: 'out/add',
      name: 'inventory.warehousing.out.add',
      component: () => import('./out/form/index.vue'),
      meta: {
        locale: '出库操作',
        hideInMenu: true,
      },
    },
    {
      path: 'out-details',
      name: 'inventory.warehousing.out-details.list',
      component: () => import('./out-details/list/index.vue'),
      meta: {
        sort: 4,
        locale: '出库明细',
      },
    },
    {
      path: 'out-details/add',
      name: 'inventory.warehousing.out-details.add',
      component: () => import('./out-details/form/index.vue'),
      meta: {
        locale: '新建出库单',
        hideInMenu: true,
      },
    },
    {
      path: 'out-details/detail/:id',
      name: 'inventory.warehousing.out-details.detail',
      component: () => import('./out-details/details/index.vue'),
      meta: {
        locale: '出库明细详情',
        hideInMenu: true,
      },
    },
  ],
};
