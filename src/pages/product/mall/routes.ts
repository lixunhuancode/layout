export default {
  path: 'mall',
  name: 'product.mall',
  component: () => import('@/components/routeView.vue'),
  meta: {
    locale: '门店商品',
  },
  children: [
    {
      path: 'product/list',
      name: 'product.mall.product.list',
      component: () => import('./product/list/index.vue'),
      meta: {
        locale: '门店渠道',
      },
    },
  ],
};
