import { defineStore } from 'pinia';
import type { PageWithItemsVO, CmsPageItem } from './types';

interface CmsState {
  /** cms所有内容数据 */
  form: PageWithItemsVO;
  /** 所有模块列表 */
  pageItems: Array<CmsPageItem>;
  /** 选中的模块itemCode */
  selectItemCode: string;
  moduleConfig: object;
}

const useCmsStore = defineStore('cms', {
  state: (): CmsState => ({
    form: {},
    pageItems: [],
    selectItemCode: 'page',
    // showTypes 为 itemData 里 list 中需要展示的字段
    moduleConfig: {
      notice: {
        title: '公告',
        listName: '公告',
        showTypes: ['title', 'hrefUrl'],
      },
      grideMenu: {
        title: '图文导航',
        listName: '导航',
        showTypes: ['imageUrl', 'title', 'hrefUrl'],
      },
      banner: {
        title: '轮播图',
        listName: '图片',
        showTypes: ['imageUrl', 'hrefUrl', 'showTime'],
      },
      imageWithTitle: {
        title: '推荐',
        listName: '导航',
        maxListLength: 10,
        showTypes: ['imageUrl', 'hrefUrl'],
      },
      image: {
        title: '图片',
        listName: '图片',
        showTypes: ['imageUrl', 'hrefUrl', 'showTime'],
      },
      imageInterval: {
        title: '图片',
        listName: '图片',
        showTypes: ['imageUrl', 'hrefUrl', 'showTime'],
      },
      imagePopup: {
        title: '弹窗',
        listName: '图片',
        showTypes: ['imageUrl', 'hrefUrl', 'showTime'],
      },
      productCustom: {
        title: '商品',
        listName: '商品',
        showTypes: ['customSpuName', 'customSpuDesc', 'hrefUrl', 'score'],
      },
      productListFalls: {
        title: '瀑布流商品',
        listName: '商品',
        showTypes: ['hrefUrl', 'score'],
      },
    },
  }),

  getters: {
    /** 选中的模块数据 */
    selectItem(state: CmsState): CmsPageItem {
      const data = state.pageItems?.find(
        (item) => item.itemCode === state.selectItemCode
      );
      return data || {};
    },
  },

  actions: {
    resetInfo() {
      this.form = {};
      this.pageItems = [];
      this.selectItemCode = 'page';
    },
  },
});
export default useCmsStore;
