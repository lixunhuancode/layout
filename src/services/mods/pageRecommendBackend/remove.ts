/**
 * @desc 删除页面入口推荐(个人中心)
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.IdDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.IdDTO };

export function request(data: defs.IdDTO, options: AxiosRequestConfig) {
  return axiosHandle(`backend/cms/pageRecommend/delete`, {
    data,
    method: 'POST',
    ...options,
  });
}
