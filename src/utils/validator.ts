/* 表单自定义验证方法 */
/*
 * 验证手机号
 * */
export const checkPhone = (value: any, cb: any) => {
  if (!/^1[3|4|5|6|7|8|9][0-9]\d{8}$/.test(value)) {
    cb('请输入正确的手机号');
  }
};
/*
 * 身份证校验
 * */
export const checkIdNumber = (value: any, cb: any) => {
  if (
    value &&
    !/^\d{6}(18|19|20)?\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|X|x)$/i.test(
      value
    )
  ) {
    cb('请输入正确的身份证号');
  }
};
