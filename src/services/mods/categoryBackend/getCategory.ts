/**
   * @desc 查询品类信息
查询品类信息
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** 品类编码 */
  categoryCode: string;
}

export const init = new defs.CategoryVO();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`backend/category/getCategory/${params.categoryCode}`, {
    params,
    method: 'GET',
    ...options,
  });
}
