/**
    * @desc 查询规格组列表
查询规格组列表
    */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = new defs.ProductPropertyVO();

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`backend/property/query/list`, {
    method: 'GET',
    ...options,
  });
}
