/**
 * @description 内容管理-公共类接口
 */
import * as uploadFile from './uploadFile';
import * as uploadFilePrivate from './uploadFilePrivate';
import * as uploadImage from './uploadImage';

export { uploadFile, uploadFilePrivate, uploadImage };
