import { DEFAULT_LAYOUT } from '@/router/routes/base';
import Version from './Version/routes';
import Operation from './Operation/routes';
import Mall from './Mall/routes';
import Integral from './Integral/routes';
import Account from './Account/routes';
import Permission from './Permission/routes';
import Role from './Role/routes';
import Dict from './Dict/routes';

export default {
  path: '/setting',
  name: 'setting',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '设置',
    requiresAuth: false,
    icon: 'icon-c-setting',
    order: 99,
  },
  children: [
    ...Version,
    ...Operation,
    ...Mall,
    ...Integral,
    ...Dict,
    ...Account,
    ...Role,
    ...Permission,
  ],
};
