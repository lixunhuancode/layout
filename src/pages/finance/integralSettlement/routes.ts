import Collect from './collect/routes';
import FinalStatement from './finalStatement/routes';

export default [...Collect, ...FinalStatement];
