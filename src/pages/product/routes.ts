import { DEFAULT_LAYOUT } from '@/router/routes/base';
import guide from './guide/routes';
import management from './management/routes';
import mall from './mall/routes';
import price from './price/routes';

export default {
  path: '/product',
  name: 'product',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '商品',
    icon: 'icon-c-product',
    sort: 2,
  },
  children: [...guide, management, mall, price],
};
