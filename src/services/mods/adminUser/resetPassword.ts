/**
 * @desc 重置密码
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.ResetPasswordDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.ResetPasswordDTO };

export function request(
  data: defs.ResetPasswordDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`admin/user/resetPassword`, {
    data,
    method: 'POST',
    ...options,
  });
}
