export default [
  {
    path: 'management/list',
    name: 'order.management.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '订单管理',
      requiresAuth: false,
    },
  },
  {
    path: 'management/detail/:id',
    name: 'order.management.detail',
    component: () => import('./Detail/index.vue'),
    meta: {
      locale: '订单详情',
      hideInMenu: true,
    },
  },
];
