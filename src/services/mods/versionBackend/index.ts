/**
 * @description 后端版本管理
 */
import * as add from './add';
import * as getAll from './getAll';
import * as remove from './remove';
import * as getDetailById from './getDetailById';
import * as getPageInfo from './getPageInfo';
import * as getListByIds from './getListByIds';
import * as update from './update';

export {
  add,
  getAll,
  remove,
  getDetailById,
  getPageInfo,
  getListByIds,
  update,
};
