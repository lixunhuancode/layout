/**
   * @desc 取消订单
取消订单
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.OrderCancelDTO;

export const init = undefined;

export type ParamsAndBody = { data: defs.OrderCancelDTO };

export function request(
  data: defs.OrderCancelDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`front/order/cancel`, {
    data,
    method: 'POST',
    ...options,
  });
}
