import {
  successResponseWrap,
  mock,
  pagerResponseWrap,
} from '@/utils/setup-mock';

export const departmentList = [
  {
    id: '64',
    name: '运营部',
    code: 'A041',
    type: '经销',
    address: '重庆市渝中区',
    mobile: '13111112222',
    principal: '张三',
    remark: 'test1111',
    num: '10',
  },
  {
    id: '65',
    name: '财务部',
    code: 'A042',
    type: '联营',
    address: '重庆市渝中区',
    mobile: '18811111111',
    principal: '李四',
    remark: 'test1111',
    num: '10',
  },
  {
    id: '66',
    name: '采购部',
    code: 'A042',
    type: '联营',
    address: '重庆市渝中区',
    mobile: '18811111111',
    principal: '李四',
    remark: 'test1111',
    num: '10',
  },
  {
    id: '67',
    name: '技术部',
    code: 'A042',
    type: '联营',
    address: '重庆市渝中区',
    mobile: '18811111111',
    principal: '李四',
    remark: 'test1111',
    num: '10',
  },
  {
    id: '68',
    name: '总经办',
    code: 'A042',
    type: '联营',
    address: '重庆市渝中区',
    mobile: '18811111111',
    principal: '李四',
    remark: 'test1111',
    num: '10',
  },
  {
    id: '69',
    name: '销售部',
    code: 'A042',
    type: '联营',
    address: '重庆市渝中区',
    mobile: '18811111111',
    principal: '李四',
    remark: 'test1111',
    num: '10',
  },
];

export const list = (req: any) => {
  return mock(pagerResponseWrap(req, departmentList));
};

export const all = () => {
  return mock(successResponseWrap(departmentList));
};

export default mock;
