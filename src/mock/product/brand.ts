import {
  successResponseWrap,
  mock,
  pagerResponseWrap,
} from '@/utils/setup-mock';

export const data = [
  {
    id: 1,
    a1: 10001,
    a2: 'https://img-mall-test.sjgo365.com/cbest-mall-center/images/20230517/NqtVaBFQ24Whend8ChHxjb2khmmS7NRZsv2qGAzb.png',
    a3: 'SK-II',
    a4: '启用',
    a5: ['禁用', '删除'],
  },
  {
    id: 2,
    a1: 10002,
    a2: 'https://img-mall-test.sjgo365.com/cbest-mall-center/images/20230517/VX99dBl1N2ncgUxGM8vzvyysEnxwyOBZFhUBwUbc.png',
    a3: '迪桑娜',
    a4: '禁用',
    a5: ['编辑', '启用', '删除'],
  },
  {
    id: 3,
    a1: 10003,
    a2: 'https://img-mall-test.sjgo365.com/cbest-mall-center/images/20230517/1qjnl4mKwcMmkH9o3c6KXuKdooN4P5W5KG7XFJj6.png',
    a3: '周大生',
    a4: '启用',
    a5: ['编辑', '删除'],
  },
  {
    id: 4,
    a1: 10004,
    a2: 'https://img-mall-test.sjgo365.com/cbest-mall-center/images/20230517/ID5pvNlCwBJxO4c1jpKf2qqzTjZxAFFIDBeR6UQb.png',
    a3: '耐克',
    a4: '启用',
    a5: ['编辑', '删除'],
  },
  {
    id: 5,
    a1: 10005,
    a2: 'https://img-mall-test.sjgo365.com/cbest-mall-center/images/20230517/btCPY7a4jIuvsnWzy3ltckQY3HV0SZ2CMAm2PsSh.png',
    a3: 'ELAND',
    a4: '启用',
    a5: ['编辑', '删除'],
  },
  {
    id: 6,
    a1: 10006,
    a2: 'https://img-mall-test.sjgo365.com/cbest-mall-center/images/20230517/alePh6JR7XREMAMhbpAmtxtk9ow8FGulgxI4Y0bK.png',
    a3: 'Teenie Weenie',
    a4: '启用',
    a5: ['编辑', '删除'],
  },
  {
    id: 7,
    a1: 10007,
    a2: 'https://img-mall-test.sjgo365.com/cbest-mall-center/images/20230517/nmX9WMjKr4SN97JVUrbRmk77TkwUjG1x6rJ7T0rr.png',
    a3: '美的',
    a4: '启用',
    a5: ['编辑', '删除'],
  },
  {
    id: 8,
    a1: 10008,
    a2: 'https://img-mall-test.sjgo365.com/cbest-mall-center/images/20230517/Smr6OT8rHKUmvdSM54y3OOkBw5JmtF7U4znSxmpe.png',
    a3: '格力',
    a4: '启用',
    a5: ['编辑', '删除'],
  },
  {
    id: 9,
    a1: 10009,
    a2: 'https://img-mall-test.sjgo365.com/cbest-mall-center/images/20230517/Gb1gRyzhEYiP33M70KYK7kS5cklPRjaOIUi9BBDM.png',
    a3: '回力',
    a4: '启用',
    a5: ['编辑', '删除'],
  },
  {
    id: 10,
    a1: 10010,
    a2: 'https://img-mall-test.sjgo365.com/cbest-mall-center/images/20230517/aU5NVUCw32uUJtYKh7bnWR0dttD8w0VEg8jPCjPD.png',
    a3: '斐乐',
    a4: '启用',
    a5: ['编辑', '删除'],
  },
];

export const list = (req: any) => {
  return mock(pagerResponseWrap(req, data));
};

export const all = () => {
  return mock(successResponseWrap(data));
};

export default mock;
