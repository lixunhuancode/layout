export default [
  {
    path: 'permission/list',
    name: 'products.permission.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '资源管理',
      requiresAuth: false,
    },
  },
];
