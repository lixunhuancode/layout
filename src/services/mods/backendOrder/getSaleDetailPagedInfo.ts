/**
   * @desc 分页获取交易明细列表
分页获取交易明细列表
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.SaleDetailListQueryDTO;

export const init = undefined;

export type ParamsAndBody = { data: defs.SaleDetailListQueryDTO };

export function request(
  data: defs.SaleDetailListQueryDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/order/getSaleDetailPagedInfo`, {
    data,
    method: 'POST',
    ...options,
  });
}
