import { DEFAULT_LAYOUT } from '@/router/routes/base';
import ProductLabel from './ProductLabel/routes';
import MemberLabel from './MemberLabel/routes';
import Brand from './Brand/routes';
import CardLabel from './CardLabel/routes';
import BusinessType from './BusinessType/routes';
import CarType from './CarType/routes';
import CarBrand from './CarBrand/routes';
import RechargeReason from './RechargeReason/routes';

export default {
  path: '/info',
  name: 'info',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '信息',
    icon: 'icon-c-info',
    order: 9,
  },
  children: [
    ...MemberLabel,
    ...CardLabel,
    ...ProductLabel,
    ...BusinessType,
    ...Brand,
    ...CarType,
    ...CarBrand,
    ...RechargeReason,
  ],
};
