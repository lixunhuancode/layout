/**
 * @desc 获取核销码
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** publishBatchId */
  publishBatchId: number;
  /** templateId */
  templateId: number;
}

export const init = new defs.ResponseModel();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`coupon/getVerifyCode`, {
    params,
    method: 'GET',
    ...options,
  });
}
