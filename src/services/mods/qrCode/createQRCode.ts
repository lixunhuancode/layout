/**
 * @desc 生成通用普通二维码
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.CreateQRCodeDTO;

export const init = undefined;

export type ParamsAndBody = { data: defs.CreateQRCodeDTO };

export function request(
  data: defs.CreateQRCodeDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`qrCode/createQRCode`, {
    data,
    method: 'POST',
    ...options,
  });
}
