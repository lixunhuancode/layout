export default [
  {
    path: 'distribution.batch/list',
    name: 'purchase.distribution.batch.list',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '批量分货',
    },
  },
  {
    path: 'distribution.batch/form',
    name: 'purchase.distribution.batch.form',
    component: () => import('./form/index.vue'),
    meta: {
      locale: '新建批量分货单',
      hideInMenu: true,
    },
  },
  {
    path: 'distribution.batch/form/:id',
    name: 'purchase.distribution.batch.update',
    component: () => import('./form/index.vue'),
    meta: {
      locale: '编辑批量分货单',
      hideInMenu: true,
    },
  },
  {
    path: 'distribution.batch/detail/:id',
    name: 'purchase.distribution.batch.detail',
    component: () => import('./form/index.vue'),
    meta: {
      locale: '批量分货详情',
      hideInMenu: true,
    },
  },
];
