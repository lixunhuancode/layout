/**
 * @desc 模板解析
 */

import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export const init = new defs.ResponseModel();

export function request(options: AxiosRequestConfig) {
  return axiosHandle(`admin/coupon/publishBatch/parseTemplate`, {
    method: 'POST',
    ...options,
  });
}
