/**
   * @desc 删除回收站商品(单个删除传一个spuId)
删除回收站商品(单个删除传一个spuId)
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = Array<string>;

export const init = false;

export type ParamsAndBody = { data: Array<string> };

export function request(data: Array<string>, options: AxiosRequestConfig) {
  return axiosHandle(`backend/productRecycle/deleteRecycleProduct`, {
    data,
    method: 'POST',
    ...options,
  });
}
