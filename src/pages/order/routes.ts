import { DEFAULT_LAYOUT } from '@/router/routes/base';
import management from './management/routes';
import performance from './performance/routes';

export default {
  path: '/order',
  name: 'order',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '订单',
    icon: 'icon-c-order',
    sort: 3,
  },
  children: [management, performance],
};
