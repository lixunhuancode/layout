export default [
  {
    path: 'link/list',
    name: 'cms.link.list',
    component: () => import('./list/index.vue'),
    meta: {
      sort: 3,
      locale: '小程序链接',
    },
  },
];
