import { DEFAULT_LAYOUT } from '@/router/routes/base';
import Management from './Management/routes';
import Records from './Records/routes';
import Warn from './Warn/routes';
import Batch from './Batch/routes';

const a = [{ name: { c: 23 } }, { name: { c: 1 } }, { name: { c: 2 } }];
a.sort((a, b) => a.name.c - b.name.c);
export default {
  path: '/stock',
  name: 'stock',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '库存',
    requiresAuth: false,
    icon: 'icon-c-stock',
    order: 5,
  },
  children: [...Management, ...Records, ...Warn, ...Batch],
};
