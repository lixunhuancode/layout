/**
   * @desc 更新车辆信息
更新车辆信息
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.ModifyCarDTO;

export const init = false;

export type ParamsAndBody = { data: defs.ModifyCarDTO };

export function request(data: defs.ModifyCarDTO, options: AxiosRequestConfig) {
  return axiosHandle(`webapi/user/updateCarInfo`, {
    data,
    method: 'POST',
    ...options,
  });
}
