export default {
  path: 'storeCoupon',
  name: 'marketing.storeCoupon',
  component: () => import('./list/index.vue'),
  meta: {
    locale: '优惠券派发',
    sort: 2,
  },
};
