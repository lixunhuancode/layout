export default [
  {
    path: 'details',
    name: 'member.stored.details',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '储值明细',
      sort: 99,
    },
  },
];
