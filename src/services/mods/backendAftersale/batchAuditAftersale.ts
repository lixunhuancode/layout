/**
   * @desc 批量审核售后单
批量审核售后单
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.AftersaleAuditDTO;

export const init = undefined;

export type ParamsAndBody = { data: defs.AftersaleAuditDTO };

export function request(
  data: defs.AftersaleAuditDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/aftersale/batchAuditAftersale`, {
    data,
    method: 'POST',
    ...options,
  });
}
