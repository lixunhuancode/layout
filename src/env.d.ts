import 'vite/client';
import './services/api';
import { UseDialog } from '@/components/MDialog/types';
import { ConfirmFn } from '@/components/MDialog/Confirm/types';
import * as StoreModules from './store/index';

declare module '*.vue' {
  import { DefineComponent } from 'vue';
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
  const component: DefineComponent<{}, {}, any>;
  export default component;
}
interface ImportMetaEnv {
  readonly VITE_API_BASE_URL: string;
}

declare module '@vue/runtime-core' {
  export interface ComponentCustomProperties {
    $useDialog: UseDialog;
    $confirm: ConfirmFn;
    $store: typeof StoreModules;
  }
}
