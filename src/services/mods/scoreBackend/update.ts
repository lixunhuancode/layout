/**
 * @desc 编辑
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.SettingScoreDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.SettingScoreDTO };

export function request(
  data: defs.SettingScoreDTO,
  options: AxiosRequestConfig,
) {
  return axiosHandle(`backend/score/update`, {
    data,
    method: 'POST',
    ...options,
  });
}
