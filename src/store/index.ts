import { createPinia } from 'pinia';
import useAppStore from './modules/app';
import useUserStore from './modules/user';
import useTabBarStore from './modules/tab-bar';
import useDictionary from './modules/dictionary/index';
import useCms from './modules/cms/index';

const pinia = createPinia();

export { useAppStore, useUserStore, useTabBarStore, useDictionary, useCms };
export default pinia;
