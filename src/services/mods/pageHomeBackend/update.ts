/**
 * @desc 编辑首页
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.PageDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.PageDTO };

export function request(data: defs.PageDTO, options: AxiosRequestConfig) {
  return axiosHandle(`backend/cms/page/home/update`, {
    data,
    method: 'POST',
    ...options,
  });
}
