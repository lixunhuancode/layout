/**
   * @desc 删除标签
删除标签
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** labelCode */
  labelCode: string;
}

export const init = false;

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`backend/productLabel/deleteLabel/${params.labelCode}`, {
    params,
    method: 'POST',
    ...options,
  });
}
