export default [
  {
    path: 'merchandise-sales/list',
    name: 'statistics.merchandise-sales.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '商品销售报表',
    },
  },
  {
    path: 'merchandise-sales/detail',
    name: 'statistics.merchandise-sales.detail',
    component: () => import('./Detail/index.vue'),
    meta: {
      locale: '商品销售报表-明细',
      hideInMenu: true,
    },
  },
];
