export default [
  {
    path: 'transaction/list',
    name: 'statistics.transaction.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '交易统计报表',
      requiresAuth: false,
    },
  },
  {
    path: 'transaction/detail',
    name: 'statistics.transaction.detail',
    component: () => import('./Detail/index.vue'),
    meta: {
      locale: '交易统计明细',
      requiresAuth: false,
      hideInMenu: true,
    },
  },
];
