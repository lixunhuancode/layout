export default [
  {
    path: 'recharge/list',
    name: 'member.recharge.list',
    component: () => import('@/views/Member/Recharge/List/index.vue'),
    meta: {
      locale: '会员充值',
      requiresAuth: false,
    },
  },
  {
    path: 'recharge/form',
    name: 'member.recharge.form',
    component: () => import('@/views/Member/Recharge/Form/index.vue'),
    meta: {
      locale: '会员充值',
      requiresAuth: false,
      hideInMenu: true,
    },
  },
  {
    path: 'recharge/edit/:id',
    name: 'member.recharge.edit',
    component: () => import('@/views/Member/Recharge/Form/index.vue'),
    meta: {
      locale: '会员充值修改',
      requiresAuth: false,
      hideInMenu: true,
    },
  },
  {
    path: 'recharge/detail/:id',
    name: 'member.recharge.detail',
    component: () => import('@/views/Member/Recharge/Detail/index.vue'),
    meta: {
      locale: '充值详情',
      requiresAuth: false,
      hideInMenu: true,
    },
  },
];
