/**
 * @desc verifySmsCode
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** reflash */
  reflash: boolean;
}

export const init = undefined;

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(`sms/getWechatAccessToken`, {
    params,
    method: 'GET',
    ...options,
  });
}
