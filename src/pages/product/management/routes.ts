export default {
  path: 'management',
  name: 'product.management',
  component: () => import('@/components/routeView.vue'),
  meta: {
    locale: '商品资料',
  },
  children: [
    {
      path: 'basic/list',
      name: 'product.management.basic',
      component: () => import('./basic/list/index.vue'),
      meta: {
        sort: 1,
        locale: '商品档案',
      },
    },
    {
      path: 'basic/form',
      name: 'product.management.basic.form',
      component: () => import('./basic/form/index.vue'),
      meta: {
        locale: '新建商品',
        hideInMenu: true,
      },
    },
    {
      path: 'basic/form/:id',
      name: 'product.management.basic.update',
      component: () => import('./basic/form/index.vue'),
      meta: {
        locale: '编辑商品',
        hideInMenu: true,
      },
    },
    {
      path: 'basic/detail/:id',
      name: 'product.management.basic.detail',
      component: () => import('./basic/form/index.vue'),
      meta: {
        locale: '商品详情',
        hideInMenu: true,
      },
    },
    {
      path: 'channel/list',
      name: 'product.management.channel.list',
      component: () => import('./channel/list/index.vue'),
      meta: {
        sort: 2,
        locale: '渠道管理',
      },
    },
    {
      path: 'audit/list',
      name: 'product.management.audit.list',
      component: () => import('./audit/list/index.vue'),
      meta: {
        sort: 3,
        locale: '自建审核',
      },
    },
    {
      path: 'mall-product/self-built',
      name: 'product.management.mall-product.self-built',
      component: () => import('./mall-product/self-built/index.vue'),
      meta: {
        sort: 3,
        locale: '自建商品',
      },
    },
    {
      path: 'category/list',
      name: 'product.management.category',
      component: () => import('./category/list/index.vue'),
      meta: {
        sort: 4,
        locale: '营销分类',
      },
    },
    {
      path: 'unit/list',
      name: 'product.management.unit',
      component: () => import('./unit/list/index.vue'),
      meta: {
        sort: 5,
        locale: '单位管理',
      },
    },
    {
      path: 'brand/list',
      name: 'product.management.brand',
      component: () => import('./brand/list/index.vue'),
      meta: {
        sort: 6,
        locale: '品牌管理',
      },
    },
  ],
};
