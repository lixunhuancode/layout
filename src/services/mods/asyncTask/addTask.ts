/**
 * @desc 任务添加
 */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export type RequestBody = defs.AsyncTaskDTO;

export const init = new defs.ResponseModel();

export type ParamsAndBody = { data: defs.AsyncTaskDTO };

export function request(data: defs.AsyncTaskDTO, options: AxiosRequestConfig) {
  return axiosHandle(`admin/async-task/add`, {
    data,
    method: 'POST',
    ...options,
  });
}
