export default [
  {
    path: 'link/list',
    name: 'cms.link.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '小程序链接',
    },
  },
];
