/**
   * @desc 根据售后id查询售后单详细信息
根据售后id查询售后单详细信息
   */
import { AxiosRequestConfig } from 'axios';
import axiosHandle from '@/api/request';
import * as defs from '../../baseClass';

export class Params {
  /** aftersaleNo */
  aftersaleNo: string;
}

export const init = new defs.AftersaleDetailInfoVO();

export type ParamsAndBody = { params: Params };

export function request(params: Params, options: AxiosRequestConfig) {
  return axiosHandle(
    `backend/aftersale/getAftersaleByAftersaleNo/${params.aftersaleNo}`,
    {
      params,
      method: 'GET',
      ...options,
    },
  );
}
