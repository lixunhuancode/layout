/**
 * @description 内容管理-服务保障-前台接口
 */
import * as detail from './detail';
import * as detailByCode from './detailByCode';
import * as queryByCodes from './queryByCodes';

export { detail, detailByCode, queryByCodes };
