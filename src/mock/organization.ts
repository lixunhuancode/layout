import {
  successResponseWrap,
  mock,
  pagerResponseWrap,
} from '@/utils/setup-mock';

export const organizationList = [
  {
    id: '64',
    name: '总部',
    code: 'A041',
    type: '经销',
    address: '重庆市渝中区',
    mobile: '13111112222',
    principal: '张三',
    remark: 'test1111',
    num: '10',
    children: [
      {
        id: '64-01',
        name: '门店1',
        code: 'A041',
        type: '经销',
        address: '重庆市渝中区',
        mobile: '13111112222',
        principal: '张三',
        remark: 'test1111',
        num: '10',
      },
    ],
  },
];

export const list = (req: any) => {
  return mock(pagerResponseWrap(req, organizationList));
};

export const all = () => {
  return mock(successResponseWrap(organizationList));
};

export default mock;
