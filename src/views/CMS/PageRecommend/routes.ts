export default [
  {
    path: 'page-recommend/list',
    name: 'cms.page-recommend.list',
    component: () => import('./List/index.vue'),
    meta: {
      locale: '个人中心',
    },
  },
];
