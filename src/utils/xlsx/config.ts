// 表格字段对应key
// 会员积分充值导入
const prontRecharge = {
  title: '充值用户',
  config: {
    手机号: 'phone',
    姓名: 'userName',
    积分: 'point',
  },
};
// 测试使用
const demo = {
  title: '测试文件',
  config: {
    测试列: 'id',
  },
};
export default {
  prontRecharge,
  demo,
};
