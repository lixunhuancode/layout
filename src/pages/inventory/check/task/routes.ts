export default [
  {
    path: 'task/list',
    name: 'inventory.check.task.list',
    component: () => import('./list/index.vue'),
    meta: {
      locale: '盘点任务',
    },
  },
  {
    path: 'task/add',
    name: 'inventory.check.task.add',
    component: () => import('./form/index.vue'),
    meta: {
      locale: '新建盘点单',
      hideInMenu: true,
    },
  },
  {
    path: 'task/detail/:id',
    name: 'inventory.check.task.detail',
    component: () => import('./detail/index.vue'),
    meta: {
      locale: '查看',
      hideInMenu: true,
    },
  },
];
