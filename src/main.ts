import { createApp } from 'vue';
import ArcoVue from '@arco-design/web-vue';
import ArcoVueIcon from '@arco-design/web-vue/es/icon';
import globalComponents from '@/components';
import { dialogInstall } from '@/components/MDialog/index';

import router from './router';
import store, * as storeModules from './store';
import directive from './directive';
import App from './App.vue';

// import '@arco-design/web-vue/dist/arco.css';
import '@arco-themes/vue-cbest-lst/css/arco.css';

import '@/assets/style/global.less';
import '@/api/interceptor';
import '@/services/index';

const app = createApp(App);
app.use(ArcoVue, {});
app.use(ArcoVueIcon);

app.config.globalProperties.$t = (src: string) => src;

app.config.globalProperties.$store = storeModules;

app.use(router);
app.use(store);
app.use(globalComponents);
app.use(directive);
app.use(dialogInstall);

app.mount('#app');
